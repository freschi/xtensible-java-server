package at.femoweb.packman;

import at.femoweb.config.Article;
import at.femoweb.config.Catalog;
import at.femoweb.config.Config;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Stopable;
import at.femoweb.xjs.modules.JavaRessourceService;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;
import at.femoweb.packman.Package;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import jdk.nashorn.internal.runtime.JSONFunctions;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.*;
import java.lang.Class;
import java.lang.NullPointerException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;

@Named("packman")
@at.femoweb.xjs.annotations.Service
public class Packman extends BootableModule implements Initable, Service, Stopable {

    private BootableLogicalUnit bootableLogicalUnit;
    private boolean repoMode = false;
    private boolean buildPackets = false;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        Packman packman = new Packman();
        packman.bootableLogicalUnit = logicalUnit;
        if(settings.has("buildPackets")) {
            packman.buildPackets = settings.getBoolean("buildPackets");
        }
        if(settings.has("repoMode")) {
            packman.repoMode = settings.getBoolean("repoMode");
        }
        return packman;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {
        File packRoot = new File("packman");
        if(!packRoot.exists()) {
            Server.log.info("Creating packman directory");
            packRoot.mkdir();
        } else {
            Server.log.debug("Packman directory already exists");
        }
        Catalog catalog = Config.openCatalog("packman", true);

    }

    @Override
    public void init(Properties properties) {
        if(bootableLogicalUnit != null) {
            Catalog catalog = Config.openCatalog("packman");
            Article article = catalog.openArticle("local");
            Group packets = article.openParagraph("packets").openEntry().getGroup("provided");
            List providedList = new List();
            JavaRessourceService javaRessourceService = (JavaRessourceService) bootableLogicalUnit.getService("jres-service");
            Class<?>[] classes = javaRessourceService.getAllClasses(at.femoweb.xjs.annotations.Package.class);
            HashMap<String, Package> packages = new HashMap<>();
            for(Class<?> c : classes) {
                at.femoweb.xjs.annotations.Package p = c.getAnnotation(at.femoweb.xjs.annotations.Package.class);
                Package pack;
                if(!packages.containsKey(p.value())) {
                    pack = new Package(p.value());
                    providedList.addString(p.value());
                    packages.put(p.value(), pack);
                } else {
                    pack = packages.get(p.value());
                }
                pack.putClass(c);
            }
            article.openParagraph("packets").openEntry().setList("providedList", providedList);
            for(Package pack : packages.values()) {
                Group group = pack.exportPackage(bootableLogicalUnit);
                packets.setGroup(pack.getName(), group);
                Server.log.info(group.export());
            }
        }
    }

    @Override
    public Object put(Object... objects) {
        Object object = objects[0];
        if(object instanceof Properties) {
            Properties properties = (Properties) object;
            String action = properties.getProperty("action");
            if(action.equals("search")) {
                return lookupPacket(properties.getProperty("query"));
            } else if (action.equals("add-repo")) {
                Server.log.warn(properties.toString());
                String url = properties.getProperty("url");
                String name = properties.getProperty("repo-name");
                return addRepo(url, name);
            } else if (action.equals("install")) {
                return installPackage(properties.getProperty("package"), properties.getProperty("unit"));
            }
        }
        return null;
    }

    @Override
    public void shutdown(Properties properties) {
        if(bootableLogicalUnit == null) {
            try {
                Config.openCatalog("packman").export();
            } catch (IOException | NullPointerException e) {
                Server.log.warn("Could not store packman configuration catalog", e);
            }
        }
    }

    public boolean addRepo(String url, String name) {
        Catalog catalog = Config.openCatalog("packman");
        Group remoteRepos = catalog.openArticle("remote").openParagraph("repos").openEntry();
        Group repo = remoteRepos.getGroup(name);
        repo.setString("url", url);
        repo.setString("name", name);
        try {
            catalog.export();
        } catch (IOException e) {
            Server.log.warn("Could not store Repository catalog", e);
        }
        return true;
    }

    public java.util.List<Properties> lookupPacket(String query) {
        Catalog catalog = Config.openCatalog("packman");
        Group remoteRepos = catalog.openArticle("remote").openParagraph("repos").openEntry();
        ArrayList<Properties> properties = new ArrayList<Properties>();
        for(String key : remoteRepos.getKeys()) {
            try {
                Group repo = remoteRepos.getGroup(key);
                String url = repo.getString("url");
                Connection.Response response = Jsoup.connect(url).ignoreContentType(true).execute();
                JsonArray repoData = new JsonParser().parse(response.body()).getAsJsonArray();
                for (int i = 0; i < repoData.size(); i++) {
                    JsonObject packet = repoData.get(i).getAsJsonObject();
                    String name = packet.get("name").getAsString();
                    if (name.startsWith(query) || query.startsWith(name)) {
                        Properties packetRep = new Properties();
                        packetRep.setProperty("name", name);
                        packetRep.setProperty("version", packet.get("version").getAsString());
                        packetRep.setProperty("url", repo.getString("url"));
                        properties.add(packetRep);
                        if(packet.has("dependencies")) {
                            JsonArray dependencies = packet.getAsJsonArray("dependencies");
                            if(dependencies.size() > 0) {
                                String deps = "";
                                for (int j = 0; j < dependencies.size(); j++) {
                                    deps += dependencies.get(j).getAsString();
                                    if(j < dependencies.size() - 1) {
                                        deps += ";";
                                    }
                                }
                                packetRep.setProperty("dependencies", deps);
                            }
                        }
                    }
                }
            } catch (IOException e) {
                Server.log.warn("Could not fetch data from remote repos", e);
            }
        }
        return properties;
    }

    public java.util.List<Properties> installPackage(String packet, String unit) {
        java.util.List<Properties> packs = lookupPacket(packet);
        java.util.List<Properties> result = new LinkedList<Properties>();
        if(packs.size() == 1) {
            Properties state = new Properties();
            state.setProperty("state", "success");
            state.setProperty("package", packs.get(0).getProperty("name"));
            state.setProperty("version", packs.get(0).getProperty("version"));
            state.setProperty("url", packs.get(0).getProperty("url"));
            state.setProperty("time", "0");
            result.add(state);
        } else {
            Properties state = new Properties();
            state.setProperty("message", "Ambigous package or not found!");
            state.setProperty("state", "failed");
            result.add(state);
        }
        return result;
    }

}