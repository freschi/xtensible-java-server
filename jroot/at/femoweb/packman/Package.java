package at.femoweb.packman;

import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.PackageRoot;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.dependencies.Dependencies;
import at.femoweb.xjs.dependencies.Dependency;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.modules.JavaRessourceService;

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Class;
import java.lang.String;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Package {

    private String name;
    private String version;
    private ArrayList<Class<?>> classList;
    private HashMap<Class<?>, String> versions;
    private ArrayList<String> dependencies;

    public Package(String name) {
        this.name = name;
        this.classList = new ArrayList<>();
        this.versions = new HashMap<>();
        this.version = "1.0";
        this.dependencies = new ArrayList<>();
    }

    public void putClass(Class<?> c) {
        if(c.isAnnotationPresent(at.femoweb.xjs.annotations.Package.class) && c.getAnnotation(at.femoweb.xjs.annotations.Package.class).value().equals(name)) {
            classList.add(c);
            if(c.isAnnotationPresent(Version.class)) {
                if(c.isAnnotationPresent(PackageRoot.class)) {
                    version = c.getAnnotation(Version.class).value();
                }
                versions.put(c, c.getAnnotation(Version.class).value());
            }
            if(c.isAnnotationPresent(Dependencies.class)) {
                Dependencies dependencies = c.getAnnotation(Dependencies.class);
                for(Dependency dependency : dependencies.value()) {
                    this.dependencies.add(dependency.value());
                }
            }
        }
    }

    public Group exportPackage(BootableLogicalUnit bootableLogicalUnit) {
        Group group = new Group();
        group.setString("name", name);
        group.setString("version", version);
        List packages = group.getList("packages");
        packages.addString("bin");
        List classes = group.getList("classes");
        List dependencies = group.getList("dependencies");
        for(String dependency : this.dependencies) {
            dependencies.addString(dependency);
        }
        Server.log.debug("Creating package " + name);
        try {
            File packBin = new File("packman/" + name + "/" + version + "/bin/" + name + "-" + version + "-bin.jar");
            if(!packBin.getParentFile().exists()) {
                packBin.getParentFile().mkdirs();
            }
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(packBin));
            Server.log.debug("Looking up .class files for package " + name);
            File root = ((JavaRessourceService)bootableLogicalUnit.getService("jres-service")).getJbin();
            for(Class<?> c : classList) {
                Group clazz = classes.addGroup();
                clazz.setString("name", c.getName());
                clazz.setString("version", versions.getOrDefault(c, "1.0"));
                try {
                    File classFile = new File(root, c.getName().replace(".", "/") + ".class");
                    ZipEntry entry = new ZipEntry(c.getName().replace(".", "/") + ".class");
                    zipOutputStream.putNextEntry(entry);
                    FileInputStream fileInputStream = new FileInputStream(classFile);
                    InputStream inputStream = new DigestInputStream(fileInputStream, messageDigest);
                    byte[] buffer = new byte[1024];
                    int read;
                    while ((read = inputStream.read(buffer)) > 0) {
                        zipOutputStream.write(buffer, 0, read);
                    }
                    inputStream.close();
                    clazz.setString("hash", Base64.getEncoder().encodeToString(messageDigest.digest()));
                    zipOutputStream.closeEntry();
                } catch (IOException e) {
                    Server.log.error("Could not write class file for " + c.getName() + " to package", e);
                } finally {
                    messageDigest.reset();
                }
            }
            ZipEntry zipEntry = new ZipEntry("_pack_meta");
            zipOutputStream.putNextEntry(zipEntry);
            zipOutputStream.write(("meta=" + group.export()).getBytes());
            zipOutputStream.closeEntry();
            zipOutputStream.finish();
            zipOutputStream.close();
        } catch (IOException e) {
            Server.log.warn("Could not create package", e);
        } catch (NoSuchAlgorithmException e) {
            Server.log.warn("No MD5 found on this server", e);
        }
        return group;
    }

    public String getName() {
        return name;
    }
}