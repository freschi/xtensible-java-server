package at.femoweb.packman;

import at.femoweb.config.Article;
import at.femoweb.config.Catalog;
import at.femoweb.config.Config;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.html.Tag;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonWriter;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.lang.Override;
import java.util.HashMap;
import java.util.Properties;

@ContentProvider(regex = "\\/repo\\/.*")
@Named("packman-repo")
public class PackmanProvider extends BootableModule implements Provider {

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        return this;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        response.setStatus(200);
        String command = request.getRequestPath().substring(6);
        Properties properties = new Properties();
        String resp = "";
        if(command.contains("?")) {
            String query = command.substring(command.indexOf("?") + 1);
            command = command.substring(0, command.indexOf("?"));
            String[] props = query.split("&");
            for (String prop : props) {
                properties.setProperty(prop.substring(0, prop.indexOf("=")), prop.substring(prop.indexOf("=") + 1));
            }
            resp += "Request Properties :" + properties.toString();
        }
        String format = "raw";
        if(properties.containsKey("format")) {
            resp += "\nRequested format: " + (format = properties.getProperty("format"));
        }
        resp += "\nExecuting command " + command;
        response.header("Content-Type", "text/plain");
        Catalog catalog = Config.openCatalog("packman");
        Article article = catalog.openArticle("local");
        Group packets = article.openParagraph("packets").openEntry().getGroup("provided");
        List providedList = article.openParagraph("packets").openEntry().getList("providedList");
        if(format.equals("html")) {
            Tag table = new Tag("table");
            for (int i = 0; i < providedList.count(); i++) {
                String name = providedList.getString(i);
                Group group = packets.getGroup(name);
                int maxLine = 0;
                List packages = group.getList("packages");
                maxLine = packages.count();
                List classes = group.getList("classes");
                if(maxLine < classes.count()) {
                    maxLine = classes.count();
                }
                List dependencies = group.getList("dependencies");
                if(maxLine < dependencies.count()) {
                    maxLine = dependencies.count();
                }
                for (int j = 0; j < maxLine; j++) {
                    Tag row = new Tag("tr");
                    if(j == 0) {
                        Tag val = new Tag("td");
                        val.attribute("rowspan", maxLine + "");
                        val.add(group.getString("name"));
                        row.add(val);
                        Tag vers = new Tag("td");
                        vers.attribute("rowspan", maxLine + "");
                        vers.add(group.getString("version"));
                        row.add(vers);
                    }
                    if(j < packages.count()) {
                        Tag val = new Tag("td");
                        val.add(packages.getString(j));
                        row.add(val);
                    } else if (j == packages.count()) {
                        Tag val = new Tag("td");
                        val.attribute("rowspan", (maxLine - j) + "");
                        row.add(val);
                    }
                    if(j < classes.count()) {
                        Tag val = new Tag("td");
                        Group cla = classes.getGroup(j);
                        val.add(cla.getString("name") + ":" + cla.getString("version"));
                        row.add(val);
                        Tag hash = new Tag("td");
                        hash.add("[" + cla.getString("hash") + "]");
                        row.add(hash);
                    } else if (j == packages.count()) {
                        Tag val = new Tag("td");
                        val.attribute("rowspan", (maxLine - j) + "");
                        row.add(val);
                    }
                    if(j < dependencies.count()) {
                        Tag val = new Tag("td");
                        val.add(dependencies.getString(j));
                        row.add(val);
                    } else if (j == packages.count()) {
                        Tag val = new Tag("td");
                        val.attribute("rowspan", (maxLine - j) + "");
                        row.add(val);
                    }
                    table.add(row);
                }
            }
            resp = table.render();
            response.header("Content-Type", "text/html");
        } else if (format.equals("json")) {
            JsonArray jsonArray = new JsonArray();
            for (int i = 0; i < providedList.count(); i++) {
                String name = providedList.getString(i);
                Group group = packets.getGroup(name);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("name", name);
                jsonObject.addProperty("version", group.getString("version"));
                JsonArray versions = new JsonArray();
                for (int j = 0; j < group.getList("packages").count(); j++) {
                    versions.add(new JsonPrimitive(group.getList("packages").getString(j)));
                }
                JsonArray dependencies = new JsonArray();
                for(int j = 0; j < group.getList("dependencies").count(); j++) {
                    dependencies.add(new JsonPrimitive(group.getList("dependencies").getString(j)));
                }
                jsonObject.add("packages", versions);
                jsonObject.add("dependencies", dependencies);
                JsonArray classes = new JsonArray();
                for (int j = 0; j < group.getList("classes").count(); j++) {
                    JsonObject clazz = new JsonObject();
                    Group c = group.getList("classes").getGroup(j);
                    clazz.addProperty("name", c.getString("name"));
                    clazz.addProperty("version", c.getString("version"));
                    clazz.addProperty("hash", c.getString("hash"));
                    classes.add(clazz);
                }
                jsonObject.add("classes", classes);
                jsonArray.add(jsonObject);
            }
            resp = jsonArray.toString();
        } else {
            Tag packs = new Tag("packages");
            for (int i = 0; i < providedList.count(); i++) {
                String name = providedList.getString(i);
                Group group = packets.getGroup(name);
                Tag pack = new Tag("package", packs);
                Tag tname = new Tag("name", pack);
                tname.add(name);
                Tag tversion = new Tag("version", pack);
                tversion.add(group.getString("version"));
                Tag tversions = new Tag("packages", pack);
                for (int j = 0; j < group.getList("packages").count(); j++) {
                    Tag tpackage = new Tag("packages", tversions);
                    tpackage.add(group.getList("packages").getString(j));
                }
                Tag tclasses = new Tag("classes", pack);
                for (int j = 0; j < group.getList("classes").count(); j++) {
                    Tag clazz = new Tag("class", tclasses);
                    Group c = group.getList("classes").getGroup(j);
                    Tag tcname = new Tag("name", clazz);
                    tcname.add(c.getString("name"));
                    Tag tcversion = new Tag("version", clazz);
                    tcversion.add(c.getString("version"));
                    Tag thash = new Tag("hash", clazz);
                    thash.add(c.getString("hash"));
                }
            }
            resp = packs.render();
        }
        response.setEntity(resp);
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }
}