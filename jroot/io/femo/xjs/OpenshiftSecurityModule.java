package io.femo.xjs;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;

import java.lang.Override;

@ContentProvider(regex = ".*")
@Named("openshift-security-module")
public class OpenshiftSecurityModule extends BootableModule implements Provider {

    private static boolean openshift = false;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        return this;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {
        openshift = System.getenv("OPENSHIFT_LOG_DIR") != null;
        if(openshift) {
            Server.log.info("Assuming OpenShift Security Enabled");
        }
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        response.setStatus(308);
        response.setStatusLine("Permanent Redirect");
        response.header("Location", "https://xjs-proxerread.rhcloud.com" + request.getRequestPath());
        response.header("XJS-Security-Notice", "Insecure Protocol");
    }

    @Override
    public boolean handles(HttpRequest request) {
        return openshift && request.header("X-Forwarded-Proto") != null && request.header("X-Forwarded-Proto").equalsIgnoreCase("HTTP");
    }
}