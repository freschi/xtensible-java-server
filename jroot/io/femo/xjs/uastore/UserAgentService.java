package io.femo.xjs.uastore;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.*;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.PackageRoot;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.services.SessionService;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;
import net.sf.uadetector.*;
import net.sf.uadetector.DeviceCategory;
import net.sf.uadetector.ReadableDeviceCategory;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.json.datareader.JsonDataReader;
import net.sf.uadetector.json.datastore.SimpleJsonDataStore;
import net.sf.uadetector.service.UADetectorServiceFactory;

import java.lang.Object;
import java.lang.Override;

@Service(type = ServiceType.SESSION)
@Named("user-agent-service")
@Package("xjs-analytics")
@PackageRoot
@Version("0.1")
public class UserAgentService extends BootableModule implements SessionService {

    private BootableLogicalUnit logicalUnit;
    private UserAgentStringParser parser;
    private Group userAgents;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        UserAgentService userAgentService = new UserAgentService();
        userAgentService.logicalUnit = logicalUnit;
        userAgentService.parser = UADetectorServiceFactory.getResourceModuleParser();
        userAgentService.userAgents = logicalUnit.getCatalog().openArticle("user-agent-service").openParagraph("user-agents").openEntry();
        return userAgentService;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public void setupSession(HttpRequest request, HttpResponse response) {
        String userAgentString = request.header("User-Agent");
        ReadableUserAgent userAgent = parser.parse(userAgentString);
        if(userAgent.getDeviceCategory().getCategory() == ReadableDeviceCategory.Category.UNKNOWN) {
            Server.log.warn("Unknown User-Agent " + userAgentString);
        } else {
            Group browsers = userAgents.getGroup("browser");
            String name = userAgent.getName().toLowerCase();
            Group browser = browsers.getGroup(name);
            String version = userAgent.getVersionNumber().toVersionString();
            if (browser.has(version)) {
                browser.setInt(version, browser.getInt(version) + 1);
            } else {
                browser.setInt(version, 1);
            }
            Group os = userAgents.getGroup("os");
            name = userAgent.getOperatingSystem().getName().toLowerCase();
            if (os.has(name)) {
                os.setInt(name, os.getInt(name) + 1);
            } else {
                os.setInt(name, 1);
            }
        }
    }

    @Override
    public void requestNewSession(HttpRequest request, HttpResponse response) {

    }

    @Override
    public void destroySession(HttpResponse response) {

    }

    @Override
    public Object put(Object... objects) {
        return null;
    }
}