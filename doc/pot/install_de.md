# Anwender Dokumentation

Dieses Dokument beschreibt wie Sie das **Project Organisation Tool** installieren und warten.

Diese Software befindet sich momentan im Alpha Stadium und verfügt über keinen Installationsmechanismus, sondern muss vom Quellcode installiert werden.

## Installation

Systemvoraussetzungen:
* Java 8 **JDK**
* Git
* Maven
* MySQL

### Laden der Abhängigkeiten und Erstellen der Anwendung

**Für diesen Schritt benötigen Sie eine bestehende Internetverbindung**

Laden Sie den Quellcode herunter

    git clone https://freschi@bitbucket.org/freschi/xtensible-java-server.git
    git checkout bootstrap --track origin/bootstrap

Wechseln Sie in das Verzeichnis, in das der Quellcode geladen wurde und führen Sie maven aus.

    mvn package

Kopieren Sie die nun aus dem Verzeichnis xtensible-java-server-core/target folgende Dateien und Verzeichnisse in das Verzeichnis in das der Quellcode geladen wurde.

* xjs-server.jar
* dependency-jars/

Führen Sie nun das SQL Skript *sql/create.sql* aus und passen Sie die Verbindungsdetails in der Datei *external/jroot/at/femoweb/pot/PotConnectionService.java* die Parameter in die Connection URLs für beide Connections ein.

Sie können die Anwendung nun starten indem Sie folgenden Befehl ausführen.

    java -jar xjs-server.jar

Zum Beenden des Servers müssen Sie den Prozess beenden. Beim Starten des Servers werden sämtliche Klassen der Anwendung neu kompiliert und die Anwendung dann gestartet.

## Wartung

Zur Wartung bietet die Anwendung eine einfache Terminalumgebung, die Sie über den Pfad **/admin/** auf der Applikation erreichen können.

Klicken Sie auf die Schaltfläche [Connect] um eine Sitzung zu öffnen. Diese Funktion steht Ihnen standardmäßig nur über localhost zur Verfügung. Um eine Verbindung herstellen zu können müssen Sie Ihre Benutzerdaten eingeben.

Mit dem Befehl **help** können Sie sich eine Übersicht über alle möglichen Befehle machen. Sie können sämtliche Befehle auch mit eindeutigen Kürzeln eingeben.

Wichtige Befehle

Befehl|Funktion
------|--------
list users|Gibt eine Tabelle aller Benutzer und deren Informationen aus
list projects|Gibt eine Tabelle aller Projekte aus
list structure|Gibt die Struktur des Servers aus
user [name]|Gibt Informationen zu den angegebenen User aus
version|Gibt die aktuelle Version aus

# Disclaimer

This document is subject to sudden changes ans is only valid for and only for **Project Organisation Tool** Version **0.4a**.

Copyright (c) 2015 [FeMo.IO](mailto:office@femo.io) Some Rights Reserved.
