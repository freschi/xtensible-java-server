# Endbenutzer Dokumentation

Dieses Dokument beschreibt die Funktionen des **Project Organisation Tools** auf Deutsch. Für eine weitere Fassungen besuchen Sie bitte [http://femo.io/doc/pot/enduser](http://femo.io/doc/pot/enduser)

# Benutzeraktionen

## Login

Beim Aufrufen der Applikation ([http://pot.femo.io](http://pot.femo.io)) wird Ihnen, insofern Sie sich nicht bereits angemeldet haben eine Login Maske gezeigt.

![Endbenutzer Login Maske](img/enduser_login.png)

Geben Sie bei *Username* und *Password* Ihren Benutzernamen und Ihr Passwort ein und bestätigen Sie entweder mit **Enter** oder mit [Login]. Beachten Sie bitte, das bei Drücken der **Löschtaste** im Feld *Password* der gesamte Inhalt des Feldes gelöscht wird.

Sollten Sie noch keine Zugangsdaten für diese Seite besitzen betätigen Sie bitte die Schaltfläsche [Register] um einen neuen Benutzer anlegen zu können.

## Registrierung

Nach betätigen der [Register] Schaltfläche gelangen Sie auf die Registrierungsseite. Diese kann auch direkt über [http://pot.femo.io/register](http://pot.femo.io/register) aufgerufen werden.

Auf dieser Seite können Sie einen neuen Benutzer anlegen, oder über die Schaltfläche [Back] zur Loginseite zurückgelangen.

![Endbenutzer Registrierungsmaske](img/enduser_register.png)

Diese Maske gliedert sich in die zwei Spalten *Login Information* (Anmelde Informationen) und *User Information* (Benutzer Informationen).

In die Felder *Username*, *Password* und *Retype Password* der ersten Spalte geben Sie bitte die Daten an mit denen Sie sich in Zukunft
bei dem Portal anmelden möchten. Ein bereits verwendeter Name wird vom System nicht angenommen und Ihnen wird eine Fehlermeldung angezeigt.

In die Felder *Firstname*, *Lastname*, *Display Name* und *E-Mail* der zweiten Spalte geben Sie bitte die Daten an, die anderen Nutzern angezeigt, bzw
von der Applikation genutzt werden um diverse Funktionen, wie sortierte Listen oder automatisierte E-Mails anzubieten. Öffentlich sichtbar sind
nur ihr Display Name (Anzeigename) und Ihre E-Mail Addresse.

Bestätigen Sie Ihre Daten mit dem Klicken auf [Register]. Ihre Daten werden nun vom System verarbeitet und Sie werden auf die Startseite weitergeleitet.

## Benutzereinstellungen

Um in das Fenster für die Benutzereinstellungen zu gelangen drücken Sie bitte die Schaltfläche [Settings] in der rechten oberen Ecke der Applikation.

![Enbenutzer Einstellungen](img/enduser_settings_button.png)

Nach betätigen der Schaltfläche gelangen Sie in das Menü für Benutzereinstellungen. Sie können dieses Menü wieder verlassen, indem Sie die Schaltfläche
[Back] betätigen.

### Ändern der E-Mail Addresse

![Benutzereinstellungen E-Mail](img/enduser_change_email.png)

Um Ihre verwendete E-Mail Addresse zu ändern, geben Sie die neue E-Mail Addresse in das Feld *E-Mail* ein und bestätigen Sie die Eingabe mit dem Drücken der
Schaltfläche [Save E-Mail]. Von nun an werden Nachrichten an diese E-Mail Addresse gesendet.

### Ändern des Anzeigenamens

![Benutzereinstellungen Anzeigenamen](img/enduser_change_display.png)

Um Ihren Anzeigenamen zu ändern, geben Sie den neuen Anzeigenamen in das Feld *Display* ein und bestätigen Sie die Eingabe mit dem Drücken der
Schaltfläche [Save Display Name]. Zukünftig wird dieser Name in allen Berichten und Anzeigen verwendet.

### Ändern des Passworts

![Benutzereinstellungen Passwort](img/enduser_change_password.png)

Um Ihr Passwort zu ändern, geben Sie ihr momentan verwendetes Passwort bei *Current Password* und ihr neues Passwort sowohl bei *New Password* als auch
bei *Retype New Password* ein. Bestätigen Sie diese Eingabe mit dem Drücken der Schaltfläche [Change Password]. Von nun an müssen Sie sich mit dem neuen
Passwort anmelden.

## Abmelden

Um Ihre Sitzung zu beenden betätigen Sie die Schaltfläche [Logout] in der oberen Rechten Ecke der Applikation.

![Enbenutzer Einstellungen](img/enduser_settings_button.png)

Nach Beendigung Ihrer Sitzung müssen Sie sich erneut anmelden.

# Seiten

Nach der erfolgreichen Anmeldung haben Sie Zugriff auf die Funktionen des **Project Organisation Tools** über die diversen Seiten der Applikation.

## Startseite

Nach einer erfolgreichen Anmeldung oder einem Neuladen der Seite gelangen Sie immer auf diese Seite.

![Enbenutzer Startseite](img/enduser_dashboard.png)

Auf dieser Seite werden Ihnen einerseits Ihre Projekte auf der rechten Seite, andererseits Ihre To-Dos auf der linken Seite angezeigt.

Um ein Projekt zu öffnen klicken Sie auf den Kurznamen des Projekts in der linken Tabelle.

Um ein neues Projekt zu erstellen klicken Sie auf die Schaltfläche [Add]

## Projektseiten

![Endbenutzer Projektstartseite](img/enduser_project_dashboard.png)

Nach dem Sie ein Projekt geöffnet haben werden Ihnen die Funktionen in einem Menü mit Registerkarten angezeigt. Um zur Übersicht zurückzukehren betätigen
Sie die Schaltfläche [Back] in der oberen rechten Ecke des Menüs.

### Info

Diese Seite ist die erste Seite, die Ihnen nach dem Öffnen eines Projekts angezeigt wird.

![Endbenutzer Projektinfos](img/enduser_project_info.png)

In der oberen Tabelle werden die Benutzer angezeigt, die am Projekt mitarbeiten. Projektleiter werden mit einem Stern neben dem Namen gekennzeichnet.
Normale Mitarbeiter werden mit einem Benutzer-Symbol neben dem Namen angezeigt. In der zweiten Spalte wird die E-Mail Addresse angezeigt, in der dritten
Spalte stehen die Stunden, die in der Freizeit aufgewendet wurden und in der vierten die Stunden, die in der Schule aufgewendet wurden.

In der unteren Tabelle werden die Benutzer angezeigt, die das Projekt überwachen können. In der zweiten Spalte wird die E-Mail Addresse des Benutzers angezeigt.

Die Schaltfläche [Manage People] leitet Sie auf die Seite zur Bearbeitung der Projektteilnehmer weiter. Dieser Menüpunkt ist nur verfügbar wenn Sie
als Projektleiter für das Projekt gespeichert sind.

![Projekt Mitgliederverwaltung](img/enduser_project_members.png)

In der oberen Liste werden in der ersten Spalte der Anzeigenamen des Benutzers angezeigt, in der zweiten wird die E-Mail angezeigt. In der dritten Spalte
kann die Rolle des Benutzers ausgewählt werden.

Folgende Rollen sind dafür verfügbar:

Symbol|Rolle|Berechtigungen
------|-----|--------------
![Projektleiter](img/enduser_member_leader.png)|Projektleiter|Benützung aller Funktionen, scheint in allen Berichten auf, kann Einstellungen ändern
![Projektmitglied](img/enduser_member_member.png)|Projektmitglied|Benützung aller Funktionen, scheint in allen Berichten auf
![Projektbetreuer](img/enduser_member_custodian.png)|Projektbetruer|Kann die eingestellten Daten betrachten, scheint in allen Berichten auf
![Projektbeobachter](img/enduser_member_watcher.png)|Projektbeobachter|Kann die eingestellten Daten betrachten, scheint nicht auf
![Entfernen eines Mitglieds](img/enduser_member_remove.png)|Entfernen eines Projektmitglieds|---

Um eine weitere Person zu einem Projekt hinzuzufügen wählen Sie diese Person im Feld *Person* aus und wählen Sie dessen Rolle im Feld *Role* aus. Bestätigen
Sie diese Auswahl mit dem Drücken der Schaltfläche [Add] und der Benutzer wird dem Projekt hinzugefügt.

Um dieses Fenster zu verlassen und zu der Info Seite zurückzukehren betätigen Sie die Schaltfläche [Done].

### Diary

![Zeitaufzeichnung](img/enduser_project_diary.png)

Auf dieser Seite wird eine Übersicht der bereits gearbeiteten Stunden im Projekt angezeigt. Die Felder der Tabelle sind in folgender Tabelle detailiert beschrieben.

Feld|Beschreibung
----|------------
 |Gibt an, ob die Tätigkeit in der Freizeit oder in der Schule durchgeführt wurde
Date|Das Datum an dem die Tätigkeit durchgeführt wurde
Hours|Die Dauer der Tätigkeit
Description|Die eingetrage Beschreibung für die Tätigkeiten
Type|Der Typ der Tätigkeit
Task|Die Arbeitspaketnummer zu der die Tätigkeit zugeordnet wird
 |Schaltflächen zum ändern und entfernen der Tätigkeit

 Um eine neue Tätigkeit anzulegen betätigen Sie bitte die Schaltfläche [Add].

 ![Eintrag hinzufügen](img/enduser_project_diary_add.png)

 Tragen Sie in das Feld *Date* das Datum der Aktivität ein. Das heutige Datum wird automatisch ausgewählt. Die aufgewendeten Stunden werden in das Feld *Hours* ein, und die Beschreibung des Tätigkeit in das Feld *Description*. Zusätzlich muss der Typ der Tätigkeit für *Type* ausgewählt werden.

Optional können noch angegeben werden, ob die Tätigkeit in der Schule *In School* durchgeführt wurde und zu welchem Arbeitspaket die Tätigkeit gehört.

Um die Daten zu Speichern und betätigen Sie die Schaltfläche [Finish]. Um zu der Überischt zurückzukehren ohne zu Speichern betätigen Sie die Schaltfläche [Back].

Um eine Tätigkeit zu bearbeiten betätigen Sie die Schaltfläche mit dem Editierungssymbol. Dadurch gelangen Sie zur Bearbeitungsseite für Tätigkeiten. Die Eingabemaske die erscheint ist dieselbe, jedoch sind bereits die Daten eingetragen, die schon bekannt sind.

Um eine Tätigkeit zu entfernen betätigen Sie die Schaltfläche mit dem Löschsymbol. Dadurch gelangen Sie zur Löschseite für Tätigkeiten.

![Eintrag entfernen](img/enduser_project_diary_delete.png)

Hier wird Ihnen eine Zusammenfassung der Tätigkeit angezeigt. Um zu der Übersicht aller Tätigkeiten zurückzukehren betätigen Sie die Schaltfläche [Back]. Um die Tätigkeit endgültig zu entfernen betätigen Sie die Schaltfläche [Delete]. Nach Betätigung der Schaltfläche wird die Tätigkeit dauerhaft gelöscht.

### Reports

![Enduser Berichte](img/enduser_project_reports.png)

Auf dieser Seite werden Ihnen die verschiedenen Möglichkeiten geboten, verschiedene Berichte herunterzuladen oder an gewisse Projektmitglieder zu senden. Die Option Berichte zu senden steht nur Projektleitern zur Verfügung.

Um Schnellberichte erstellen zu können, muss dazu unter *Project Settings* zuerst ein Standardberichtersteller ausgewählt werden.

Für jeden der möglichen Berichte stehen mehrere Möglichekeiten zur Verfügung.

Schaltfläche|Aktion
------------|------
Download|Leitet Sie weiter auf eine Seite um den Bericht herunterzuladen
Send to Custodians|Sendet den Bericht an die Projektbetreuer via E-Mail, wobei der Bericht als Anhang versendet wird
Send to Creators|Sendet den Bericht an die Projektleiter vie E-Mail, wobei der Bericht als Anhang versendet wird
QR Code|Erstellt einen QR Code mit dem auf den Bericht zugegriffen werden kann

#### Work Time Reports

Diese Berichte sind in zwei Kategorien unterteilt *Common Reports* (Übliche Berichte) und *Custom Report* (Benutzerdefinierter Bericht).

Unter den üblichen Berichten können sowohl Berichte über den gesamten Projektzeitraum erstellt werden oder ein Bericht für die letzte Kalenderwoche.

Um einen Benutzerdefinierten Bericht zu erstellen, geben Sie den Zeitraum in *From* und *To* ein und wählen Sie den Berichtersteller für *Provider* aus.

#### Work Unit Reports

Erstellt einen Bericht zu den Arbeitspaketen, in denen Fortschritt, Stunden und Abhängikeiten dargestellt werden.

### Bugs

![Enduser Bugs](img/enduser_project_bugs.png)

Auf dieser Seite werden die gespeicherten Bugs für das Projekt angezeigt.

Feld|Beschreibung
----|------------
State|Der Zustand des Bugs (UNASSIGNED, ASSIGNED, INWORK, ROLLOUT, FINISHED)
Name|Ein kurzer Name des Bugs
Description|Eine Beschreibung des Bugs
Assignee|Die Person die der Behebung des Bugs zugeteilt wurde
 |Schaltflächen zur Bearbeitung und zum Löschen eines Bugs

Um einen neuen Bug hinzuzufügen, klicken Sie auf die Schaltfläche [Add new Bug] und Sie gelangen zur Eingabemaske zur Erstellung und Bearbeitung von Bugs. Um einen Bug zu bearbeiten klicken Sie auf die Schaltfläche neben dem Bug den Sie bearbeiten möchten.

![Bugs Eingabemaske](img/enduser_project_bugs_add.png)

In dieser Eingabemaske geben Sie nun die Daten des Bugs ein, den Sie hinzufügen möchten. In das Feld *Name* tragen Sie einen Kurznamen ein, in das Feld *Description* tragen Sie eine Beschreibung für den Bug ein. Abschließend wählen Sie den Status des Bugs in *State* aus, genauso wie den Bearbeiter in *Assignee* und klicken auf [Save] um die Daten zu speichern.

Um zur Übersicht zurückzukehren betätigen Sie die Schaltfläche [Back].

Um einen Bug zu entfernen klicken Sie auf die Löschen-Schaltfläche neben dem Bug den Sie entfernen möchten. Darufhin gelangen Sie zu einem Dialog, bei dem Sie gefragt werden, ob Sie den Bug wirklich löschen möchten.

![Bug Entfernen](img/enduser_project_bugs_delete.png)

In diesem Dialog wird Ihnen eine Zusammenfassung des Bugs angezeigt. Um den Bug endgültig zu löschen klicken Sie auf die Schaltfläche [Delete]. Um zur Übersicht zurückkehren klicken Sie auf die Schaltfläche [Back].

### Changelog

Auf dieser Seite wird Ihnen der Changelog des Projekts angezeigt. Dieser gliedert sich in Versionen und Änderungen.

![Changelog](img/enduser_project_changelog.png)

Die Änderungen werden nach Version gegliedert, wobei die neueste Version immer zuoberst angezeigt wird.

Um eine neue Änderung hinzuzufügen, klicken Sie auf die Schaltfläche [Add Change]. Anschließend wird Ihnen die Eingabemaske zur Erstellung einer neuen Änderung angezeigt.

![Changelog Änderung](img/enduser_project_changelog_change.png)

Geben Sie bei *Description* eine Beschreibung der Änderung ein, die Sie durchgeführt haben. Bestätigen Sie die Eingabe, indem Sie auf [Store] klicken, oder kehren Sie zu der Übersicht zurück, indem auf [Back] klicken.

Um eine neue Version hinzuzufügen klicken Sie auf die Schaltfläche [Add Version]. Diese Schaltfläche kann nur von einem Projektleiter genutzt werden. Anschließend wird Ihnen eine Maske angezeigt, zur Erstellung einer neuen Version.

![Changelog Version](img/enduser_project_changelog_version.png)

Geben Sie bei *Version Number* die Versionsnummer ein, die diese Version haben soll und geben Sie bei *Version Name* den Namen der Version ein. Bestätigen Sie die Eingabe, indem Sie auf [Store] klicken, oder kehren Sie zu der Übersicht zurück, indem auf [Back] klicken.

### Work Units

Auf dieser Seite können Sie ein Gantt-Chart hochladen und die bereits eingelesenen Arbeitspakete ansehen.

![Arbeitspakete](img/enduser_project_work_units.png)

Um Arbeitspakete hochzuladen klicken Sie auf die Schaltfläche [Select Gantt File] und wählen Sie eine Datei zum Upload aus. Klicken Sie anschließend auf [Upload] um die Datei hochzuladen und vom Server verarbeiten zu lassen. Ihnen werden nun Meldungen angezeigt, die bei der Auswertung der Datei erstellt werden und Ihnen werden Optionen zur Weiterverarbeitung der Daten angezeigt.

![Einpflegen eines Gantt-Chart](img/enduser_project_work_units_upload.png)

In *Custom ID* wählen Sie eine Spalte aus, aus der die ID gewählt werden soll, sollten Sie eine andere ID als die automatisch erstellte ID für die Arbeitspakete wollen. Genauso können Sie mit *Planned Hours* eine Spalte auswählen, in der die Planstunden pro Arbeitspaket eingetragen sind.

Um den Vorgang abzuschließen und die Daten einzupflegen klicken Sie auf die Schaltfläche [Do it!]. Ihre Arbeitspakete werden nun in das System eingeplegt und stehen in Zukunft für weitere Funktionen zur Verfügung.

Die eingepflegten Arbeitspakete werden nun in der Tabelle angezeigt.

![Arbeitspakete](img/enduser_project_work_units_table.png)

Feld|Beschreibung
----|------------
 |Zeigt an, ob das Arbeitspaket einen Meilenstein darstellt oder nicht
ID|Die Nummer des Arbeitspakets
Name|Der Name des Arbeitspakets
Start|Das Datum, an dem das Arbeitspaket gestartet wird
End|Das Datum, an dem das Arbeitspaket beendet wird
 |Schaltflächen zum Betrachten und Bearbeiten eines Arbeitspakets

Um ein Arbeitspaket detailiert zu betrachten klicken Sie auf die grüne Schaltfläche in der Zeile des Arbeitspakets.

![Arbeitspaket Info](img/enduser_project_work_units_work_unit.png)

Ihnen wird nun eine Zusammenfassung des Arbeitspakets gezeigt, sowie dessen Unterpakete, Abhängigkeiten und Nachfolger. Sie können zu einem anderen Arbeitspaket wechseln, indem Sie auf dessen Nummer und Bezeichnung klicken.

Um zur Übersicht zurückzukehren klicken Sie auf die Schaltfläche [Back]. Um ein Arbeitspaket zu editieren können Sie entweder in diesem Fenster auf [Edit Task] klicken oder in der Übersicht auf das Editieren Symbol in der Zeile des Arbeitspakets klicken. Ihnen wird nun eine Maske zur Bearbeitung des Arbeitspakets angezeigt.

![Arbeitspaket editieren](img/enduser_project_work_units_edit.png)

Zusätzlich zu den bereits beschriebenen Feldern können hier noch die Felder *Description*, für eine Beschreibung des Arbeitspakets, *Dependency Problems*, um kurz Probleme aufzuzeigen, die bei Verzögerungen auftretten könnnen. Außerdem kann ausgewählt werden, ob das Arbeitspaket ein Meilenstein ist oder nicht.

Um die Änderungen zu speichern klicken Sie auf die Schaltfläche [Store]. Um zur Übersicht zurückzukehren drücken Sie auf [Back].

### WBS

Auf dieser Seite können Sie den Projektstrukturplan, der aus den Arbeitspaketen erstellt wurde betrachten.

![Projektstrukturplan](img/enduser_project_wbs.png)

In dem angezeigten Projektstrukturplan sind die einzelnen Arbeitspakete hierachisch dargestellt und Meilensteine gelb hervorgehoben.

Um den Projektstrukturplan an die Projektbetreuer zu senden klicken Sie auf die Schaltfläche [Send to Custodians]. Der Plan wird als Anhang den Betreuern per E-Mail gesendet.

### Milestones

Auf dieser Seite werden die Meilensteine des Projekts dediziert hervorgehoben angezeigt.

![Meilensteine](img/enduser_project_milestones.png)

Feld|Beschreibung
----|------------
ID|Die Nummer des Arbeitspakets
Name|Der Name des Arbeitspakets
Planned Date|Das Datum, an dem der Meilenstein planmäßig beendet hätte werden sollen
Due Date|Das Datum, an dem der Meilenstein beendet wurde

### Documents

**Augrund neuer gesetzlicher Regelungen bezüglich des Datenschutzgesetzes in der EU ist diese Funktion nicht mehr eindeutig gesetzlich geregelt.**

Auf dieser Seite werden die Dokumente, die mit diesem Projekt verknüpft sind dargestellt. Sie können die eingegebenen Dokumente in einem neuen Fenster oder auf der Seite öffnen. Diese Funktionen sind jedoch nur experimentell implementiert und wird sich noch häufig ändern.

### Project Settings

Sämtliche Unterpunkte dieser Seite, abgesehen von 'Make me a Creator', können nur von Projektleitern genutzt werden.

#### Make me a Creator

![Mach mich zum Projektleiter](img/enduser_project_settings_creator.png)

Sollten Sie sich als Ersteller des Projekts, die Rechte als Projektleiter genommen haben, können Sie über das Betätigen der Schaltfläche [Make me a Creator] wieder zu einem Projektleiter machen.

#### Default Provider

![Standardberichtersteller](img/enduser_project_settings_default.png)

Wählen Sie hier den Standardberichtersteller für das Projekt aus der Liste *Default Provider* aus und bestätigen Sie die Auswahl mit [Save Default Report Provider].

#### Change Project Information

![Projektinformationen ändern](img/enduser_project_settings_info.png)

Um den Projektnamen zu ändern geben Sie den neuen Namen in *Project Name* ein und speichern Sie diese in dem Sie auf [Save Project Name] klicken. Der Name wird gespeichert und wird fortan für alle Berichte verwendet.

Um das Projektkürzel zu ändern geben Sie das neue Kürzel in *Project Short* ein und speichern Sie diese in dem Sie auf [Save Project Short] klicken. Dieses Kürzel wird gespeichert und wird von dann an verwendet.

Um die Projektbeschreibung zu ändern geben Sie die neue Beschreibung in *Project Description* ein und speichern Sie diese indem Sie auf [Save Project Description] klicken. Diese Beschreibung wird gespeichert und von dann an verwendet.

## Neues Projekt anlegen

Um ein neues Projekt hinzuzufügen klicken Sie auf der Startseite auf [Add]. Sie werden zur Maske weitergeleitet, auf der Sie ein neues Projekt anlegen können.

![Projekt anlegen](img/enduser_add_project.png)

Geben Sie in *Project Name* den Namen des Projekts, in das Feld *Project Short* das Projektkürzel und in *Description* eine kurze Beschreibung des Projekt ein und bestätigen Sie diese Angaben indem Sie auf [Add Project] klicken. Sie werden auf die Seite zur Verwaltung der Projektmitglieder weitergeleitet. Diese wurde bereits weiter oben im Dokument beschrieben.

# Disclaimer

This document is subject to sudden changes ans is only valid for and only for **Project Organisation Tool** Version **0.4a**.

Copyright (c) 2015 [FeMo.IO](mailto:office@femo.io) Some Rights Reserved.
