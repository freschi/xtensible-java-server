# Entwicklerdokumentation

Dieses Dokument beschreibt wie Sie das **Project Organisation Tool** weiterentwickeln können.



# Disclaimer

This document is subject to sudden changes ans is only valid for and only for **Project Organisation Tool** Version **0.4a**.

Copyright (c) 2015 [FeMo.IO](mailto:office@femo.io) Some Rights Reserved.
