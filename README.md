# README #

This README would normally document whatever steps are necessary to get your application up and running, but it won't since documentation is non existant and basically not working...

### What can this server do? ###

* Serve all types of files with no preprocessing
* Create fully HTML 5 compilant pages from own file format
* Add nice asynchronous actions to your webpage with our easy action API
* Create new Providers and Services and (re-)load them at runtime
* Modular construct, shipped modules can easily be overriden

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Felix Resch](mailto:felix.resch@femoweb.at)
* Other community or team contact