package at.femoweb;

import at.femoweb.http.HttpRequest;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;

import java.lang.Override;
import java.lang.String;

@Resource("user-agent-condition")
public class UserAgentWarningCondition implements ConditionCheck {

    @Override
    public boolean check(String[] args) {
        HttpRequest request = (HttpRequest) Session.get("_xjs_http_request");
        return request.header("User-Agent") == null || !request.header("User-Agent").contains("Firefox");
    }
}