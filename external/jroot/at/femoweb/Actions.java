package at.femoweb;


import at.femoweb.html.Document;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.XJSUser;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.femo.security.RequiresAuthentication;
import io.femo.security.Secured;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import java.lang.*;
import java.lang.Exception;
import java.lang.Integer;
import java.lang.String;
import java.lang.StringBuilder;
import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IntSummaryStatistics;

@Secured
public class Actions {

    @XAction(name = "destroy_session")
    public void destroySession (@XUpdate Updates updates) {
        updates.changeLocation("/");
        Session.destroy();
    }
    
    @XAction(name = "pot_add_project")
    @Secured
    @RequiresAuthentication
    public void addProject (@XUpdate Updates updates) {
        updates.setPartial("femoweb_pot_main", "project/add.xjs");
    }
    
    @XAction(name = "pot_return_main")
    public void returnToMain(@XUpdate Updates updates) {
        updates.setPartial("femoweb_pot_main", "main.xjs");
        updates.title("Project Organisation Tool");
    }
    
    @XAction(name = "pot_add_project_commit")
    @Secured
    @RequiresAuthentication
    public void saveProject(@XUpdate Updates updates, @XParam(name = "pot_project_name") String name, @XParam(name = "pot_project_short") String pshort, @XParam(name = "pot_project_descr") String descr) {
        boolean check = true;
        if(name == null || name.equals("")) {
            updates.highlightError("pot_project_name");
            check = false;
        } else {
            updates.removeError("pot_project_name");
        }
        if(pshort == null || pshort.equals("")) {
            updates.highlightError("pot_project_short");
            check = false;
        } else {
            updates.removeError("pot_project_short");
        }
        if(descr == null || descr.equals("")) {
            updates.highlightError("pot_project_descr");
            check = false;
        } else {
            updates.removeError("pot_project_descr");
        }
        if(check) {
            Connection connection = (Connection) Session.get("connection");
            XJSUser user = (XJSUser) Session.get("_xjs_user");
            try {
                Statement statement = connection.createStatement();
                String query = "INSERT INTO p_projects (p_name, p_short, p_descr, p_u_creator, p_r_reportprovider) VALUES ('" + name + "', '" + pshort + "', '" + descr + "', " + user.getProperty("user.id") + ", (select idr_reportprovider from r_reportprovider limit 1))";
                statement.execute(query);
                ResultSet resultSet = statement.executeQuery("SELECT max(idp_projects) FROM p_projects");
                String id = null;
                if(resultSet.first()) {
                    Session.set("curr-project", id = resultSet.getString(1));
                }
                query = "INSERT INTO w_works (w_u_user, w_p_project, w_diary_perm, w_info_perm, w_report_perm) VALUES (" + user.getProperty("user.id") + ", " + id + ", 2, 3, 2)";
                statement.execute(query);
                statement.close();
                updates.setPartial("femoweb_pot_main", "project/add/people.xjs");
            } catch (SQLException e) {
                Server.log.warn("Could not create project. Maybe you forgot to remove report provider...", e);
                updates.addMessage(Updates.Severity.DANGER, "Internal Database error. Aborting Project creation!");
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Not all information provided. Aborting Project creation!");
        }
    }

    @XAction(name = "pot_femoweb_project_show")
    @Secured
    @RequiresAuthentication
    public void showProject(@XUpdate Updates updates, @XOptional JsonElement optional) {
        JsonObject object = optional != null ? optional.getAsJsonObject() : null;
        String project;
        if(object != null && object.has("project")) {
            project = object.get("project").getAsString();
        } else if (Session.is("curr-project")) {
            project = Session.get("curr-project").toString();
        } else {
            updates.addMessage(Updates.Severity.WARNING, "There is no project to be shown");
            return;
        }
        Session.set("curr-project", project);
        updates.setPartial("femoweb_pot_main", "project/overview.xjs");
        updates.title(Session.get("project-short") + " - POT");
    }
    
    @XAction(name = "pot_femoweb_project_show_add")
    public void showAddProject(@XUpdate Updates updates) {
        updates.setPartial("femoweb_pot_main", "project/add/people.xjs");
    }
    
    @XAction(name = "pot_project_add_person")
    public void addPerson(@XUpdate Updates updates, @XParam(name = "pot_project_person_role") String role, @XParam(name = "pot_project_person") String person) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO w_works (w_u_user, w_p_project, w_info_perm, w_diary_perm, w_report_perm) VALUES (");
        String project = Session.get("curr-project").toString();
        builder.append(person);
        builder.append(", ");
        builder.append(project);
        builder.append(", ");
        if(Integer.parseInt(role) >= 2) {
            builder.append(role);
            builder.append(", 2, 2)");
        } else {
            builder.append(role);
            builder.append(", ");
            builder.append(role);
            builder.append(", ");
            builder.append(role);
            builder.append(")");
        }
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                statement.execute(builder.toString());
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not add person to project", e);
            }
        }
        showAddProject(updates);
    }


    
}