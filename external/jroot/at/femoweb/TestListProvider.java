package at.femoweb;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.xjs.ListProvider;
import at.femoweb.xjs.modules.xjs.ListProvider.ListItem;

import java.lang.Override;
import java.util.ArrayList;
import java.util.List;

@Resource("test.conf-list-provider")
public class TestListProvider implements ListProvider {

    private static final String[] NAMES = new String[] {"Pater", "Zumpf", "Swoboda"};
    
    @Override
    public List<ListItem> createList() {
        ArrayList<ListItem> list = new ArrayList<>();
        for (int i = 0; i < NAMES.length; i++) {
            ListItem item = new ListItem();
            item.setTag(createPersonTag(NAMES[i]));
            item.setValue(NAMES[i].toLowerCase());
            list.add(item);
        }
        return list;
    }
    
    private Tag createPersonTag(String name) {
        Tag person = new Tag("span");
        Tag icon = new Tag("span");
        icon.type(TagType.FULL);
        icon.attribute("class", "glyphicon glyphicon-user");
        person.add(icon);
        person.add(name);
        return person;
    }
}