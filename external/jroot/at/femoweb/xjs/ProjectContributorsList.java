package at.femoweb.xjs;

import at.femoweb.data.core.DataContext;
import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagContent;
import at.femoweb.xjs.util.ResultCallback;
import at.femoweb.xjs.util.SQL;
import at.femoweb.xjs.util.ValueProvider;
import io.femo.xjs.tags.FaIcon;

import java.lang.Integer;
import java.lang.Override;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


@Resource("pot-contributors")
@Package("femoio-pot")
public class ProjectContributorsList implements TagContent {

    @Override
    public Tag generateTags() {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                final String project = Session.get("curr-project").toString();
                final Tag table = new Tag("table");
                SQL.query(connection, "SELECT u_display,\n" +
                        "       u_email,\n" +
                        "       Coalesce(DATA.ahours, 0) ahours,\n" +
                        "       Coalesce(DATA.bhours, 0) bhours,\n" +
                        "       w_info_perm\n" +
                        "FROM   w_works\n" +
                        "       INNER JOIN u_user\n" +
                        "               ON w_u_user = idu_user\n" +
                        "       LEFT JOIN (SELECT A.e_p_project project,\n" +
                        "                         A.e_u_user    user,\n" +
                        "                         A.hours       ahours,\n" +
                        "                         B.hours       bhours\n" +
                        "                  FROM   (SELECT e_p_project,\n" +
                        "                                 e_u_user,\n" +
                        "                                 Sum(e_hours)       hours,\n" +
                        "                                 Count(ide_entries) count\n" +
                        "                          FROM   e_entries\n" +
                        "                          WHERE  e_inschool = 0\n" +
                        "                          GROUP  BY e_u_user,\n" +
                        "                                    e_p_project) AS A\n" +
                        "                         JOIN (SELECT e_p_project,\n" +
                        "                                      e_u_user,\n" +
                        "                                      Sum(e_hours)       hours,\n" +
                        "                                      Count(ide_entries) count\n" +
                        "                               FROM   e_entries\n" +
                        "                               WHERE  e_inschool = 1\n" +
                        "                               GROUP  BY e_u_user,\n" +
                        "                                         e_p_project) AS B\n" +
                        "                           ON A.e_u_user = B.e_u_user\n" +
                        "                              AND A.e_p_project = B.e_p_project) AS DATA\n" +
                        "              ON w_p_project = project\n" +
                        "                 AND w_u_user = user\n" +
                        "WHERE  w_p_project = ?\n" +
                        "       AND w_info_perm >= 2\n" +
                        "ORDER  BY w_info_perm DESC,\n" +
                        "          u_lastname,\n" +
                        "          u_firstname; \n")
                .values(new ValueProvider() {
                    @Override
                    public boolean next() {
                        return false;
                    }

                    @Override
                    public void putData(PreparedStatement preparedStatement) throws SQLException {
                        preparedStatement.setInt(1, Integer.parseInt(project));
                    }
                }).execute(new ResultCallback() {
                    @Override
                    public void result(ResultSet r) throws SQLException {
                        if(r.first()) {
                            table.attribute("class", "table table-bordered table-striped table-hover");
                            Tag hrow = new Tag("tr");
                            Tag hname = new Tag("th");
                            hname.add("Name");
                            hrow.add(hname);
                            Tag hmail = new Tag("th");
                            hmail.add("E-Mail");
                            hrow.add(hmail);
                            Tag hhours = new Tag("th");
                            hhours.add("Total Hours");
                            Tag hicon = new FaIcon();
                            hicon.attribute("icon", "home");
                            hhours.add(hicon);
                            hrow.add(hhours);
                            Tag hentries = new Tag("th");
                            hentries.add("Total Hours");
                            hicon = new FaIcon();
                            hicon.attribute("icon", "building");
                            hentries.add(hicon);
                            hrow.add(hentries);
                            table.add(hrow);
                            do {
                                Tag row = new Tag("tr");
                                Tag name = new Tag("td");
                                if(r.getInt(5) == 2) {
                                    Tag span = new Tag("span");
                                    Tag icon = new FaIcon();
                                    icon.attribute("icon", "user");
                                    span.add(icon);
                                    span.add(r.getString(1));
                                    name.add(span);
                                } else if (r.getInt(5) == 3) {
                                    Tag span = new Tag("span");
                                    Tag icon = new FaIcon();
                                    icon.attribute("icon", "star");
                                    span.add(icon);
                                    span.add(r.getString(1));
                                    name.add(span);
                                }
                                row.add(name);
                                Tag linkCont = new Tag("td");
                                Tag link = new Tag("a");
                                link.add(r.getString(2));
                                link.attribute("href", "mailto:" + r.getString(2));
                                linkCont.add(link);
                                row.add(linkCont);
                                Tag hours = new Tag("td");
                                hours.add(String.format("%.2f", r.getDouble(3)));
                                row.add(hours);
                                Tag entries = new Tag("td");
                                entries.add(String.format("%.2f", r.getDouble(4)));
                                row.add(entries);
                                table.add(row);
                            } while (r.next());
                        }
                    }
                });
                if(table.children().size() != 0)
                    return table;
                /*Statement statement = connection.createStatement();

                ResultSet resultSet = statement.executeQuery("select u_display, u_email, sum(e_hours), count(ide_entries), w_info_perm from w_works INNER join u_user on w_u_user = idu_user left join (select * from e_entries where e_p_project = " + project + ") as dev on dev.e_u_user = idu_user where w_info_perm >= 2 and w_p_project = " + project + " group by idu_user order by u_lastname, u_firstname");
                if(resultSet.first()) {
                    //Tag table = new Tag("table");

                    return table;
                }*/
            } catch (SQLException e) {
                Server.log.warn("Not able to create contributors list", e);
            }
        }
        Tag message = new Tag("strong");
        message.add("Could not create contributors list");
        return message;
    }
}