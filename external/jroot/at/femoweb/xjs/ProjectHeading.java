package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagContent;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-project-heading")
public class ProjectHeading implements TagContent {

    @Override
    public Tag generateTags() {
        Tag div = new Tag("div");
        if(Session.is("curr-project")) {
            Connection connection = (Connection) Session.get("connection");
            if(connection != null) {
                try {
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery("select p_name, p_short, p_descr from p_projects where idp_projects = " + Session.get("curr-project"));
                    String name, pshort, descr;
                    if(resultSet.first()) {
                        Session.set("project-name", name = resultSet.getString(1));
                        Session.set("project-short", pshort = resultSet.getString(2));
                        Session.set("project-descr", descr = resultSet.getString(3));
                        Tag heading = new Tag("h2");
                        heading.add(pshort);
                        div.add(heading);
                        Tag subtitle = new Tag("strong");
                        subtitle.add(name);
                        div.add(subtitle);
                    } else {
                        Tag message = new Tag("strong");
                        message.add("Error while connecting to database");
                        div.add(message);
                    }
                } catch (SQLException e) {
                    Server.log.warn("Error while fetching project information", e);
                    Tag message = new Tag("strong");
                    message.add("Error while connecting to database");
                    div.add(message);
                }
            } else {
                Tag message = new Tag("strong");
                message.add("No connection set");
                div.add(message);
            }
        } else {
            Tag heading = new Tag("h2");
            heading.add("No Project set");
            div.add(heading);
        }
        return div;
    }
}