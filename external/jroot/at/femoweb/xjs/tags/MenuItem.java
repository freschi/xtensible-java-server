package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:menuItem", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class MenuItem implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag item = new Tag("li");
        String content = "_xjs_null";
        if(tag.attribute("content") != null) {
            content = tag.attribute("content").replace("/", "&#47;");
        }
        String parents = "";
        if(tag.attribute("parent") != null) {
            parents = tag.attribute("parent") + " &gt; ";
        }
        parents += tag.attribute("label");
        if(tag.children().size() > 0) {
            Tag container = new Tag("div");
            Tag link = new Tag("a");
            if(tag.attribute("label") != null)
                link.add(tag.attribute("label"));
            container.add(link);
            Tag list = new Tag("ul");
            list.attribute("style", "list-style-type: none; padding-left: 10px");
            Replacer replacer = (Replacer) Session.get("_xjs_replacer");
            link.attribute("onclick", "invoke('_xjs_menuitem_change_item', {content:'" + content + "', title:'" + parents +"'});");
            for(Tag t : tag.children()) {
                t.attribute("parent", parents);
            }
            list.add(tag.children());
            replacer.replace(list);
            container.add(list);
            item.add(container);
        } else {
            Tag link = new Tag("a");
            if(tag.attribute("label") != null)
                link.add(tag.attribute("label"));
            link.attribute("onclick", "invoke('_xjs_menuitem_change_item', {content:'" + content + "', title:'" + parents +"'});");
            item.add(link);
        }
        return item;
    }

    @XAction(name = "_xjs_menuitem_change_item")
    public void changeContent(@XUpdate Updates updates, @XParam(name = "_xjs_menu_content") String content, @XParam(name = "_xjs_menu_title") String title, @XOptional JsonElement optional) {
        JsonObject optionals = optional.getAsJsonObject();
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Service parser = logicalUnit.getService("xjs-parser-service");
        if(parser != null) {
            updates.html(content, (String) parser.put(optionals.get("content").getAsString()));
        } else {
            updates.html(content, "<strong>Could not find Parser Service. Please make sure it is installed!</strong>");
        }
        updates.html(title, optionals.get("title").getAsString());
    }
}