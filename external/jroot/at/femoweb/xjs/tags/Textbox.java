package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:textbox", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class Textbox implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        if(tag.attribute("id") != null) {
            Tag formGroup = new Tag("div");
            String id = tag.attribute("id");
            formGroup.attribute("class", "form-group");
            formGroup.attribute("id", id + "_group");
            Tag label = new Tag("label");
            label.attribute("for", id);
            label.attribute("class", "control-label");
            if(tag.attribute("label") != null) {
                label.add(tag.attribute("label"));
            } else {
                label.add(tag.children());
                Replacer replacer = (Replacer) Session.get("_xjs_replacer");
                if(replacer != null) {
                    replacer.replace(label);
                }
            }
            formGroup.add(label);
            formGroup.add(new Tag("br"));
            Tag input = new Tag("input");
            input.attribute("id", id);
            input.attribute("class", "form-control");
            if(tag.attribute("type") != null) {
                if(tag.attribute("type").equalsIgnoreCase("date")) {
                    input.attribute("class", input.attribute("class") + " _xjs_datepicker");
                    input.attribute("type", "text");
                } else  if (tag.attribute("type").equalsIgnoreCase("sql-date")) {
                    input.attribute("class", input.attribute("class") + " _xjs_sqldatepicker");
                    input.attribute("data-date-format", "YYYY-MM-DD");
                    input.attribute("type", "text");
                } else {
                    input.attribute("type", tag.attribute("type"));
                }
            } else {
                input.attribute("type", "text");
            }
            if(tag.attribute("field") != null && Session.is(tag.attribute("field")) && Session.get(tag.attribute("field")) != null) {
                input.attribute("value", Session.get(tag.attribute("field")).toString());
            } else if (tag.attribute("value") != null) {
                input.attribute("value", tag.attribute("value"));
            }
            if(tag.attribute("onkeydown") != null) {
                tag.attribute("onkeydown", "invoke('" + tag.attribute("onkeydown") + "', {evt: event});");
            }
            formGroup.add(input);
            return formGroup;
        } else {
            Tag message = new Tag("strong");
            message.add("Textbox requires id");
            return message;
        }
    }
}