package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:fa", type = TagReplacerInfo.ReplacementType.DROP_INTERNAL)
@Version("0.1a")
@Package("xjs-tags")
public class FaIcon implements TagReplacer {

    @Override
    public Tag replace(Tag tag) {
        Tag icon = new Tag("i");
        icon.attribute("class", "fa fa-" + tag.attribute("icon"));
        icon.type(TagType.FULL);
        return icon;
    }

    @Override
    public boolean replaces(String tagName) {
        return false;
    }
}