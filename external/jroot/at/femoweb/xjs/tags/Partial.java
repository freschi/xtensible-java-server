package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:partial")
@Package("xjs-tags")
@Version("0.1a")
public class Partial implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Service service = logicalUnit.getService("xjs-parser-service");
        Tag div = new Tag("div");
        if(service != null) {
            if(tag.attribute("path") != null) {
                div.add(service.put(tag.attribute("path")).toString());
            }
        }
        div.type(TagType.FULL);
        return div;
    }
}