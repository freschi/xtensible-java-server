package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import at.femoweb.xjs.services.RegistrationService;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;

import java.io.UnsupportedEncodingException;
import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:register", type = TagReplacerInfo.ReplacementType.DROP_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class Register implements TagReplacer {


    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag register = new Tag("div");
        register.attribute("class", "container");
        Tag jumbotron = new Tag("div", register);
        //jumbotron.attribute("class", "jumbotron");
        Tag heading = new Tag("h1", jumbotron);
        heading.add("Register");
        Tag row = new Tag("div", jumbotron);
        row.attribute("class", "row");
        Tag lcol = new Tag("div", row);
        lcol.attribute("class", "col-md-6 col-sm-6 col-xs-6");
        Tag headLogin = new Tag("h2", lcol);
        headLogin.add("Login Information");
        Tag rcol = new Tag("div", row);
        rcol.attribute("class", "col-md-6 col-sm-6 col-xs-6");
        Tag headUser = new Tag("h2", rcol);
        headUser.add("User Information");
        boolean enabled = true;
        {
            Tag irow = new Tag("div");
            Tag column = new Tag("div");
            irow.attribute("class", "row");
            column.attribute("class", "col-md-12 col-sm-12 col-xs-12");
            irow.add(column);
            LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
            if (logicalUnit == null) {
                Tag alert = new Tag("div");
                alert.attribute("class", "alert alert-danger");
                alert.add("Session not compatible with Register Component. Please contact your webmaster");
                column.add(alert);
                enabled = false;
            } else {
                Service service = logicalUnit.getService(tag.attribute("module"));
                if (service == null) {
                    Tag alert = new Tag("div");
                    alert.attribute("class", "alert alert-danger");
                    alert.add("Registration Module not loaded. Please contact your webmaster");
                    column.add(alert);
                    enabled = false;
                }
            }
            if(!enabled)
                jumbotron.add(irow);
        }
        {   //Username Input
            Tag group = new Tag("div", lcol);
            group.attribute("class", "form-group");
            group.attribute("id", "name_group");
            Tag label = new Tag("label");
            label.attribute("for", "name");
            label.attribute("class", "control-label");
            label.add("Username");
            group.add(label);
            Tag text = new Tag("input");
            text.attribute("id", "name");
            text.attribute("type", "text");
            text.attribute("class", "form-control");
            text.attribute("style", "max-width: 250px");
            group.add(text);
        }
        {   //Password input
            Tag group = new Tag("div", lcol);
            group.attribute("class", "form-group");
            group.attribute("id", "pass_group");
            Tag label = new Tag("label");
            label.attribute("for", "pass");
            label.attribute("class", "control-label");
            label.add("Password");
            group.add(label);
            Tag text = new Tag("input");
            text.attribute("id", "pass");
            text.attribute("type", "password");
            text.attribute("class", "form-control");
            text.attribute("style", "max-width: 250px");
            group.add(text);
        }
        {   //Password input
            Tag group = new Tag("div", lcol);
            group.attribute("class", "form-group");
            group.attribute("id", "pass_repeat_group");
            Tag label = new Tag("label");
            label.attribute("for", "pass_repeat");
            label.attribute("class", "control-label");
            label.add("Retype Password");
            group.add(label);
            Tag text = new Tag("input");
            text.attribute("id", "pass_repeat");
            text.attribute("type", "password");
            text.attribute("class", "form-control");
            text.attribute("style", "max-width: 250px");
            group.add(text);
        }
        {   //Firstname Input
            Tag group = new Tag("div", rcol);
            group.attribute("class", "form-group");
            group.attribute("id", "firstname_group");
            Tag label = new Tag("label");
            label.attribute("for", "firstname");
            label.attribute("class", "control-label");
            label.add("First Name");
            group.add(label);
            Tag text = new Tag("input");
            text.attribute("id", "firstname");
            text.attribute("type", "text");
            text.attribute("class", "form-control");
            text.attribute("style", "max-width: 250px");
            group.add(text);
        }
        {   //Lastname Input
            Tag group = new Tag("div", rcol);
            group.attribute("class", "form-group");
            group.attribute("id", "lastname_group");
            Tag label = new Tag("label");
            label.attribute("for", "lastname");
            label.attribute("class", "control-label");
            label.add("Last Name");
            group.add(label);
            Tag text = new Tag("input");
            text.attribute("id", "lastname");
            text.attribute("type", "text");
            text.attribute("class", "form-control");
            text.attribute("style", "max-width: 250px");
            group.add(text);
        }
        {   //Displayname Input
            Tag group = new Tag("div", rcol);
            group.attribute("class", "form-group");
            group.attribute("id", "display_group");
            Tag label = new Tag("label");
            label.attribute("for", "display");
            label.attribute("class", "control-label");
            label.add("Display Name");
            group.add(label);
            Tag text = new Tag("input");
            text.attribute("id", "display");
            text.attribute("type", "text");
            text.attribute("class", "form-control");
            text.attribute("style", "max-width: 250px");
            group.add(text);
        }
        {   //Email Input
            Tag group = new Tag("div", rcol);
            group.attribute("class", "form-group");
            group.attribute("id", "email_group");
            Tag label = new Tag("label");
            label.attribute("for", "email");
            label.attribute("class", "control-label");
            label.add("E-Mail");
            group.add(label);
            Tag text = new Tag("input");
            text.attribute("id", "email");
            text.attribute("type", "email");
            text.attribute("class", "form-control");
            text.attribute("style", "max-width: 250px");
            group.add(text);
        }
        Tag button = new Tag("a", jumbotron);
        if(enabled) {
            button.attribute("class", "btn btn-primary");
            button.attribute("onclick", "invoke('_xjs_register');");
        } else {
            button.attribute("class", "btn btn-default");
        }
        Tag icon = new Tag("span", button);
        icon.attribute("class", "glyphicon glyphicon-ok");
        button.add("Register");
        Tag info = new Tag("input", jumbotron);
        info.attribute("hidden", "hidden");
        info.attribute("id", "_xjs_module");
        info.attribute("value", tag.attribute("module"));
        info = new Tag("input", jumbotron);
        info.attribute("hidden", "hidden");
        info.attribute("id", "_xjs_forwarding_page");
        info.attribute("value", tag.attribute("forwarding-page"));
        info = new Tag("input", jumbotron);
        info.attribute("hidden", "hidden");
        info.attribute("id", "_xjs_auto_login");
        info.attribute("value", tag.attribute("auto-login"));
        return register;
    }

    @XAction(name = "_xjs_register")
    public void register(@XUpdate Updates updates, @XParam(name = "_xjs_module") String module, @XParam(name = "_xjs_forwarding_page") String forwardingPage,
                         @XParam(name = "_xjs_auto_login") String autoLogin, @XParam(name = "name") String name, @XParam(name = "pass") String pass,
                         @XParam(name = "pass_repeat") String passRepeat, @XParam(name = "firstname") String firstname, @XParam(name = "lastname") String lastname,
                         @XParam(name = "display") String display, @XParam(name = "email") String email) {
        if(module.equals("")) {
            updates.addMessage(Updates.Severity.DANGER, "Module not set!");
            return;
        }
        boolean correct = true;
        if(name.equals("")) {
            updates.highlightError("name");
            correct = false;
        } else {
            updates.removeError("name");
        }
        if(pass.equals("")) {
            updates.highlightError("pass");
            correct = false;
        } else {
            updates.removeError("pass");
        }
        if(passRepeat.equals("")) {
            updates.highlightError("pass_repeat");
            correct = false;
        } else {
            updates.removeError("pass_repeat");
        }
        if(firstname.equals("")) {
            updates.highlightError("firstname");
            correct = false;
        } else {
            updates.removeError("firstname");
        }
        if(lastname.equals("")) {
            updates.highlightError("lastname");
            correct = false;
        } else {
            updates.removeError("lastname");
        }
        if(display.equals("")) {
            updates.highlightError("display");
            correct = false;
        } else {
            updates.removeError("display");
        }
        if(email.equals("")) {
            updates.highlightError("email");
            correct = false;
        } else {
            updates.removeError("email");
        }
        if(!correct) {
            updates.addMessage(Updates.Severity.WARNING, "Not all fields are set!");
            return;
        }
        if(!pass.equals(passRepeat)) {
            updates.highlightError("pass");
            updates.highlightError("pass_repeat");
            updates.addMessage(Updates.Severity.WARNING, "Passwords do not match");
        }
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        if(logicalUnit.hasService(module)) {
            Service service = logicalUnit.getService(module);
            if(service instanceof RegistrationService) {
                try {
                    if(((RegistrationService) service).register(name, pass.getBytes("UTF-8"), firstname, lastname, display, email)) {
                        if(forwardingPage.equals("")) {
                            forwardingPage = "";
                        }
                        updates.changeLocation(forwardingPage);
                    } else {
                        updates.addMessage(Updates.Severity.WARNING, "Registration failed! Check if you entered everything correctly!");
                    }
                } catch (UnsupportedEncodingException e) {
                    Server.log.warn("Server does not support UTF-8", e);
                }
            }
        }

    }
}