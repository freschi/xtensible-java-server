package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:field")
@Package("xjs-tags")
@Version("0.1a")
public class Field implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag r;
        Tag field = new Tag("span");
        field.name("span");
        if(tag.attribute("bold") != null) {
            r = new Tag("strong");
            r.add(field);
        } else {
            r = field;
        }
        if(tag.attribute("id") != null) {
            field.attribute("id", tag.attribute("id"));
        }
        field.type(TagType.FULL);
        if(tag.attribute("field") != null && Session.get(tag.attribute("field")) != null) {
            field.add(Session.get(tag.attribute("field")));
        }
        return r;
    }
}