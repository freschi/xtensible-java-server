package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.lang.String;
import java.util.function.Predicate;

@TagReplacerInfo(tag = "xjs:tabs", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class Tabs implements TagReplacer {
    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag tabs = new Tag("div");
        Tag list = new Tag("ul");
        list.attribute("class", "nav nav-tabs");
        tabs.add(list);
        String partial = null;
        Replacer replacer = (Replacer) Session.get("_xjs_replacer");
        int index = 0;
        int max = (int) tag.children().stream().filter(new Predicate<Tag>() {
            @Override
            public boolean test(Tag t) {
                return t.name() != null && t.name().equals("xjs:tab") && t.attribute("partial") != null;
            }
        }).count();
        for(Tag item : tag.children()) {
            if(item == null || item.name() == null)
                continue;
            if(item.name().equals("xjs:tab") && item.attribute("partial") != null) {
                Tag itemCont = new Tag("li");
                itemCont.attribute("role", "presentation");
                if(partial == null) {
                    partial = item.attribute("partial");
                    itemCont.attribute("class", "active");
                }
                Tag link = new Tag("a");
                link.attribute("onclick", "invoke('_xjs_tab_change', {partial:'" + item.attribute("partial") + "', content: '" + tag.attribute("id") + "', index: " + index + ", max: " + max + "});");
                itemCont.attribute("id", tag.attribute("id") + "_" + index);
                index++;
                link.add(item.children());
                if(replacer != null) {
                    replacer.replace(link);
                }
                itemCont.add(link);
                list.add(itemCont);
            }
        }
        if(tag.attribute("id") != null) {
            Tag content = new Tag("div");
            content.attribute("class", "container");
            content.attribute("id", tag.attribute("id"));
            LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
            if(logicalUnit.hasService("xjs-parser-service")) {
                Service service = logicalUnit.getService("xjs-parser-service");
                content.add(service.put(partial));
            }
            tabs.add(content);
        }
        return tabs;
    }

    @XAction(name = "_xjs_tab_change")
    public void changeTab(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        String content, partial;
        int index, max;
        JsonObject object = jsonElement.getAsJsonObject();
        content = object.get("content").getAsString();
        partial = object.get("partial").getAsString();
        index = object.get("index").getAsInt();
        max = object.get("max").getAsInt();
        for (int i = 0; i < max; i++) {
            if(i == index) {
                updates.addClass(content + "_" + i, "active");
            } else {
                updates.removeClass(content + "_" + i, "active");
            }
        }
        updates.setPartial(content, partial);
    }
}