package at.femoweb.xjs.tags;

import at.femoweb.xjs.annotations.Depends;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.PackageRoot;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.dependencies.Dependencies;
import at.femoweb.xjs.dependencies.Dependency;

@Package("xjs-tags")
@PackageRoot
@Version("0.1a")
@Dependencies({
        @Dependency("xjs-auth-default")
})
public class PackageInfo {

}