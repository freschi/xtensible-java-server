package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:checkbox")
@Version("0.1a")
@Package("xjs-tags")
public class Checkbox implements TagReplacer {

    @Override
    public Tag replace(Tag tag) {
        Tag cb = new Tag("div");
        cb.attribute("class", "checkbox");
        Tag input = new Tag("input");
        input.attribute("type", "checkbox");
        Tag label = new Tag("label");
        if(tag.attribute("id") != null) {
            input.attribute("id", tag.attribute("id"));
            label.attribute("for", tag.attribute("id"));
        }
        if(tag.attribute("label") != null) {
            label.add(tag.attribute("label"));
        }
        if(tag.attribute("type") != null) {
            cb.attribute("class", cb.attribute("class") + " checkbox-" + tag.attribute("type"));
        }
        if(tag.attribute("circle") != null) {
            cb.attribute("class", cb.attribute("class") + " checkbox-circle");
        }
        if(tag.attribute("circle") != null) {
            input.attribute("disabled", "disabled");
        }
        if(tag.attribute("field") != null) {
            if(Session.is(tag.attribute("field"))) {
                String val = Session.get(tag.attribute("field")).toString();
                if(!val.equals("0")) {
                    input.attribute("checked", "checked");
                }
            }
        }
        cb.add(input);
        cb.add(label);
        return cb;
    }

    @Override
    public boolean replaces(String tagName) {
        return false;
    }
}