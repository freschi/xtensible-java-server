package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnauthorizedUserException;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import at.femoweb.xjs.services.LoginService;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.OnLoginHandler;
import com.google.gson.JsonElement;

import java.lang.Override;
import java.lang.String;
import java.util.List;

@TagReplacerInfo(tag = "xjs:login")
@Package("xjs-tags")
@Version("0.1a")
public class Login implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        if(Session.is("_xjs_user")) {
            Tag component = new Tag("div");
            component.attribute("class", "container");
            component.attribute("id", "_xjs_login_controlled");
            component.type(TagType.FULL);
            if(tag.attribute("logged-in") != null)
                component.add("Loading component " + tag.attribute("logged-in"));
            return component;
        } else {
            Tag login = new Tag("div");
            login.attribute("class", "jumbotron");
            //login.attribute("id", "_xjs_login_controlled");
            Tag row = new Tag("div");
            Tag column = new Tag("div");
            row.attribute("class", "row");
            column.attribute("class", "col-md-12 col-sm-12 col-xs-12");
            row.add(column);
            Tag heading = new Tag("h1");
            heading.add("Login");
            column.add(heading);
            login.add(row);
            boolean enabled = true;
            {
                row = new Tag("div");
                column = new Tag("div");
                row.attribute("class", "row");
                column.attribute("class", "col-md-12 col-sm-12 col-xs-12");
                row.add(column);
                LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
                if (logicalUnit == null) {
                    Tag alert = new Tag("div");
                    alert.attribute("class", "alert alert-danger");
                    alert.add("Session not compatible with Login Component. Please contact your webmaster");
                    column.add(alert);
                    enabled = false;
                } else {
                    Service service = logicalUnit.getService(tag.attribute("module"));
                    if (service == null) {
                        Tag alert = new Tag("div");
                        alert.attribute("class", "alert alert-danger");
                        alert.add("Authentication Module not loaded. Please contact your webmaster");
                        column.add(alert);
                        enabled = false;
                    }
                }
                if(!enabled)
                    login.add(row);
            }
            {   //Username Input
                Tag group = new Tag("div");
                group.attribute("class", "form-group");
                group.attribute("id", "name_control");
                Tag label = new Tag("label");
                label.attribute("for", "name");
                label.attribute("class", "control-label");
                label.add("Username");
                group.add(label);
                Tag text = new Tag("input");
                text.attribute("id", "name");
                text.attribute("type", "text");
                text.attribute("class", "form-control");
                text.attribute("style", "max-width: 250px");
                group.add(text);
                login.add(group);
            }
            {   //Password input
                Tag group = new Tag("div");
                group.attribute("class", "form-group");
                group.attribute("id", "pass_control");
                Tag label = new Tag("label");
                label.attribute("for", "pass");
                label.attribute("class", "control-label");
                label.add("Password");
                group.add(label);
                Tag text = new Tag("input");
                text.attribute("id", "pass");
                text.attribute("type", "password");
                text.attribute("class", "form-control");
                text.attribute("style", "max-width: 250px");
                text.attribute("onkeydown", "invoke('_xjs_login_input', {key: event.keyCode});");
                group.add(text);
                login.add(group);
            }
            Tag button = new Tag("a");
            if (enabled) {
                button.attribute("class", "btn btn-primary");
                button.attribute("onclick", "invoke('_xjs_login');");
            } else {
                button.attribute("class", "btn btn-default");
            }
            button.add("Login");
            row = new Tag("div");
            column = new Tag("div");
            row.attribute("class", "row");
            column.attribute("class", "col-md-12 col-sm-12 col-xs-12");
            row.add(column);
            if(tag.attribute("registerLink") != null) {
                Tag registerCont = new Tag("p");
                Tag register = new Tag("a", registerCont);
                register.attribute("style", "font-size: small");
                register.add("No account yet? Register!");
                register.attribute("href", tag.attribute("registerLink"));
                column.add(registerCont);
            }
            column.add(button);
            login.add(row);
            Tag p = new Tag("input");
            p.attribute("value", tag.attribute("module"));
            p.attribute("hidden", "hidden");
            p.attribute("id", "module");
            login.add(p);
            p = new Tag("input");
            p.attribute("value", tag.attribute("logged-in"));
            p.attribute("hidden", "hidden");
            p.attribute("id", "partial");
            login.add(p);
            Tag component = new Tag("div");
            component.attribute("id", "_xjs_login_controlled");
            component.add(login);
            component.attribute("class", "container");
            return component;
        }
    }

    @XAction(name = "_xjs_login")
    public void _xjs_login (@XUpdate Updates updates, @XParam(name = "name") String name, @XParam(name = "pass") String pass, @XParam(name = "module") String module, @XParam(name = "partial") String partial) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        LoginService loginService = (LoginService) logicalUnit.getService(module);
        if(loginService.checkCredentials(name, pass.getBytes())) {
            UnifiedUser user;
            try {
                user = loginService.getUserData(name, pass.getBytes());
            } catch (UnauthorizedUserException e) {
                Server.log.warn("Authentication Error", e);
                updates.addMessage(Updates.Severity.DANGER, e.getMessage());
                return;
            }
            Session.set("_xjs_user", user);
            Session.set("user.name", user.getUsername());
            Session.set("user.id", user.getProperty("user.id"));
            updates.addMessage(Updates.Severity.INFO, "This Server is still in alpha state");
            updates.addMessage(Updates.Severity.SUCCESS, "Login succesfull");
            if(!partial.equalsIgnoreCase("null"))
                updates.html("_xjs_login_controlled", "Loading component " + partial);
            else
                updates.html("_xjs_login_controlled", "");
            if(Session.is("_xjs_onlogin_list")) {
                List handlers = (List) Session.get("_xjs_onlogin_list");
                for(Object handler : handlers) {
                    if(handler instanceof OnLoginHandler)
                        ((OnLoginHandler)handler).replace(updates);
                }
            }
            if(user.getProperty("user.email") != null) {
                Session.set("user.email", user.getProperty("user.email"));
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Login failed");
            updates.addClass("pass_control", "has-error");
            updates.addClass("name_control", "has-error");
            updates.value("pass", "");
            updates.focus("name");
        }
    }

    @XAction(name = "_xjs_login_input")
    public void _xjs_login_input(@XUpdate Updates updates, @XParam(name = "name") String name, @XParam(name = "pass") String pass, @XParam(name = "module") String module, @XParam(name = "partial") String partial, @XOptional JsonElement jsonElement) {
        int key = jsonElement.getAsJsonObject().get("key").getAsInt();
        if(key == 13) {
            _xjs_login(updates, name, pass, module, partial);
        } else if (key == 8) {
            updates.value("pass", "");
        }
    }
}