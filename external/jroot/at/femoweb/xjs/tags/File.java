package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:file", type = TagReplacerInfo.ReplacementType.DROP_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class File implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag input = new Tag("input");
        input.attribute("type", "file");
        input.attribute("class", "filestyle");
        if(tag.attribute("id") != null) {
            input.attribute("id", tag.attribute("id"));
        }
        if(tag.attribute("type") != null) {
            input.attribute("data-buttonName", "btn-" + tag.attribute("type"));
        }
        if(tag.attribute("text") != null) {
            input.attribute("data-buttonText", tag.attribute("text"));
        }
        if(tag.attribute("icon") != null) {
            input.attribute("data-iconName", "glyphicon-" + tag.attribute("icon"));
        }
        if(tag.attribute("size") != null) {
            input.attribute("data-size", tag.attribute("size"));
        }
        if(tag.attribute("filetype") != null) {
            input.attribute("accept", tag.attribute("filetype"));
        }
        return input;
    }
}