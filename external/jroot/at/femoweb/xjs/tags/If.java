package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:if", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class If implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag component = new Tag("span");
        if(tag.attribute("check") != null) {
            Object checkObj = Session.get(tag.attribute("check"));
            Replacer replacer = (Replacer) Session.get("_xjs_replacer");
            if(checkObj instanceof ConditionCheck) {
                ConditionCheck check = (ConditionCheck) checkObj;
                if(check.check(null)) {
                    component.add(tag.children());
                    if(replacer != null) {
                        replacer.replace(component);
                    }
                }
            } else {
                Tag message = new Tag("strong");
                message.add("Check is no ConditionCheck");
                component.add(message);
            }
        } else {
            Tag message = new Tag("strong");
            message.add("If block has no check attribute defined");
            component.add(message);
        }
        return component;
    }
}