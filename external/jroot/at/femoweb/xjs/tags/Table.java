package at.femoweb.xjs.tags;

import at.femoweb.html.Selection;
import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.*;
import java.lang.Double;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@TagReplacerInfo(tag = "xjs:table", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class Table implements TagReplacer {

    @Override
    public Tag replace(Tag tag) {
        try {
            if (tag.attribute("type").equals("sql")) {
                return createSQLTable(tag);
            } else {
                Tag error = new Tag("strong");
                error.add("Table type could not be determined");
                return error;
            }
        } catch (Throwable t) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            t.printStackTrace(new PrintStream(byteArrayOutputStream));
            Tag error = new Tag("pre");
            error.add(new String(byteArrayOutputStream.toByteArray()));
            return error;
        }
    }

    private Tag createSQLTable(Tag tag) {
        Tag table = new Tag("table");
        table.attribute("class", "table table-striped table-bordered table-hover");
        Tag columns = null;
        Tag source = null;
        for(Tag t : tag.children()) {
            if(t.name().equals("xjs:columns")) {
                columns = t;
            } else if (t.name().equals("xjs:source")) {
                source = t;
            }
        }
        Tag hrow = new Tag("tr");
        for(Tag column : columns.children()) {
            if(column.name().equals("xjs:column")) {
                Tag title = column.select().name("xjs:title").first();
                Tag head = new Tag("th");
                head.add(title.children());
                hrow.add(head);
            }
        }
        table.add(hrow);
        String con = null;
        String query = null;
        for(Tag s : source.children()) {
            if(s.name().equals("sql:query")) {
                query = s.children().get(0).text();
            } else if (s.name().equals("sql:connection")) {
                con = s.children().get(0).text();
            }
        }
        Connection connection = (Connection) Session.get(con);
        if(connection != null) {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                if(source.select().name("sql:params").size() == 1) {
                    for(Tag param : source.select().name("sql:params").first().children()) {
                        String type = param.select().name("sql:type").first().text();
                        int index = Integer.parseInt(param.select().name("sql:index").first().text());
                        String key = param.select().name("sql:value").first().text();
                        switch (type) {
                            case "number": {
                                preparedStatement.setLong(index, Long.parseLong(Session.get(key).toString()));
                                break;
                            }
                            case "decimal": {
                                preparedStatement.setDouble(index, Double.parseDouble(Session.get(key).toString()));
                                break;
                            }
                            case "date":
                            case "string": {
                                preparedStatement.setString(index, Session.get(key).toString());
                                break;
                            }
                        }
                    }
                }
                ResultSet resultSet = preparedStatement.executeQuery();
                if(resultSet.first()) {
                    do {
                        Tag row = new Tag("tr");
                        for(Tag column : columns.children()) {
                            Tag data = column.select().name("xjs:data").first();
                            Selection format = column.select().name("xjs:format");
                            String value = null;
                            if(data != null) {
                                for (Tag d : data.children()) {
                                    if (d.name().equals("sql:column")) {
                                        value = resultSet.getString(d.text());
                                    }
                                }
                            }
                            row.add(formatCell(value, format, resultSet));
                        }
                        table.add(row);
                    } while (resultSet.next());
                }
            } catch (SQLException e) {
                Server.log.warn("Error while fetching data from database", e);
            }
        }
        return table;
    }

    private Tag formatCell(String value, Selection format, ResultSet resultSet) throws SQLException{
        if(format.size() > 0) {
            Tag f = format.first();
            if(f.children().size() == 1) {
                Tag form = f.get(0);
                if(form.name().equals("xjs:link")) {
                    String type = form.select().name("link:type").first().text();
                    Tag target = form.select().name("link:target").first();
                    String targ = null;
                    for(Tag t : target.children()) {
                        if(t.name().equals("sql:column")) {
                            targ = resultSet.getString(t.text());
                        }
                    }
                    Tag tag = new Tag("td");
                    Tag link = new Tag("a");
                    if(type.equals("mailto")) {
                        link.attribute("href", "mailto:" + targ);
                        link.add(value);
                    } else {
                        link.add(value);
                    }
                    tag.add(link);
                } else if (form.name().equals("xjs:crud")) {
                    Tag td = new Tag("td");
                    for(Tag t : form.children()) {
                        if (t.name().startsWith("crud:")) {
                            Tag btn = new Tag("a");
                            if (t.name().equals("crud:edit")) {
                                btn.attribute("class", "btn btn-info btn-xs");
                            } else if (t.name().equals("crud:delete")) {
                                btn.attribute("class", "btn btn-danger btn-xs");
                            } else if (t.name().equals("crud:view")) {
                                btn.attribute("class", "btn btn-success btn-xs");
                            } else if (t.name().equals("crud:share")) {
                                btn.attribute("class", "btn btn-warning btn-xs");
                            }
                            String icon = null;
                            String action = null;
                            String field = null;
                            String id = null;
                            boolean valid = true;
                            for (Tag s : t.children()) {
                                if (s.name().equals("crud:icon")) {
                                    icon = s.text();
                                } else if (s.name().equals("crud:action")) {
                                    action = s.text();
                                } else if (s.name().equals("crud:idField")) {
                                    field = s.text();
                                } else if (s.name().equals("crud:id")) {
                                    for (Tag s_ : s.children()) {
                                        if (s_.name().equals("sql:column")) {
                                            id = resultSet.getString(s_.text());
                                        }
                                    }
                                } else if (s.name().equals("crud:if")) {
                                    ConditionCheck check = (ConditionCheck) Session.get(s.text());
                                    if(!check.check(null)) {
                                        valid = false;
                                        break;
                                    }
                                }
                            }
                            if(!valid)
                                continue;
                            Tag i = new Tag("span");
                            i.attribute("class", "glyphicon glyphicon-" + icon);
                            btn.add(i);
                            btn.attribute("onclick", "invoke('" + action + "', {" + field + ": '" + id + "'});");
                            td.add(btn);
                        }
                    }
                    return td;
                } else if (form.name().equals("xjs:htmlEscape")) {
                    Tag td = new Tag("td");
                    td.add(StringEscapeUtils.escapeHtml4(value));
                        return td;
                } else if (form.name().equals("xjs:num")) {
                    Tag td = new Tag("td");
                    String type = null;
                    String formatString = null;
                    for(Tag t_ : form.children()) {
                        if(t_.name().equals("num:format")) {
                            formatString = t_.text();
                        } else if (t_.name().equals("num:type")) {
                            type = t_.text();
                        }
                    }
                    String v = value;
                    if(type.equals("integer")) {
                        v = String.format(formatString, Long.parseLong(value));
                    } else if (type.equals("decimal")) {
                        v = String.format(formatString, Double.parseDouble(value));
                    }
                    td.add(v);
                } else if (form.name().equals("xjs:bool")) {
                    String type = null;
                    List<Tag> tval = null;
                    List<Tag> fval = null;
                    for(Tag t_ : form.children()) {
                        if(t_.name().equals("bool:type")) {
                            type = t_.text();
                        } else if (t_.name().equals("bool:true")) {
                            tval = t_.children();
                        } else if (t_.name().equals("bool:false")) {
                            fval = t_.children();
                        }
                    }
                    Replacer replacer = (Replacer) Session.get("_xjs_replacer");
                    Tag td = new Tag("td");
                    if(type.equals("num")) {
                        Long l = Long.parseLong(value);
                        Tag con = new Tag("span");
                        if(l == 0) {
                            con.add(fval);
                        } else {
                            con.add(tval);
                        }
                        replacer.replace(con);
                        td.add(con);
                    }
                    return td;
                } else if (form.name().equals("xjs:emptyIfNull")) {
                    if(value == null) {
                        Tag td = new Tag("td");
                        return td;
                    }
                }
            }
            Tag nf = new Tag("td");
            nf.add(value);
            return nf;
        } else {
            Tag tag = new Tag("td");
            tag.add(value);
            return tag;
        }
    }

    @Override
    public boolean replaces(String tagName) {
        return false;
    }
}