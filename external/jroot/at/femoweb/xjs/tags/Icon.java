package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:icon")
@Package("xjs-tags")
@Version("0.1a")
public class Icon implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag icon = new Tag("span");
        icon.type(TagType.FULL);
        if(tag.attribute("icon") != null) {
            icon.attribute("class", "glyphicon glyphicon-" + tag.attribute("icon"));
        }
        return icon;
    }
}