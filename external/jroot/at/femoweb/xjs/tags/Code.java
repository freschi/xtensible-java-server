package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagContent;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Object;
import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:code")
@Package("xjs-tags")
@Version("0.1a")
public class Code implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        if(tag.attribute("id") != null) {
            Object content = Session.get(tag.attribute("id"));
            if (content != null && content instanceof TagContent) {
                return ((TagContent) content).generateTags();
            } else {
                Tag strong = new Tag("strong");
                strong.add(tag.attribute("id") + " is no TagContent provider");
                return strong;
            }
        } else {
            Tag strong = new Tag("strong");
            strong.add("No id set for code element");
            return strong;
        }
    }
}