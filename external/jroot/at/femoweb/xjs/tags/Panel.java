package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:panel", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class Panel implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag panel = new Tag("div");
        if(tag.attribute("type") != null) {
            panel.attribute("class", "panel panel-" + tag.attribute("type"));
        } else {
            panel.attribute("class", "panel panel-" + tag.attribute("primary"));
        }
        Tag heading = new Tag("div");
        heading.attribute("class", "panel-heading");
        Tag body = new Tag("div");
        body.attribute("class", "panel-body");
        Tag footer = new Tag("div");
        footer.attribute("class", "panel-footer");
        Replacer replacer = (Replacer) Session.get("_xjs_replacer");
        for(Tag child : tag.children()) {
            if(child.name() != null) {
                if(child.name().equals("xjs:head")) {
                    heading.add(child.children());
                } else if (child.name().equals("xjs:footer")) {
                    footer.add(child.children());
                } else if (child.name().equals("xjs:body")) {
                    body.add(child.children());
                } else {
                    body.add(child);
                }
            }
        }
        if(heading.children().size() != 0) {
            replacer.replace(heading);
            panel.add(heading);
        }
        if(body.children().size() != 0) {
            replacer.replace(body);
            panel.add(body);
        }
        if(footer.children().size() != 0) {
            replacer.replace(footer);
            panel.add(footer);
        }
        return panel;
    }
}