package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:container")
@Version("0.1a")
@Package("xjs-tags")
public class Container implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag r = new Tag("div");
        r.type(TagType.FULL);
        if(tag.attribute("class") != null) {
            r.attribute("class", "container " + tag.attribute("class"));
        } else {
            r.attribute("class", "container");
        }
        if(tag.attribute("id") != null)
            r.attribute("id", tag.attribute("id"));
        return r;
    }
}