package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:alert")
@Package("xjs-tags")
@Version("0.1a")
public class Alert implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag div = new Tag("div");
        if(tag.attribute("type") != null) {
            div.attribute("class", "alert alert-" + tag.attribute("type"));
        } else {
            div.attribute("class", "alert alert-warning");
        }
        if(tag.attribute("style") != null) {
            div.attribute("style", tag.attribute("style"));
        }
        return div;
    }
}