package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ListProvider;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:comboBox", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
@Package("xjs-tags")
@Version("0.1a")
public class ComboBox implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag div = new Tag("div");
        Tag group = new Tag("div");
        group.attribute("class", "btn-group");
        Tag btn = new Tag("a");
        btn.attribute("class", "btn dropdown-toggle btn-default");
        btn.attribute("style", "min-width: 250px;");
        btn.attribute("data-toggle", "dropdown");
        String set_value = null;
        if(tag.attribute("value") != null || tag.attribute("field") != null) {
            if (tag.attribute("field") != null && Session.is(tag.attribute("field")) && Session.get(tag.attribute("field")) != null) {
                set_value = Session.get(tag.attribute("field")).toString();
            } else if(tag.attribute("value") != null) {
                set_value = tag.attribute("value");
            } else {
                if (tag.attribute("label") != null) {
                    btn.add(tag.attribute("label"));
                } else {
                    btn.add("Please choose ...");
                }
            }
        } else {
            if (tag.attribute("label") != null) {
                btn.add(tag.attribute("label"));
            } else {
                btn.add("Please choose ...");
            }
        }
        if(tag.attribute("id") != null) {
            btn.attribute("id", tag.attribute("id") + "_btn");
        }
        Tag list = new Tag("ul");
        list.attribute("class", "dropdown-menu");
        Replacer replacer = (Replacer) Session.get("_xjs_replacer");
        boolean found = false;
        if(tag.attribute("list") != null) {
            Object listProvider = Session.get(tag.attribute("list"));
            if(listProvider != null && listProvider instanceof ListProvider) {
                ListProvider provider = (ListProvider) listProvider;
                for(ListProvider.ListItem listItem : provider.createList()) {
                    list.add(createItem(tag.attribute("id"), listItem.getValue(), listItem.getTag(), replacer));
                    if(set_value != null && listItem.getValue().equals(set_value)) {
                        found = true;
                        btn.add(listItem.getTag());
                        if(replacer != null) {
                            replacer.replace(btn);
                        }
                    }
                }
            } else {
                btn.attribute("class", btn.attribute("class") + " disabled");
            }
        } else {
            int value = 0;
            for(Tag child : tag.children()) {
                if(child == null || child.name() == null)
                    continue;
                if(child.name().equalsIgnoreCase("xjs:item")) {
                    list.add(createItem(tag.attribute("id"), value + "", child, replacer));
                    if(set_value != null && Integer.parseInt(set_value) == value) {
                        btn.add(child.children());
                        found = true;
                        if(replacer != null) {
                            replacer.replace(btn);
                        }
                    }
                    value++;
                }
            }
        }
        Tag caret = new Tag("span");
        caret.attribute("class", "caret");
        caret.type(TagType.FULL);
        btn.add(caret);
        group.add(btn);
        group.add(list);
        div.add(group);
        if(tag.attribute("id") != null) {
            Tag value = new Tag("input");
            value.attribute("type", "hidden");
            value.attribute("id", tag.attribute("id"));
            if(found) {
                value.attribute("value", set_value);
            }
            div.add(value);
        }
        return div;
    }
    
    private Tag createItem(String id, String value, Tag child, Replacer replacer) {
        Tag item = new Tag("li");
        Tag link = new Tag("a");
        if(child == null)
            link.add("null");
        else
            link.add(child.children());
        if(id != null) {
            link.attribute("onclick", "_xjs_update_combobox('" + id + "', '" + value + "');");
            link.attribute("id", id + "_" + value);
        }
        item.add(link);
        if(replacer != null) {
            replacer.replace(link);
        }
        return item;
    }
}