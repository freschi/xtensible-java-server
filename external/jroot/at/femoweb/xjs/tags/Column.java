package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:column")
@Package("xjs-tags")
@Version("0.1a")
public class Column implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag r = new Tag("div");
        r.type(TagType.FULL);
        String style = "";
        if(tag.attribute("size-md") != null) {
            style += "col-md-" + tag.attribute("size-md");
        }
        style += " ";
        if(tag.attribute("size-sm") != null) {
            style += "col-sm-" + tag.attribute("size-sm");
        }
        style += " ";
        if(tag.attribute("size-xs") != null) {
            style += "col-xs-" + tag.attribute("size-xs");
        }
        r.attribute("class", style);
        if(tag.attribute("style") != null) {
            r.attribute("style", tag.attribute("style"));
        }
        return r;
    }
}