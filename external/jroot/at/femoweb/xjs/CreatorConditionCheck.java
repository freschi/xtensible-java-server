package at.femoweb.xjs;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;

import java.lang.Override;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-creator-check")
@Package("femoweb_pot")
@Version("0.1")
public class CreatorConditionCheck implements ConditionCheck {

    @Override
    public boolean check(String[] args) {
        XJSUser user = (XJSUser) Session.get("_xjs_user");
        String userId = user.read("user.id");
        String project = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select w_info_perm from w_works where w_u_user = " + userId + " and w_p_project = " + project);
                return resultSet.first() && resultSet.getInt(1) == 3;
            } catch (SQLException e) {
                Server.log.warn("Could not perform permission check", e);
                return false;
            }
        } else {
            return false;
        }
    }

}