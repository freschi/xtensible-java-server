package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.TagContent;
import com.google.gson.JsonElement;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("people-list")
public class ProjectMemberList implements TagContent {

    @Override
    public Tag generateTags() {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                String project = Session.get("curr-project").toString();
                ResultSet resultSet = statement.executeQuery("select w_info_perm, u_display, u_email, idu_user from w_works inner join u_user on w_u_user = idu_user where w_p_project = " + project + " order by w_info_perm desc, u_lastname, u_firstname");
                if(resultSet.first()) {
                    Tag table = new Tag("table");
                    table.attribute("class", "table table-bordered table-striped table-hover");
                    Tag hrow = new Tag("tr");
                    Tag hname = new Tag("th");
                    hname.add("Name");
                    hrow.add(hname);
                    Tag hmail = new Tag("th");
                    hmail.add("E-Mail");
                    hrow.add(hmail);
                    Tag hedit = new Tag("th");
                    hedit.add("Change");
                    hrow.add(hedit);
                    table.add(hrow);
                    do {
                        Tag row = new Tag("tr");
                        Tag name = new Tag("td");
                        String icon_name = "";
                        switch (resultSet.getInt(1)) {
                            case 0: icon_name = "eye-close"; break;
                            case 1: icon_name = "eye-open"; break;
                            case 2: icon_name = "user"; break;
                            case 3: icon_name = "star"; break;
                        }
                        Tag span = new Tag("span");
                        Tag icon = new Tag("span");
                        icon.attribute("class", "glyphicon glyphicon-" + icon_name);
                        span.add(icon);
                        span.add(resultSet.getString(2));
                        name.add(span);
                        row.add(name);
                        Tag linkCont = new Tag("td");
                        Tag link_ = new Tag("a");
                        link_.add(resultSet.getString(3));
                        link_.attribute("href", "mailto:" + resultSet.getString(3));
                        linkCont.add(link_);
                        row.add(linkCont);
                        String uid = resultSet.getString(4);
                        Tag comp = new Tag("span");
                        {
                            Tag link = new Tag("a");
                            link.attribute("class", "btn btn-warning btn-xs");
                            link.attribute("onclick", "invoke('pot_people_change', {id: 3, person: '" + uid + "'});");
                            Tag c_icon = new Tag("span");
                            c_icon.attribute("class", "glyphicon glyphicon-star");
                            link.add(c_icon);
                            comp.add(link);
                        }
                        {
                            Tag link = new Tag("a");
                            link.attribute("class", "btn btn-primary btn-xs");
                            link.attribute("onclick", "invoke('pot_people_change', {id: 2, person: '" + uid + "'});");
                            Tag c_icon = new Tag("span");
                            c_icon.attribute("class", "glyphicon glyphicon-user");
                            link.add(c_icon);
                            comp.add(link);
                        }
                        {
                            Tag link = new Tag("a");
                            link.attribute("class", "btn btn-success btn-xs");
                            link.attribute("onclick", "invoke('pot_people_change', {id: 1, person: '" + uid + "'});");
                            Tag c_icon = new Tag("span");
                            c_icon.attribute("class", "glyphicon glyphicon-eye-open");
                            link.add(c_icon);
                            comp.add(link);
                        }
                        {
                            Tag link = new Tag("a");
                            link.attribute("class", "btn btn-info btn-xs");
                            link.attribute("onclick", "invoke('pot_people_change', {id: 0, person: '" + uid + "'});");
                            Tag c_icon = new Tag("span");
                            c_icon.attribute("class", "glyphicon glyphicon-eye-close");
                            link.add(c_icon);
                            comp.add(link);
                        }
                        {
                            Tag link = new Tag("a");
                            link.attribute("class", "btn btn-danger btn-xs");
                            link.attribute("onclick", "invoke('pot_people_change', {id: 100, person: '" + uid + "'});");
                            Tag c_icon = new Tag("span");
                            c_icon.attribute("class", "glyphicon glyphicon-trash");
                            link.add(c_icon);
                            comp.add(link);
                        }
                        Tag item = new Tag("td");
                        item.add(comp);
                        row.add(item);
                        table.add(row);
                    } while (resultSet.next());
                    return table;
                }
            } catch (SQLException e) {
                Server.log.warn("Not able to create contributors list", e);
            }
        }
        Tag message = new Tag("strong");
        message.add("Could not create people list");
        return message;
    }
    
    @XAction(name = "pot_people_change")
    public void changePerson(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        int id = jsonElement.getAsJsonObject().get("id").getAsInt();
        String uid = jsonElement.getAsJsonObject().get("person").getAsString();
        String project = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                if(id < 100) {
                    statement.execute("UPDATE w_works SET w_info_perm = " + id + " WHERE w_p_project = " + project + " and w_u_user = " + uid);
                } else {
                    statement.execute("DELETE FROM w_works WHERE w_p_project = " + project + " and w_u_user = " + uid);
                }
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not change permission of people", e);
            }
        }
        updates.setPartial("femoweb_pot_main", "project/add/people.xjs");
    }
}