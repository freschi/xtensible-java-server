package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.TagContent;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.print.DocFlavor;
import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-diary-entries-provider")
public class DiaryEntriesProvider implements TagContent {

    @Override
    public Tag generateTags() {
        Tag table = new Tag("table");
        table.attribute("class", "table table-striped table-bordered table-hover");
        Connection connection = (Connection) Session.get("connection");
        String project = Session.get("curr-project").toString();
        String uid = ((UnifiedUser)Session.get("_xjs_user")).getProperty("user.id");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select e_date, e_hours, e_descr, t_name, ide_entries from e_entries inner join t_types on e_t_type = idt_types where e_p_project = " + project + " and e_u_user = " + uid + " order by e_date desc");
                if(resultSet.first()) {
                    Tag hrow = new Tag("tr");
                    Tag hdate = new Tag("th");
                    hdate.add("Date");
                    hrow.add(hdate);
                    Tag hhours = new Tag("th");
                    hhours.add("Hours");
                    hrow.add(hhours);
                    Tag hdescr = new Tag("th");
                    hdescr.add("Description");
                    hrow.add(hdescr);
                    Tag htname = new Tag("th");
                    htname.add("Type");
                    hrow.add(htname);
                    Tag hedit = new Tag("th");
                    hedit.add("Change");
                    hrow.add(hedit);
                    table.add(hrow);
                    do {
                        Tag row = new Tag("tr");
                        Tag date = new Tag("td");
                        date.add(resultSet.getString(1));
                        row.add(date);
                        Tag hours = new Tag("td");
                        hours.add(resultSet.getString(2));
                        row.add(hours);
                        Tag descr = new Tag("td");
                        descr.add(StringEscapeUtils.escapeHtml4(resultSet.getString(3)));
                        row.add(descr);
                        Tag tname = new Tag("td");
                        tname.add(resultSet.getString(4));
                        row.add(tname);
                        Tag edit = new Tag("td");
                        {
                            Tag link = new Tag("a");
                            link.attribute("class", "btn btn-info btn-xs");
                            link.attribute("onclick", "invoke('pot_alter_diary', {id: " + resultSet.getInt(5) + ", action: 'edit'});");
                            Tag icon = new Tag("span");
                            icon.attribute("class", "glyphicon glyphicon-edit");
                            link.add(icon);
                            edit.add(link);
                        }
                        {
                            Tag link = new Tag("a");
                            link.attribute("class", "btn btn-danger btn-xs");
                            link.attribute("onclick", "invoke('pot_alter_diary', {id: " + resultSet.getInt(5) + ", action: 'delete'});");
                            Tag icon = new Tag("span");
                            icon.attribute("class", "glyphicon glyphicon-trash");
                            link.add(icon);
                            edit.add(link);
                        }
                        row.add(edit);
                        table.add(row);
                    } while (resultSet.next());
                }
            } catch (SQLException e) {
                Server.log.warn("Could not write entry list", e);
            }
        }
        return table; 
    }
    
    @XAction(name = "pot_alter_diary")
    public void alterDiary(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        JsonObject object = jsonElement != null ? jsonElement.getAsJsonObject() : null;
        if(object != null && object.has("id")) {
            String id = object.get("id").getAsString();
            Connection connection = (Connection) Session.get("connection");
            if(connection != null) {
                try {
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery("SELECT e_date, e_hours, e_descr, e_t_type, e_inschool, e_ta_task FROM e_entries WHERE ide_entries = " + id);
                    if(resultSet.first()) {
                        Session.set("pot.curr.entry.id", id);
                        Session.set("pot.curr.entry.date", resultSet.getString(1));
                        Session.set("pot.curr.entry.hours", resultSet.getString(2));
                        Session.set("pot.curr.entry.descr", resultSet.getString(3));
                        Session.set("pot.curr.entry.type", resultSet.getString(4));
                        Session.set("pot.curr.entry.inschool", resultSet.getString(5));
                        String val = resultSet.getString(6);
                        Session.set("pot.curr.entry.task", val);
                    }
                } catch (SQLException e) {
                    Server.log.warn("Could not prepare entry alter procedure", e);
                    updates.addMessage(Updates.Severity.WARNING, "Internal Server Error. Try reloading the server!");
                    return;
                }
            }
        }
        if(object == null || object.has("action") && !object.get("action").getAsString().equals("delete")) {
            updates.setPartial("pot_diary_content", "project/diary/entry.xjs");
        } else {
            updates.setPartial("pot_diary_content", "project/diary/delete.xjs");
        }
    }

    @XAction(name = "pot_alter_diary_edit")
    public void editDiary(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        jsonElement.getAsJsonObject().addProperty("action", "edit");
        alterDiary(updates, jsonElement);
    }

    @XAction(name = "pot_alter_diary_delete")
    public void deleteDiary(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        jsonElement.getAsJsonObject().addProperty("action", "delete");
        alterDiary(updates, jsonElement);
    }
    
    @XAction(name = "pot_return_diary")
    public void returnToDiary(@XUpdate Updates updates) {
        Session.remove("pot.curr.entry.id");
        Session.remove("pot.curr.entry.date");
        Session.remove("pot.curr.entry.hours");
        Session.remove("pot.curr.entry.descr");
        Session.remove("pot.curr.entry.type");
        Session.remove("pot.curr.entry.inschool");
        Session.remove("pot.curr.entry.task");
        updates.setPartial("pot_diary_content", "project/diary/table.xjs");
    }
    
    @XAction(name = "pot_diary_delete")
    public void deleteEntry(@XUpdate Updates updates) {
        if(Session.is("pot.curr.entry.id")) {
            String id = Session.get("pot.curr.entry.id").toString();
            Connection connection = (Connection) Session.get("connection");
            if(connection != null) {
                try {
                    Statement statement = connection.createStatement();
                    statement.execute("DELETE FROM e_entries WHERE ide_entries = " + id);
                    statement.close();
                    returnToDiary(updates);
                } catch (SQLException e) {
                    Server.log.warn("Could not remove entry", e);
                }
            }
        }
    }
    
    @XAction(name = "pot_entry_commit")
    public void entryCommit(@XUpdate Updates updates, @XParam(name = "pot_diary_date") String date, @XParam(name = "pot_diary_hours") String hours,
                            @XParam(name = "pot_diary_descr") String descr, @XParam(name = "pot_diary_type") String type, @XParam(name = "pot_diary_lesson") String lesson,
                            @XParam(name = "pot_diary_task") String task) {
        boolean alright = true;
        if(date == null || date.equals("")) {
            updates.highlightError("pot_diary_date");
            alright = false;
        } else {
            updates.removeError("pot_diary_date");
        }
        if(hours == null || hours.equals("")) {
            updates.highlightError("pot_diary_hours");
            alright = false;
        } else {
            updates.removeError("pot_diary_hours");
        }
        if(descr == null || descr.equals("")) {
            updates.highlightError("pot_diary_descr");
            alright = false;
        } else {
            updates.removeError("pot_diary_descr");
        }
        if(type == null || type.equals("")) {
            updates.highlightError("pot_diary_type");
            alright = false;
        } else {
            updates.removeError("pot_diary_type");
        }
        if(alright) {
            if(Session.is("pot.curr.entry.id")) {
                String query = "UPDATE e_entries SET e_date = '" + date + "', e_hours = " + hours + ", e_descr = '" + descr + "', e_t_type = " + type + ", e_inschool = " + (Boolean.parseBoolean(lesson) ? 1 : 0) +
                        ", e_ta_task = " + task + " WHERE ide_entries = " + Session.get("pot.curr.entry.id").toString();
                Connection connection = (Connection) Session.get("connection");
                if (connection != null) {
                    try {
                        Statement statement = connection.createStatement();
                        statement.execute(query);
                        statement.close();
                        returnToDiary(updates);
                    } catch (SQLException e) {
                        Server.log.warn("Could not update entry", e);
                        updates.addMessage(Updates.Severity.DANGER, "Internal Server Error. Could not alter entry!");
                    }
                }
            } else {
                String query = "INSERT INTO e_entries (e_u_user, e_p_project, e_date, e_hours, e_descr, e_t_type, e_inschool, e_ta_task) VALUES (";
                UnifiedUser user = (UnifiedUser) Session.get("_xjs_user");
                String uid = user.getProperty("user.id");
                String pid = Session.get("curr-project").toString();
                query += uid + ", " + pid + ", '" + date + "', " + hours + ", '" + descr + "', " + type + ", " + (Boolean.parseBoolean(lesson) ? 1 : 0) + ", "  + task + ");";
                Connection connection = (Connection) Session.get("connection");
                if (connection != null) {
                    try {
                        Statement statement = connection.createStatement();
                        statement.execute(query);
                        statement.close();
                        returnToDiary(updates);
                    } catch (SQLException e) {
                        Server.log.warn("Could not insert entry", e);
                        updates.addMessage(Updates.Severity.DANGER, "Internal Server Error. Could not add entry!");
                    }
                }
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Not all information was set!");
        }
    }
}