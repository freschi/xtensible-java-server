package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.Version;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xjs.Replacer;

import java.lang.Override;
import java.util.ArrayList;

@Package("xjs-auth-default")
@Version("0.1a")
public class DefaultReplaceHandler implements OnLoginHandler {

    private ArrayList<Tag> children;
    private String id;

    public DefaultReplaceHandler (ArrayList<Tag> children, String id) {
        this.children = children;
        this.id = id;
    }

    @Override
    public void replace(Updates updates) {
        Tag tag = new Tag("div");
        tag.add(children);
        Replacer replacer = (Replacer) Session.get("_xjs_replacer");
        if(replacer != null)
            replacer.replace(tag);
        String html = tag.render();
        updates.html(id, html);
    }
}