package at.femoweb.xjs;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnauthorizedUserException;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.services.DataConnectionService;
import at.femoweb.xjs.services.LoginService;
import at.femoweb.xjs.services.RegistrationService;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;

import java.io.UnsupportedEncodingException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;

@Service(type = ServiceType.OTHER)
@Named("femoweb-user")
public class XJSAuth extends Module implements LoginService, RegistrationService {

    @Override
    public boolean checkCredentials(String username, byte[] password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            String pwd = Base64.getEncoder().encodeToString(digest.digest(password));
            Connection connection;
            if (Session.get("connection") == null) {
                try {
                    LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
                    DataConnectionService data = (DataConnectionService) logicalUnit.getService("pot-connection-service");
                    connection = data.connectDataSource(null);
                    Session.set("connection", connection);
                } catch (SQLException e) {
                    Server.log.fatal("Could not connect to database for authentication", e);
                    return false;
                }
            } else {
                connection = (Connection) Session.get("connection");
            }
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select count(idl_login) from l_login where l_pass = '" + pwd + "' and l_uname = '" + username + "'");
                if (resultSet.first() && resultSet.getInt(1) == 1) {
                    return true;
                } else {
                    return false;
                }
            } catch (SQLException e) {
                Server.log.error("Could not create statement", e);
                return false;
            }
        } catch (NoSuchAlgorithmException e) {
            Server.log.warn("Error while hashing password", e);
            return false;
        }
    }

    @Override
    public UnifiedUser getUserData(String username, byte[] password) {
        XJSUser user = new XJSUser();
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select u_display, u_lastname, u_firstname, u_email, idu_user from l_login inner join u_user on l_u_user = idu_user where l_uname = '" + username + "';");
                if(resultSet.first()) {
                    user.setUsername(resultSet.getString(1));
                    user.setProperty("user.name", user.getUsername());
                    user.setProperty("user.lastname", resultSet.getString(2));
                    user.setProperty("user.firstname", resultSet.getString(3));
                    user.setProperty("user.email", resultSet.getString(4));
                    user.setProperty("user.id", resultSet.getString(5));
                } else {
                    throw new UnauthorizedUserException(UnauthorizedUserException.Reason.AUTHENTICATION_FAILURE);
                }
                user.setLogin(username);
            } catch (SQLException e) {
                Server.log.warn("Could not log in user", e);
            }
        }
        return user;
    }

    @Override
    public boolean register(String username, byte[] password, String firstname, String lastname, String display, String email) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        DataConnectionService data = (DataConnectionService) logicalUnit.getService("pot-connection-service");
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            String pwd = Base64.getEncoder().encodeToString(digest.digest(password));
            Connection connection = data.connectDataSource(null);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT count(idl_login) FROM l_login WHERE l_uname = '" + username + "'");
            if(resultSet.first() && resultSet.getInt(1) == 0) {
                statement.execute("INSERT INTO u_user (u_firstname, u_lastname, u_display, u_email) VALUES ('" + firstname + "', '" + lastname + "', '" + display + "', '" + email + "')");
                resultSet = statement.executeQuery("SELECT MAX(idu_user) FROM u_user");
                if(resultSet.first()) {
                    int id = resultSet.getInt(1);
                    statement.execute("INSERT INTO l_login (l_uname, l_pass, l_u_user) VALUES ('" + username + "', '" + pwd + "', " + id + ")");
                    return true;
                }
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            Server.log.warn("Could not connect to SQL Server", e);
        } catch (NoSuchAlgorithmException e) {
            Server.log.warn("Server can't md5", e);
        }
        return false;
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }
}