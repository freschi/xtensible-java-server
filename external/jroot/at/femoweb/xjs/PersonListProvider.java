package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ListProvider;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Resource("pot-person-provider")
public class PersonListProvider implements ListProvider {

    @Override
    public List<ListItem> createList() {
        ArrayList<ListItem> list = new ArrayList<>();
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT idu_user, u_display FROM u_user ORDER BY u_lastname, u_firstname");
                if(resultSet.first()) {
                    do {
                        ListItem listItem = new ListItem();
                        listItem.setValue(resultSet.getString(1));
                        Tag tag = new Tag("span");
                        tag.add(resultSet.getString(2));
                        listItem.setTag(tag);
                        list.add(listItem);
                    } while (resultSet.next());
                }
            } catch (SQLException e) {
                Server.log.warn("Could not load user list", e);
            }
        }
        return list;
    }
}