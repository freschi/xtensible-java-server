package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagContent;
import at.femoweb.xjs.util.ResultCallback;
import at.femoweb.xjs.util.SQL;
import at.femoweb.xjs.util.ValueProvider;

import java.lang.Integer;
import java.lang.Override;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-project-list")
public class ProjectList implements TagContent {

    @Override
    public Tag generateTags() {
        final Tag table = new Tag("table");
        table.attribute("class", "table table-striped table-bordered table-hover");
        Connection connection = (Connection) Session.get("connection");
        Tag hrow = new Tag("tr");
        Tag hlink = new Tag("th");
        hlink.add("Project Short");
        hrow.add(hlink);
        Tag hname = new Tag("th");
        hname.add("Project Name");
        hrow.add(hname);
        table.add(hrow);
        final Tag[] ret = new Tag[1];
        try {
            Statement statement = connection.createStatement();
            SQL.query(connection, "select idp_projects, p_name, p_short from (select distinct idp_projects, p_name, p_short from (select * from e_entries where e_u_user = ?) as a inner join p_projects on idp_projects = a.e_p_project inner join (select max(e_date) date, e_p_project from e_entries group by e_p_project) as b on a.e_p_project = b.e_p_project where a.e_date = b.date order by b.date desc) as a UNION (SELECT idp_projects, p_name, p_short FROM p_projects inner join w_works on w_p_project = idp_projects where w_u_user = ? order by p_short)")
                    .values(new ValueProvider() {
                        @Override
                        public boolean next() {
                            return false;
                        }

                        @Override
                        public void putData(PreparedStatement preparedStatement) throws SQLException {
                            preparedStatement.setInt(1, Integer.parseInt(Session.getUser().getProperty("user.id")));
                            preparedStatement.setInt(2, Integer.parseInt(Session.getUser().getProperty("user.id")));
                        }
                    }).execute(new ResultCallback() {
                @Override
                public void result(ResultSet resultSet) throws SQLException {
                    if(resultSet.first()) {
                        do {
                            Tag row = new Tag("tr");
                            Tag linkItem = new Tag("td");
                            Tag link = new Tag("a");
                            link.add(resultSet.getString("p_short"));
                            link.attribute("onclick", "invoke('pot_femoweb_project_show', {project: '" + resultSet.getInt("idp_projects") + "'});");
                            linkItem.add(link);
                            row.add(linkItem);
                            Tag name = new Tag("td");
                            name.add(resultSet.getString("p_name"));
                            row.add(name);
                            table.add(row);
                        } while (resultSet.next());
                        ret[0] = table;
                    } else {
                        Tag message = new Tag("strong");
                        message.add("You have no projects yet.");
                        ret[0] = message;
                    }
                }
            });
        } catch (SQLException e) {
            Server.log.warn("Could not fetch project list from database", e);
            Tag error = new Tag("strong");
            error.add("Error while fetching data from database");
            ret[0] = error;
        }
        return ret[0];
    }

}