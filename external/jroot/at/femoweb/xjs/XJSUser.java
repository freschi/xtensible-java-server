package at.femoweb.xjs;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.modules.Push;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.structure.SessionedThread;

import java.lang.*;
import java.lang.Exception;
import java.lang.InterruptedException;
import java.lang.Override;
import java.lang.String;
import java.lang.Thread;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.HashMap;

public class XJSUser extends UnifiedUser {

    public HashMap<String, String> values;

    @Override
    protected void store(String name, String value) {
        values.put(name, value);
        if(name.equals("user.name")) {
            updateDB(getProperty("user.id"), "u_display", value);
        } else if (name.equals("user.email")) {
            updateDB(getProperty("user.id"), "u_email", value);
        }
    }

    private void updateDB(String id, String key, String value) {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                statement.execute("UPDATE u_user SET " + key + " = '" + value + "' WHERE idu_user = " + id);
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not update database", e);
            }
        }
    }

    @Override
    protected void remove(String name) {
        values.remove(name);
    }

    @Override
    protected String read(String value) {
        return values.get(value);
    }

    @Override
    protected String[] getKeys() {
        return values.keySet().toArray(new String[]{});
    }

    @Override
    protected void connect() {
        values = new HashMap<>();
    }

    @Override
    protected void lock() {

    }

    @Override
    protected void unlock() {

    }

    @XAction(name = "pot_account_name_update")
    public void updateDisplay(@XUpdate Updates updates, @XParam(name = "pot_account_display") String display) {
        if(display.equals("")) {
            updates.highlightError("pot_account_display");
            updates.addMessage(Updates.Severity.DANGER, "Display cannot be empty!");
        }
        ((UnifiedUser)Session.get("_xjs_user")).setProperty("user.name", display);
        updates.addMessage(Updates.Severity.SUCCESS, "Updated your display name to " + display);
    }

    @XAction(name = "pot_account_email_update")
    public void updateEmail(@XUpdate Updates updates, @XParam(name = "pot_account_email") String email) {
        if(email.equals("")) {
            updates.highlightError("pot_account_email");
            updates.addMessage(Updates.Severity.DANGER, "E-Mail cannot be empty!");
        }
        ((UnifiedUser)Session.get("_xjs_user")).setProperty("user.email", email);
        updates.addMessage(Updates.Severity.SUCCESS, "Updated your email to " + email);
    }

    @XAction(name = "pot_account_password_change")
    public void changePassword(@XUpdate Updates updates, @XParam(name = "pot_account_pass_curr") String current, @XParam(name = "pot_account_pass_new") String pwd, @XParam(name = "pot_account_pass_repeat") String repeat) {
        if(current.equals("")) {
            updates.highlightError("pot_account_pass_curr");
            updates.addMessage(Updates.Severity.WARNING, "Your current password is empty");
            updates.value("pot_account_pass_new", "");
            updates.value("pot_account_pass_repeat", "");
            updates.value("pot_account_pass_curr", "");
            updates.focus("pot_account_pass_curr");
            return;
        } else {
            updates.removeError("pot_account_pass_curr");
        }
        boolean not_set = false;
        if(pwd.equals("")) {
            updates.highlightError("pot_account_pass_new");
            not_set = true;
        } else {
            updates.removeError("pot_account_pass_new");
        }
        if(repeat.equals("")) {
            updates.highlightError("pot_account_pass_repeat");
            not_set = true;
        } else {
            updates.removeError("pot_account_pass_repeat");
        }
        if(not_set) {
            updates.addMessage(Updates.Severity.WARNING, "New password not set");
            updates.value("pot_account_pass_new", "");
            updates.value("pot_account_pass_repeat", "");
            updates.value("pot_account_pass_curr", "");
            updates.focus("pot_account_pass_curr");
            return;
        }
        if(!pwd.equals(repeat)) {
            updates.highlightError("pot_account_pass_new");
            updates.highlightError("pot_account_pass_repeat");
            updates.addMessage(Updates.Severity.WARNING, "The passwords do not match!");
            return;
        } else {
            updates.removeError("pot_account_pass_new");
            updates.removeError("pot_account_pass_repeat");
        }
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            UnifiedUser unifiedUser = (UnifiedUser) Session.get("_xjs_user");
            try {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                String curr_hash = Base64.getEncoder().encodeToString(digest.digest(current.getBytes("UTF-8")));
                String pwd_hash = Base64.getEncoder().encodeToString(digest.digest(pwd.getBytes("UTF-8")));
                String repeat_hash = Base64.getEncoder().encodeToString(digest.digest(repeat.getBytes("UTF-8")));
                try {
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery("SELECT l_pass = '" + curr_hash + "' FROM l_login WHERE l_uname = '" + unifiedUser.getLogin() + "'");
                    if(resultSet.first() && resultSet.getInt(1) == 1) {
                        statement.execute("UPDATE l_login SET l_pass = '" + pwd_hash + "' WHERE l_uname = '" + unifiedUser.getLogin() + "'");
                        updates.addMessage(Updates.Severity.INFO, "Your password has been changed successfully. You will be loged out soon");
                        updates.push(true);
                        updates.changeLocation("/");
                        Session.destroy();
                    } else {
                        updates.addMessage(Updates.Severity.WARNING, "Your current password was incorrect!");
                        updates.highlightError("pot_account_pass_curr");
                    }
                    statement.close();
                } catch (SQLException e) {
                    Server.log.warn("Could not change password", e);
                    updates.addMessage(Updates.Severity.DANGER, "Could not change password");
                }
            } catch (Exception e) {
                Server.log.warn("Server cant md5 or utf-8", e);
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Server has no valid SQL Connection");
        }
        updates.value("pot_account_pass_new", "");
        updates.value("pot_account_pass_repeat", "");
        updates.value("pot_account_pass_curr", "");
    }
}