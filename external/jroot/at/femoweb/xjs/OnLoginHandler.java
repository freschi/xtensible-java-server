package at.femoweb.xjs;

import at.femoweb.xjs.modules.xaction.Updates;

public interface OnLoginHandler {

    public void replace(Updates updates);
}