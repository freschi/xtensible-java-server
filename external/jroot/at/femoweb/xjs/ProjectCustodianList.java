package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagContent;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-custodians")
public class ProjectCustodianList implements TagContent {

    @Override
    public Tag generateTags() {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                String project = Session.get("curr-project").toString();
                ResultSet resultSet = statement.executeQuery("select u_display, u_email from w_works inner join u_user on w_u_user = idu_user where w_info_perm = 1 and w_p_project = " + project + " order by u_lastname, u_firstname");
                if(resultSet.first()) {
                    Tag table = new Tag("table");
                    table.attribute("class", "table table-bordered table-striped table-hover");
                    Tag hrow = new Tag("tr");
                    Tag hname = new Tag("th");
                    hname.add("Name");
                    hrow.add(hname);
                    Tag hmail = new Tag("th");
                    hmail.add("E-Mail");
                    hrow.add(hmail);
                    table.add(hrow);
                    do {
                        Tag row = new Tag("tr");
                        Tag name = new Tag("td");
                        Tag icon = new Tag("span");
                        icon.attribute("class", "glyphicon glyphicon-eye-open");
                        name.add(icon);
                        name.add(resultSet.getString(1));
                        row.add(name);
                        Tag linkCont = new Tag("td");
                        Tag link = new Tag("a");
                        link.add(resultSet.getString(2));
                        link.attribute("href", "mailto:" + resultSet.getString(2));
                        linkCont.add(link);
                        row.add(linkCont);
                        table.add(row);
                    } while (resultSet.next());
                    return table;
                }
            } catch (SQLException e) {
                Server.log.warn("Not able to create custodians list", e);
            }
        }
        Tag message = new Tag("strong");
        message.add("Could not create contributors list");
        return message;
    }
}