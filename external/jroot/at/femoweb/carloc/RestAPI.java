package at.femoweb.carloc;

import at.femoweb.config.Article;
import at.femoweb.config.Paragraph;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Startable;
import at.femoweb.xjs.lifecycle.Stopable;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.ContentProviderType;
import at.femoweb.xjs.structure.Module;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.lang.StringIndexOutOfBoundsException;
import java.util.Properties;

@ContentProvider(regex = "\\/carloc\\/rest\\/.*", type = ContentProviderType.CONTENT)
@Named("carloc-provider")
public class RestAPI extends BootableModule implements Startable, Stopable, Provider {

    private BootableLogicalUnit logicalUnit;
    private Article store;
    private Group users;
    private Group vehicles;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        RestAPI restAPI = new RestAPI();
        restAPI.logicalUnit = logicalUnit;
        return restAPI;
    }

    @Override
    public void logInfo() {
        Server.log.info("Car Location service running on unit " + logicalUnit.getName());
    }

    @Override
    public void setup() {

    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        Server.log.info(request.getRequestPath().substring(13));
        try {
            String query = request.getRequestPath().substring(13);
            String auth = query.substring(0, (query.contains("@") ? query.indexOf("@") : (query.contains("/") ? query.indexOf("/") : query.length())));
            String user = auth.substring(0, auth.indexOf(":"));
            String pass = auth.substring(auth.indexOf(":") + 1, auth.length());
            String car = null;
            if(query.contains("@")) {
                car = query.substring(query.indexOf("@") + 1, query.contains("/") ? query.indexOf("/") : query.length());
            }
            String cmd = null;
            if(query.contains("/")) {
                cmd = query.substring(query.indexOf("/") + 1);
            }
            Server.log.info("User: " + user + "; Pass: " + pass + "; Car: " + car + "; CMD: " + cmd);
            if(!(users.has(user) && users.getGroup(user).getString("password").equals(pass))) {
                response.setStatus(403);
                response.setStatusLine("Forbidden");
                response.setEntity("User credentials not recognized");
                return;
            }
            if(car != null && !(vehicles.has(car) && vehicles.getGroup(car).getGroup("users").has(user))) {
                response.setStatus(403);
                response.setStatusLine("Forbidden");
                response.setEntity("User has no permission for this car");
                return;
            }
            if(cmd != null) {
                if(cmd.equals("location")) {
                    Group v = vehicles.getGroup(car);
                    if (v.has("last")) {
                        Group last = v.getGroup("last");
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("user", last.getString("by"));
                        jsonObject.addProperty("username", users.getGroup(last.getString("by")).getString("name"));
                        jsonObject.addProperty("longitude", last.getDouble("longitude"));
                        jsonObject.addProperty("latitude", last.getDouble("latitude"));
                        jsonObject.addProperty("timestamp", last.getLong("timestamp"));
                        response.setStatus(200);
                        response.setStatusLine("OK");
                        response.setEntity(jsonObject.toString());
                    } else {
                        response.setStatus(200);
                        response.setStatusLine("OK");
                        response.setEntity("");
                    }
                } else if (cmd.startsWith("location/new")) {

                } else {
                    response.setStatus(404);
                    response.setStatusLine("Not Found");
                    response.setEntity("Command not found");
                }
            } else {
                response.setStatus(404);
                response.setStatusLine("Not Found");
                response.setEntity("No command specified");
            }
        } catch (StringIndexOutOfBoundsException e) {
            response.setStatus(403);
            response.setStatusLine("Forbidden");
        }

    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }

    @Override
    public void start() {
        store = logicalUnit.getCatalog().openArticle("carloc");
        users = store.openParagraph("users").openEntry();
        if(!users.has("users")) {
            Group root = users.getGroup("root");
            root.setString("login", "root");
            root.setString("password", "AxB534zt");
            root.setString("name", "Superuser");
        }
        vehicles = store.openParagraph("vehicles").openEntry();
    }

    @Override
    public void shutdown(Properties properties) {

    }
}