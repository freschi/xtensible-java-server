package at.femoweb;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.Module;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.Override;
import java.lang.StringBuilder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

//@ContentProvider(regex = ".*")
//@Named("maintainance")
public class Maintainance extends Module implements Provider, Initable {

    private HashMap<String, Translation> translations;

    @Override
    public void init(Properties properties) {
        try {
            File file = new File("maintainance");
            if (!file.exists()) {
                file.mkdir();
            }
            File en = new File(file, "en");
            if (!en.exists()) {
                Server.log.info("Writing English Version of Maintainance Page");
                en.mkdir();
                File trfile = new File(en, "translation.properties");
                getDefaultTranslation().store(new FileOutputStream(trfile), null);
            }
            translations = new HashMap<>();
        } catch (IOException e) {
            Server.log.warn("Could not initialize Maintaineance Page", e);
        }
    }

    private static Properties getDefaultTranslation () {
        Properties translation = new Properties();
        translation.setProperty("title", "Site Maintenance");
        translation.setProperty("text.heading", "We&rsquo;ll be back soon!");
        translation.setProperty("text.preLink", "Sorry for the inconvenience but we&rsquo;re performing some maintenance at the moment. If you need to you can always ");
        translation.setProperty("link.email", "office@at.femoweb.at");
        translation.setProperty("link.text", "contact us");
        translation.setProperty("text.postLink", ", otherwise we&rsquo;ll be back online shortly!");
        return translation;
    }

    private Translation getTranslation (String name) {
        if(translations.containsKey(name)) {
            Translation translation = translations.get(name);
            if(translation.isCurrent())
                translation.load();
            return translation;
        } else {
            Translation translation = new Translation(Locale.forLanguageTag(name));
            translations.put(name, translation);
            if(translation.exists()) {
                translation.load();
            } else {
                return getTranslation("en");
            }
            return translation;
        }
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        response.setStatus(200);
        String lang = request.header("Accept-Language");
        String preferred = lang.substring(0, lang.indexOf(";"));
        Locale locale = Locale.forLanguageTag(preferred);
        if(locale != null) {
            Server.log.debug("Requested Language " + locale.getDisplayLanguage(Locale.ENGLISH) + "; Locale " + locale.toLanguageTag());
        }
        Translation translation = getTranslation(locale.toLanguageTag());
        StringBuilder builder = new StringBuilder();
        builder.append("<!doctype html>\n");
        builder.append("<title>");
        builder.append(translation.getTranslationString("title"));
        builder.append("</title>\n");
        builder.append("<style>\n" + "body { text-align: center; padding: 150px; }\n" +
                "h1 { font-size: 50px; }\n" +
                "body { font: 20px Helvetica, sans-serif; color: #333; }\n" +
                "article { display: block; text-align: left; width: 650px; margin: 0 auto; }\n" +
                "a { color: #dc8100; text-decoration: none; }\n" +
                "a:hover { color: #333; text-decoration: none; }\n" +
                "</style>\n" +
                " \n" +
                "<article>\n");
        builder.append("<h1>");
        builder.append(translation.getTranslationString("text.heading"));
        builder.append("</h1>\n");
        builder.append("<div>\n");
        builder.append("<p>");
        builder.append(translation.getTranslationString("text.preLink"));
        builder.append("<a href=\"mailto:");
        builder.append(translation.getTranslationString("link.email"));
        builder.append("\">");
        builder.append(translation.getTranslationString("link.text"));
        builder.append("</a>");
        builder.append(translation.getTranslationString("text.postLink"));
        builder.append("</p>\n");
        builder.append("<p>&mdash; FeMoWeb</p>\n" +
                "</div>\n" +
                "</article>");
        response.setEntity(builder.toString());

        response.header("Content-Type", "text/html");
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }

    public class Translation {

        private long loaded;
        private File res;
        private Locale lang;
        private Properties properties;

        public Translation (Locale lang) {
            this.lang = lang;
            this.res = new File("maintainance/" + lang.toLanguageTag() + "/translation.properties");
            properties = new Properties();
            load();
        }

        public long getLoaded() {
            return loaded;
        }

        public void setLoaded(long loaded) {
            this.loaded = loaded;
        }

        public File getRes() {
            return res;
        }

        public void setRes(File res) {
            this.res = res;
        }

        public Locale getLang() {
            return lang;
        }

        public void setLang(Locale lang) {
            this.lang = lang;
        }

        public Properties getProperties() {
            return properties;
        }

        public void setProperties(Properties properties) {
            this.properties = properties;
        }

        public String getTranslationString(String key) {
            return properties.getProperty(key);
        }

        public boolean isCurrent () {
            if(res == null)
                return false;
            return res.lastModified() > loaded;
        }

        public void load () {
            if(res == null)
                properties = getDefaultTranslation();
            if(!res.exists())
                properties = getDefaultTranslation();
            try {
                loaded = res.lastModified();
                properties.clear();
                properties.load(new FileInputStream(res));
            } catch (Exception e) {
                Server.log.warn("Could not load Translation", e);
            }
        }

        public boolean exists () {
            return res.exists();
        }
    }
}