package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagContent;

import java.lang.Override;

@Resource("pot-document-show-edit")
public class EditDocumentContent implements TagContent {

    @Override
    public Tag generateTags() {
        if(Session.is("curr-doc")) {
            Tag iframe = new Tag("iframe");
            iframe.attribute("src", "https://docs.google.com/document/d/" + Session.get("curr-doc").toString() + "/edit");
            iframe.attribute("style", "width: 100%; min-height: 500px");
            return iframe;
        } else {
            Tag message = new Tag("strong");
            message.add("Current document not set!");
            return message;
        }
    }

}