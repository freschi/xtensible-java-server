package at.femoweb.pot;


import java.lang.String;
import java.util.Date;

public class PotReportLinkRequest {
    
    private Date from;
    private Date to;
    private String reportProvider;

    public PotReportLinkRequest(Date from, Date to, String reportProvider) {
        this.from = from;
        this.to = to;
        this.reportProvider = reportProvider;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getReportProvider() {
        return reportProvider;
    }

    public void setReportProvider(String reportProvider) {
        this.reportProvider = reportProvider;
    }
}