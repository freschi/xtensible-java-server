package at.femoweb.pot;

import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.Module;

import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.util.Date;
import java.util.IntSummaryStatistics;

@at.femoweb.xjs.annotations.Service
@Named("pot-report-service")
public class PotReportService extends Module implements Service {

    @Override
    public Object put(Object... objects) {
        Object object = objects[0];
        if(object instanceof PotReportLinkRequest) {
            PotReportLinkRequest request = (PotReportLinkRequest) object;
            Date from = request.getFrom();
            Date to = request.getTo();
            String provider = request.getReportProvider();
            if(from.after(to)) {
                Date t = to;
                to = from;
                from = t;
            }
            return "/pot/report/report.pdf?from=" + Long.toString(from.getTime(), 32) + "&to=" + Long.toString(to.getTime(), 32) + "&provider=" + provider + "&project=" + Session.get("curr-project").toString();
        } else if (object instanceof PotReportRequest) {
            PotReportRequest reportRequest = (PotReportRequest) object;
            return reportRequest.generateReport();
        }
        return null;
    }
}