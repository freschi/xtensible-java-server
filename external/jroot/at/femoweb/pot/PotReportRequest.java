package at.femoweb.pot;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Session;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfWriter;

import java.lang.Exception;
import java.lang.Object;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.Date;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PotReportRequest {
    
    private Date from;
    private Date to;
    private String id;
    private String provider;
    private Document document;
    private PdfWriter writer;

    public PotReportRequest(Document document, PdfWriter writer, Date from, Date to, String id, String provider) {
        this.from = from;
        this.to = to;
        this.id = id;
        this.provider = provider;
        this.document = document;
        this.writer = writer;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Document generateReport() {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT COUNT(idp_projects) FROM p_projects WHERE idp_projects = " + id);
                if(resultSet.first()) {
                    if(resultSet.getInt(1) != 1) {
                        return null;
                    }
                } else {
                    return null;
                }
                PotReportProvider potReportProvider;
                try {
                    Class<?> providerCl = Class.forName(provider.replace("_", "."));
                    Object providerObj = providerCl.newInstance();
                    if(!(providerObj instanceof PotReportProvider)) {
                        return null;
                    }
                    potReportProvider = (PotReportProvider) providerObj;
                } catch (Exception e) {
                    Server.log.warn("Could not find report provider", e);
                    throw new RuntimeException(e);
                }
                potReportProvider.setup();
                potReportProvider.setStatement(statement);
                potReportProvider.setRequest(this);
                potReportProvider.setupPageEventHandler(writer);
                document.open();
                document.addAuthor("FeMo.IO POT");
                document.addCreator("FeMo.IO POT");
                document.addTitle("Project Report");
                try {
                    potReportProvider.createTitlePage(document);
                    document.newPage();
                    potReportProvider.createUserPages(document);
                } catch (DocumentException e) {
                    Server.log.warn("Could not generate report", e);
                }
                document.close();
                return document;
            } catch (SQLException e) {
                Server.log.warn("Could not generate report", e);
            }
        }
        throw new RuntimeException("No session established yet. Please login first!");
    }
}