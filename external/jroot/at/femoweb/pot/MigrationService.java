package at.femoweb.pot;

import at.femoweb.config.entries.Group;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.services.StartupService;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;

import java.io.UnsupportedEncodingException;
import java.lang.Object;
import java.lang.Override;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;

@Service
@Named("pot-migration-service")
public class MigrationService extends BootableModule implements StartupService {

    @Override
    public boolean onStartup() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/projektorg", "root", "");
            Statement statement = connection.createStatement();
            Statement correction = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT idl_login, l_pass FROM l_login");
            MessageDigest digest = MessageDigest.getInstance("MD5");
            if(resultSet.first()) {
                do {
                    if(resultSet.getString(2).endsWith("==")) {
                        Server.log.info("Hashed password found!");
                    } else {
                        Server.log.warn("Plain text password found! [" + resultSet.getString(2) + "]");
                        String pwd = Base64.getEncoder().encodeToString(digest.digest(resultSet.getString(2).getBytes("UTF-8")));
                        String query = "UPDATE l_login SET l_pass = '" + pwd + "' WHERE idl_login = " + resultSet.getString(1);
                        correction.addBatch(query);
                        Server.log.info("Update: " + query);
                    }
                } while (resultSet.next());
            }
            resultSet.close();
            statement.close();
            correction.executeBatch();
            resultSet = correction.executeQuery("show columns from e_entries where Field like 'e_inschool'");
            if(!resultSet.first()) {
                Server.log.info("Adding e_inschool to e_entries");
                correction.execute("alter table e_entries add e_inschool tinyint(1) default 0");
            }
            resultSet.close();
            resultSet = correction.executeQuery("show columns from e_entries where Field like 'e_ta_task'");
            if(!resultSet.first()) {
                Server.log.info("Adding e_inschool to e_entries");
                correction.addBatch("alter table e_entries add e_ta_task int");
                correction.addBatch("alter table e_entries add foreign key (e_ta_task) references ta_tasks(idta_tasks)");
                correction.executeBatch();
            }
            resultSet.close();
            correction.close();
            connection.close();
        } catch (SQLException e) {
            Server.log.error("Migration did not work properly", e);
        } catch (NoSuchAlgorithmException e) {
            Server.log.fatal("Server does not support md5. Login wont work!");
        } catch (UnsupportedEncodingException e) {
            Server.log.fatal("Hurr Durr, yourr machine cant UTF-8");
        }
        return true;
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        return this;
    }

    @Override
    public void logInfo() {
        Server.log.info("Migration Service for POT activated");
    }

    @Override
    public void setup() {

    }
}