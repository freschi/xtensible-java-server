package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.modules.Push;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.SessionNotFoundException;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.DataConnectionService;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.SessionedThread;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.awt.*;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.UnsupportedEncodingException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@ContentProvider(regex = "\\/pot\\/psp\\/([0-9]*|cp)\\/.*")
@Named("pot-psp-provider")
public class PsPProvider extends Module implements Provider {

    private static final boolean PRINT_DEBUG_LINES = false;

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        String proj = request.getRequestPath().substring(9, request.getRequestPath().indexOf("/", 9));
        if(proj.equals("cp")) {
            proj = Session.get("curr-project").toString();
        }
        String format = "svg";
        if(request.getRequestPath().contains("?")) {
            String query = request.getRequestPath().substring(request.getRequestPath().indexOf("?") + 1);
            if(query.contains("format=")) {
                int end;
                format = query.substring(query.indexOf("format=") + 7, (end = query.indexOf("&", query.indexOf("format=") + 7)) > 0 ? end : query.length());
            }
        }
        LogicalUnit logicalUnit;
        try {
            logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        } catch (SessionNotFoundException e) {
            response.setStatus(302);
            response.setStatusLine("Found");
            response.header("Location", request.getRequestPath());
            return;
        }
        DataConnectionService dataConnectionService = (DataConnectionService) logicalUnit.getService("pot-connection-service");
        try {
            Connection connection = dataConnectionService.connectDataSource(null);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT p_name FROM p_projects WHERE idp_projects = " + proj);
            String projectName = "NOT FOUND";
            if(resultSet.first()) {
                projectName = resultSet.getString(1);
            }
            resultSet = statement.executeQuery("SELECT idta_tasks, ta_id, ta_p_project, ta_name, ta_milestone FROM ta_tasks WHERE ta_ta_parent is NULL and ta_p_project = " + proj + " ORDER BY ta_id ASC");
            ArrayList<Task> tasks = new ArrayList<>();
            if(resultSet.first()) {
                do {
                    Task task = new Task(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getBoolean(5));
                    tasks.add(task);
                } while (resultSet.next());
            }
            for(Task task : tasks) {
                task.loadChildren(statement);
            }
            if(format.equalsIgnoreCase("text")) {
                String entity = "";
                for (Task task : tasks) {
                    entity += task.renderAsText();
                }
                response.setEntity(entity);
                response.header("Content-Type", "text/plain");
            } else if (format.equalsIgnoreCase("svg")) {
                Tag svg = renderSVG(tasks, projectName);
                response.setEntity(svg.render());
                response.header("Pragma", "no-cache");
                response.header("Cache-Control", "no-cache, no-store, must-revalidate");
                response.header("Content-Type", "image/svg+xml");
                response.header("Expires", "0");
            }
            response.setStatus(200);
        } catch (SQLException e) {
            Server.log.warn("Could not create psp", e);
            response.setEntity("Could not create psp" + e.getMessage());
            response.setStatus(500);
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }

    private Tag renderSVG(ArrayList<Task> tasks, String projectName) {
        float width, height;
        width = 2 * 100 + tasks.size() * (150 + 40);
        height = 1500;
        Font font = new Font("Helvetica", Font.BOLD, 48);
        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_4BYTE_ABGR);
        image.getGraphics().setFont(font);
        FontMetrics metrics = image.getGraphics().getFontMetrics();
        Tag svg = new Tag("svg");
        svg.attribute("xmlns", "http://www.w3.org/2000/svg");
        svg.attribute("version", "1.1");
        svg.attribute("width", width + "");
        svg.attribute("height", height + "");
        Tag text = new Tag("text", svg);
        text.add(projectName);
        text.attribute("style", "font-size: 48px; font-family: Helvetica;");
        Server.log.info("Text width " + metrics.stringWidth(projectName) + " px");
        text.attribute("x", (width / 2 - (metrics.stringWidth(projectName) * 4.085) / 2) + "");
        text.attribute("y", "58");
        if(PRINT_DEBUG_LINES) {
            Tag line = new Tag("line", svg);
            line.attribute("x1", (width / 2) + "");
            line.attribute("y1", "0");
            line.attribute("x2", (width / 2) + "");
            line.attribute("y2", "1500");
            line.attribute("style", "stroke:rgb(255,0,0);stroke-width:0.5");
            Tag hline = new Tag("line", svg);
            hline.attribute("x1", "0");
            hline.attribute("y1", "98");
            hline.attribute("x2", "" + width);
            hline.attribute("y2", "98");
            hline.attribute("style", "stroke:rgb(0,255,0);stroke-width:0.5");
        }
        height = 0;
        for (int i = 0; i < tasks.size(); i++) {
            float center = 100 + (150 + 40) * i + (150 + 40) / 2;
            if(PRINT_DEBUG_LINES) {
                Tag vline = new Tag("line", svg);
                vline.attribute("x1", center + "");
                vline.attribute("y1", "98");
                vline.attribute("x2", "" + center);
                vline.attribute("y2", "" + height);
                vline.attribute("style", "stroke:rgb(0,0,255);stroke-width:0.5");
            }
            Task task = tasks.get(i);
            task.setX((center - (150) / 2));
            task.setY(83);
            svg.add(task.renderSVG(image.getGraphics()));
            if(height < task.getTotalHeight()) {
                height = task.getTotalHeight();
            }
            height += 20;
            svg.attribute("height", height + "");
        }
        return svg;
    }

    @XAction(name = "pot_work_units_drop")
    public void dropAllUnits(@XUpdate Updates updates) {
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM ta_tasks WHERE ta_p_project = " + Session.get("curr-project"));
            statement.close();
        } catch (SQLException e) {
            Server.log.warn("Could not drop all work units", e);
        }
    }

    public class Task {

        private int dbId;
        private String id;
        private int project;
        private String name;
        private ArrayList<Task> children;
        private Task parent;
        private boolean milestone;

        //Values needed for svg export
        private float x = -1;
        private float y = -1;
        private float width;
        private float height;
        private int r;
        private int g;
        private int b;
        private int fontSize;
        private float totalHeight;

        private static final int R = 240;
        private static final int G = 240;
        private static final int B = 250;
        private static final float WIDTH = 150;
        private static final float HEIGHT = 50;
        private static final float DISTANCE = 20;
        private static final int FONT_SIZE = 13;
        private static final float FACTOR_R = 0.95f;
        private static final float FACTOR_G = 0.95f;
        private static final float FACTOR_B = 0.95f;

        public float getTotalHeight() {
            return totalHeight;
        }

        public void setX(float x) {
            this.x = x;
        }

        public void setY(float y) {
            this.y = y;
        }

        public Task(int dbId, String id, int project, String name, boolean milestone) {
            this.dbId = dbId;
            this.id = id;
            this.project = project;
            this.name = name;
            this.children = new ArrayList<Task>();
            this.milestone = milestone;
        }

        public int getDbId() {
            return dbId;
        }

        public void setDbId(int dbId) {
            this.dbId = dbId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getProject() {
            return project;
        }

        public void setProject(int project) {
            this.project = project;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<Task> getChildren() {
            return children;
        }

        public void setChildren(ArrayList<Task> children) {
            this.children = children;
        }

        public String renderAsText() {
            return renderAsText(0);
        }

        private String renderAsText(int level) {
            String res = "";
            for (int i = 0; i < level; i++) {
                res += "\t";
            }
            res += getId() + " - " + getName() + "\n";
            for (Task task : children) {
                res += task.renderAsText(level + 1);
            }
            return res;
        }

        public void loadChildren(Statement statement) throws SQLException{
            ResultSet resultSet = statement.executeQuery("SELECT idta_tasks, ta_id, ta_p_project, ta_name, ta_milestone FROM ta_tasks WHERE ta_ta_parent = " + getDbId() + " ORDER BY ta_id asc");
            if(resultSet.first()) {
                do {
                    Task task = new Task(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getBoolean(5));
                    task.parent = this;
                    children.add(task);
                } while (resultSet.next());
            }
            resultSet.close();
            for(Task task : children) {
                task.loadChildren(statement);
            }
        }

        public List<Tag> renderSVG(Graphics graphics) {
            return renderSVG(0, graphics);
        }

        private List<Tag> renderSVG(int position, Graphics graphics) {
            if(parent == null) {
                if(x < 0 && y < 0) {
                    return null;
                }
                width = WIDTH;
                height = HEIGHT;
                r = R;
                g = G;
                b = B;
                fontSize = FONT_SIZE;
            } else {
                x = (float) (parent.x + parent.width * 0.1);
                y = (float) (parent.y + parent.totalHeight + /*(position == 0 && parent != null && parent.parent == null ? DISTANCE : 0)*/ + height * position);
                width = (float) (parent.width * 0.9);
                height = (float) (parent.height * 0.9);
                r = (int) (parent.r * FACTOR_R);
                g = (int) (parent.g * FACTOR_G);
                b = (int) (parent.b * FACTOR_B);
                fontSize = (int) (parent.fontSize * 0.9);
            }
            Font font = new Font("Helvetica", Font.PLAIN, fontSize);
            graphics.setFont(font);
            FontMetrics metric = graphics.getFontMetrics();
            Tag rect = new Tag("rect");
            rect.attribute("width", "" + width);
            rect.attribute("height", "" + height);
            rect.attribute("x", "" + x);
            rect.attribute("y", "" + y);
            if(milestone) {
                rect.attribute("style", "fill:rgb(255,247,0);stroke:black");
            } else {
                rect.attribute("style", "fill:rgb(" + r + "," + g + "," + b + ");stroke:black");
            }
            Tag idText = new Tag("text");
            idText.add(getId());
            idText.attribute("x", (x + 5) + "");
            idText.attribute("y", (y + fontSize) + "");
            idText.attribute("style", "font-family:Helvetica;font-size:" + fontSize + "px;font-weight:bold;");
            Tag nameText = new Tag("text");
            nameText.attribute("x", (x + 5) + "");
            nameText.attribute("y", (y + fontSize * 2) + "");
            nameText.attribute("style", "font-family:Helvetica;font-size:" + fontSize + "px;");
            if(metric.stringWidth(getName()) < width) {
                nameText.add(getName());
            } else {
                String[] parts = getName().split(" ");
                String s = "";
                int wordCount = 0;
                int count = 0;
                for (int i = 0; i < parts.length; i++) {
                    String t = s + (s.equals("") ? "" : " ") + parts[i];
                    wordCount++;
                    int w;
                    if((w = metric.stringWidth(t)) > width && wordCount > 1) {
                        Tag span = new Tag("tspan", nameText);
                        span.attribute("x", (x + 5) + "");
                        if(count > 0) {
                            span.attribute("dy", "" + fontSize);
                        }
                        span.add(s);
                        count++;
                        wordCount = 0;
                        s = "";
                        i--;
                    } else if (w > width && wordCount == 1) {
                        Tag span = new Tag("tspan", nameText);
                        span.attribute("x", (x + 5) + "");
                        if(count > 0) {
                            span.attribute("dy", "" + fontSize);
                        }
                        span.add(t);
                        count++;
                        wordCount = 0;
                        s = "";
                    } else {
                        s = t;
                    }
                }
                if(s.length() > 0) {
                    Tag span = new Tag("tspan", nameText);
                    span.attribute("x", (x + 5) + "");
                    span.attribute("dy", "" + fontSize);
                    span.add(s);
                }
            }
            ArrayList<Tag> tags = new ArrayList<Tag>();
            tags.add(rect);
            tags.add(idText);
            tags.add(nameText);
            totalHeight = height + (children.size() > 0 ? DISTANCE : 0);
            for(int i = 0; i < children.size(); i++) {
                Task c = children.get(i);
                tags.addAll(c.renderSVG(i, graphics));
                totalHeight += c.totalHeight;
                if(i < children.size() - 1) {
                    totalHeight += DISTANCE;
                }
                Tag line = new Tag("polyline");
                line.attribute("fill", "none");
                line.attribute("stroke", "black");
                Point pP = getParentalPoint(), cP = c.getChildPoint(), iP = getIntermediate(pP, cP);
                line.attribute("points", pP.getX() + "," + pP.getY() + " " + iP.getX() + "," + iP.getY() + " " + cP.getX() + "," + cP.getY());
                tags.add(line);
            }
            return tags;
        }

        public Point getParentalPoint() {
            return new Point((int) (x + width * 0.05), (int) (y + height));
        }

        public Point getChildPoint() {
            return new Point((int) x, (int) (y + height / 2));
        }

        public Point getIntermediate(Point parental, Point child) {
            return new Point((int) parental.getX(), (int) child.getY());
        }
    }

    private MimeMessage getMessage() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "alfa3026.alfahosting-server.de");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        javax.mail.Session session = javax.mail.Session.getInstance(props,
                new Authenticator() {
                    @Override
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication("web1448p5", "MpZsOZSr9RO5");
                    }
                });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setSentDate(new Date());
            message.setFrom(new InternetAddress("noreply@femoweb.at"));
            return message;
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @XAction(name = "pot_work_units_mail_custodians")
    public void sendPspToCustodians(@XUpdate Updates updates) {
        updates.addMessage(Updates.Severity.INFO, "Sending psp link to custodians!");
        updates.push(true);
        new SessionedThread() {
            @Override
            public void execute() {
                MimeMessage mimeMessage = getMessage();
                Address[] addresses;
                String projectName = "NOT FOUND";
                Connection connection = (Connection) Session.get("connection");
                if(connection == null) {
                    updates.addMessage(Updates.Severity.WARNING, "No connection could be found");
                    return;
                }
                Tag svg = null;
                try {
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery("SELECT p_name FROM p_projects WHERE idp_projects = " + Session.get("curr-project"));
                    if(resultSet.first()) {
                        projectName = resultSet.getString(1);
                    }
                    resultSet = statement.executeQuery("SELECT idta_tasks, ta_id, ta_p_project, ta_name, ta_milestone FROM ta_tasks WHERE ta_ta_parent is NULL and ta_p_project = " + Session.get("curr-project") + " ORDER BY ta_id ASC");
                    ArrayList<Task> tasks = new ArrayList<>();
                    if(resultSet.first()) {
                        do {
                            Task task = new Task(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getBoolean(5));
                            tasks.add(task);
                        } while (resultSet.next());
                    }
                    for(Task task : tasks) {
                        task.loadChildren(statement);
                    }
                    svg = renderSVG(tasks, projectName);
                } catch (SQLException e) {
                    Server.log.warn("Could not create psp", e);
                }
                if(svg == null) {
                    updates.addMessage(Updates.Severity.WARNING, "Could not create psp");
                    return;
                }
                try (Statement statement = connection.createStatement()) {
                    ResultSet resultSet = statement.executeQuery("select u_email from w_works inner join u_user on idu_user = w_u_user where w_info_perm = 1 and w_p_project = " + Session.get("curr-project"));
                    ArrayList<Address> addressArrayList = new ArrayList<>();
                    if (resultSet.first()) {
                        do {
                            try {
                                addressArrayList.add(new InternetAddress(resultSet.getString(1)));
                            } catch (AddressException e) {
                                Server.log.warn("Could not create Internet Address from " + resultSet.getString(1), e);
                            }
                        } while (resultSet.next());
                    }
                    addresses = addressArrayList.toArray(new Address[]{});
                    Server.log.info("Recipients: " + InternetAddress.toString(addresses));
                } catch (SQLException e) {
                    Server.log.warn("Could not create receipient list", e);
                    updates.addMessage(Updates.Severity.DANGER, "Error while creating custodian list");
                    return;
                }
                try {
                    mimeMessage.setRecipients(Message.RecipientType.TO, addresses);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                    mimeMessage.setSubject("[INFO] PSP for " + projectName);
                    Multipart multipart = new MimeMultipart();
                    BodyPart bodyPart = new MimeBodyPart();
                    bodyPart.setText(createMessage(projectName, ((UnifiedUser) Session.get("_xjs_user")).getUsername(), simpleDateFormat).render());
                    bodyPart.addHeader("Content-Type", "text/html");
                    multipart.addBodyPart(bodyPart);
                    InternetHeaders headers = new InternetHeaders();
                    headers.addHeader("Content-Type", "image/svg+xml");
                    ByteArrayDataSource dataSource = new ByteArrayDataSource(svg.render().getBytes("UTF-8"), "image/svg+xml");
                    dataSource.setName("psp_" + projectName.toLowerCase().replace(" ", "_") + ".svg");
                    MimeBodyPart attachment = new MimeBodyPart();
                    attachment.setDataHandler(new DataHandler(dataSource));
                    attachment.setFileName(dataSource.getName());
                    multipart.addBodyPart(attachment);
                    mimeMessage.setContent(multipart);
                    Server.log.info("Forwarding messages to server");
                    javax.mail.Transport.send(mimeMessage);
                    Server.log.info("Sending complete");
                    Push.get().addMessage(Updates.Severity.SUCCESS, "Sending complete");
                } catch (MessagingException e) {
                    Server.log.warn("Could not send message(s)", e);
                } catch (UnsupportedEncodingException e) {
                    Server.log.warn("Server can't utf-8", e);
                }
                Push.get().push(false);
            }
        }.start();
    }

    public Tag createMessage(String projectName, String user, DateFormat format) {
        Tag msg = new Tag("body");
        Tag heading = new Tag("h1");
        heading.add(projectName);
        msg.add(heading);
        Tag message = new Tag("span");
        message.add("This is an automatically sent email, requested by " + user);
        msg.add(message);
        Tag detailHeading = new Tag("h3");
        detailHeading.add("Details");
        msg.add(detailHeading);
        Tag details = new Tag("div");
        Tag table = new Tag("table");
        {
            Tag userRow = new Tag("tr");
            Tag userStronk = new Tag("th");
            userStronk.add("Requested by");
            userRow.add(userStronk);
            Tag userName = new Tag("td");
            userName.add(user);
            userRow.add(userName);
            table.add(userRow);
        }
        {
            Tag dateRow = new Tag("tr");
            Tag dateStronk = new Tag("th");
            dateStronk.add("Request date");
            dateRow.add(dateStronk);
            Tag date = new Tag("td");
            date.add(format.format(new Date()));
            dateRow.add(date);
            table.add(dateRow);
        }
        {
            Tag projectRow = new Tag("tr");
            Tag projectStronk = new Tag("th");
            projectStronk.add("Project");
            projectRow.add(projectStronk);
            Tag project = new Tag("td");
            project.add(projectName);
            projectRow.add(project);
            table.add(projectRow);
        }
        details.add(table);
        msg.add(details);
        msg.add(new Tag("hr"));
        message = new Tag("small");
        message.attribute("style", "color: light-gray");
        message.add("Please do not respond to this email address. If you received this email unwillingly please contact the");
        Tag webmaster = new Tag("a");
        webmaster.attribute("href", "mailto:office@femoweb.at");
        webmaster.add("webmaster");
        message.add(webmaster);
        msg.add(message);
        return msg;
    }
}