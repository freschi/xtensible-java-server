package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Resources;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.ConditionCheck;
import at.femoweb.xjs.modules.xjs.TagContent;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-work-units-provider")
public class PotWorkUnitsProvider implements TagContent {

    @Override
    public Tag generateTags() {
        Tag table = new Tag("table");
        table.attribute("class", "table table-striped table-bordered table-hover");
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            ConditionCheck check = (ConditionCheck) Session.get("pot-creator-check");
            boolean crud = check.check(null);
            ResultSet resultSet = statement.executeQuery("SELECT ta_id, ta_name, ta_start, ta_end, idta_tasks FROM ta_tasks WHERE ta_p_project = " + Session.get("curr-project").toString());
            Tag hrow = new Tag("tr");
            Tag hid = new Tag("th");
            hid.add("ID");
            hrow.add(hid);
            Tag hname = new Tag("th");
            hname.add("Name");
            hrow.add(hname);
            Tag hstart = new Tag("th");
            hstart.add("Start");
            hrow.add(hstart);
            Tag hend = new Tag("th");
            hend.add("End");
            hrow.add(hend);
            if(crud) {
                Tag hcrud = new Tag("th");
                hrow.add(hcrud);
            }
            table.add(hrow);
            if(resultSet.first()) {
                do {
                    Tag row = new Tag("tr");
                    Tag id = new Tag("td");
                    id.add(resultSet.getString(1));
                    row.add(id);
                    Tag name = new Tag("td");
                    name.add(resultSet.getString(2));
                    row.add(name);
                    Tag start = new Tag("td");
                    start.add(resultSet.getString(3));
                    row.add(start);
                    Tag end = new Tag("td");
                    end.add(resultSet.getString(4));
                    row.add(end);
                    if(crud) {
                        Tag dcrud = new Tag("td");
                        Tag view = new Tag("a");
                        view.attribute("class", "btn btn-success btn-xs");
                        view.attribute("onclick", "invoke('pot_work_units_provider_view', {task: " + resultSet.getString(5) + "});");
                        Tag vicon = new Tag("span");
                        vicon.attribute("class", "glyphicon glyphicon-eye-open");
                        view.add(vicon);
                        dcrud.add(view);
                        Tag edit = new Tag("a");
                        edit.attribute("class", "btn btn-info btn-xs");
                        edit.attribute("onclick", "invoke('pot_work_units_provider_edit', {task: " + resultSet.getString(5) + "});");
                        Tag eicon = new Tag("span");
                        eicon.attribute("class", "glyphicon glyphicon-edit");
                        edit.add(eicon);
                        dcrud.add(edit);
                        Tag delete = new Tag("a");
                        delete.attribute("class", "btn btn-danger btn-xs");
                        delete.attribute("onclick", "invoke('pot_work_units_provider_delete', {task: " + resultSet.getString(5) + "});");
                        Tag dicon = new Tag("span");
                        dicon.attribute("class", "glyphicon glyphicon-trash");
                        delete.add(dicon);
                        dcrud.add(delete);
                        row.add(dcrud);
                    }
                    table.add(row);
                } while (resultSet.next());
            }
        } catch (SQLException e) {
            Server.log.warn("Error while creating table", e);
        }
        return table;
    }

    @XAction(name = "pot_work_units_provider_view")
    public void onView(@XUpdate Updates updates, @XOptional JsonElement element) {
        JsonObject extra = element.getAsJsonObject();
        if(extra.has("task")) {
            Session.set("curr-task", extra.get("task").getAsString());
            updates.setPartial("pot_work_units_content", "project/wu/view.xjs");
        }
    }

    @XAction(name = "pot_work_units_provider_edit")
    public void onEdit(@XUpdate Updates updates, @XOptional JsonElement element) {
        if(element != null) {
            JsonObject extra = element.getAsJsonObject();
            String task = extra.get("task").getAsString();
            Connection connection = (Connection) Session.get("connection");
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select ta_name, ta_start, ta_end, ta_hours, ta_id, ta_descr, ta_problems, ta_milestone from ta_tasks where idta_tasks = " + task);
                if(resultSet.first()) {
                    Session.set("pot_wu_name", resultSet.getString(1));
                    Session.set("pot_wu_start", resultSet.getString(2));
                    Session.set("pot_wu_end", resultSet.getString(3));
                    Session.set("pot_wu_hours", resultSet.getString(4));
                    Session.set("pot_wu_id", resultSet.getString(5));
                    Session.set("pot_wu_descr", resultSet.getString(6));
                    Session.set("pot_wu_problems", resultSet.getString(7));
                    Session.set("pot_wu_milestone", resultSet.getString(8));
                }
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not prepare task edit", e);
                return;
            }
            Session.set("curr-task", task);
        }
        updates.setPartial("pot_work_units_content", "project/wu/edit.xjs");
    }

    @XAction(name = "pot_work_units_provider_delete")
    public void onDelete(@XUpdate Updates updates, @XOptional JsonElement element) {
        if(element != null) {
            JsonObject extra = element.getAsJsonObject();
            String task = extra.get("task").getAsString();
            Connection connection = (Connection) Session.get("connection");
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select ta_name, ta_start, ta_end, ta_hours, ta_id, ta_descr, ta_problems from ta_tasks where idta_tasks = " + task);
                if(resultSet.first()) {
                    Session.set("pot_wu_name", resultSet.getString(1));
                    Session.set("pot_wu_start", resultSet.getString(2));
                    Session.set("pot_wu_end", resultSet.getString(3));
                    Session.set("pot_wu_hours", resultSet.getString(4));
                    Session.set("pot_wu_id", resultSet.getString(5));
                    Session.set("pot_wu_descr", resultSet.getString(6));
                    Session.set("pot_wu_problems", resultSet.getString(7));
                }
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not prepare task edit", e);
                return;
            }
            Session.set("curr-task", task);
        }
        updates.setPartial("pot_work_units_content", "project/wu/delete.xjs");
    }
}