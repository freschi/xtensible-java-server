package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ListProvider;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Resource("pot.types.list-provider")
public class TypesListProvider implements ListProvider {

    @Override
    public List<ListItem> createList() {
        ArrayList<ListItem> items = new ArrayList<>();
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT idt_types, t_name FROM t_types");
                if(resultSet.first()) {
                    do {
                        ListItem listItem = new ListItem();
                        listItem.setValue(resultSet.getString(1));
                        Tag tag = new Tag("span");
                        tag.add(resultSet.getString(2));
                        listItem.setTag(tag);
                        items.add(listItem);
                    } while (resultSet.next());
                }
            } catch (SQLException e) {
                Server.log.warn("Could not create type list", e);
            }
        }
        return items;
    }
}