package at.femoweb.pot;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.ConditionCheck;

import java.lang.Override;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-db-creator-check")
public class DbCreatorCheck implements ConditionCheck {

    @Override
    public boolean check(String[] args) {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select p_u_creator = idu_user from u_user join p_projects where idu_user = " + ((UnifiedUser)Session.get("_xjs_user")).getProperty("user.id") + " and idp_projects = " + Session.get("curr-project").toString());
                if(resultSet.first()) {
                    if(resultSet.getInt(1) == 1)
                        return true;
                }
            } catch (SQLException e) {
                Server.log.warn("Could not check for creator", e);
            }
        }
        return false;
    }
    
    @XAction(name = "pot_settings_creator_onclick")
    public void potDbCreatorUpgrade(@XUpdate Updates updates) {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                String uid, pid;
                uid = ((UnifiedUser)Session.get("_xjs_user")).getProperty("user.id");
                pid = Session.get("curr-project").toString();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("select p_u_creator = idu_user from u_user join p_projects where idu_user = " + uid + " and idp_projects = " + pid);
                if(resultSet.first()) {
                    if(resultSet.getInt(1) == 1) {
                        statement.execute("UPDATE w_works SET w_info_perm = 3 WHERE w_u_user = " + uid + " AND w_p_project = " + pid);
                        updates.addMessage(Updates.Severity.SUCCESS, "You have been promoted to creator");
                    }
                }
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not check for creator", e);
            }
        }
    }

    @XAction(name = "pot_settings_default_provider_onclick")
    public void setProjectDefaultProvider(@XUpdate Updates updates, @XParam(name = "pot_default_provider") String provider) {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                String pid;
                pid = Session.get("curr-project").toString();
                Statement statement = connection.createStatement();
                statement.execute("UPDATE p_projects SET p_r_reportprovider = '" + provider.replace("_", ".") + "' WHERE idp_projects = " + pid);
                statement.close();
                updates.addMessage(Updates.Severity.INFO, "Changed default provider to " + provider);
            } catch (SQLException e) {
                Server.log.warn("Could not set default provider", e);
                updates.addMessage(Updates.Severity.WARNING, "Could not set default provider");
            }
        }
    }
}