package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ListProvider;
import at.femoweb.xjs.modules.xjs.ListProvider.ListItem;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.lang.Override;
import java.util.ArrayList;
import java.util.List;

@Resource("pot-custom-column-list")
public class CustomColumnList implements ListProvider {

    @Override
    public List<ListItem> createList() {
        if(Session.is("work-units")) {
            ArrayList<ListItem> listItems = new ArrayList<ListItem>();
            Document document = (Document) Session.get("work-units");
            Element taskproperties = document.select("taskproperties").first();
            for(Element property : taskproperties.children()) {
                if(property.attr("type").equals("custom")) {
                    ListItem listItem = new ListItem();
                    Tag item = new Tag("p");
                    item.add(property.attr("name"));
                    listItem.setTag(item);
                    listItem.setValue(property.attr("id"));
                    listItems.add(listItem);
                }
            }
            return listItems;
        }
        return null;
    }
}