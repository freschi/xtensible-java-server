package at.femoweb.pot;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.util.ResultCallback;
import at.femoweb.xjs.util.SQL;
import at.femoweb.xjs.util.ValueProvider;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Integer;
import java.lang.Override;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

@ContentProvider(regex = "\\/pot\\/report\\/report\\.pdf.*")
@Named("pot-report-provider")
public class PotReportContentProvider extends Module implements Provider {

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        Server.log.info(request.getRequestPath());
        String path = request.getRequestPath();
        if(path.contains("?")) {
            String query = path.substring(path.indexOf("?") + 1);
            Server.log.info("Query: " + query);
            String from = null, to = null, provider = null, project = null;
            for(String item : query.split("&")) {
                String key, value;
                key = item.substring(0, item.indexOf("="));
                value = item.substring(item.indexOf("=") + 1);
                if(key.equals("from")) {
                    from = value;
                } else if (key.equals("to")) {
                    to = value;
                } else if (key.equals("provider")) {
                    provider = value;
                } else if (key.equals("project")) {
                    project = value;
                }
            }
            if(Session.exists() && Session.isAuthenticated()) {
                Connection connection = (Connection) Session.get("connection");
                try (PreparedStatement statement = connection.prepareStatement("SELECT COUNT(w_p_project) FROM w_works WHERE w_u_user = ? AND w_p_project = ?")) {
                    statement.setInt(1, Integer.parseInt(((UnifiedUser)Session.get("_xjs_user")).getProperty("user.id")));
                    statement.setInt(2, Integer.parseInt(project));
                    ResultSet resultSet = statement.executeQuery();
                    if(resultSet.first()) {
                        if(resultSet.getInt(1) == 0) {
                            response.setStatus(403);
                            response.setStatusLine("Forbidden");
                            response.header("Content-Type", "text/plain");
                            response.setEntity("Access Denied");
                            return;
                        }
                    } else {
                        response.setStatus(403);
                        response.setStatusLine("Forbidden");
                        response.header("Content-Type", "text/plain");
                        response.setEntity("Access Denied");
                        return;
                    }
                } catch (SQLException e) {
                    response.setStatus(500);
                    response.setStatusLine("Internal Server Error");
                    response.header("Content-Type", "text/plain");
                    response.setEntity("Internal Server Error");
                    Server.log.error("Error while checking report access permission", e);
                    return;
                }
            } else {
                response.setStatus(403);
                response.setStatusLine("Forbidden");
                response.header("Content-Type", "text/plain");
                response.setEntity("Access Denied");
                return;
            }
            Server.log.info("From: " + from + ", To: " + to + ", Provider: " + provider + ", Project: " + project);
            Date _from = new Date(Long.parseLong(from, 32));
            Date _to = new Date(Long.parseLong(to, 32));
            Service service = ((LogicalUnit)Session.get("_xjs_logical_unit")).getService("pot-report-service");
            Document document = new Document(PageSize.A4, 36, 36, 54, 54);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                if(document != null) {
                    PdfWriter pdfWriter = PdfWriter.getInstance(document, byteArrayOutputStream);
                    pdfWriter.setBoxSize("art", new Rectangle(36, 54, 559, 788));          //Setup box sizes for page numbers and thelike
                    service.put(new PotReportRequest(document, pdfWriter, _from, _to, project, provider));
                    response.header("Content-Type", "application/pdf");
                    response.setEntity(byteArrayOutputStream.toByteArray());
                    response.setStatus(200);
                } else {
                    response.header("Content-Type", "text/plain");
                    response.setEntity("Could not generate document");
                    response.setStatus(404);
                }
            } catch (DocumentException e) {
                Server.log.warn("Could not create byte stream", e);
                try {
                    byteArrayOutputStream.write("Error while creating report".getBytes("UTF-8"));
                } catch (IOException e1) {
                    Server.log.warn("Error while preparing response", e);
                }
            }
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }
}