package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ListProvider;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Resource("pot.tasks.list-provider")
public class PotTasksListProvider implements ListProvider {

    @Override
    public List<ListItem> createList() {
        ListItem listItem = new ListItem();
        listItem.setValue("null");
        Tag def = new Tag("span");
        def.add("No Specific Task");
        listItem.setTag(def);
        ArrayList<ListItem> list = new ArrayList<ListItem>();
        list.add(listItem);
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select idta_tasks, CONCAT(ta_id, ' - ', ta_name) display from ta_tasks where ta_p_project = " + Session.get("curr-project").toString() + " order by ta_id");
            if(resultSet.first()) {
                do {
                    ListItem item = new ListItem();
                    item.setValue(resultSet.getString(1));
                    Tag val = new Tag("span");
                    val.add(resultSet.getString(2));
                    item.setTag(val);
                    list.add(item);
                } while (resultSet.next());
            }
        } catch (SQLException e) {
            Server.log.warn("Error while creating task list", e);
        }
        return list;
    }
}