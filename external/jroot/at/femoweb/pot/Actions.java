package at.femoweb.pot;

import at.femoweb.html.Document;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.XJSUser;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.femo.security.Secured;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import java.lang.*;
import java.lang.Exception;
import java.lang.Integer;
import java.lang.String;
import java.lang.StringBuilder;
import java.sql.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IntSummaryStatistics;

public class Actions {

    @XAction(name = "pot_work_units_import")
    public void importFile(@XUpdate Updates updates, @XParam(name = "pot_work_units_custom_id") String customIdColumn, @XParam(name = "pot_work_units_hours") String hoursColumn) {
        org.jsoup.nodes.Document xml = (org.jsoup.nodes.Document) Session.get("work-units");
        Element project = xml.select("project").first();
        Element tasks = project.select("tasks").first();
        Element taskproperties = tasks.select("taskproperties").first();
        boolean createIds = false;
        if(customIdColumn.equals("")) {
            createIds = true;
        }
        if(hoursColumn.equals("")) {
            for(Element property : taskproperties.children()) {
                if(property.attr("name").equals("duration")) {
                    hoursColumn = property.attr("id");
                }
            }
        }
        HashMap<Integer, Task> taskMap = new HashMap<Integer, Task>();
        ArrayList<Task> taskList = readTasks(tasks, null, hoursColumn, createIds, customIdColumn);
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            for (Task task : taskList) {
                if(task.getId() == null)
                    continue;
                String query = "INSERT INTO ta_tasks (ta_name, ta_id, ta_start, ta_end, ta_hours, ta_p_project) VALUES (" +
                        "'" + task.getName() + "', '" + task.getId() + "', '" + task.getStart().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "', " +
                        "'" + task.getEnd().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "', " + task.getHours() + ", " + Session.get("curr-project").toString() + ")";
                statement.addBatch(query);
                taskMap.put(task.getIntId(), task);
                Server.log.info(task.toString());
            }
            statement.executeBatch();
            for(Task task : taskList) {
                if(task.getParent() != null) {
                    String select = "SELECT tasks.idta_tasks FROM ta_tasks as tasks WHERE tasks.ta_id = '" + task.getParent().getId() + "' AND tasks.ta_p_project = " + Session.get("curr-project").toString();
                    ResultSet resultSet = statement.executeQuery(select);
                    String query = "UPDATE ta_tasks SET ta_ta_parent = " + (resultSet.first() ? resultSet.getInt(1) : "null") + " WHERE ta_id = '" + task.getId() + "' AND ta_p_project = " + Session.get("curr-project").toString();
                    statement.addBatch(query);
                }
            }
            statement.executeBatch();
            for(Task task : taskList) {
                if(task.getDepends().size() > 0) {
                    String select = "SELECT idta_tasks FROM ta_tasks WHERE ta_id = '" + task.getId() + "' AND ta_p_project = " + Session.get("curr-project").toString();
                    ResultSet resultSet = statement.executeQuery(select);
                    if(!resultSet.first()) {
                        continue;
                    }
                    String dependency = resultSet.getString(1);
                    for(Integer integer : task.getDepends()) {
                        Task t = taskMap.get(integer);
                        if(t != null) {
                            select = "SELECT idta_tasks FROM ta_tasks WHERE ta_id = '" + t.getId() + "' AND ta_p_project = " + Session.get("curr-project").toString();
                            resultSet = statement.executeQuery(select);
                            if (!resultSet.first())
                                continue;
                            String taskId = resultSet.getString(1);
                            statement.addBatch("INSERT INTO de_dependencies (de_ta_task, de_ta_dependency) VALUES (" + taskId + ", " + dependency + ")");
                        }
                    }
                }
            }
            statement.executeBatch();
            statement.close();
        } catch (SQLException e) {
            Server.log.warn("Could not save tasks", e);
        }
        updates.addMessage(Updates.Severity.INFO, (createIds ? "Creating IDs" : "ID Column: " + customIdColumn) + "<br />" + "Hours Column: " + hoursColumn);
    }

    private ArrayList<Task> readTasks(Element element, Task current, String hoursColumn, boolean createId, String idColumn) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        ArrayList<Task> list = new ArrayList<Task>();
        int no = 1;
        for(Element task : element.children()) {
            if(task.tagName().equals("task")) {
                Task t = new Task();
                t.setParent(current);
                t.setName(task.attr("name"));
                t.setStart(LocalDate.parse(task.attr("start"), dateTimeFormatter));
                t.setEnd(t.getStart().plus(Integer.parseInt(task.attr("duration")), ChronoUnit.DAYS));
                t.setIntId(Integer.parseInt(task.attr("id")));
                if(hoursColumn == null) {
                    t.setHours(Double.parseDouble(task.attr("duration")));
                }
                if(createId) {
                    if(current == null || current.getId() == null) {
                        t.setId("" + (no ++));
                    } else {
                        t.setId(current.getId() + "." + (no++));
                    }
                }
                list.add(t);
                list.addAll(readTasks(task, t, hoursColumn, createId, idColumn));
            } else if (task.tagName().equals("customproperty")) {
                if(task.attr("taskproperty-id").equals(hoursColumn)) {
                    current.setHours(Double.parseDouble(task.attr("value")));
                } else if (task.attr("taskproperty-id").equals(idColumn)) {
                    current.setId(task.attr("value"));
                }
            } else if (task.tagName().equals("depend")) {
                current.getDepends().add(Integer.parseInt(task.attr("id")));
            }
        }
        return list;
    }

    @XAction(name = "pot_work_units_upload", requireFormEncoded = true)
    public void testFileUpload(@XUpdate Updates updates, @XParam(name = "pot_work_units_file") String file) {
        if(!file.startsWith("<?xml")) {
            updates.addMessage(Updates.Severity.WARNING, "You must upload an xml file for this to function!");
            return;
        }
        try {
            org.jsoup.nodes.Document xml = Jsoup.parse(file, "http://femoweb.at(/", Parser.xmlParser());
            String msg = "";
            Element project = xml.select("project").first();
            if(project != null) {
                msg += "Found project tag!\n";
                msg += "Project Name <" + project.attr("name") + ">\n";
            }
            Element tasks = project.select("tasks").first();
            Element taskproperties = tasks.select("taskproperties").first();
            for(Element property : taskproperties.children()) {
                if(property.attr("type").equals("custom")) {
                    msg += "Found custom column <" + property.attr("name") + ">\n";
                }
            }
            msg += "Found " + tasks.select("task").size() + " task(s)\n";
            Session.set("work-units", xml);
            updates.setPartial("pot_work_units_dialog", "project/work_units/details_dialog.xjs");
            updates.html("pot_work_units_file_disp", StringEscapeUtils.escapeHtml4(msg));
        } catch (Exception e) {
            Server.log.warn("Could not parse xml", e);
            updates.addMessage(Updates.Severity.WARNING, "XML could not be parsed");
        }
    }

    private class Task {

        private int intId;
        private Task parent;
        private String name;
        private String id;
        private LocalDate start;
        private LocalDate end;
        private double hours;
        private ArrayList<Integer> depends;

        public int getIntId() {
            return intId;
        }

        public void setIntId(int intId) {
            this.intId = intId;
        }

        public Task() {
            this.depends = new ArrayList<Integer>();
        }

        public Task getParent() {
            return parent;
        }

        public void setParent(Task parent) {
            this.parent = parent;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public LocalDate getStart() {
            return start;
        }

        public void setStart(LocalDate start) {
            this.start = start;
        }

        public LocalDate getEnd() {
            return end;
        }

        public void setEnd(LocalDate end) {
            this.end = end;
        }

        public double getHours() {
            return hours;
        }

        public void setHours(double hours) {
            this.hours = hours;
        }

        public ArrayList<Integer> getDepends() {
            return depends;
        }

        @java.lang.Override
        public String toString() {
            return "Task{" +
                    "intId=" + intId +
                    ", parent=" + (parent != null ? parent.getId() : "NULL") +
                    ", name='" + name + '\'' +
                    ", id='" + id + '\'' +
                    ", start=" + start +
                    ", end=" + end +
                    ", hours=" + hours +
                    ", depends=" + depends +
                    '}';
        }
    }

    //select task.ta_id, task.ta_name, task.ta_start, task.ta_end, task.ta_hours, parent.ta_id, parent.ta_name, p_name, u_display from ta_tasks as task left join ta_tasks as parent on task.ta_ta_parent = parent.idta_tasks inner join p_projects on task.ta_p_project = idp_projects left join u_user on task.ta_u_responsible = idu_user;
    //select task.ta_id, task.ta_name, dependency.ta_id, dependency.ta_name from de_dependencies inner join ta_tasks as task on de_ta_task = task.idta_tasks inner join ta_tasks as dependency on de_ta_dependency = dependency.idta_tasks;
}