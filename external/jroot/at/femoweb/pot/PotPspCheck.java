package at.femoweb.pot;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;

import java.lang.Override;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-psp-check")
public class PotPspCheck implements ConditionCheck {
    @Override
    public boolean check(String[] args) {
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select count(idta_tasks) from ta_tasks where ta_p_project = " + Session.get("curr-project"));
            int no = resultSet.first() ? resultSet.getInt(1) : 0;
            statement.close();
            return no > 0;
        } catch (SQLException e) {
            Server.log.warn("Could not check for work units", e);
        }
        return false;
    }
}