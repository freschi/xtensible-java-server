package at.femoweb.pot.data;

import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;

import java.lang.String;

@Entity(creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE, schema = PotSchema.class, tableName = "p_projects")
public class Project {

    @Field(name = "idp_projects")
    private int id;

    @Field(name = "p_name")
    private String name;

    @Field(name = "p_short")
    private String pshort;

    @Field(name = "p_descr")
    private String description;

    @Field(name = "p_u_creator")
    private int creator;

    @Field(name = "p_u_custodion")
    private int custodian;

    @Field(name = "p_r_reportprovider")
    private String reportProvider;

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public int getCustodian() {
        return custodian;
    }

    public void setCustodian(int custodian) {
        this.custodian = custodian;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPshort() {
        return pshort;
    }

    public void setPshort(String pshort) {
        this.pshort = pshort;
    }

    public String getReportProvider() {
        return reportProvider;
    }

    public void setReportProvider(String reportProvider) {
        this.reportProvider = reportProvider;
    }
}