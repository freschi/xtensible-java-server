package at.femoweb.pot.data;

import at.femoweb.data.core.annotations.AutoIncrement;
import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.Id;

import java.lang.String;
import java.util.Date;

@Entity(schema = PotSchema.class, creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE, tableName = "e_entries")
public class Entry {

    @Field(name = "ide_entries")
    @Id
    @AutoIncrement
    private int id;

    private Date date;

    @Field(name = "e_hours")
    private double hours;

    @Field(name = "e_descr")
    private String description;

    @Field(name = "e_t_type")
    private int type;

    @Field(name = "e_p_project")
    private int project;

    @Field(name = "e_u_user")
    private int user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getProject() {
        return project;
    }

    public void setProject(int project) {
        this.project = project;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}