package at.femoweb.pot.data;

import at.femoweb.data.core.annotations.AutoIncrement;
import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.Id;

import java.lang.String;

@Entity(tableName = "u_user", creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE, schema = PotSchema.class)
public class User {

    @Field(name = "idu_user")
    @AutoIncrement
    @Id
    private int id;

    @Field(name = "u_display")
    private String display;

    @Field(name = "u_lastname")
    private String lastname;

    @Field(name = "u_firstname")
    private String firstname;

    @Field(name = "u_email")
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}