package at.femoweb.pot.data;

import at.femoweb.data.core.annotations.AutoIncrement;
import at.femoweb.data.core.annotations.Entity;
import at.femoweb.data.core.annotations.Field;
import at.femoweb.data.core.annotations.Id;

import java.lang.String;

@Entity(schema = PotSchema.class, creationPolicy = Entity.CreationPolicy.DROP_AND_CREATE, tableName = "l_login")
public class Login {

    @Field(name = "idl_login")
    @AutoIncrement
    @Id
    private int id;

    @Field(name = "l_uname")
    private String username;

    @Field(name = "l_pass")
    private String pass;

    @Field(name = "l_u_user")
    private int user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}