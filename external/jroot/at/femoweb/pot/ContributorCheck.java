package at.femoweb.pot;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;

import java.lang.Override;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-contributor-check")
public class ContributorCheck implements ConditionCheck {
    @Override
    public boolean check(String[] args) {
        String pid, uid;
        uid = ((UnifiedUser) Session.get("_xjs_user")).getProperty("user.id");
        pid = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT w_info_perm FROM w_works WHERE w_u_user = " + uid + " AND w_p_project = " + pid);
                return resultSet.first() && resultSet.getInt(1) >= 2;
            } catch (SQLException e) {
                Server.log.warn("Could not verify project contribution", e);
            }
        }
        return false;
    }
}