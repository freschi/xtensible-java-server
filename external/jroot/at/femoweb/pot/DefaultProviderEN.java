package at.femoweb.pot;

import at.femoweb.xjs.Server;
import com.itextpdf.text.*;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.IOException;
import java.lang.Object;
import java.lang.Override;
import java.net.MalformedURLException;
import java.nio.charset.MalformedInputException;
import java.sql.Statement;


public class DefaultProviderEN extends PotReportProvider {

    public static final Font font = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.LIGHT_GRAY);

    private FontStorage fontStorage;

    public void setupFonts(FontStorage fontStorage) {
        this.fontStorage = fontStorage;
        //fontStorage.setSubtitleFont(new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.LIGHT_GRAY));
    }

    public void setupTranslation(Translation translation) {
    }

    public void styleTable(PdfPTable table) {
        table.setWidthPercentage(90);
    }

    public void setupPageEventHandler (PdfWriter writer) {
        writer.setPageEvent(new PdfPageEventHelper() {

            private String title;
            private int pagenumber;
            /*private Image logo;
            private Image stern;*/

            @Override
            public void onOpenDocument(PdfWriter writer, Document document) {
                super.onOpenDocument(writer, document);
                title = "Projektbericht - " + getProjectName();
                /*try {
                    logo = Image.getInstance("res/spg/spengerlogo.jpg");
                    logo.scaleAbsolute(100, 20);
                    stern = Image.getInstance("res/spg/stern.jpg");
                    stern.scaleAbsolute(40, 40);
                } catch (BadElementException | IOException e) {
                    Server.log.warn("Could not load spengerlogo", e);
                }*/
            }

            @Override
            public void onStartPage(PdfWriter writer, Document document) {
                super.onStartPage(writer, document);
                pagenumber++;
            }

            @Override
            public void onEndPage(PdfWriter writer, Document document) {
                super.onEndPage(writer, document);
                Rectangle rect = writer.getBoxSize("art");
                ColumnText.showTextAligned(writer.getDirectContent(),
                        Element.ALIGN_RIGHT,
                        new Phrase(String.format("%02d", pagenumber), font),
                        rect.getRight(), rect.getBottom() - 18, 0);
                ColumnText.showTextAligned(writer.getDirectContent(),
                        Element.ALIGN_RIGHT,
                        new Phrase(this.title, font),
                        rect.getRight(), rect.getTop(), 0);
                /*Chunk chLogo = new Chunk(logo, -100, -20);
                ColumnText.showTextAligned(writer.getDirectContent(),
                        Element.ALIGN_LEFT,
                        new Phrase(chLogo),
                        rect.getLeft() + logo.getScaledWidth(), rect.getBottom(), 0);
                if(pagenumber == 1) {
                    Chunk chStern = new Chunk(stern, -40, -40);
                    ColumnText.showTextAligned(writer.getDirectContent(),
                            Element.ALIGN_LEFT,
                            new Phrase(chStern),
                            rect.getLeft() + stern.getScaledWidth(), rect.getTop() + stern.getScaledHeight() / 2, 0);
                }*/
            }
        });

    }
}