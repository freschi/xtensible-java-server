package at.femoweb.pot;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BugActions {

    @XAction(name = "pot_bugs_edit")
    public void onEdit(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String id = null;
        if(jsonObject.has("bug")) {
            id = jsonObject.get("bug").getAsString();
        }
        if(id != null) {
            Connection connection = (Connection) Session.get("connection");
            try {
                Statement statement = connection.createStatement();
                Session.set("curr-bug", id);
                ResultSet resultSet = statement.executeQuery("select b_s_state, b_description, b_u_worker, b_nickname from b_bugs where idb_bugs = " + id);
                if(resultSet.first()) {
                    Server.log.info("Fetching bug info");
                    Session.set("pot.bug.curr.state", resultSet.getString(1));
                    Session.set("pot.bug.curr.descr", resultSet.getString(2));
                    Session.set("pot.bug.curr.worker", resultSet.getString(3));
                    Session.set("pot.bug.curr.name", resultSet.getString(4));
                }
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not retrieve Bug info", e);
            }
        }
        updates.setPartial("pot_bugs_content", "project/bugs/edit.xjs");
    }

    @XAction(name = "pot_bugs_delete")
    public void onDelete(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String id = null;
        if(jsonObject.has("bug")) {
            id = jsonObject.get("bug").getAsString();
        }
        if(id != null) {
            Connection connection = (Connection) Session.get("connection");
            try {
                Statement statement = connection.createStatement();
                Session.set("curr-bug", id);
                ResultSet resultSet = statement.executeQuery("select s_name, b_description, u_display, b_nickname from b_bugs inner join s_states on b_s_state = ids_states inner join u_user on b_u_worker = idu_user where idb_bugs = " + id);
                if(resultSet.first()) {
                    Server.log.info("Fetching bug info");
                    Session.set("pot.bug.curr.state", resultSet.getString(1));
                    Session.set("pot.bug.curr.descr", resultSet.getString(2));
                    Session.set("pot.bug.curr.worker", resultSet.getString(3));
                    Session.set("pot.bug.curr.name", resultSet.getString(4));
                }
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not retrieve Bug info", e);
            }
        }
        updates.setPartial("pot_bugs_content", "project/bugs/delete.xjs");
    }

    @XAction(name = "pot_bugs_dcommit")
    public void onDeleteCommit(@XUpdate Updates updates) {
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            if (Session.is("curr-bug")) {
                statement.execute("delete from b_bugs where idb_bugs = " + Session.get("curr-bug"));
            }
            statement.close();
            onBack(updates);
        } catch (SQLException e) {
            Server.log.warn("Could not commit user", e);
            updates.addMessage(Updates.Severity.WARNING, "Could not commit user " + e.getMessage());
        }
    }

    @XAction(name = "pot_bugs_add")
    public void onAdd(@XUpdate Updates updates) {
        updates.setPartial("pot_bugs_content", "project/bugs/edit.xjs");
    }

    @XAction(name = "pot_bugs_back")
    public void onBack(@XUpdate Updates updates) {
        Session.remove("pot.bug.curr.state");
        Session.remove("pot.bug.curr.descr");
        Session.remove("pot.bug.curr.worker");
        Session.remove("pot.bug.curr.name");
        Session.remove("curr-bug");
        updates.setPartial("pot_bugs_content", "project/bugs/table.xjs");
    }

    @XAction(name = "pot_bugs_commit")
    public void onCommit(@XUpdate Updates updates, @XParam(name = "pot_bugs_name") String name, @XParam(name = "pot_bugs_descr") String descr, @XParam(name = "pot_bugs_assignee") String assignee, @XParam(name = "pot_bugs_state") String state) {
        boolean ok = true;
        if(name == null || name.equals("")) {
            updates.highlightError("pot_bugs_name");
            ok = false;
        } else {
            updates.removeError("pot_bugs_name");
        }
        if(descr == null || descr.equals("")) {
            updates.highlightError("pot_bugs_descr");
            ok = false;
        } else {
            updates.removeError("pot_bugs_descr");
        }
        if(state == null || state.equals("")) {
            updates.highlightError("pot_bugs_state");
            ok = false;
        } else {
            updates.removeError("pot_bugs_state");
        }
        if(ok) {
            Connection connection = (Connection) Session.get("connection");
            try {
                Statement statement = connection.createStatement();
                if (Session.is("curr-bug")) {
                    statement.execute("update b_bugs set b_nickname = '" + name + "', b_description = '" + descr + "', b_u_worker = " + assignee + ", b_s_state = " + state + " where idb_bugs = " + Session.get("curr-bug"));
                } else {
                    String query = "insert into b_bugs (b_nickname, b_description, b_u_worker, b_s_state, b_p_project) values ('" +
                            name + "', '" + descr + "', " + assignee + ", " + state + ", " + Session.get("curr-project").toString() + ")";
                    Server.log.warn("Query: " + query);
                    statement.execute(query);
                }
                statement.close();
                onBack(updates);
            } catch (SQLException e) {
                Server.log.warn("Could not commit user", e);
                updates.addMessage(Updates.Severity.WARNING, "Could not commit user " + e.getMessage());
            }
        }
    }

}