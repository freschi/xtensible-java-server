package at.femoweb.pot;

import at.femoweb.data.core.ConnectionFactory;
import at.femoweb.data.core.DataContext;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.services.DataConnectionService;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;

import java.lang.Object;
import java.lang.Override;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Service(type = ServiceType.PERSISTENCE)
@Named("pot-connection-service")
public class PotConnectionService extends Module implements DataConnectionService {

    @Override
    public Connection connectDataSource(Properties properties) throws SQLException {
        Connection connection = null;
        if (System.getenv("OPENSHIFT_MYSQL_DB_HOST") != null) {
            String db_host = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
            String db_port = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
            String db_username = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
            String db_password = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
            connection = DriverManager.getConnection("jdbc:mysql://" + db_host + ":" + db_port + "/projektorg", db_username, db_password);
        } else {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/projektorg", "root", "");
        }
        return connection;
    }

    @Override
    public ConnectionFactory connectDataCore(Properties properties) throws SQLException {
        if (System.getenv("OPENSHIFT_MYSQL_DB_HOST") != null) {
            String db_host = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
            String db_port = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
            String db_username = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
            String db_password = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
            return DataContext.connect("jdbc:mysql://" + db_host + ":" + db_port + "/projektorg", db_username, db_password);
        } else {
            return DataContext.connect("jdbc:mysql://localhost:3306/projektorg", "root", "");
        }
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }
}