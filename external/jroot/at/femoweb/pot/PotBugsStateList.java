package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ListProvider;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Resource("pot-bugs-state-list")
public class PotBugsStateList implements ListProvider {

    @Override
    public List<ListItem> createList() {
        ArrayList<ListItem> listItems = new ArrayList<ListItem>();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select ids_states, s_name from s_states");
            Server.log.info("Reading contributor list");
            if(resultSet.first()) {
                do {
                    ListItem listItem = new ListItem();
                    String value = resultSet.getString(1);
                    listItem.setValue(value);
                    Tag span = new Tag("span");
                    span.add(resultSet.getString(2));
                    listItem.setTag(span);
                    listItems.add(listItem);
                } while (resultSet.next());
            }
        } catch (SQLException e) {
            Server.log.warn("Could not create states list", e);
        }
        return listItems;
    }
}