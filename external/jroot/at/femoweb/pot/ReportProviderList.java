package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.http.HttpRequest;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Push;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.ListProvider;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.SessionedThread;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Exception;
import java.lang.Override;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

@Resource("report-provider-list")
public class ReportProviderList implements ListProvider {

    @Override
    public List<ListItem> createList() {
        ArrayList<ListItem> items = new ArrayList<>();
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT idr_reportprovider, r_name FROM r_reportprovider");
                if(resultSet.first()) {
                    do {
                        ListItem listItem = new ListItem();
                        listItem.setValue(resultSet.getString(1).replace("_", "\\_").replace(".", "_"));
                        Tag tag = new Tag("span");
                        tag.add(resultSet.getString(2));
                        listItem.setTag(tag);
                        items.add(listItem);
                    } while (resultSet.next());
                }
            } catch (SQLException e) {
                
            }
        }
        return items;
    }
    
    @XAction(name = "pot_report_custom_qr")
    public void createCustomReportQR (@XUpdate Updates updates, @XParam(name = "pot_report_custom_from") String from, @XParam(name = "pot_report_custom_to") String to, @XParam(name = "pot_report_custom_provider") String provider) {
        boolean alright = true;
        if(from == null || from.equals("")) {
            updates.highlightError("pot_report_custom_from");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_from");
        }
        if(to == null || to.equals("")) {
            updates.highlightError("pot_report_custom_to");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_to");
        }
        if(provider == null || provider.equals("")) {
            updates.highlightError("pot_report_custom_provider");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_provider");
        }
        if(alright) {
            LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
            if (logicalUnit != null && logicalUnit.hasService("pot-report-service")) {
                Service service = logicalUnit.getService("pot-report-service");
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date _from = dateFormat.parse(from);
                    Date _to = dateFormat.parse(to);
                    HttpRequest request = (HttpRequest) Session.get("_xjs_http_request");
                    String url = "http://" + request.header("Host") + service.put(new PotReportLinkRequest(_from, _to, provider)).toString();
                    try {
                        String qr_url = "https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=" + URLEncoder.encode(url, "UTF-8");
                        updates.openPopup(qr_url);
                    } catch (Exception e) {
                        Server.log.warn("Error while encodign QR request url", e);
                    }
                } catch (ParseException e) {
                    Server.log.warn("Invalid date format", e);
                }
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Not all fields set");
        }
    }

    @XAction(name = "pot_report_custom_dl")
    public void createCustomReportDL (@XUpdate Updates updates, @XParam(name = "pot_report_custom_from") String from, @XParam(name = "pot_report_custom_to") String to, @XParam(name = "pot_report_custom_provider") String provider) {
        boolean alright = true;
        if(from == null || from.equals("")) {
            updates.highlightError("pot_report_custom_from");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_from");
        }
        if(to == null || to.equals("")) {
            updates.highlightError("pot_report_custom_to");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_to");
        }
        if(provider == null || provider.equals("")) {
            updates.highlightError("pot_report_custom_provider");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_provider");
        }
        if(alright) {
            LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
            if (logicalUnit != null && logicalUnit.hasService("pot-report-service")) {
                Service service = logicalUnit.getService("pot-report-service");
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date _from = dateFormat.parse(from);
                    Date _to = dateFormat.parse(to);
                    HttpRequest request = (HttpRequest) Session.get("_xjs_http_request");
                    String url = "http://" + request.header("Host") + service.put(new PotReportLinkRequest(_from, _to, provider)).toString();
                    updates.openPopup(url);
                } catch (ParseException e) {
                    Server.log.warn("Invalid date format", e);
                }
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Not all fields set");
        }
    }

    @XAction(name = "pot_report_custom_mail_custodians")
    public void mailCustomReportCustodians (@XUpdate Updates updates, @XParam(name = "pot_report_custom_from") String from, @XParam(name = "pot_report_custom_to") String to, @XParam(name = "pot_report_custom_provider") String provider) {
        /*boolean alright = true;
        if(from == null || from.equals("")) {
            updates.highlightError("pot_report_custom_from");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_from");
        }
        if(to == null || to.equals("")) {
            updates.highlightError("pot_report_custom_to");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_to");
        }
        if(provider == null || provider.equals("")) {
            updates.highlightError("pot_report_custom_provider");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_provider");
        }
        Date _from = new Date(Long.parseLong(from, 32));
        Date _to = new Date(Long.parseLong(to, 32));
        String project = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        Service service = ((LogicalUnit)Session.get("_xjs_logical_unit")).getService("pot-report-service");
        if(alright) {
            updates.addMessage(Updates.Severity.INFO, "Mailing reports to Custodians");
            Document document = new Document(PageSize.A4, 36, 36, 54, 54);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                PdfWriter pdfWriter = PdfWriter.getInstance(document, byteArrayOutputStream);
                pdfWriter.setBoxSize("art", new Rectangle(36, 54, 559, 788));          //Setup box sizes for page numbers and thelike
                service.put(new PotReportRequest(document, pdfWriter, _from, _to, project, provider));
            } catch (DocumentException e) {
                Server.log.warn("Could not create byte stream", e);
                try {
                    byteArrayOutputStream.write("Error while creating report".getBytes("UTF-8"));
                } catch (IOException e1) {
                    Server.log.warn("Error while preparing response", e);
                }
            }
            MimeMessage mimeMessage = getMessage();
            Address[] addresses;
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("select u_email from w_works inner join u_user on idu_user = w_u_user where w_info_perm = 1 and w_p_project = 1");
                ArrayList<Address> addressArrayList = new ArrayList<>();
                if(resultSet.first()) {
                    do {
                        try {
                            addressArrayList.add(new InternetAddress(resultSet.getString(1)));
                        } catch (AddressException e) {
                            Server.log.warn("Could not create Internet Address from " + resultSet.getString(1), e);
                        }
                    } while (resultSet.next());
                }
                addresses = addressArrayList.toArray(new Address[]{});
                Server.log.info("Recipients: " + InternetAddress.toString(addresses));
            } catch (SQLException e) {
                Server.log.warn("Could not create receipient list", e);
                updates.addMessage(Updates.Severity.DANGER, "Error while creating custodian list");
                return;
            }
            try {
                mimeMessage.setRecipients(Message.RecipientType.TO, addresses);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                mimeMessage.setSubject("Work Time Report for Project " + project + " [" + simpleDateFormat.format(_from) + " - " + simpleDateFormat.format(_to) + "]");
                mimeMessage.setText("<strong>This is a test message</strong>", "utf-8", "html");
                Transport.send(mimeMessage);
            } catch (MessagingException e) {
                Server.log.warn("Could not send message(s)", e);
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Not all fields set");
        }*/
        mailCustomReport(updates, from, to, provider, 1);
    }

    private void mailCustomReport(Updates updates, String from, String to, String provider, int level) {
        boolean alright = true;
        if(from == null || from.equals("")) {
            updates.highlightError("pot_report_custom_from");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_from");
        }
        if(to == null || to.equals("")) {
            updates.highlightError("pot_report_custom_to");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_to");
        }
        if(provider == null || provider.equals("")) {
            updates.highlightError("pot_report_custom_provider");
            alright = false;
        } else {
            updates.removeError("pot_report_custom_provider");
        }
        if(alright) {
            updates.addMessage(Updates.Severity.INFO, "Mailing reports to Creators");
            updates.push(true);
            SessionedThread thread = new SessionedThread() {
                @Override
                public void execute() {
                    try {
                        SimpleDateFormat sqlDate = new SimpleDateFormat("yyyy-MM-dd");
                        Date _from = sqlDate.parse(from);
                        Date _to = sqlDate.parse(to);
                        String project = Session.get("curr-project").toString();
                        Connection connection = (Connection) Session.get("connection");
                        Service service = ((LogicalUnit) Session.get("_xjs_logical_unit")).getService("pot-report-service");
                        Document document = new Document(PageSize.A4, 36, 36, 54, 54);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        try {
                            PdfWriter pdfWriter = PdfWriter.getInstance(document, byteArrayOutputStream);
                            pdfWriter.setBoxSize("art", new Rectangle(36, 54, 559, 788));          //Setup box sizes for page numbers and thelike
                            service.put(new PotReportRequest(document, pdfWriter, _from, _to, project, provider));
                        } catch (DocumentException e) {
                            Server.log.warn("Could not create byte stream", e);
                            try {
                                byteArrayOutputStream.write("Error while creating report".getBytes("UTF-8"));
                            } catch (IOException e1) {
                                Server.log.warn("Error while preparing response", e);
                            }
                        }
                        MimeMessage mimeMessage = getMessage();
                        Address[] addresses;
                        String projectName;
                        try (Statement statement = connection.createStatement()) {
                            ResultSet nameSet = statement.executeQuery("select p_name from p_projects where idp_projects = " + project);
                            if(nameSet.first()) {
                                projectName = nameSet.getString(1);
                            } else {
                                updates.addMessage(Updates.Severity.DANGER, "Could not fetch project name");
                                return;
                            }
                            ResultSet resultSet = statement.executeQuery("select u_email from w_works inner join u_user on idu_user = w_u_user where w_info_perm = " + level + " and w_p_project = " + project);
                            ArrayList<Address> addressArrayList = new ArrayList<>();
                            if (resultSet.first()) {
                                do {
                                    try {
                                        addressArrayList.add(new InternetAddress(resultSet.getString(1)));
                                    } catch (AddressException e) {
                                        Server.log.warn("Could not create Internet Address from " + resultSet.getString(1), e);
                                    }
                                } while (resultSet.next());
                            }
                            addresses = addressArrayList.toArray(new Address[]{});
                            Server.log.debug("Recipients: " + InternetAddress.toString(addresses));
                        } catch (SQLException e) {
                            Server.log.warn("Could not create receipient list", e);
                            updates.addMessage(Updates.Severity.DANGER, "Error while creating custodian list");
                            return;
                        }
                        try {
                            mimeMessage.setRecipients(Message.RecipientType.TO, addresses);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                            mimeMessage.setSubject("[INFO] Work Time Report for " + projectName + " (" + simpleDateFormat.format(_from) + " - " + simpleDateFormat.format(_to) + ")");
                            Multipart multipart = new MimeMultipart();
                            BodyPart bodyPart = new MimeBodyPart();
                            bodyPart.setText(createMessage(projectName, _from, _to, ((UnifiedUser) Session.get("_xjs_user")).getUsername(), simpleDateFormat).render());
                            bodyPart.addHeader("Content-Type", "text/html");
                            multipart.addBodyPart(bodyPart);
                            InternetHeaders headers = new InternetHeaders();
                            headers.addHeader("Content-Type", "application/pdf");
                            ByteArrayDataSource dataSource = new ByteArrayDataSource(byteArrayOutputStream.toByteArray(), "application/pdf");
                            dataSource.setName("report_" + projectName.toLowerCase().replace(" ", "_") + ".pdf");
                            MimeBodyPart attachment = new MimeBodyPart();
                            attachment.setDataHandler(new DataHandler(dataSource));
                            attachment.setFileName(dataSource.getName());
                            multipart.addBodyPart(attachment);
                            mimeMessage.setContent(multipart);
                            Server.log.debug("Forwarding messages to server");
                            Transport.send(mimeMessage);
                            Server.log.debug("Sending complete");
                            Push.get().addMessage(Updates.Severity.SUCCESS, "Messages sent!");
                            Push.get().push(false);
                        } catch (MessagingException e) {
                            Server.log.warn("Could not send message(s)", e);
                        }
                    } catch (ParseException e) {
                        Server.log.warn("Could not parse date(s)", e);
                    }
                }
            };
            thread.start();
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Not all fields set");
        }
    }

    @XAction(name = "pot_report_whole_dl")
    public void downloadWholeReport(@XUpdate Updates updates) {
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select min(e_date), max(e_date), p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = resultSet.getString(1);
                to = resultSet.getString(2);
                provider = resultSet.getString(3);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            if(provider == null || provider.equals("")) {
                updates.addMessage(Updates.Severity.WARNING, "No default provider set for this project. Please set one in Project Settings.");
            } else {
                createCustomReportDL(updates, from, to, provider);
            }
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    @XAction(name = "pot_report_whole_qr")
    public void createWholeReportQR(@XUpdate Updates updates) {
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select min(e_date), max(e_date), p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = resultSet.getString(1);
                to = resultSet.getString(2);
                provider = resultSet.getString(3);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            createCustomReportQR(updates, from, to, provider);
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    @XAction(name = "pot_report_whole_mail_custodians")
    public void mailWholeReportCustodians(@XUpdate Updates updates) {
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select min(e_date), max(e_date), p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = resultSet.getString(1);
                to = resultSet.getString(2);
                provider = resultSet.getString(3);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            mailCustomReport(updates, from, to, provider, 1);
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    @XAction(name = "pot_report_whole_mail_creators")
    public void mailWholeReportCreators(@XUpdate Updates updates) {
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select min(e_date), max(e_date), p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = resultSet.getString(1);
                to = resultSet.getString(2);
                provider = resultSet.getString(3);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            mailCustomReport(updates, from, to, provider, 3);
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    @XAction(name = "pot_report_lweek_dl")
    public void createLWeekReportDL(@XUpdate Updates updates) {
        String[] dates = createDatesLWeek();
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = dates[0];
                to = dates[1];
                provider = resultSet.getString(1);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            createCustomReportDL(updates, from, to, provider);
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    @XAction(name = "pot_report_lweek_qr")
    public void createLWeekReportQR(@XUpdate Updates updates) {
        String[] dates = createDatesLWeek();
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = dates[0];
                to = dates[1];
                provider = resultSet.getString(1);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            createCustomReportQR(updates, from, to, provider);
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    @XAction(name = "pot_report_lweek_mail_custodians")
    public void mailLWeekReportCustodians(@XUpdate Updates updates) {
        String[] dates = createDatesLWeek();
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = dates[0];
                to = dates[1];
                provider = resultSet.getString(1);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            mailCustomReport(updates, from, to, provider, 1);
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    @XAction(name = "pot_report_lweek_mail_creators")
    public void mailLWeekReportCreators(@XUpdate Updates updates) {
        String[] dates = createDatesLWeek();
        String id = Session.get("curr-project").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            String from, to, provider;
            ResultSet resultSet = statement.executeQuery("select p_r_reportprovider from e_entries inner join p_projects on e_p_project = idp_projects where e_p_project = " + id);
            if(resultSet.first()) {
                from = dates[0];
                to = dates[1];
                provider = resultSet.getString(1);
            } else {
                updates.addMessage(Updates.Severity.WARNING, "Could not fetch data for report");
                return;
            }
            mailCustomReport(updates, from, to, provider, 3);
        } catch (SQLException e) {
            Server.log.warn("Could not fetch information to build report", e);
        }
    }

    private String[] createDatesLWeek() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar start = new GregorianCalendar();
        GregorianCalendar end = new GregorianCalendar();
        start.setFirstDayOfWeek(Calendar.MONDAY);
        start.set(Calendar.WEEK_OF_YEAR, start.get(Calendar.WEEK_OF_YEAR) - 1);
        start.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        end.setFirstDayOfWeek(Calendar.MONDAY);
        end.set(Calendar.WEEK_OF_YEAR, end.get(Calendar.WEEK_OF_YEAR) - 1);
        end.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return new String[] {simpleDateFormat.format(start.getTime()), simpleDateFormat.format(end.getTime())};
    }

    @XAction(name = "pot_report_custom_mail_creators")
    public void mailCustomReportCreators (@XUpdate Updates updates, @XParam(name = "pot_report_custom_from") String from, @XParam(name = "pot_report_custom_to") String to, @XParam(name = "pot_report_custom_provider") String provider) {
        mailCustomReport(updates, from, to, provider, 3);
    }

    private Tag createMessage(String projectName, Date _from, Date _to, String user, DateFormat format) throws ParseException {
        Tag msg = new Tag("body");
        Tag heading = new Tag("h1");
        heading.add(projectName);
        msg.add(heading);
        Tag message = new Tag("span");
        message.add("This is an automatically sent email, requested by " + user);
        msg.add(message);
        Tag detailHeading = new Tag("h3");
        detailHeading.add("Details");
        msg.add(detailHeading);
        Tag details = new Tag("div");
        Tag table = new Tag("table");
        {
            Tag userRow = new Tag("tr");
            Tag userStronk = new Tag("th");
            userStronk.add("Requested by");
            userRow.add(userStronk);
            Tag userName = new Tag("td");
            userName.add(user);
            userRow.add(userName);
            table.add(userRow);
        }
        {
            Tag dateRow = new Tag("tr");
            Tag dateStronk = new Tag("th");
            dateStronk.add("Request date");
            dateRow.add(dateStronk);
            Tag date = new Tag("td");
            date.add(format.format(new Date()));
            dateRow.add(date);
            table.add(dateRow);
        }
        {
            Tag projectRow = new Tag("tr");
            Tag projectStronk = new Tag("th");
            projectStronk.add("Project");
            projectRow.add(projectStronk);
            Tag project = new Tag("td");
            project.add(projectName);
            projectRow.add(project);
            table.add(projectRow);
        }
        {
            Tag fromRow = new Tag("tr");
            Tag fromStronk = new Tag("th");
            fromStronk.add("Begin Date");
            fromRow.add(fromStronk);
            Tag fromText = new Tag("td");
            fromText.add(format.format(_from));
            fromRow.add(fromText);
            table.add(fromRow);
        }
        {
            Tag toRow = new Tag("tr");
            Tag toStronk = new Tag("th");
            toStronk.add("End Date");
            toRow.add(toStronk);
            Tag toText = new Tag("td");
            toText.add(format.format(_to));
            toRow.add(toText);
            table.add(toRow);
        }
        details.add(table);
        msg.add(details);
        msg.add(new Tag("hr"));
        message = new Tag("small");
        message.attribute("style", "color: light-gray");
        message.add("Please do not respond to this email address. If you received this email unwillingly please contact the");
        Tag webmaster = new Tag("a");
        webmaster.attribute("href", "mailto:office@femo.io");
        webmaster.add("webmaster");
        message.add(webmaster);
        msg.add(message);
        return msg;
    }

    private MimeMessage getMessage() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "alfa3026.alfahosting-server.de");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        javax.mail.Session session = javax.mail.Session.getInstance(props,
                new Authenticator() {
                    @Override
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication("web1448p5", "MpZsOZSr9RO5");
                    }
                });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setSentDate(new Date());
            message.setFrom(new InternetAddress("noreply@femo.io"));
            return message;
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }
}