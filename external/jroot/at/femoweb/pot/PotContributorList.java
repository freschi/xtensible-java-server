package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ListProvider;

import java.lang.Override;
import java.lang.Throwable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Resource("pot-contributor-list")
public class PotContributorList implements ListProvider {

    @Override
    public List<ListItem> createList() {
        ArrayList<ListItem> listItems = new ArrayList<ListItem>();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select w_u_user, u_display, w_diary_perm from w_works inner join u_user on w_u_user = idu_user where w_diary_perm > 1 and w_p_project = " + Session.get("curr-project").toString() + " order by w_diary_perm desc, u_lastname asc, u_firstname asc");
            Server.log.info("Reading contributor list");
            if(resultSet.first()) {
                do {
                    ListItem listItem = new ListItem();
                    String value = resultSet.getString(1);
                    listItem.setValue(value);
                    Tag span = new Tag("span");
                    Tag icon = new Tag("i");
                    if(resultSet.getInt(3) == 2) {
                        icon.attribute("class", "fa fa-user");
                    } else {
                        icon.attribute("class", "fa fa-star");
                    }
                    span.add(icon);
                    span.add(resultSet.getString(2));
                    listItem.setTag(span);
                    listItems.add(listItem);
                } while (resultSet.next());
            }
        } catch (SQLException e) {
            Server.log.warn("Could not create contributor list", e);
        }
        return listItems;
    }
}