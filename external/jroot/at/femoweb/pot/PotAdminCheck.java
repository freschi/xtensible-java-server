package at.femoweb.pot;

import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;

import java.lang.Override;
import java.lang.String;

@Resource("pot-admin-check")
public class PotAdminCheck implements ConditionCheck {

    @Override
    public boolean check(String[] args) {
        UnifiedUser unifiedUser = (UnifiedUser) Session.get("_xjs_user");
        return unifiedUser.getLogin().equals("felix");
    }
}