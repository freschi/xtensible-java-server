package at.femoweb.pot;

import at.femoweb.xjs.Server;
import com.itextpdf.text.*;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.awt.*;
import java.awt.SecondaryLoop;
import java.lang.Object;
import java.lang.String;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public abstract class PotReportProvider {

    private Statement statement;
    private FontStorage fontStorage;
    private PotReportRequest request;
    private Translation translation;
    private SimpleDateFormat sqlFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static final int REPORT_TYPE_DIARY = 0;
    public static final int REPORT_TYPE_WORK_UNITS = 1;

    private int reportType;

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public int getReportType() {
        return this.reportType;
    }

    float bold = 1.5f;
    float normal = 0.5f;
    float none = 0.f;


    public void setTranslation(at.femoweb.pot.PotReportProvider.Translation translation) {
        this.translation = translation;
    }

    public PotReportProvider () {

    }

    public void setup () {
        fontStorage = new FontStorage();
        translation = new Translation();
        setupFonts(fontStorage);
        setupTranslation(translation);
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public void setRequest(PotReportRequest request) {
        this.request = request;
    }

    public final void createTitlePage(Document document) throws DocumentException {
        String prjName, prjShort, prjDescr;
        try {
            ResultSet resultSet = statement.executeQuery("SELECT p_name, p_short, p_descr FROM p_projects WHERE idp_projects = " + request.getId());
            if(resultSet.first()) {
                prjName = resultSet.getString(1);
                prjShort = resultSet.getString(2);
                prjDescr = resultSet.getString(3);
            } else {
                prjName = "Error";
                prjShort = "Err";
                prjDescr = "No project found!";
            }
        } catch (SQLException e) {
            Server.log.warn("Error while creating title page", e);
            prjName = "Error";
            prjShort = "Err";
            prjDescr = e.getMessage();
        }
        Paragraph title = new Paragraph(prjShort + " - " + prjName, fontStorage.getTitleFont());
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);
        SimpleDateFormat format = new SimpleDateFormat(translation.getStrDateFormat());
        Paragraph subtitle = new Paragraph(format.format(request.getFrom()) + " - " + format.format(request.getTo()), fontStorage.getSubtitleFont());
        subtitle.setAlignment(Element.ALIGN_CENTER);
        document.add(subtitle);
        Paragraph subsubtitle = new Paragraph(translation.getStrCreatedOn() + " " + new Date().toString(), fontStorage.getSubsubtitleFont());
        subsubtitle.setAlignment(Element.ALIGN_CENTER);
        document.add(subsubtitle);
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph(prjDescr, fontStorage.getStandardFont()));
        Paragraph memberSection = new Paragraph(translation.getStrMembers(), fontStorage.getSectionFont());
        memberSection.setSpacingAfter(10);
        document.add(memberSection);
        try {
            ResultSet resultSet = statement.executeQuery("select u_display from w_works inner join u_user on w_u_user = idu_user where w_p_project = " + request.getId() + " and w_info_perm >= 2 order by u_lastname, u_firstname");
            List members = new List(false, false, 10);
            if(resultSet.first()) {
                do {
                    members.add(new ListItem(resultSet.getString(1), fontStorage.getListItemFont()));
                } while (resultSet.next());
            }
            document.add(members);
        } catch (SQLException e) {
            Server.log.warn("Could not create project memeber list", e);
        }
        Paragraph summarySection = new Paragraph(translation.getStrSummary(), fontStorage.getSectionFont());
        summarySection.setSpacingAfter(10);
        document.add(summarySection);
        try {
            ResultSet resultSet = statement.executeQuery("select t_name, idt_types from e_entries inner join t_types on e_t_type = idt_types where e_p_project = " + request.getId() +" and e_date >= '" + sqlFormat.format(request.getFrom()) +"' and e_date <= '" + sqlFormat.format(request.getTo()) + "' group by e_t_type");
            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> ids = new ArrayList<>();
            if(resultSet.first()) {
                do {
                    names.add(resultSet.getString(1));
                    ids.add(resultSet.getString(2));
                } while (resultSet.next());
            }
            resultSet = statement.executeQuery("select u_display, sum(e_hours), idu_user from e_entries inner join t_types on e_t_type = idt_types inner join u_user on e_u_user = idu_user where e_p_project = " + request.getId() +" and e_date >= '" + sqlFormat.format(request.getFrom()) +"' and e_date <= '" + sqlFormat.format(request.getTo()) + "' group by idu_user order by u_lastname, u_firstname");
            PdfPTable table = new PdfPTable(2 + names.size());
            PdfPCell pCell = new PdfPCell(new Phrase(translation.getStrMember(), fontStorage.getTableHeaderFont()));
            pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            //top, right, bottom, left
            styleCell(pCell, bold, none, none, bold);
            table.addCell(pCell);
            for(String name : names) {
                PdfPCell cell = new PdfPCell(new Phrase(name, fontStorage.getTableHeaderFont()));
                if(names.size() >= 4) {
                    cell.setRotation(90);
                }
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                styleCell(cell, bold, none, none, normal);
                table.addCell(cell);
            }
            pCell = new PdfPCell(new Phrase(translation.getStrTotal(), fontStorage.getTableHeaderFont()));
            pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            styleCell(pCell, bold, bold, none, normal);
            table.addCell(pCell);
            table.setHeaderRows(1);
            ArrayList<UserInfo> userInfos = new ArrayList<>();
            if(resultSet.first()) {
                do {
                    UserInfo info = new UserInfo();
                    info.setUname(resultSet.getString(1));
                    info.setHours(resultSet.getDouble(2));
                    info.setUid(resultSet.getString(3));
                    userInfos.add(info);
                } while (resultSet.next());
            }
            for(UserInfo userInfo : userInfos) {
                pCell = new PdfPCell(new Phrase(userInfo.getUname(), fontStorage.getTableHeaderFont()));
                styleCell(pCell, normal, none, none, bold);
                table.addCell(pCell);
                for(String id : ids) {
                    PdfPCell cell = new PdfPCell(new Phrase(formatHour(userInfo.getTypeHours(id)), fontStorage.getTableCellFont()));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    styleCell(pCell, normal, none, none, normal);
                    table.addCell(cell);
                }
                PdfPCell tcell = new PdfPCell(new Phrase(formatHour(userInfo.getHours()), fontStorage.getTableHeaderFont()));
                tcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                styleCell(tcell, normal, bold, none, normal);
                table.addCell(tcell);
                table.completeRow();
            }
            pCell = new PdfPCell(new Phrase(translation.getStrTotal(), fontStorage.getTableHeaderFont()));
            styleCell(pCell, bold, none, bold, bold);
            table.addCell(pCell);
            for(String id : ids) {
                resultSet = statement.executeQuery("select sum(e_hours) from e_entries where e_t_type = " + id + " and e_p_project = " + request.getId() +" and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "'");
                if(resultSet.first()) {
                    PdfPCell cell = new PdfPCell(new Phrase(formatHour(resultSet.getDouble(1)), fontStorage.getTableHeaderFont()));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    styleCell(cell, bold, none, bold, normal);
                    table.addCell(cell);
                } else {
                    table.addCell("");
                }
            }
            resultSet = statement.executeQuery("select sum(e_hours) from e_entries where e_p_project = " + request.getId() +" and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "';");
            if(resultSet.first()) {
                PdfPCell cell = new PdfPCell(new Phrase(formatHour(resultSet.getDouble(1)), fontStorage.getTableHeaderFont()));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                styleCell(cell, bold, bold, bold, normal);
                table.addCell(cell);
            } else {
                PdfPCell cell = new PdfPCell(new Phrase("---", fontStorage.getTableHeaderFont()));
                styleCell(cell, bold, bold, bold, normal);
                table.addCell(cell);
            }
            table.completeRow();
            styleTable(table);
            document.add(table);
        } catch (SQLException e) {
            Server.log.warn("Could not generate work time summary", e);
        }
    }
    
    public final void createUserPages(Document document) throws DocumentException {
        try {
            ResultSet resultSet = statement.executeQuery("select idu_user, u_display from e_entries inner join u_user on e_u_user = idu_user where e_p_project = " + request.getId() + " and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "' group by idu_user order by u_lastname, u_firstname");
            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> ids = new ArrayList<>();
            if (resultSet.first()) {
                do {
                    ids.add(resultSet.getString(1));
                    names.add(resultSet.getString(2));
                } while (resultSet.next());
            } else {
                return;
            }
            Server.log.debug("Found " + ids.size() + " matching users");
            for (int i = 0; i < ids.size(); i++) {
                createUserPage(document, ids.get(i), names.get(i), i);
            }
        } catch (SQLException e) {
            Server.log.warn("Could not generate user pages", e);
        }
    }
    
    private final void createUserPage (Document document, String id, String name, int index) throws DocumentException, SQLException {
        Chapter chapter = new Chapter(new Paragraph(translation.getStrWorkReport() + " " + name, fontStorage.getChapterFont()), index + 1);
        chapter.setNumberDepth(0);
        chapter.setBookmarkTitle(name);
        Section summary = chapter.addSection(new Paragraph(translation.getStrSumry(), fontStorage.getSectionFont()), 1);
        summary.setNumberDepth(0);
        createUserPageSummary(summary, id);
        Section details = chapter.addSection(new Paragraph(translation.getStrDetails(), fontStorage.getSectionFont()), 2);
        details.setNumberDepth(0);
        createUserPageDetail(details, id);
        document.add(chapter);
        document.newPage();
    }
    
    private final void createUserPageSummary(Section section, String id) throws DocumentException, SQLException {
        PdfPTable table = new PdfPTable(4);
        table.setSpacingBefore(10);
        table.setHeaderRows(1);
        PdfPCell cell = new PdfPCell(new Phrase(""));
        styleCell(cell, bold, normal, none, bold);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(translation.getStrLastWeek(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, normal, none, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(translation.getStrThisWeek(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, normal, none, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(translation.getStrTotal(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, bold, none, none);
        table.addCell(cell);
        Calendar from = Calendar.getInstance();
        from.setTime(request.getFrom());
        Calendar to = Calendar.getInstance();
        to.setTime(request.getTo());
        if(weekApart(from, to)) {
            
        } else {
            ResultSet resultSet = statement.executeQuery("select t_name, sum(e_hours) from e_entries inner join t_types on idt_types = e_t_type where e_u_user = " + id + " and e_p_project = " + request.getId() + " and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "' group by e_t_type order by sum(e_hours) desc;");
            double total = 0;
            if(resultSet.first()) {
                do {
                    cell = new PdfPCell(new Phrase(resultSet.getString(1), fontStorage.getTableHeaderFont()));
                    styleCell(cell, normal, normal, none, bold);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase("---", fontStorage.getTableCellFont()));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    styleCell(cell, normal, normal, none, none);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase("---", fontStorage.getTableCellFont()));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    styleCell(cell, normal, normal, none, none);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(formatHour(resultSet.getDouble(2)), fontStorage.getTableHeaderFont()));
                    total += resultSet.getDouble(2);
                    styleCell(cell, normal, bold, none, none);
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);
                } while (resultSet.next());
            }
            cell = new PdfPCell(new Phrase(translation.getStrTotal(), fontStorage.getTableHeaderFont()));
            styleCell(cell, bold, normal, bold, bold);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("---", fontStorage.getTableHeaderFont()));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            styleCell(cell, bold, normal, bold, none);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("---", fontStorage.getTableHeaderFont()));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            styleCell(cell, bold, normal, bold, none);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(formatHour(total), fontStorage.getTableHeaderFont()));
            styleCell(cell, bold, bold, bold, none);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);
        }
        styleTable(table);
        section.add(table);
        ResultSet result = statement.executeQuery("select sum(e_hours) from e_entries where e_inschool <> 0 and e_u_user = " + id + " and e_p_project = " + request.getId() + " and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "'");
        if(result.first() && result.getString(1) != null) {
            Paragraph paragraph = new Paragraph();
            paragraph.add(new Chunk(translation.getStrWorkedLessons() + " ", fontStorage.getBoldSmallFont()));
            paragraph.add(new Chunk(formatHour(result.getDouble(1)), fontStorage.getStandardSmallFont()));
            section.add(paragraph);
        }
        result = statement.executeQuery("select sum(e_hours) from e_entries where e_inschool = 0 and e_u_user = " + id + " and e_p_project = " + request.getId() + " and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "'");
        if(result.first() && result.getString(1) != null) {
            Paragraph paragraph = new Paragraph();
            paragraph.add(new Chunk(translation.getStrWorkedPrivate() + " ", fontStorage.getBoldSmallFont()));
            paragraph.add(new Chunk(formatHour(result.getDouble(1)), fontStorage.getStandardSmallFont()));
            section.add(paragraph);
        }
        result = statement.executeQuery("select ta_id, ta_name, sum(e_hours) from e_entries inner join ta_tasks on e_ta_task = idta_tasks where not e_ta_task is NULL and e_u_user = " + id + " and e_p_project = " + request.getId() + " and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "' group by e_ta_task");
        if(result.first()) {
            PdfPTable pTable = new PdfPTable(3);
            PdfPCell hcell = new PdfPCell(new Phrase(translation.getStrWorkUnitId(), fontStorage.getTableHeaderFont()));
            styleCell(hcell, bold, none, none, bold);
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pTable.addCell(hcell);
            hcell = new PdfPCell(new Phrase(translation.getStrWorkUnitName(), fontStorage.getTableHeaderFont()));
            styleCell(hcell, bold, none, none, normal);
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pTable.addCell(hcell);
            hcell = new PdfPCell(new Phrase(translation.getStrHours(), fontStorage.getTableHeaderFont()));
            styleCell(hcell, bold, bold, none, normal);
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            pTable.addCell(hcell);
            boolean hasNext = true;
            do {
                String wuid, name, hours;
                wuid = result.getString(1);
                name = result.getString(2);
                hours = formatHour(result.getDouble(3));
                hasNext = result.next();
                PdfPCell scell = new PdfPCell(new Phrase(wuid, fontStorage.getTableCellFont()));
                styleCell(scell, normal, none, hasNext ? none : bold, bold);
                pTable.addCell(scell);
                scell = new PdfPCell(new Phrase(name, fontStorage.getTableCellFont()));
                styleCell(scell, normal, none, hasNext ? none : bold, normal);
                pTable.addCell(scell);
                scell = new PdfPCell(new Phrase(hours, fontStorage.getTableCellFont()));
                styleCell(scell, normal, bold, hasNext ? none : bold, normal);
                scell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                pTable.addCell(scell);
            } while (hasNext);
            pTable.setSpacingBefore(10f);
            pTable.setWidths(new float[]{0.2f, 0.6f, 0.2f});
            styleTable(pTable);
            section.add(pTable);
        }
    }
    
    private boolean weekApart(Calendar from, Calendar to) {
        return false;
    }
    
    private final void createUserPageDetail(Section section, String id) throws DocumentException, SQLException {
        PdfPTable table = new PdfPTable(5);
        table.setSpacingBefore(10);
        table.setHeaderRows(1);
        float bold = 1.5f;
        float normal = 0.5f;
        float none = 0.f;
        PdfPCell cell = new PdfPCell(new Phrase(translation.getStrDate(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, normal, none, bold);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(translation.getStrWorkUnit(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, normal, none, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(translation.getStrDescription(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, normal, none, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(translation.getStrHours(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, normal, none, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(translation.getStrType(), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        styleCell(cell, bold, bold, none, none);
        table.addCell(cell);
        ResultSet resultSet = statement.executeQuery("select e_date, e_descr, e_hours, t_name, ta_id from e_entries inner join t_types on e_t_type = idt_types left join ta_tasks on e_ta_task = idta_tasks where e_u_user = " + id + " and e_p_project = " + request.getId() + " and e_date >= '" + sqlFormat.format(request.getFrom()) + "' and e_date <= '" + sqlFormat.format(request.getTo()) + "' order by e_date desc");
        double total = 0;
        if(resultSet.first()) {
            SimpleDateFormat format = new SimpleDateFormat(translation.getStrDateFormat());
            do {
                cell = new PdfPCell(new Phrase(format.format(resultSet.getDate(1)), fontStorage.getTableCellFont()));
                styleCell(cell, normal, normal, none, bold);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(resultSet.getString(5), fontStorage.getTableCellFont()));
                styleCell(cell, normal, normal, none, none);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(resultSet.getString(2), fontStorage.getTableCellFont()));
                styleCell(cell, normal, normal, none, none);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(formatHour(resultSet.getDouble(3)), fontStorage.getTableCellFont()));
                total += resultSet.getDouble(3);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                styleCell(cell, normal, normal, none, none);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(resultSet.getString(4), fontStorage.getTableCellFont()));
                styleCell(cell, normal, bold, none, none);
                table.addCell(cell);
            } while (resultSet.next());
        }
        cell = new PdfPCell(new Phrase(translation.getStrTotal(), fontStorage.getTableHeaderFont()));
        styleCell(cell, bold, normal, bold, bold);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(""));
        styleCell(cell, bold, normal, bold, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(""));
        styleCell(cell, bold, normal, bold, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(formatHour(total), fontStorage.getTableHeaderFont()));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        styleCell(cell, bold, normal, bold, none);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(""));
        styleCell(cell, bold, bold, bold, none);
        table.addCell(cell);
        styleTable(table);
        table.setWidths(new float[] {16f, 15f, 45f, 13f, 21f});
        section.add(table);
    }
    
    public void styleCell(PdfPCell cell, float top, float right, float bottom, float left) {
        cell.setBorderWidthTop(top);
        cell.setBorderWidthRight(right);
        cell.setBorderWidthBottom(bottom);
        cell.setBorderWidthLeft(left);
    }

    public String formatHour(double value) {
        return value == 0 ? "" : String.format(translation.getStrHourFormat(), value);
    }
    
    protected String getProjectName() {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT p_name FROM p_projects WHERE idp_projects = " + request.getId());
            if(resultSet.first()) {
                return resultSet.getString(1);
            } else {
                return null;
            }
        } catch (SQLException e) {
            Server.log.warn("Could not load Project name", e);
            return null;
        }
        
    }

    public abstract void setupFonts(FontStorage fontStorage);
    public abstract void setupTranslation(Translation translation);
    public abstract void styleTable(PdfPTable table);
    public abstract void setupPageEventHandler(PdfWriter writer);

    public static class FontStorage {

        private Font standardFont = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);
        private Font titleFont = new Font(Font.FontFamily.HELVETICA, 32, Font.BOLD);
        private Font subtitleFont = new Font(Font.FontFamily.HELVETICA, 22, Font.BOLD, BaseColor.GRAY);
        private Font subsubtitleFont = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.GRAY);
        private Font chapterFont = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
        private Font sectionFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
        private Font boldFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        private Font listItemFont = standardFont;
        private Font tableHeaderFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        private Font tableCellFont = standardFont;
        private Font standardSmallFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
        private Font boldSmallFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
        private Font tableCellPositiveFont = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.GREEN);

        public Font getTableCellPositiveFont() {
            return tableCellPositiveFont;
        }

        public void setTableCellPositiveFont(Font tableCellPositiveFont) {
            this.tableCellPositiveFont = tableCellPositiveFont;
        }

        public Font getBoldSmallFont() {
            return boldSmallFont;
        }

        public void setBoldSmallFont(Font boldSmallFont) {
            this.boldSmallFont = boldSmallFont;
        }

        public Font getStandardSmallFont() {
            return standardSmallFont;
        }

        public void setStandardSmallFont(Font standardSmallFont) {
            this.standardSmallFont = standardSmallFont;
        }

        public Font getBoldFont() {
            return boldFont;
        }

        public void setBoldFont(Font boldFont) {
            this.boldFont = boldFont;
        }

        public Font getStandardFont() {
            return standardFont;
        }

        public void setStandardFont(Font standardFont) {
            this.standardFont = standardFont;
        }

        public Font getTitleFont() {
            return titleFont;
        }

        public void setTitleFont(Font titleFont) {
            this.titleFont = titleFont;
        }

        public Font getSubtitleFont() {
            return subtitleFont;
        }

        public void setSubtitleFont(Font subtitleFont) {
            this.subtitleFont = subtitleFont;
        }

        public Font getSubsubtitleFont() {
            return subsubtitleFont;
        }

        public void setSubsubtitleFont(Font subsubtitleFont) {
            this.subsubtitleFont = subsubtitleFont;
        }

        public Font getSectionFont() {
            return sectionFont;
        }

        public void setSectionFont(Font sectionFont) {
            this.sectionFont = sectionFont;
        }

        public Font getListItemFont() {
            return listItemFont;
        }

        public void setListItemFont(Font listItemFont) {
            this.listItemFont = listItemFont;
        }

        public Font getTableHeaderFont() {
            return tableHeaderFont;
        }

        public void setTableHeaderFont(Font tableHeaderFont) {
            this.tableHeaderFont = tableHeaderFont;
        }

        public Font getTableCellFont() {
            return tableCellFont;
        }

        public void setTableCellFont(Font tableCellFont) {
            this.tableCellFont = tableCellFont;
        }

        public Font getChapterFont() {
            return chapterFont;
        }

        public void setChapterFont(Font chapterFont) {
            this.chapterFont = chapterFont;
        }
    }

    public static class Translation {

        private String strDateFormat = "dd.MM.yyyy";
        private String strCreatedOn = "Report generated ";
        private String strMembers = "Members";
        private String strSummary = "Working Time Summary";
        private String strMember = "Member";
        private String strTotal = "Total";
        private String strHourFormat = "%.2f h";
        private String strWorkReport = "Working Time Record";
        private String strDetails = "Details";
        private String strSumry = "Summary";
        private String strLastWeek = "Last Week";
        private String strThisWeek = "This Week";
        private String strDate = "Date";
        private String strDescription = "Description";
        private String strHours = "Hours";
        private String strType = "Type";
        private String strWorkedLessons = "Hours worked during lessons";
        private String strWorkedPrivate = "Hours worked in free time";
        private String strWorkUnit = "Work Unit";
        private String strWorkUnitId = "Work Unit ID";
        private String strWorkUnitName = "Work Unit Name";
        private String strWorkUnitReport = "Work Unit Report";
        private String strResponsible = "Responsible";
        private String strPlanHours = "Plan Hours";
        private String strWorkedHours = "Worked Hours";
        private String strDueDate = "Due Date";
        private String strDifference = "Difference";
        private String strDependencies = "Dependencies";
        private String strRequiredFor = "Required For";
        private String strDependencyProblems = "Dependency Problems";

        public String getStrDependencyProblems() {
            return strDependencyProblems;
        }

        public void setStrDependencyProblems(String strDependencyProblems) {
            this.strDependencyProblems = strDependencyProblems;
        }

        public String getStrDependencies() {
            return strDependencies;
        }

        public void setStrDependencies(String strDependencies) {
            this.strDependencies = strDependencies;
        }

        public String getStrRequiredFor() {
            return strRequiredFor;
        }

        public void setStrRequiredFor(String strRequiredFor) {
            this.strRequiredFor = strRequiredFor;
        }

        public String getStrDifference() {
            return strDifference;
        }

        public void setStrDifference(String strDifference) {
            this.strDifference = strDifference;
        }

        public String getStrDueDate() {
            return strDueDate;
        }

        public void setStrDueDate(String strDueDate) {
            this.strDueDate = strDueDate;
        }

        public String getStrWorkedHours() {
            return strWorkedHours;
        }

        public void setStrWorkedHours(String strWorkedHours) {
            this.strWorkedHours = strWorkedHours;
        }

        public String getStrPlanHours() {
            return strPlanHours;
        }

        public void setStrPlanHours(String strPlanHours) {
            this.strPlanHours = strPlanHours;
        }

        public String getStrResponsible() {
            return strResponsible;
        }

        public void setStrResponsible(String strResponsible) {
            this.strResponsible = strResponsible;
        }

        public String getStrWorkUnitReport() {
            return strWorkUnitReport;
        }

        public void setStrWorkUnitReport(String strWorkUnitReport) {
            this.strWorkUnitReport = strWorkUnitReport;
        }

        public String getStrWorkUnit() {
            return strWorkUnit;
        }

        public void setStrWorkUnit(String strWorkUnit) {
            this.strWorkUnit = strWorkUnit;
        }

        public String getStrWorkUnitId() {
            return strWorkUnitId;
        }

        public void setStrWorkUnitId(String strWorkUnitId) {
            this.strWorkUnitId = strWorkUnitId;
        }

        public String getStrWorkUnitName() {
            return strWorkUnitName;
        }

        public void setStrWorkUnitName(String strWorkUnitName) {
            this.strWorkUnitName = strWorkUnitName;
        }

        public String getStrWorkedLessons() {
            return strWorkedLessons;
        }

        public void setStrWorkedLessons(String strWorkedLessons) {
            this.strWorkedLessons = strWorkedLessons;
        }

        public String getStrWorkedPrivate() {
            return strWorkedPrivate;
        }

        public void setStrWorkedPrivate(String strWorkedPrivate) {
            this.strWorkedPrivate = strWorkedPrivate;
        }

        public String getStrDateFormat() {
            return strDateFormat;
        }

        public void setStrDateFormat(String strDateFormat) {
            this.strDateFormat = strDateFormat;
        }

        public String getStrCreatedOn() {
            return strCreatedOn;
        }

        public void setStrCreatedOn(String strCreatedOn) {
            this.strCreatedOn = strCreatedOn;
        }

        public String getStrMembers() {
            return strMembers;
        }

        public void setStrMembers(String strMembers) {
            this.strMembers = strMembers;
        }

        public String getStrSummary() {
            return strSummary;
        }

        public void setStrSummary(String strSummary) {
            this.strSummary = strSummary;
        }

        public String getStrMember() {
            return strMember;
        }

        public void setStrMember(String strMember) {
            this.strMember = strMember;
        }

        public String getStrTotal() {
            return strTotal;
        }

        public void setStrTotal(String strTotal) {
            this.strTotal = strTotal;
        }

        public String getStrHourFormat() {
            return strHourFormat;
        }

        public void setStrHourFormat(String strHourFormat) {
            this.strHourFormat = strHourFormat;
        }

        public String getStrWorkReport() {
            return strWorkReport;
        }

        public void setStrWorkReport(String strWorkReport) {
            this.strWorkReport = strWorkReport;
        }

        public String getStrDetails() {
            return strDetails;
        }

        public void setStrDetails(String strDetails) {
            this.strDetails = strDetails;
        }

        public String getStrSumry() {
            return strSumry;
        }

        public void setStrSumry(String strSumry) {
            this.strSumry = strSumry;
        }

        public String getStrLastWeek() {
            return strLastWeek;
        }

        public void setStrLastWeek(String strLastWeek) {
            this.strLastWeek = strLastWeek;
        }

        public String getStrThisWeek() {
            return strThisWeek;
        }

        public void setStrThisWeek(String strThisWeek) {
            this.strThisWeek = strThisWeek;
        }

        public String getStrDate() {
            return strDate;
        }

        public void setStrDate(String strDate) {
            this.strDate = strDate;
        }

        public String getStrDescription() {
            return strDescription;
        }

        public void setStrDescription(String strDescription) {
            this.strDescription = strDescription;
        }

        public String getStrHours() {
            return strHours;
        }

        public void setStrHours(String strHours) {
            this.strHours = strHours;
        }

        public String getStrType() {
            return strType;
        }

        public void setStrType(String strType) {
            this.strType = strType;
        }
    }

    public class UserInfo {
        private String uname;
        private String uid;
        private double hours;

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public double getHours() {
            return hours;
        }

        public void setHours(double hours) {
            this.hours = hours;
        }

        public double getTypeHours(String type) throws SQLException {
            ResultSet resultSet = statement.executeQuery("select sum(e_hours) from e_entries where e_u_user = " + getUid() +" and e_t_type = " + type + " and e_p_project = " + request.getId() +" and e_date >= '" + sqlFormat.format(request.getFrom()) +"' and e_date <= '" + sqlFormat.format(request.getTo()) + "'");
            if(resultSet.first()) {
                return resultSet.getDouble(1);
            } else {
                return 0.;
            }
        }
    }
}