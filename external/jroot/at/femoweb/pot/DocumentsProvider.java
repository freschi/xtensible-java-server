package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.TagContent;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-documents-provider")
public class DocumentsProvider implements TagContent {

    @Override
    public Tag generateTags() {
        Connection connection = (Connection) Session.get("connection");
        if(connection != null) {
            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT d_name, d_link, idd_documents FROM d_documents WHERE d_p_project = " + Session.get("curr-project").toString());
                Tag table = new Tag("table");
                table.attribute("class", "table table-striped tabled-hover table-bordered");
                Tag hrow = new Tag("tr", table);
                Tag hname = new Tag("th", hrow);
                hname.add("Name");
                Tag hactions = new Tag("th", hrow);
                hactions.add("Actions");
                if(resultSet.first()) {
                    do {
                        Tag row = new Tag("tr", table);
                        Tag name = new Tag("td", row);
                        name.attribute("style", "min-width: 70%");
                        name.add(resultSet.getString(1));
                        Tag actions = new Tag("td", row);
                        Tag link = new Tag("a", actions);
                        link.attribute("class", "btn btn-success btn-xs");
                        link.attribute("target", "_blank");
                        link.attribute("href", "https://docs.google.com/document/d/" + resultSet.getString(2) + "/edit");
                        Tag icon = new Tag("span", link);
                        icon.attribute("class", "glyphicon glyphicon-new-window");
                        link.add("Open in Google Docs");
                        Tag linkLocal = new Tag("a", actions);
                        linkLocal.attribute("class", "btn btn-success btn-xs");
                        linkLocal.attribute("onclick", "invoke('pot_documents_edit', {link: '" + resultSet.getString(2) + "'});");
                        Tag locIcon = new Tag("span", linkLocal);
                        locIcon.attribute("class", "glyphicon glyphicon-new-window");
                        linkLocal.add("Open Embedded");
                        Tag linkEdit = new Tag("a", actions);
                        linkEdit.attribute("class", "btn btn-info btn-xs");
                        Tag editIcon = new Tag("span", linkEdit);
                        editIcon.attribute("class", "glyphicon glyphicon-edit");
                        editIcon.attribute("onclick", "invoke('pot_documents_edit_entry', {id: " + resultSet.getString(3) + ", name: '" + resultSet.getString(1) + "', link: '" + resultSet.getString(2) + "'});");
                        Tag linkDelete = new Tag("a", actions);
                        linkDelete.attribute("class", "btn btn-danger btn-xs");
                        Tag delIcon = new Tag("span", linkDelete);
                        delIcon.attribute("class", "glyphicon glyphicon-trash");
                    } while (resultSet.next());
                    return table;
                }
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not create document list", e);
            }
        }
        Tag message = new Tag("strong");
        message.add("No documents found yet!");
        return message;
    }

    @XAction(name = "pot_documents_edit")
    public void potDocumentEdit(@XUpdate Updates updates, @XOptional JsonElement optional) {
        JsonObject object = optional.getAsJsonObject();
        String id = object.get("link").getAsString();
        Session.set("curr-doc", id);
        updates.setPartial("pot_documents_content", "project/documents/document.xjs");
    }

    @XAction(name = "pot_documents_back")
    public void potDocumentBack(@XUpdate Updates updates) {
        Session.remove("curr-doc");
        updates.setPartial("pot_documents_content", "project/documents/table.xjs");
    }

    @XAction(name = "pot_documents_add")
    public void potDocumentsAdd(@XUpdate Updates updates) {
        updates.setPartial("pot_documents_content", "project/documents/entry.xjs");
    }

    @XAction(name = "pot_documents_edit_entry")
    public void potDocumentsEditEntry(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        JsonObject extra = jsonElement.getAsJsonObject();
        Session.set("curr-doc", extra.get("id").getAsString());
        Session.set("pot-documents-name", extra.get("name").getAsString());
        Session.set("pot-documents-link", extra.get("link").getAsString());
        potDocumentsAdd(updates);
    }

    @XAction(name = "pot_documents_add_commit")
    public void potDocumentsAddCommit(@XUpdate Updates updates, @XParam(name = "pot_documents_name") String name, @XParam(name = "pot_documents_link") String link) {
        Connection connection = (Connection) Session.get("connection");
        if(Session.is("curr-doc")) {
            try {
                Statement statement = connection.createStatement();
                statement.execute("UPDATE d_documents SET d_name = '" + name + "', d_link = '" + link + "' WHERE idd_documents = " + Session.get("curr-doc"));
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not insert document", e);
            }
        } else {
            try {
                Statement statement = connection.createStatement();
                statement.execute("INSERT INTO d_documents (d_name, d_link, d_p_project) VALUES ('" + name + "', '" + link + "', " + Session.get("curr-project").toString() + ");");
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not insert document", e);
            }
        }
        Session.remove("curr-doc");
        Session.remove("pot-documents-name");
        Session.remove("pot-documents-link");
        potDocumentBack(updates);
    }
}