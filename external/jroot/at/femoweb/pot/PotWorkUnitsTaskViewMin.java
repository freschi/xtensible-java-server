package at.femoweb.pot;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.ConditionCheck;
import at.femoweb.xjs.modules.xjs.TagContent;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-work-units-task-view-min")
public class PotWorkUnitsTaskViewMin implements TagContent {

    @Override
    public Tag generateTags() {
        Tag div = new Tag("div");
        String taskId = Session.get("curr-task").toString();
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select ta_id, ta_name, ta_start, ta_end, ta_hours, p_name, ta_ta_parent, ta_descr, ta_problems from ta_tasks inner join p_projects on ta_p_project = idp_projects where idta_tasks = " + taskId);
            resultSet.first();
            Tag strong = new Tag("strong");
            strong.add(resultSet.getString(3) + " - " + resultSet.getString(4));
            div.add(strong);
            div.add(new Tag("br"));
            Tag project = new Tag("span");
            Tag pstrong = new Tag("strong");
            pstrong.add("Project: ");
            project.add(pstrong);
            project.add(resultSet.getString(6));
            div.add(project);
            div.add(new Tag("br"));
            if(resultSet.getInt(5) > 0) {
                Tag hours = new Tag("span");
                Tag shours = new Tag("strong");
                shours.add("Hours: ");
                hours.add(shours);
                hours.add(resultSet.getString(5));
                div.add(hours);
                div.add(new Tag("br"));
            }
            if(resultSet.getString(8) != null) {
                Tag h = new Tag("h5", div);
                h.add("Task Desciption");
                Tag pre = new Tag("pre", div);
                pre.add(resultSet.getString(8));
            }
            if(resultSet.getString(9) != null) {
                Tag h = new Tag("h5", div);
                h.add("Dependency Problems");
                Tag pre = new Tag("pre", div);
                pre.add(resultSet.getString(9));
            }
            if(resultSet.getString(7) != null) {
                ResultSet result = statement.executeQuery("select ta_id, ta_name, idta_tasks from ta_tasks where idta_tasks = " + resultSet.getString(7));
                result.first();
                Tag parent = new Tag("span");
                Tag sparent = new Tag("strong");
                sparent.add("Parent: ");
                parent.add(sparent);
                Tag link = new Tag("a");
                link.attribute("onclick", "invoke('pot_work_units_provider_view', {task: " + result.getString(3) + "});");
                link.add(result.getString(1) + " - " + result.getString(2));
                parent.add(link);
                div.add(parent);
                div.add(new Tag("br"));
            }
            {
                ResultSet result = statement.executeQuery("select ta_id, ta_name, idta_tasks from ta_tasks where ta_ta_parent = " + taskId);
                if(result.first()) {
                    Tag childList = new Tag("ul");
                    Tag cheading = new Tag("h5");
                    cheading.add("Children");
                    div.add(cheading);
                    do {
                        Tag item = new Tag("li");
                        Tag link = new Tag("a");
                        link.attribute("onclick", "invoke('pot_work_units_provider_view', {task: " + result.getString(3) + "});");
                        link.add(result.getString(1) + " - " + result.getString(2));
                        item.add(link);
                        childList.add(item);
                    } while (result.next());
                    div.add(childList);
                }
            }
            {
                ResultSet result = statement.executeQuery("select ta_id, ta_name, idta_tasks from de_dependencies inner join ta_tasks on idta_tasks = de_ta_task where de_ta_dependency = " + taskId);
                if(result.first()) {
                    Tag childList = new Tag("ul");
                    Tag cheading = new Tag("h5");
                    cheading.add("Dependency for");
                    div.add(cheading);
                    do {
                        Tag item = new Tag("li");
                        Tag link = new Tag("a");
                        link.attribute("onclick", "invoke('pot_work_units_provider_view', {task: " + result.getString(3) + "});");
                        link.add(result.getString(1) + " - " + result.getString(2));
                        item.add(link);
                        childList.add(item);
                    } while (result.next());
                    div.add(childList);
                }
            }
            {
                ResultSet result = statement.executeQuery("select ta_id, ta_name, idta_tasks from de_dependencies inner join ta_tasks on idta_tasks = de_ta_dependency where de_ta_task = " + taskId);
                if(result.first()) {
                    Tag childList = new Tag("ul");
                    Tag cheading = new Tag("h5");
                    cheading.add("Dependencies");
                    div.add(cheading);
                    do {
                        Tag item = new Tag("li");
                        Tag link = new Tag("a");
                        link.attribute("onclick", "invoke('pot_work_units_provider_view', {task: " + result.getString(3) + "});");
                        link.add(result.getString(1) + " - " + result.getString(2));
                        item.add(link);
                        childList.add(item);
                    } while (result.next());
                    div.add(childList);
                }
            }
            statement.close();
        } catch (SQLException e) {
            Server.log.warn("Could not generate task info", e);
        }
        return div;
    }

    @XAction(name = "pot_work_units_back")
    public void changeBack(@XUpdate Updates updates) {
        updates.setPartial("pot_work_units_content", "project/wu/table.xjs");
        Session.remove("pot_wu_name");
        Session.remove("pot_wu_start");
        Session.remove("pot_wu_end");
        Session.remove("pot_wu_hours");
        Session.remove("pot_wu_id");
    }

    @XAction(name = "pot_work_units_store")
    public void store(@XUpdate Updates updates, @XParam(name = "wu_name") String name, @XParam(name = "wu_start") String start, @XParam(name = "wu_end") String end, @XParam(name = "wu_hours") String hours) {
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            if (Session.is("curr-task")) {
                statement.execute("update ta_tasks set ta_name = '" + name + "', ta_start = '" + start + "', ta_end = '" + end + "', ta_hours = " + hours + " where idta_tasks = " + Session.get("curr-task"));
            }
            statement.close();
            changeBack(updates);
        } catch (SQLException e) {
            Server.log.warn("Could not store task", e);
            updates.addMessage(Updates.Severity.DANGER, "Error while storing task");
        }
    }

}