package at.femoweb.term;

import at.femoweb.data.core.ConnectionFactory;
import at.femoweb.data.core.DataContext;
import at.femoweb.data.core.Schema;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.link.*;
import at.femoweb.xjs.modules.link.Link;
import at.femoweb.xjs.modules.link.LinkConnection;
import at.femoweb.xjs.modules.link.LinkHandler;
import at.femoweb.xjs.modules.link.LinkParam;
import at.femoweb.xjs.modules.link.LinkUnit;
import at.femoweb.xjs.modules.link.RequestLink;
import at.femoweb.xjs.services.DataConnectionService;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.pot.data.*;
import com.google.gson.JsonElement;
import org.apache.commons.lang3.StringEscapeUtils;

import java.lang.Object;
import java.sql.SQLException;
import java.util.Properties;

@RequestLink(type = "cmd", path = "/xjs/term", authenticate = true, authModule = "femoweb-user")
public class Terminal {

    private LogicalUnit logicalUnit;
    private DataContext context;

    @LinkHandler
    public String performAction(@LinkParam(name = "cmd", required = true)JsonElement cmd, @LinkConnection Link link, @LinkUnit LogicalUnit unit) {
        String command = cmd.getAsString();
        Server.log.info("Incoming command: " + command);
        this.logicalUnit = unit;
        if(!Session.is("connection-factory")) {
            try {
                ConnectionFactory connectionFactory = ((DataConnectionService) unit.getService("pot-connection-service")).connectDataCore(null);
                Session.set("connection-factory", connectionFactory);
                Schema schema = DataContext.generateSchema(PotSchema.class, connectionFactory);
                Session.set("schema", schema);
            } catch (SQLException e) {
                Server.log.warn("Could not connect to database", e);
            }
        }
        if(command.equals("exit") || command.equals("quit")) {
            link.write("Exiting connection");
            link.close();
        } else {
            Service service = unit.getService("command-service");
            try {
                Command com = (Command) service.put(command);
                InvokedCommand invokedCommand = buildInvokedCommand(command, com, link);
                service.put(invokedCommand);
            } catch (CommandLookupException e) {
                link.write(e.getMessage());
            }
        }
        return "command";
    }

    private String createCommandString(Command command) {
        String cmd = "";
        for(CommandValue value : command.value()) {
            if (!value.required()) {
                cmd += "[";
            }
            if (value.type() == CommandValue.Type.VALUE) {
                cmd += "<";
            }
            cmd += value.name();
            if (value.type() == CommandValue.Type.VALUE) {
                cmd += ">";
            }
            if (!value.required()) {
                cmd += "]";
            }
            cmd += " ";
        }
        return cmd;
    }

    private InvokedCommand buildInvokedCommand(String command, Command com, Link link) {
        Properties properties = new Properties();
        String[] cmds = split(command);
        for (int i = 0; i < cmds.length; i++) {
            if(com.value()[i].type() == CommandValue.Type.VALUE) {
                properties.setProperty(com.value()[i].name(), cmds[i]);
            }
        }
        return new InvokedCommand(command, com, 0, (CommandService) logicalUnit.getService("command-service"), link, properties);

    }

    private String[] split(String command) {
        return command.split(" ");
    }
}