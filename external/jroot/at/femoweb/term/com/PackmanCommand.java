package at.femoweb.term.com;

import at.femoweb.config.Catalog;
import at.femoweb.config.Config;
import at.femoweb.config.entries.Group;
import at.femoweb.term.*;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.link.Link;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;

import java.lang.SuppressWarnings;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

public class PackmanCommand {

    @Commands({
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "install"),
                    @CommandValue(name = "package", type = CommandValue.Type.VALUE),
                    @CommandValue(name = "unit"),
                    @CommandValue(name = "logical-unit", type = CommandValue.Type.VALUE)
            }),
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "install"),
                    @CommandValue(name = "package", type = CommandValue.Type.VALUE)
            })
    })
    @SuppressWarnings("unchecked")
    public void install(InvokedCommand invokedCommand) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Link link = invokedCommand.getLink();
        if(logicalUnit.hasService("packman")) {
            Service packman = logicalUnit.getService("packman");
            link.write("Valid backend found. Executing commands!");
            Properties properties = new Properties();
            properties.setProperty("action", "install");
            properties.setProperty("package", invokedCommand.getValue("package"));
            if (invokedCommand.getCommand().value().length == 5) {
                properties.setProperty("unit", invokedCommand.getValue("unit"));
            }
            List<Properties> states = (List<Properties>) packman.put(properties);
            for (Properties state : states) {
                if (state.getProperty("state").equals("success")) {
                    link.write("Installed " + state.getProperty("package") + " <" + state.getProperty("version") + "> in " + state.getProperty("time") + " ms");
                } else {
                    if (state.containsKey("messsage")) {
                        link.write(state.getProperty("message"));
                    }
                }
            }
        } else {
            link.write("Packman Backend not found. Exiting!");
        }
    }

    @Commands({
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "list"),
                    @CommandValue(name = "local")
            }),
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "list"),
                    @CommandValue(name = "repo"),
                    @CommandValue(name = "repoName", type = CommandValue.Type.VALUE)
            }),
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "list"),
                    @CommandValue(name = "installed")
            })
    })
    public void list(InvokedCommand invokedCommand) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Link link = invokedCommand.getLink();
        if(logicalUnit.hasService("packman")) {
            link.write("Valid backend found. Executing commands!");
            String command = invokedCommand.getCommand().value()[2].name();
            if(command.equals("local")) {
                link.write(String.format("%-20s %-10s %-7s %s", "Name", "Version", "Classes", "Dependencies"));
                Catalog packman = Config.openCatalog("packman");
                Group local = packman.openArticle("local").openParagraph("packets").openEntry();
                for (int i = 0; i < local.getList("providedList").count(); i++) {
                    String name = local.getList("providedList").getString(i);
                    Group packet = local.getGroup("provided").getGroup(name);
                    String dependencies = "";
                    for (int j = 0; j < packet.getList("dependencies").count(); j++) {
                        dependencies += packet.getList("dependencies").getString(j);
                        if(j < packet.getList("dependencies").count() - 1) {
                            dependencies += ", ";
                        }
                    }
                    link.write(String.format("%-20s %10s %7d %s", packet.getString("name"), packet.getString("version"), packet.getList("classes").count(), dependencies));
                }
            } else {
                link.write("Backend does not support this command!");
            }

        } else {
            link.write("Packman Backend not found. Exiting!");
        }
    }

    @Commands({
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "search"),
                    @CommandValue(name = "packet", type = CommandValue.Type.VALUE)
            })
    })
    @SuppressWarnings("unchecked")
    public void search(InvokedCommand invokedCommand) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Link link = invokedCommand.getLink();
        if(logicalUnit.hasService("packman")) {
            Service packman = logicalUnit.getService("packman");
            Properties properties = new Properties();
            properties.setProperty("action", "search");
            properties.setProperty("query", invokedCommand.getValue("packet"));
            List<Properties> resultSet = (List<Properties>) packman.put(properties);
            if(resultSet.size() > 0) {
                link.write(String.format("%-20s %-10s", "Name", "Version"));
                for (Properties props : resultSet) {
                    link.write(String.format("%-20s %-10s", props.getProperty("name"), props.getProperty("version")));
                }
            }
        } else {
            link.write("Packman Backend not found. Exiting!");
        }
    }

    @Commands({
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "repository"),
                    @CommandValue(name = "add"),
                    @CommandValue(name = "url", type = CommandValue.Type.VALUE),
                    @CommandValue(name = "name"),
                    @CommandValue(name = "repo-name", type = CommandValue.Type.VALUE)
            })
    })
    public void addRepo(InvokedCommand invokedCommand) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Link link = invokedCommand.getLink();
        if(logicalUnit.hasService("packman")) {
            Service packman = logicalUnit.getService("packman");
            Properties properties = new Properties();
            properties.setProperty("action", "add-repo");
            properties.setProperty("url", invokedCommand.getValue("url"));
            properties.setProperty("repo-name", invokedCommand.getValue("repo-name"));
            Boolean bool = (Boolean) packman.put(properties);
            if(bool) {
                link.write("Added Repository successfuly!");
            } else {
                link.write("Failed to add repository!");
            }
        } else {
            link.write("Packman Backend not found. Exiting!");
        }
    }

    @Commands({
            @Command({
                    @CommandValue(name = "packman"),
                    @CommandValue(name = "list"),
                    @CommandValue(name = "repos")
            })
    })
    public void listRepos(InvokedCommand invokedCommand) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Link link = invokedCommand.getLink();
        if(logicalUnit.hasService("packman")) {
            Catalog catalog = Config.openCatalog("packman");
            Group remoteRepos = catalog.openArticle("remote").openParagraph("repos").openEntry();
            link.write(String.format("%-20s %-40s", "Name", "Url"));
            for (String key : remoteRepos.getKeys()) {
                Group repo = remoteRepos.getGroup(key);
                link.write(String.format("%-20s %-40s", repo.getString("name"), repo.getString("url")));
            }
        } else {
            link.write("Packman Backend not found. Exiting!");
        }
    }
}