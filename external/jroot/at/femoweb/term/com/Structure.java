package at.femoweb.term.com;

import at.femoweb.term.*;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.link.Link;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.util.Collection;

public class Structure {

    @Commands({
            @Command({
                    @CommandValue(name = "list"),
                    @CommandValue(name = "structure")
            })
    })
    public void showStructure(InvokedCommand invokedCommand) {
        Link link = invokedCommand.getLink();
        link.write("Structure for Server " + Server.physicalUnit.getName());
        link.write("Logical Units " + Server.physicalUnit.getLogicalUnits().size());
        Collection<LogicalUnit> logicalUnits = Server.physicalUnit.getLogicalUnits();
        for(LogicalUnit logicalUnit : logicalUnits) {
            link.write("[" + logicalUnit.getName() + "]");
            link.write("\tAliases:");
            for(String alias : logicalUnit.getAliases()) {
                link.write("\t\t" + alias);
            }
            link.write("\tServices:");
            for(Module module : logicalUnit.getServices()) {
                link.write("\t\t" + module.getName());
            }
            link.write("\tContent Providers:");
            for(Module module : logicalUnit.getContentProviders()) {
                link.write("\t\t" + module.getName());
            }
        }
    }
}