package at.femoweb.term.com;

import at.femoweb.data.core.Schema;
import at.femoweb.data.core.internal.query.DataObject;
import at.femoweb.term.*;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.link.Link;
import at.femoweb.pot.data.*;

import java.lang.Integer;
import java.sql.SQLException;
import java.util.List;

public class User {

    @Commands({
            @Command({
                    @CommandValue(name = "user")
            }),
            @Command({
                    @CommandValue(name = "user"),
                    @CommandValue(name = "username", type = CommandValue.Type.VALUE)
            }),
            @Command({
                    @CommandValue(name = "user"),
                    @CommandValue(name = "username", type = CommandValue.Type.VALUE),
                    @CommandValue(name = "password"),
                    @CommandValue(name = "change"),
                    @CommandValue(name = "pass", type = CommandValue.Type.VALUE)
            })
    })
    public void execute(InvokedCommand invokedCommand) {
        Link link = invokedCommand.getLink();
        if(invokedCommand.getCommand().value().length == 1) {
            if(Session.is("_xjs_user")) {
                UnifiedUser unifiedUser = (UnifiedUser) Session.get("_xjs_user");
                link.write("Currently logged in as " + unifiedUser.getUsername());
            } else {
                link.write("No user logged in");
            }
        } else if (invokedCommand.getCommand().value().length == 2) {
            String uname = invokedCommand.getValue("username");
            Schema schema = (Schema) Session.get("schema");
            List<DataObject> data = schema.query(Login.class).select().field(Login.class, "l_uname").on(Login.class, "l_u_user", at.femoweb.pot.data.User.class, "idu_user")
                    .field(at.femoweb.pot.data.User.class, "u_display").field(at.femoweb.pot.data.User.class, "u_email").field(at.femoweb.pot.data.User.class, "idu_user")
                    .where(Login.class, "l_uname", uname).execute();
            for(DataObject d : data) {
                link.write("Name: " + d.getField(String.class, "u_display"));
                link.write("E-Mail: " + d.getField(String.class, "u_email"));
                List<Entry> entries = schema.query(Entry.class).select().where(Entry.class, "e_u_user", d.getField(int.class, "idu_user")).execute();
                link.write("Entries: " + entries.size());
            }
        } else if (invokedCommand.getCommand().value().length == 5) {
            link.write("Changed password for user " + invokedCommand.getValue("username"));
            Server.log.info("Changing password of user " + invokedCommand.getValue("username") + " to " + invokedCommand.getValue("pass"));
        }
    }
}