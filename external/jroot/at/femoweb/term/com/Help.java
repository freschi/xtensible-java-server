package at.femoweb.term.com;

import at.femoweb.xjs.modules.link.Link;
import at.femoweb.term.*;
import org.apache.commons.lang3.StringEscapeUtils;

public class Help {

    @Commands({
            @Command({
                    @CommandValue(name = "help")
            }),
            @Command({
                    @CommandValue(name = "help"),
                    @CommandValue(name = "cmd", type = CommandValue.Type.VALUE, required = false)
            })
    })
    public void execute(InvokedCommand invokedCommand) {
        invokedCommand.getLink().write(StringEscapeUtils.escapeHtml4(invokedCommand.getService().createCommandsString()));
    }
}