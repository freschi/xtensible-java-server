package at.femoweb.term.com;

import at.femoweb.term.Command;
import at.femoweb.term.Commands;
import at.femoweb.term.CommandValue;
import at.femoweb.term.InvokedCommand;
import at.femoweb.xjs.modules.link.Link;

public class HelloWorld {

    @Commands({
            @Command({
                    @CommandValue(name = "hello"),
                    @CommandValue(name = "world")
            }),
            @Command({
                    @CommandValue(name = "hello"),
                    @CommandValue(name = "world"),
                    @CommandValue(name = "name", required = false, type = CommandValue.Type.VALUE)
            })
    })
    public void execute(InvokedCommand command) {
        if(command.getCommand().value().length == 3) {
            command.getLink().write("Hello " + command.getValue("name"));
        } else {
            command.getLink().write("Hello World");
        }
    }
}