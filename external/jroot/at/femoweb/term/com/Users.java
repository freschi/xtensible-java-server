package at.femoweb.term.com;

import at.femoweb.data.core.Schema;
import at.femoweb.data.core.internal.query.DataObject;
import at.femoweb.term.*;
import at.femoweb.pot.data.User;
import at.femoweb.pot.data.Login;
import at.femoweb.pot.data.Entry;
import at.femoweb.pot.data.Project;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.link.Link;

import java.lang.Integer;
import java.util.List;

public class Users {

    @Commands({
        @Command({
                @CommandValue(name = "list"),
                @CommandValue(name = "users")
        }),
        @Command({
                @CommandValue(name = "list"),
                @CommandValue(name = "projects")
        })
    })
    public void invokeCommand(InvokedCommand invokedCommand) {
        String command = invokedCommand.getCommand().value()[1].name();
        Link link = invokedCommand.getLink();
        Schema schema = (Schema) Session.get("schema");
        if(command.equals("users")) {
            List<DataObject> objectList = schema.query(User.class).select().field(User.class, "idu_user").field(User.class, "u_display").field(User.class, "u_firstname")
                    .field(User.class, "u_lastname").on(User.class, "idu_user", Login.class, "l_u_user").field(Login.class, "l_uname").execute();
            link.write("Currently registered users");
            link.write(String.format("%-5s %-15s %-15s %-20s %-15s %-6s", "ID", "Firstname", "Lastname", "Display", "Login", "Entries"));
            for (DataObject dataObject : objectList) {
                List<Entry> entries = schema.query(Entry.class).select().where(Entry.class, "e_u_user", dataObject.getField(int.class, "idu_user")).execute();
                String value = String.format("%05d %-15s %-15s %-20s %-15s %6d", dataObject.getField(int.class, "idu_user"), dataObject.getField(String.class, "u_firstname"),
                        dataObject.getField(String.class, "u_lastname"), dataObject.getField(String.class, "u_display"), dataObject.getField(String.class, "l_uname"), entries.size());
                link.write(value);
            }
        } else if (command.equals("projects")) {
            List<DataObject> objectList = schema.query(Project.class).select().field(Project.class, "idp_projects").field(Project.class, "p_name").field(Project.class, "p_short").
                    field(Project.class, "p_r_reportprovider").execute();
            link.write("Currently stored projects");
            link.write(String.format("%-5s %-10s %-20s %-40s %-6s", "ID", "Short", "Name", "Default Report Provider", "Entries"));
            for(DataObject dataObject : objectList) {
                List<Entry> dataObjects = schema.query(Entry.class).select().where(Entry.class, "e_p_project", dataObject.getField(int.class, "idp_projects")).execute();
                String value = String.format("%05d %-10s %-20s %-40s %6d", dataObject.getField(int.class, "idp_projects"), dataObject.getField(String.class, "p_short"),
                        dataObject.getField(String.class, "p_name"), dataObject.getField(String.class, "p_r_reportprovider"), dataObjects.size());
                link.write(value);
            }
        }
    }
}