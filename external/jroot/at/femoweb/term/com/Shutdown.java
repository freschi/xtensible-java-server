package at.femoweb.term.com;

import at.femoweb.term.*;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.link.Link;

public class Shutdown {

    @Commands({
            @Command({
                    @CommandValue(name = "server"),
                    @CommandValue(name = "shutdown")
            }),
            @Command({
                    @CommandValue(name = "server"),
                    @CommandValue(name = "reload")
            })
    })
    public void shutdownServer(InvokedCommand invokedCommand) {
        Link link = invokedCommand.getLink();
        Command command = invokedCommand.getCommand();
        String com = command.value()[1].name();
        if(com.equals("shutdown")) {
            link.write("Shutting down server");
            link.close();
            Server.shutdown();
        } else if (com.equals("reload")) {
            link.write("Reloading server");
            link.close();
            Server.reload();
        }
    }
}