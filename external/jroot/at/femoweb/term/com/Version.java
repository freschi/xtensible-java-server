package at.femoweb.term.com;

import at.femoweb.term.*;
import at.femoweb.xjs.modules.link.Link;

public class Version {

    @Commands({
            @Command({
                    @CommandValue(name = "version")
            })
    })
    public void executeCommand(InvokedCommand invokedCommand) {
        Link link = invokedCommand.getLink();
        link.write("Terminal Emulation version 0.1");
        link.write("Copyright (c) 2015, Felix Resch");
    }
}