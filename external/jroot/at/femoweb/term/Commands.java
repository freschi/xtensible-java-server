package at.femoweb.term;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@JResTarget(ElementType.METHOD)
public @interface Commands {

    public Command[] value();
}