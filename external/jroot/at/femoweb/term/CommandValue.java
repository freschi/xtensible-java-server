package at.femoweb.term;

import at.femoweb.xjs.modules.link.RequestLink;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandValue {

    public enum Type {
        COMMAND, VALUE
    }

    public String name();
    public Type type() default Type.COMMAND;
    public boolean required() default true;
    public String description() default "";

}