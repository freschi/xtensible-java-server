package at.femoweb.term;

import at.femoweb.config.entries.Group;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Startable;
import at.femoweb.xjs.modules.JavaRessourceService;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import org.apache.commons.lang3.StringEscapeUtils;

import java.lang.*;
import java.lang.Exception;
import java.lang.IllegalAccessException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Named("command-service")
public class CommandService extends BootableModule implements Startable, at.femoweb.xjs.services.Service {

    private LogicalUnit logicalUnit;

    private ArrayList<Command> commands;
    private HashMap<Command, Object> mappings;
    private HashMap<Command, Method> methods;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        CommandService commandService = new CommandService();
        commandService.logicalUnit = logicalUnit;
        commandService.commands = new ArrayList<>();
        commandService.mappings = new HashMap<>();
        commandService.methods = new HashMap<>();
        return commandService;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public void start() {
        if(logicalUnit.hasService("jres-service")) {
            JavaRessourceService service = (JavaRessourceService) logicalUnit.getService("jres-service");
            Method[] methods = service.getAllMethods(Commands.class);
            for(Method method : methods) {
                Commands commands = method.getAnnotation(Commands.class);
                try {
                    Object object = method.getDeclaringClass().newInstance();
                    for (Command command : commands.value()) {
                        this.commands.add(command);
                        mappings.put(command, object);
                        this.methods.put(command, method);
                    }
                } catch (Exception e) {
                    Server.log.warn("Could not access command object", e);
                }
            }
            Server.log.info("Commands:\n" + createCommandsString());
        }
    }

    public String createCommandsString() {
        String cmd = "";
        for(Command command : commands) {
            for(CommandValue value : command.value()) {
                if(!value.required()) {
                    cmd += "[";
                }
                if(value.type() == CommandValue.Type.VALUE) {
                    cmd += "<";
                }
                cmd += value.name();
                if(value.type() == CommandValue.Type.VALUE) {
                    cmd += ">";
                }
                if(!value.required()) {
                    cmd += "]";
                }
                cmd += " ";
            }
            cmd += "\n";
        }
        return cmd;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object put(Object... objects) {
        if(objects[0] instanceof String) {
            final String command = (String) objects[0];
            final String[] cmd = split(command);
            final List<Command> commands = new ArrayList<Command>();
            commands.addAll(this.commands);
            List tmp = commands.stream().filter(new Predicate<Command>() {
                @Override
                public boolean test(Command command) {
                    return command.value().length == cmd.length;
                }
            }).collect(Collectors.toList());
            commands.clear();
            tmp.forEach(new Consumer() {
                @Override
                @SuppressWarnings("unchecked")
                public void accept(Object o) {
                    commands.add((Command)o);
                }
            });
            for(int i = 0; i < cmd.length; i++) {
                final int j = i;
                List tmp_ = commands.stream().filter(new Predicate<Command>() {
                    @Override
                    public boolean test(Command c) {
                        if (c.value().length >= j) {
                            CommandValue value = c.value()[j];
                            if (value.type() == CommandValue.Type.COMMAND) {
                                if (value.name().startsWith(cmd[j])) {
                                    return true;
                                }
                                return false;
                            } else {
                                return true;
                            }
                        }
                        return false;
                    }
                }).collect(Collectors.toList());
                commands.clear();
                tmp_.forEach(new Consumer() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public void accept(Object o) {
                        commands.add((Command)o);
                    }
                });
            }
            if(commands.size() == 1) {
                return commands.get(0);
            } else {
                throw new CommandLookupException();
            }
        } else if (objects[0] instanceof Command) {
            Command command = (Command) objects[0];
            return mappings.get(command);
        } else if (objects[0] instanceof InvokedCommand) {
            InvokedCommand invokedCommand = (InvokedCommand) objects[0];
            Object object = put(invokedCommand.getCommand());
            Method method = methods.get(invokedCommand.getCommand());
            try {
                method.invoke(object, invokedCommand);
            } catch (Exception e) {
                Server.log.warn("Could not execute command", e);
            }
        }
        return null;
    }

    private String[] split (String original) {
        return original.split(" ");
    }
}