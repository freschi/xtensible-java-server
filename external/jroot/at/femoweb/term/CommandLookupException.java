package at.femoweb.term;

import java.lang.RuntimeException;

public class CommandLookupException extends RuntimeException {

    public CommandLookupException() {
        super("Command could not be found or was ambigous");
    }
}