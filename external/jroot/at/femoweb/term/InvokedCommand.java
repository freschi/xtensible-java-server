package at.femoweb.term;

import at.femoweb.xjs.modules.link.Link;

import java.lang.String;
import java.util.Properties;

public class InvokedCommand {

    private String commandString;
    private Command command;
    private int id;
    private CommandService service;
    private Link link;
    private Properties values;


    public InvokedCommand(String commandString, Command command, int id, CommandService service, Link link, Properties values) {
        this.commandString = commandString;
        this.command = command;
        this.id = id;
        this.service = service;
        this.link = link;
        this.values = values;
    }

    public String getValue(String name) {
        if(values.containsKey(name)) {
            return values.getProperty(name);
        }
        return null;
    }

    public String getCommandString() {
        return commandString;
    }

    public void setCommandString(String commandString) {
        this.commandString = commandString;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CommandService getService() {
        return service;
    }

    public void setService(CommandService service) {
        this.service = service;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }
}