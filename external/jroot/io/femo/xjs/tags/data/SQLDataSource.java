package io.femo.xjs.tags.data;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.TableDataSource;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.*;
import java.lang.Class;
import java.lang.Double;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.lang.Throwable;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

@XJSTag("sql:dataSource")
public class SQLDataSource extends DynamicTag implements TableDataSource {

    private ResultSet resultSet = null;

    @Override
    public Tag renderInternal() {
        return null;
    }

    @Override
    public boolean nextRow() throws SQLException {
        return resultSet.next();
    }

    public void init() throws SQLException{
        String connectionString = select().name("sql:connection").first().text();
        String query = select().name("sql:query").first().text();
        Connection connection = (Connection) Session.get(connectionString);
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        for(Tag param : select().name("sql:params").first().select().name("sql:param")) {
            String type = param.select().name("sql:type").first().text();
            String index = param.select().name("sql:index").first().text();
            String id = param.select().name("sql:value").first().text();
            if(type.equals("number")) {
                preparedStatement.setInt(Integer.parseInt(index), Integer.parseInt(String.valueOf(Session.get(id))));
            } else if (type.equals("string")) {
                preparedStatement.setString(Integer.parseInt(index), String.valueOf(Session.get(id)));
            }
        }
        resultSet = preparedStatement.executeQuery();
    }

    @Override
    public boolean first() throws SQLException {
        return resultSet.first();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getColumn(Class<T> type, String column) throws SQLException {
        if(column == null)
            return null;
        T var;
        if(type == double.class) {
            var = (T) (Double) resultSet.getDouble(column);
        } else if (type == Date.class) {
            var = (T) resultSet.getDate(column);
        } else {
            var = (T) resultSet.getString(column);
        }
        return var;
    }

    @Override
    protected void finalize() throws Throwable {
        resultSet.close();
        resultSet.getStatement().close();
        super.finalize();
    }
}