package io.femo.xjs.tags.format;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import io.femo.xjs.tags.Format;

import java.lang.Override;
import java.lang.String;

@XJSTag("xjs:num")
public class Num extends DynamicTag implements Format<Double> {

    @Override
    public Tag renderInternal() {
        return null;
    }

    public Class<?> getType() {
        return Number.class;
    }

    public Tag format(Double value) {
        String formatString = select().name("num:format").first().text();
        String type = select().name("num:type").first().text();
        if(type.equals("decimal")) {
            TextTag val = new TextTag();
            val.text(String.format(formatString, value.doubleValue()));
            return val;
        } else if (type.equals("integer") || type.equals("int")) {
            TextTag val = new TextTag();
            val.text(String.format(formatString, value.longValue()));
            return val;
        }
        TextTag textTag = new TextTag();
        textTag.text(String.valueOf(value));
        return textTag;
    }
}