package io.femo.xjs.tags.format;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import io.femo.xjs.tags.Format;
import org.apache.commons.lang3.StringEscapeUtils;

import java.lang.Override;

@XJSTag("xjs:htmlEscape")
public class HtmlEscape extends DynamicTag implements Format<String> {

    @Override
    public Tag renderInternal() {
        return null;
    }

    public Class<?> getType() {
        return String.class;
    }

    public Tag format(String value) {
        if(value != null) {
            TextTag val = new TextTag();
            val.text(StringEscapeUtils.escapeHtml4(value));
            return val;
        }
        return new TextTag();
    }
}