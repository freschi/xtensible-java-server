package io.femo.xjs.tags.format;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.xjs.Component;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import com.google.gson.JsonObject;
import io.femo.xjs.tags.Button;
import io.femo.xjs.tags.Icon;
import io.femo.xjs.tags.Format;

import java.util.Properties;


@XJSTag("xjs:crud")
public class Crud extends DynamicTag implements Format<String> {

    @Override
    public Tag renderInternal() {
        return null;
    }

    public Class<?> getType() {
        return String.class;
    }

    public Tag format(String value) {
        Tag val = new Tag("span");
        if(select().name("crud:view").size() > 0) {
            Tag view = select().name("crud:view").first();
            Component button = new Button();
            Properties extra = new Properties();
            extra.setProperty(view.select().name("crud:idField").first().text(), value);
            button.onClick(view.select().name("crud:action").first().text(), extra);
            Tag bTag = (Tag) button;
            bTag.attribute("size", "mini");
            bTag.attribute("type", "success");
            Tag icon = new Icon();
            icon.attribute("icon", view.select().name("crud:icon").first().text());
            bTag.add(icon);
            val.add(bTag);
        }
        if(select().name("crud:edit").size() > 0) {
            Tag edit = select().name("crud:edit").first();
            Component button = new Button();
            Properties extra = new Properties();
            extra.setProperty(edit.select().name("crud:idField").first().text(), value);
            button.onClick(edit.select().name("crud:action").first().text(), extra);
            Tag bTag = (Tag) button;
            bTag.attribute("size", "mini");
            bTag.attribute("type", "info");
            Tag icon = new Icon();
            icon.attribute("icon", edit.select().name("crud:icon").first().text());
            bTag.add(icon);
            val.add(bTag);
        }
        if(select().name("crud:delete").size() > 0) {
            Tag delete = select().name("crud:delete").first();
            Component button = new Button();
            Properties extra = new Properties();
            extra.setProperty(delete.select().name("crud:idField").first().text(), value);
            button.onClick(delete.select().name("crud:action").first().text(), extra);
            Tag bTag = (Tag) button;
            bTag.attribute("size", "mini");
            bTag.attribute("type", "danger");
            Tag icon = new Icon();
            icon.attribute("icon", delete.select().name("crud:icon").first().text());
            bTag.add(icon);
            val.add(bTag);
        }

        return val;
    }
}