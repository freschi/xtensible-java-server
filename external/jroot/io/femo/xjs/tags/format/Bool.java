package io.femo.xjs.tags.format;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import io.femo.xjs.tags.Format;


@XJSTag("xjs:bool")
public class Bool extends DynamicTag implements Format {

    @Override
    public Tag renderInternal() {
        return null;
    }

    public Class<?> getType() {
        String type = select().name("bool:type").first().text();
        if(type.equals("num")) {
            return Number.class;
        } else {
            return String.class;
        }
    }

    @SuppressWarnings("unchecked")
    public Tag format(Object value) {
        if(value instanceof Double || value.getClass() == double.class) {
            double val = (double) value;
            if(val == 0) {
                Tag r = new Tag("span");
                r.add(select().name("bool:false").first().children());
                return r;
            } else {
                Tag r = new Tag("span");
                r.add(select().name("bool:true").first().children());
                return r;
            }
        } else {
            String val = String.valueOf(value);
            if(val.equals("null") || val.equals("false")) {
                Tag r = new Tag("span");
                r.add(select().name("bool:false").first().children());
                return r;
            } else {
                Tag r = new Tag("span");
                r.add(select().name("bool:true").first().children());
                return r;
            }
        }
    }
}