package io.femo.xjs.tags.format;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import io.femo.xjs.tags.Format;

import java.lang.SuppressWarnings;

@XJSTag("xjs:emptyIfNull")
public class EmptyIfNull extends DynamicTag implements Format<String> {

    @Override
    public Tag renderInternal() {
        return null;
    }

    public Class<?> getType() {
        return String.class;
    }

    @SuppressWarnings("unchecked")
    public Tag format(String value) {
        if(value != null) {
            if (children().size() > 0) {
                for (Tag child : children()) {
                    if (child instanceof Format) {
                        if (((Format) child).getType() == getType()) {
                            return ((Format)child).format(value);
                        }
                    }
                }
            }
            TextTag textTag = new TextTag();
            textTag.text(value);
            return textTag;
        } else {
            TextTag res = new TextTag();
            res.text("");
            return res;
        }
    }
}