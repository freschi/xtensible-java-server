package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.lang.String;

@XJSTag("xjs:menuItem")
@Package("xjs-tags-dyn")
public class MenuItem extends DynamicTag {

    private String content;
    private String parent;
    private String label;

    @Override
    public Tag renderInternal() {
        Tag item = new Tag("li");
        String content = "";
        if(this.content != null) {
            content = this.content.replace("/", "&#47;");
        }
        String parents = "";
        if(parent != null) {
            parents = parent += "&gt;";
        }
        parents += label;
        if(children().size() > 0) {
            Tag container = new Tag("div");
            Tag link = new Tag("a");
            if(label != null)
                link.add(label);
            container.add(link);
            Tag list = new Tag("ul");
            list.attribute("style", "list-style-type: none; padding-left: 10px");
            link.attribute("onclick", "invoke('_xjs_dyn_menutItemChange', {content:'" + content + "', title:'" + parents + "'});");
            for(Tag t : children()) {
                t.attribute("parent", parents);
            }
            list.add(children());
            container.add(list);
            item.add(container);
        } else {
            Tag link = new Tag("a");
            if(label != null)
                link.add(label);
            link.attribute("onclick", "invoke('_xjs_dyn_menutItemChange', {content:'" + content + "', title:'" + parents + "'});");
            item.add(link);
        }
        return item;
    }

    @XAction(name = "_xjs_dyn_menuItemChange")
    public void changeContent(@XUpdate Updates updates, @XParam(name = "_xjs_menu_content") String content, @XParam(name = "_xjs_menu_title") String title, @XOptional JsonElement optional) {
        JsonObject optionals = optional.getAsJsonObject();
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        Service xjsFileProviderService = logicalUnit.getService("xjs-file-provider-service");
        if(xjsFileProviderService != null) {
            updates.html(content, ((Tag)xjsFileProviderService.put(optionals.get("content").getAsString())).render());
        }
        updates.html(title, optionals.get("title").getAsString());
    }
}