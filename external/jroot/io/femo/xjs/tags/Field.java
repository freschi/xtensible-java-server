package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:field")
@Package("xjs-tags-dyn")
public class Field extends DynamicTag {

    private String bold;
    private String id;
    private String field;

    @Override
    public Tag renderInternal() {
        Tag r;
        Tag field = new Tag("span");
        field.type(TagType.FULL);
        if(bold != null) {
            r = new Tag("strong");
            r.add(field);
        } else {
            r = field;
        }
        if(id != null) {
            field.attribute("id", id);
        }
        if(this.field != null && Session.is(this.field) && Session.get(this.field) != null) {
            field.add(String.valueOf(Session.get(this.field)));
        }
        return r;
    }
}