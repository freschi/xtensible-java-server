package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:icon")
@Package("xjs-tags-dyn")
public class Icon extends DynamicTag {

    private String icon;

    @Override
    public Tag renderInternal() {
        Tag icon = new Tag("span");
        icon.type(TagType.FULL);
        if(this.icon != null) {
            icon.attribute("class", "glyphicon glyphicon-" + this.icon);
        }
        return icon;
    }
}