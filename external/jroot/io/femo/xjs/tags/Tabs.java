package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;

import java.lang.Override;
import java.util.function.Predicate;

@XJSTag("xjs:tabs")
@Package("xjs-tags-dyn")
public class Tabs extends DynamicTag {

    private String id;

    @Override
    public Tag renderInternal() {
        Tag tabs = new Tag("div");
        Tag list = new Tag("ul");
        list.attribute("class", "nav nav-tabs");
        tabs.add(list);
        String partial = null;
        int index = 0;
        int max = (int) children().stream().filter(new Predicate<Tag>() {
            @Override
            public boolean test(Tag t) {
                return t.name() != null && t.name().equals("xjs:tab") && t.attribute("partial") != null;
            }
        }).count();
        for(Tag item : children()) {
            if(item == null || item.name() == null)
                continue;
            if(item.name().equals("xjs:tab") && item.attribute("partial") != null) {
                Tag itemCont = new Tag("li");
                itemCont.attribute("role", "presentation");
                if(partial == null) {
                    partial = item.attribute("partial");
                    itemCont.attribute("class", "active");
                }
                Tag link = new Tag("a");
                link.attribute("onclick", "invoke('_xjs_tab_change', {partial:'" + item.attribute("partial")+ "', content: '" + id + "', index: " + index + ", max: " + max + "});");
                itemCont.attribute("id", id + "_" + index);
                index++;
                link.add(item.children());
                itemCont.add(link);
                list.add(itemCont);
            }
        }
        if(id != null) {
            Tag content = new Tag("div");
            content.attribute("id", id);
            LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
            assert logicalUnit != null;
            if(logicalUnit.hasService("xjs-file-provider-service")) {
                Service service = logicalUnit.getService("xjs-file-provider-service");
                content.add(service.put(partial));
            }
            tabs.add(content);
        }
        return tabs;
    }
}