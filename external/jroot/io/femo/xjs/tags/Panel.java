package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:panel")
@Package("xjs-tags-dyn")
public class Panel extends DynamicTag {

    private String type;

    @Override
    public Tag renderInternal() {
        Tag panel = new Tag("div");
        if(type != null) {
            panel.attribute("class", "panel panel-" + type);
        } else {
            panel.attribute("class", "panel panel-primary");
        }
        Tag heading = new Tag("div");
        heading.attribute("class", "panel-heading");
        Tag body = new Tag("div");
        body.attribute("class", "panel-body");
        Tag footer = new Tag("div");
        body.attribute("class", "panel-footer");
        for(Tag child : children()) {
            if(child.name().equals("xjs:head")) {
                heading.add(child.children());
            } else if (child.name().equals("xjs:footer")) {
                footer.add(child.children());
            } else if (child.name().equals("xjs:body")) {
                body.add(child.children());
            } else {
                body.add(child);
            }
        }
        if(heading.children().size() > 0) {
            panel.add(heading);
        }
        if(body.children().size() > 0) {
            panel.add(body);
        }
        if(footer.children().size() > 0) {
            panel.add(footer);
        }
        return panel;
    }
}