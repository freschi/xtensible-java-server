package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:link")
@Package("xjs-tags-dyn")
public class Link extends DynamicTag {

    private String open;
    private String target;
    private String style;

    @Override
    public Tag renderInternal() {
        Tag link = new Tag("a");
        if(target != null) {
            link.attribute("href", target);
        }
        if(open != null) {
            if(open.equalsIgnoreCase("new"))
                link.attribute("target", "_blank");
            else if (open.equalsIgnoreCase("this"))
                link.attribute("target", "_self");
            else if (open.equalsIgnoreCase("super"))
                link.attribute("target", "_parent");
            else if (open.equalsIgnoreCase("window"))
                link.attribute("target", "top");
            else
                link.attribute("target", open);
        }
        if(style != null) {
            link.attribute("style", style);
        }
        link.add(children());
        return link;
    }
}