package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:separator")
@Package("xjs-tags-dyn")
public class Separator extends DynamicTag {

    private String style;

    @Override
    public Tag renderInternal() {
        Tag r = new Tag("hr");
        if(style != null) {
            r.attribute("style", style);
        }
        return r;
    }
}