package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.TagContent;
import at.femoweb.xjs.modules.xjs.XJSTag;

@XJSTag("xjs:code")
@Package("xjs-tags-dyn")
public class Code extends DynamicTag {

    private String id;

    @Override
    public Tag renderInternal() {
        if(this.id != null) {
            Object content = Session.get(this.id);
            if(content != null && content instanceof TagContent) {
                return  ((TagContent) content).generateTags();
            } else {
                Server.log.warn("Resource " + id + " is no TagContent provider");
            }
        }
        return new TextTag();
    }
}