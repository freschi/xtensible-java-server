package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Object;
import java.lang.Override;

@XJSTag("xjs:if")
@Package("xjs-tags-dyn")
public class If extends DynamicTag {

    private String check;

    @Override
    public Tag renderInternal() {
        if(check != null) {
            Object chkObj = Session.get(check);
            if(chkObj instanceof ConditionCheck) {
                if(((ConditionCheck) chkObj).check(null)) {
                    Tag component = new Tag("span");
                    component.add(children());
                    return component;
                }
            }
        }
        TextTag empty = new TextTag();
        empty.text("");
        return empty;
    }
}