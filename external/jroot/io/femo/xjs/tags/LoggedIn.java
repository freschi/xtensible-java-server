package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import at.femoweb.xjs.OnLoginHandler;
import at.femoweb.xjs.DefaultReplaceHandler;

import java.lang.Override;
import java.util.ArrayList;
import java.util.List;

@XJSTag("xjs:loggedin")
@Package("xjs-tags-dyn")
public class LoggedIn extends DynamicTag {

    private String id;

    @Override
    @SuppressWarnings("unchecked")
    public Tag renderInternal() {
        Tag t = new Tag("div");
        t.type(TagType.FULL);
        if(this.id == null) {
            id = Long.toHexString(System.nanoTime());
        }
        t.attribute("id", id);
        if(!Session.is("_xjs_user")) {
            if(!Session.is("_xjs_onlogin_list")) {
                Session.set("_xjs_onlogin_list", new ArrayList<OnLoginHandler>());
            }
            List list = (List) Session.get("_xjs_onlogin_list");
            list.add(new DefaultReplaceHandler(children(), id));
        } else {
            t.add(children());
        }
        return t;
    }
}