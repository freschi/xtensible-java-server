package io.femo.xjs.tags;

import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.PackageRoot;
import at.femoweb.xjs.annotations.Version;

@Package("xjs-tags-dyn")
@PackageRoot
@Version("0.1")
public class PackageInfo {

}