package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:col")
@Package("xjs-tags-dyn")
public class Column extends DynamicTag {

    @Named("size-md")
    private String sizeMd;
    @Named("size-sm")
    private String sizeSm;
    @Named("size-xs")
    private String sizeXs;
    private String style;

    @Override
    public Tag renderInternal() {
        Tag c = new Tag("div");
        c.type(TagType.FULL);
        String clazz = "";
        if(sizeMd != null) {
            clazz += "col-md-" + sizeMd;
        }
        clazz += " ";
        if(sizeSm != null) {
            clazz += "col-sm-" + sizeSm;
        }
        clazz += " ";
        if(sizeXs != null) {
            clazz += "col-xs-" + sizeXs;
        }
        c.attribute("class", clazz);
        if(style != null) {
            c.attribute("style", style);
        }
        c.add(children());
        return c;
    }

    public void sizes(int size) {
        sizeMd = "" + size;
        sizeSm = "" + size;
        sizeXs = "" + size;
    }
}