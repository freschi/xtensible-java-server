package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnauthorizedUserException;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.Component;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import at.femoweb.xjs.services.LoginService;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.OnLoginHandler;
import com.google.gson.JsonElement;

import java.lang.Override;
import java.util.List;

@XJSTag("xjs:login")
@Package("xjs-tags-dyn")
public class Login extends DynamicTag {

    private String module;
    private String registerLink;

    @Override
    public Tag renderInternal() {
        if(!Session.is("_xjs_user")) {
            Tag login = new Tag("div");
            login.attribute("class", "jumbotron");
            Row row = new Row();
            Column column = new Column();
            column.sizes(12);
            row.add(column);
            Tag heading = new Tag("h1");
            heading.add("Login");
            column.add(heading);
            login.add(row);
            boolean enabled;
            {
                row = new Row();
                column = new Column();
                column.sizes(12);
                LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
                if(logicalUnit == null) {
                    Tag alert = new Alert();
                    alert.attribute("type", "danger");
                    alert.add("Session not compatible with Login Component. Please contact your webmaster");
                    column.add(alert);
                    enabled = false;
                } else {
                    if(module == null) {
                        Tag alert = new Alert();
                        alert.attribute("type", "danger");
                        alert.add("Authentication module was not set!");
                        column.add(alert);
                        enabled = false;
                    } else {
                        Service service = logicalUnit.getService(module);
                        if(service == null) {
                            Tag alert = new Alert();
                            alert.attribute("type", "danger");
                            alert.add("Authentication module could not be found!");
                            column.add(alert);
                            enabled = false;
                        }
                    }
                }
            }
            Tag uname = new Textbox();
            uname.attribute("id", "name");
            uname.attribute("style", "max-width: 250px");
            uname.attribute("label", "Username");
            login.add(uname);
            Tag pass = new Textbox();
            pass.attribute("id", "pass");
            pass.attribute("type", "password");
            pass.attribute("label", "Password");
            pass.attribute("style", "max-width: 250px");
            pass.attribute("onkeydown", "_xjs_dyn_login_input");
            login.add(pass);
            Component button = new Button();
            button.onClick("_xjs_dyn_login");
            Tag bTag = (Tag) button;
            bTag.attribute("type", "primary");
            bTag.add("Login");
            login.add(bTag);
            if(registerLink != null) {
                Tag register = new Button();
                register.attribute("target", registerLink);
                register.add("Register");
                login.add(register);
            }
            Tag p = new Tag("input");
            p.attribute("value", module);
            p.attribute("hidden", "hidden");
            p.attribute("id", "module");
            login.add(p);
            Tag component = new Tag("div");
            component.add(login);
            component.attribute("id", "_xjs_login_controlled");
            return component;
        } else {
            TextTag empty = new TextTag();
            empty.text("");
            return empty;
        }
    }

    @XAction(name = "_xjs_dyn_login")
    public void performLogin(@XUpdate Updates updates, @XParam(name = "name") String name, @XParam(name = "pass") String pass, @XParam(name = "module") String module) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        LoginService loginService = (LoginService) logicalUnit.getService(module);
        if(loginService.checkCredentials(name, pass.getBytes())) {
            UnifiedUser user;
            try {
                user = loginService.getUserData(name, pass.getBytes());
            } catch (UnauthorizedUserException e) {
                Server.log.warn("Authentication Error", e);
                updates.addMessage(Updates.Severity.DANGER, e.getMessage());
                return;
            }
            Session.set("_xjs_user", user);
            Session.set("user.name", user.getUsername());
            Session.set("user.id", user.getProperty("user.id"));
            updates.html("_xjs_login_controlled", "");
            if(Session.is("_xjs_onlogin_list")) {
                List handlers = (List) Session.get("_xjs_onlogin_list");
                for(Object handler : handlers) {
                    if(handler instanceof OnLoginHandler)
                        ((OnLoginHandler)handler).replace(updates);
                }
            }
            if(user.getProperty("user.email") != null) {
                Session.set("user.email", user.getProperty("user.email"));
            }
        } else {
            updates.addMessage(Updates.Severity.DANGER, "Login failed");
            updates.highlightError("name");
            updates.highlightError("pass");
            updates.value("pass", "");
            updates.focus("name");
        }
    }

    @XAction(name = "_xjs_dyn_login_input")
    public void processInput(@XUpdate Updates updates, @XParam(name = "name") String name, @XParam(name = "pass") String pass, @XParam(name = "module") String module, @XOptional JsonElement jsonElement) {
        int key = jsonElement.getAsJsonObject().get("key").getAsInt();
        if(key == 13) {
            performLogin(updates, name, pass, module);
        } else if (key == 8) {
            updates.value("pass", "");
        }
    }
}