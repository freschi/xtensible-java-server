package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;
import java.util.ArrayList;
import java.util.Collection;

@XJSTag("xjs:column")
@Package("xjs-tags-dyn")
public class TableColumn extends DynamicTag {

    public TableColumn() {
        name("xjs:column");
    }

    public Format getFormat() {
        if(select().name("xjs:format").size() > 0 && select().name("xjs:format").first().get(0) instanceof Format) {
            return (Format) select().name("xjs:format").first().get(0);
        } else {
            return null;
        }
    }

    public Collection<Tag> getTitle() {
        if(select().name("xjs:title").size() > 0) {
            return select().name("xjs:title").first().children();
        } else {
            Collection<Tag> collection = new ArrayList<>();
            collection.add(new TextTag());
            return collection;
        }
    }

    public String getColumn() {
        if(select().name("xjs:data").size() > 0) {
            return select().name("xjs:data").get(0).get(0).text();
        } else {
            return null;
        }
    }

    @Override
    public Tag renderInternal() {
        return null;
    }
}