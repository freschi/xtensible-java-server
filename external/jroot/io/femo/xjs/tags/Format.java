package io.femo.xjs.tags;

import at.femoweb.html.Tag;

public interface Format<T> {

    Class<?> getType();
    Tag format(T value);
}