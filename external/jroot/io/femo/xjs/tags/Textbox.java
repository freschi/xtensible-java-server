package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.Component;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.lang.String;
import java.lang.UnsupportedOperationException;
import java.util.Properties;

@XJSTag("xjs:textbox")
@Package("xjs-tags-dyn")
public class Textbox extends DynamicTag implements Component {

    private String id;
    private String label;
    private String type;
    private String field;
    private String value;
    private String onkeydown;

    @Override
    public Tag renderInternal() {
        if(id != null) {
            Tag formGroup = new Tag("div");
            formGroup.attribute("class", "form-group");
            formGroup.attribute("id", id + "_group");
            Tag label = new Tag("label");
            label.attribute("class", "control-label");
            label.type(TagType.FULL);
            if(this.label != null) {
                label.add(this.label);
            } else {
                label.add(this.children());
            }
            formGroup.add(label);
            formGroup.add(new Tag("br"));
            Tag input = new Tag("input");
            input.attribute("id", id);
            input.attribute("class", "form-control");
            if(type != null) {
                if(type.equalsIgnoreCase("date")) {
                    input.attribute("class", input.attribute("class") + " _xjs_datepicker");
                    input.attribute("type", "text");
                } else  if (type.equalsIgnoreCase("sql-date")) {
                    input.attribute("class", input.attribute("class") + " _xjs_sqldatepicker");
                    input.attribute("data-date-format", "YYYY-MM-DD");
                    input.attribute("type", "text");
                } else {
                    input.attribute("type", type);
                }
            } else {
                input.attribute("type", "text");
            }
            if(field != null && Session.is(field) && Session.get(field) != null) {
                input.attribute("value", String.valueOf(Session.get(field)));
            } else if (value != null) {
                input.attribute("value", value);
            }
            if(onkeydown != null) {
                input.attribute("onkeydown", "invoke('" + onkeydown + "', {key: event.keyCode});");
            }
            formGroup.add(input);
            return formGroup;
        } else {
            Server.log.warn("Textbox requires id");
            return new TextTag();
        }
    }

    @Override
    public void onClick(String action) {

    }

    @Override
    public void onClick(String action, Properties extra) {

    }

    @Override
    public void onKeyDown(String action) {

    }

    @Override
    public void onKeyDown(String action, Properties extra) {
        throw new UnsupportedOperationException("This input component does not support passing extras");
    }

}