package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:row")
@Package("xjs-tags-dyn")
public class Row extends DynamicTag {

    @Named("class")
    private String clazz;
    private String style;

    @Override
    public Tag renderInternal() {
        Tag r = new Tag("div");
        r.type(TagType.FULL);
        if(clazz != null) {
            r.attribute("class", "row " + clazz);
        } else {
            r.attribute("class", "row");
        }
        if(style != null) {
            r.attribute("style", style);
        }
        r.add(children());
        return r;
    }
}