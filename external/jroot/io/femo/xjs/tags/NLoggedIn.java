package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.OnLoginHandler;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@XJSTag("xjs:nloggedin")
@Package("xjs-tags-dyn")
public class NLoggedIn extends DynamicTag {

    private String id;

    @Override
    @SuppressWarnings("unchecked")
    public Tag renderInternal() {
        Tag t = new Tag("div");
        t.type(TagType.FULL);
        if(id == null) {
            id = "_xjs_placeholder" + Long.toHexString(System.nanoTime());
        }
        t.attribute("id", id);
        if(!Session.is("_xjs_user")) {
            t.add(this.children());
            if(!Session.is("_xjs_onlogin_list")) {
                Session.set("_xjs_onlogin_list", new ArrayList<OnLoginHandler>());
            }
            List<OnLoginHandler> list = (List<OnLoginHandler>) Session.get("_xjs_onlogin_list");
            list.add(new EmptyReplaceHandler(id));
        }
        return t;
    }

    private class EmptyReplaceHandler implements OnLoginHandler {

        private String id;

        public EmptyReplaceHandler (String id) {
            this.id = id;
        }

        @Override
        public void replace(Updates updates) {
            updates.html(id, "");
            Server.log.info("Emptying " + id);
        }
    }
}