package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import at.femoweb.xjs.services.RegistrationService;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.UnsupportedEncodingException;
import java.lang.Override;
import java.util.Properties;

@XJSTag("xjs:register")
@Package("xjs-tags-dyn")
public class Register extends DynamicTag {

    private String module;
    private String forwardingPage;
    private String autoLogin;

    @Override
    public Tag renderInternal() {
        Tag register = new Tag("div");
        //register.attribute("class", "jumbotron");
        Tag heading = new Tag("h1", register);
        heading.add("Register");
        Tag row = new Row();
        Column lcol = new Column();
        row.add(lcol);
        lcol.sizes(6);
        Tag headLogin = new Tag("h2", lcol);
        headLogin.add("Login Information");
        Column rcol = new Column();
        row.add(rcol);
        rcol.sizes(6);
        Tag headUser = new Tag("h2", rcol);
        headUser.add("User Information");
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        if(logicalUnit == null) {
            Alert alert = new Alert();
            alert.attribute("type", "danger");
            Tag text = new Tag("strong", alert);
            text.add("Session not compatible with XJS Register Component. Please contact your webmaster!");
            register.add(alert);
        } else {
            Service service = logicalUnit.getService(module);
            if(service == null) {
                Alert alert = new Alert();
                alert.attribute("type", "danger");
                Tag text = new Tag("strong", alert);
                text.add("Registration module not loaded. Please contact your webmaster!");
                register.add(alert);
            }
        }
        Tag uname = new Textbox();
        uname.attribute("label", "Username");
        uname.attribute("id", "name");
        lcol.add(uname);
        Tag pass = new Textbox();
        pass.attribute("type", "password");
        pass.attribute("id", "pass");
        pass.attribute("label", "Password");
        lcol.add(pass);
        Tag passRep = new Textbox();
        passRep.attribute("type", "password");
        passRep.attribute("id", "pass_repeat");
        passRep.attribute("label", "Retype Password");
        lcol.add(passRep);
        Tag firstName = new Textbox();
        firstName.attribute("id", "firstname");
        firstName.attribute("label", "First Name");
        rcol.add(firstName);
        Tag lastName = new Textbox();
        lastName.attribute("id", "lastname");
        lastName.attribute("label", "Last Name");
        rcol.add(lastName);
        Tag display = new Textbox();
        display.attribute("id", "display");
        display.attribute("label", "Display Name");
        rcol.add(display);
        Tag email = new Textbox();
        email.attribute("type", "email");
        email.attribute("id", "email");
        email.attribute("label", "E-Mail");
        rcol.add(email);
        register.add(row);
        Button button = new Button();
        button.attribute("type", "primary");
        Tag icon = new FaIcon();
        icon.attribute("icon", "floppy-o");
        button.add(icon);
        button.add("Register");
        register.add(button);
        Properties properties = new Properties();
        properties.setProperty("module", module);
        if(forwardingPage != null)
            properties.setProperty("forwardingPage", forwardingPage);
        if(autoLogin != null)
            properties.setProperty("autoLogin", autoLogin);
        button.onClick("_xjs_dyn_register", properties);
        return register;
    }

    @XAction(name = "_xjs_dyn_register")
    public void register(@XUpdate Updates updates, @XParam(name = "name") String name, @XParam(name = "pass") String pass,
                         @XParam(name = "pass_repeat") String passRepeat, @XParam(name = "firstname") String firstname, @XParam(name = "lastname") String lastname,
                         @XParam(name = "display") String display, @XParam(name = "email") String email, @XOptional JsonElement optionals) {
        JsonObject optional = optionals.getAsJsonObject();
        String module = null, autoLogin = null, forwardingPage = null;
        if(optional.has("module")) {
            module = optional.get("module").getAsString();
        }
        if(optional.has("autoLogin")) {
            autoLogin = optional.get("autoLogin").getAsString();
        }
        if(optional.has("forwardingPage")) {
            forwardingPage = optional.get("forwardingPage").getAsString();
        }
        boolean correct = true;
        if(name.equals("")) {
            updates.highlightError("name");
            correct = false;
        } else {
            updates.removeError("name");
        }
        if(pass.equals("")) {
            updates.highlightError("pass");
            correct = false;
        } else {
            updates.removeError("pass");
        }
        if(passRepeat.equals("")) {
            updates.highlightError("pass_repeat");
            correct = false;
        } else {
            updates.removeError("pass_repeat");
        }
        if(firstname.equals("")) {
            updates.highlightError("firstname");
            correct = false;
        } else {
            updates.removeError("firstname");
        }
        if(lastname.equals("")) {
            updates.highlightError("lastname");
            correct = false;
        } else {
            updates.removeError("lastname");
        }
        if(display.equals("")) {
            updates.highlightError("display");
            correct = false;
        } else {
            updates.removeError("display");
        }
        if(email.equals("")) {
            updates.highlightError("email");
            correct = false;
        } else {
            updates.removeError("email");
        }
        if(!correct) {
            updates.addMessage(Updates.Severity.WARNING, "Not all fields are set!");
            return;
        }
        if(!pass.equals(passRepeat)) {
            updates.highlightError("pass");
            updates.highlightError("pass_repeat");
            updates.addMessage(Updates.Severity.WARNING, "Passwords do not match");
        }
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        if(logicalUnit.hasService(module)) {
            Service service = logicalUnit.getService(module);
            if(service instanceof RegistrationService) {
                try {
                    if(((RegistrationService) service).register(name, pass.getBytes("UTF-8"), firstname, lastname, display, email)) {
                        if(forwardingPage.equals("")) {
                            forwardingPage = "";
                        }
                        updates.changeLocation(forwardingPage);
                    } else {
                        updates.addMessage(Updates.Severity.WARNING, "Registration failed! Check if you entered everything correctly!");
                    }
                } catch (UnsupportedEncodingException e) {
                    Server.log.warn("Server does not support UTF-8", e);
                }
            }
        }
    }
}