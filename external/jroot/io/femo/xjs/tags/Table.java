package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.TableDataSource;
import at.femoweb.xjs.modules.xjs.XJSTag;

import javax.swing.table.TableCellEditor;
import java.lang.Override;
import java.lang.Throwable;
import java.util.Collection;

@XJSTag("xjs:table")
@Package("xjs-tags-dyn")
public class Table extends DynamicTag {

    @Override
    public Tag renderInternal() {
        Tag source = select().name("xjs:source").first();
        Tag table = new Tag("table");
        table.attribute("class", "table table-bordered table-hover table-striped");
        TableDataSource tableDataSource;
        if(source.get(0) instanceof TableDataSource) {
            tableDataSource = (TableDataSource) source.get(0);
        } else {
            return new TextTag();
        }
        Columns columns = null;
        for(Tag tag : children()) {
            if(tag instanceof Columns) {
                columns = (Columns) tag;
            }
        }
        if(columns == null) {
            return new TextTag();
        }
        try {
            tableDataSource.init();
            Collection<Tag>[] headers = columns.getHeaders();

            Tag hrow = new Tag("tr", new Tag("thead", table));
            for (Collection<Tag> tag : headers) {
                Tag th = new Tag("th", hrow);
                th.add(tag);
            }
            if (tableDataSource.first()) {
                do {
                    columns.first();
                    Tag row = new Tag("tr", table);
                    while (columns.next()) {
                        Tag td = new Tag("td", row);
                        columns.renderElement(tableDataSource, td);
                    }
                } while (tableDataSource.nextRow());
            }
        } catch (Throwable e) {
            Server.log.warn("Could not create data table", e);
        }
        return table;
    }
}