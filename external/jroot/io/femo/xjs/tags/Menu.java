package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:menu")
@Package("xjs-tags-dyn")
public class Menu extends DynamicTag {

    private String content;
    private String title;

    @Override
    public Tag renderInternal() {
        Tag menu = new Tag("ul");
        menu.attribute("style", "list-style-type: none; padding-left: 10px");
        Tag input = new Tag("input");
        input.attribute("type", "hidden");
        input.attribute("id", "_xjs_menu_content");
        if(content != null) {
            input.attribute("value", content);
        }
        menu.add("input");
        input = new Tag("input");
        input.attribute("type", "hidden");
        input.attribute("id", "_xjs_menu_title");
        if(title != null) {
            input.attribute("value", title);
        }
        menu.add(input);
        return menu;
    }
}