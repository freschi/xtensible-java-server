package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:file")
@Package("xjs-tags-dyn")
public class File extends DynamicTag {

    private String id;
    private String type;
    private String text;
    private String icon;
    private String size;
    private String filetype;

    @Override
    public Tag renderInternal() {
        Tag input = new Tag("input");
        input.attribute("type", "file");
        input.attribute("class", "filestyle");
        if(id != null) {
            input.attribute("id", id);
        }
        if(type != null) {
            input.attribute("data-buttonName", "btn-" + type);
        }
        if(text != null) {
            input.attribute("data-buttonText", text);
        }
        if(icon != null) {
            input.attribute("data-iconName", "glyphicon-" + icon);
        }
        if(size != null) {
            input.attribute("data-size", size);
        }
        if(filetype != null) {
            input.attribute("accept", filetype);
        }
        return input;
    }
}