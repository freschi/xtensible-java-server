package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:menuLabel")
@Package("xjs-tags-dyn")
public class MenuLabel extends DynamicTag {

    @Override
    public Tag renderInternal() {
        Tag label = new Tag("li");
        label.attribute("style", "font-weight:bold");
        label.add(children());
        return label;
    }
}