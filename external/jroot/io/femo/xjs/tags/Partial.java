package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.XJSFileProvider;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSFileProviderService;
import at.femoweb.xjs.modules.xjs.XJSTag;
import at.femoweb.xjs.structure.LogicalUnit;

import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.util.Collection;

@XJSTag("xjs:partial")
@Package("xjs-tags-dyn")
public class Partial extends DynamicTag {

    private String path;

    //@Override
    public void createFromTag(Tag tag) {
        if(tag.attribute("path") != null) {
            this.path = tag.attribute("path");
        }
        Server.log.info("Created Tag from " + tag.render());
    }

    @Override
    public Tag renderInternal() {
        XJSFileProviderService service = (XJSFileProviderService) ((LogicalUnit)Session.get("_xjs_logical_unit")).getService("xjs-file-provider-service");
        Object result = service.put(path);
        if(result instanceof Tag) {
            return ((Tag) result);
        } else if (result instanceof Collection) {
            Tag res = new Tag("div");
            for(Object object : (Collection) result) {
                if(object instanceof Tag) {
                    res.add((Tag) object);
                } else {
                    res.add(object.toString());
                }
            }
            return res;
        } else {
            Tag res = new Tag("span");
            res.add(String.valueOf(res));
            return res;
        }
    }
}