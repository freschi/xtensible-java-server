package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.ListProvider;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:comboBox")
@Package("xjs-tags-dyn")
public class ComboBox extends DynamicTag {

    private String value;
    private String field;
    private String label;
    private String id;
    private String list;

    @Override
    public Tag renderInternal() {
        Tag div = new Tag("div");
        Tag group = new Tag("div");
        group.attribute("class", "btn-group");
        Tag btn = new Tag("a");
        btn.attribute("class", "btn dropdown-toggle btn-default");
        btn.attribute("style", "min-width: 250px;");
        btn.attribute("data-toggle", "dropdown");
        String set_value = null;
        if(value != null || field != null) {
            if (field != null && Session.is(field) && Session.get(field) != null) {
                set_value = Session.get(field).toString();
            } else if(value != null) {
                set_value = value;
            } else {
                if (label != null) {
                    btn.add(label);
                } else {
                    btn.add("Please choose ...");
                }
            }
        } else {
            if (label != null) {
                btn.add(label);
            } else {
                btn.add("Please choose ...");
            }
        }
        if(id != null) {
            btn.attribute("id", id + "_btn");
        }
        Tag list = new Tag("ul");
        list.attribute("class", "dropdown-menu");
        boolean found = false;
        if(this.list != null) {
            Object listProvider = Session.get(this.list);
            if(listProvider != null && listProvider instanceof ListProvider) {
                ListProvider provider = (ListProvider) listProvider;
                for(ListProvider.ListItem listItem : provider.createList()) {
                    list.add(createItem(id, listItem.getValue(), listItem.getTag()));
                    if(set_value != null && listItem.getValue().equals(set_value)) {
                        found = true;
                        btn.add(listItem.getTag());
                    }
                }
            } else {
                btn.attribute("class", btn.attribute("class") + " disabled");
            }
        } else {
            int value = 0;
            for(Tag child : children()) {
                if(child == null || child.name() == null)
                    continue;
                if(child.name().equalsIgnoreCase("xjs:item")) {
                    list.add(createItem(id, value + "", child));
                    if(set_value != null && Integer.parseInt(set_value) == value) {
                        btn.add(child.children());
                        found = true;
                    }
                    value++;
                }
            }
        }
        Tag caret = new Tag("span");
        caret.attribute("class", "caret");
        caret.type(TagType.FULL);
        btn.add(caret);
        group.add(btn);
        group.add(list);
        div.add(group);
        if(id != null) {
            Tag value = new Tag("input");
            value.attribute("type", "hidden");
            value.attribute("id", id);
            if(found) {
                value.attribute("value", set_value);
            }
            div.add(value);
        }
        return div;
    }

    private Tag createItem(String id, String value, Tag child) {
        Tag item = new Tag("li");
        Tag link = new Tag("a");
        if(child == null)
            link.add("null");
        else
            link.add(child.children());
        if(id != null) {
            link.attribute("onclick", "_xjs_update_combobox('" + id + "', '" + value + "');");
            link.attribute("id", id + "_" + value);
        }
        item.add(link);
        return item;
    }
}