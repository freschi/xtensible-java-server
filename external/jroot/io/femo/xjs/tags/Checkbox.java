package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;

@XJSTag("xjs:checkbox")
@Package("xjs-tags-dyn")
public class Checkbox extends DynamicTag {

    private String id;
    private String label;
    private String type;
    private String circle;
    private String field;
    private String disabled;

    //@Override
    public void createFromTag(Tag tag) {
        this.id = tag.attribute("id");
        this.label = tag.attribute("label");
        this.type = tag.attribute("type");
        this.circle = tag.attribute("circle");
        this.field = tag.attribute("field");
    }

    @Override
    public Tag renderInternal() {
        Tag cb = new Tag("div");
        cb.attribute("class", "checkbox");
        Tag input = new Tag("input");
        input.attribute("type", "checkbox");
        Tag label = new Tag("label");
        if(this.id != null) {
            input.attribute("id", this.id);
            label.attribute("for", this.id);
        }
        if(this.label != null) {
            label.add(this.label);
        }
        if(this.type != null) {
            cb.attribute("class", cb.attribute("class") + " checkbox-" + this.type);
        }
        if(this.circle != null) {
            cb.attribute("class", cb.attribute("class") + " checkbox-circle");
        }
        if(this.disabled != null) {
            input.attribute("disabled", "disabled");
        }
        if(this.field != null) {
            if(Session.is(this.field)) {
                String val = String.valueOf(Session.get(this.field));
                if(!val.equals("0")) {
                    input.attribute("checked", "checked");
                }
            }
        }
        cb.add(input);
        cb.add(label);
        return cb;
    }
}