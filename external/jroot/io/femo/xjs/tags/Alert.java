package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Override;
import java.lang.String;

@XJSTag("xjs:alert")
@Package("xjs-tags-dyn")
public class Alert extends DynamicTag {

    private String type;

    //@Override
    public void createFromTag(Tag tag) {
        if(tag.attribute("type") != null) {
            this.type = tag.attribute("type");
        }
    }

    @Override
    public Tag renderInternal() {
        Tag alert = new Tag("div");
        alert.attribute("class", "alert alert-" + type);
        alert.add(children());
        return alert;
    }
}