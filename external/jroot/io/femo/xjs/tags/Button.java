package io.femo.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.Component;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.XJSTag;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.lang.String;
import java.util.Properties;

@XJSTag("xjs:button")
@Package("xjs-tags-dyn")
public class Button extends DynamicTag implements Component {

    private String onclick;
    private String onclickExtra;
    private String target;
    private String type;
    private String size;
    private String open;
    private String block;
    private String style;
    private String disabled;

    @Override
    public Tag renderInternal() {
        Tag button;
        if(onclick != null) {
            button = new Tag("a");
            if(onclickExtra != null) {
                button.attribute("onclick", "invoke('" + onclick + "', " + onclickExtra + ");");
            } else {
                button.attribute("onclick", "invoke('" + onclick + "');");
            }
        } else if (target != null) {
            button = new Tag("a");
            button.attribute("href", target);
        } else {
            button = new Tag("a");
        }
        if(type != null) {
            if (type.equalsIgnoreCase("primary")) {
                button.attribute("class", "btn btn-primary");
            } else if (type.equalsIgnoreCase("info")) {
                button.attribute("class", "btn btn-info");
            } else if (type.equalsIgnoreCase("warning")) {
                button.attribute("class", "btn btn-warning");
            } else if (type.equalsIgnoreCase("danger")) {
                button.attribute("class", "btn btn-danger");
            } else if (type.equalsIgnoreCase("success")) {
                button.attribute("class", "btn btn-success");
            } else if (type.equalsIgnoreCase("link")) {
                button.attribute("class","btn btn-link");
            } else {
                button.attribute("class", "btn btn-default");
            }
        } else {
            button.attribute("class", "btn btn-default");
        }
        if(size != null) {
            if(size.equalsIgnoreCase("large")) {
                button.attribute("class" , button.attribute("class") + " btn-lg");
            } else if(size.equalsIgnoreCase("small")) {
                button.attribute("class" , button.attribute("class") + " btn-sm");
            } else if(size.equalsIgnoreCase("mini")) {
                button.attribute("class" , button.attribute("class") + " btn-xs");
            }
        }
        if(open != null) {
            if(open.equalsIgnoreCase("new"))
                button.attribute("target", "_blank");
            else if (open.equalsIgnoreCase("this"))
                button.attribute("target", "_self");
            else if (open.equalsIgnoreCase("super"))
                button.attribute("target", "_parent");
            else if (open.equalsIgnoreCase("window"))
                button.attribute("target", "top");
            else
                button.attribute("target", open);
        } else {
            button.attribute("target", "_self");
        }
        if(block != null) {
            button.attribute("class", button.attribute("class") + " btn-block");
        }
        if(style != null) {
            button.attribute("style", style);
        }
        if(disabled != null) {
            button.attribute("disabled", "disabled");
        }
        button.add(this.children());
        return button;
    }

    @Override
    public void onClick(String action) {
        onclick = action;
    }

    @Override
    public void onClick(String action, Properties extra) {
        onclick = action;
        /*final JsonObject object = new JsonObject();
        extra.forEach((k, v) -> {
            object.addProperty(String.valueOf(k), String.valueOf(v));
        });*/
        onclickExtra = Component.convertProperties(extra);
    }

    @Override
    public void onKeyDown(String action) {

    }

    @Override
    public void onKeyDown(String action, Properties extra) {

    }
}