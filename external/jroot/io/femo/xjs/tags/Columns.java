package io.femo.xjs.tags;

import at.femoweb.html.Selection;
import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.modules.xjs.DynamicTag;
import at.femoweb.xjs.modules.xjs.TableDataSource;
import at.femoweb.xjs.modules.xjs.XJSTag;

import java.lang.Number;
import java.lang.Override;
import java.lang.SuppressWarnings;
import java.lang.Throwable;
import java.util.Collection;
import java.util.Date;

@XJSTag("xjs:columns")
@Package("xjs-tags-dyn")
public class Columns extends DynamicTag {

    private TableColumn current = null;
    private int index = 0;

    @SuppressWarnings("unchecked")
    public Collection<Tag>[] getHeaders() {
        Collection<Tag>[] tags = new Collection[select().name("xjs:column").size()];
        int counter = 0;
        for(Tag tag : children()) {
            if(tag != null && tag.name() != null && tag.name().equals("xjs:column")) {
                TableColumn column = (TableColumn) tag;
                tags[counter] = column.getTitle();
                counter++;
            }
        }
        return tags;
    }

    public boolean next() {
        Selection selection = select().name("xjs:column");
        if(selection.size() > index) {
            current = (TableColumn) selection.get(index);
            index++;
            return true;
        }
        return false;
    }

    public void first() {
        current = null;
        index = 0;
    }

    @SuppressWarnings("unchecked")
    public void renderElement(TableDataSource tableDataSource, Tag td) throws Throwable{
        Format format = current.getFormat();
        if(format != null) {
            Class<?> type = format.getType();
            if (type == Number.class) {
                double value = Double.parseDouble(String.valueOf(tableDataSource.getColumn(double.class, current.getColumn())));
                td.add(String.valueOf(format.format(value)));
            } else if (type == Date.class) {
                Date value = tableDataSource.getColumn(Date.class, current.getColumn());
                td.add(String.valueOf(format.format(value)));
            } else {
                String value = tableDataSource.getColumn(String.class, current.getColumn());
                td.add(String.valueOf(format.format(value)));
            }
        } else {
            td.add(String.valueOf(tableDataSource.getColumn(String.class, current.getColumn())));
        }
    }

    @Override
    public Tag renderInternal() {
        return null;
    }
}