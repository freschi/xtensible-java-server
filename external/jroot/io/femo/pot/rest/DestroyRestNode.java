package io.femo.pot.rest;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.http.rest.RestGet;
import at.femoweb.xjs.modules.http.rest.RestNode;
import at.femoweb.xjs.modules.http.rest.Security;

import java.lang.Override;

@RestNode(path = "pot/destroy", methods = "get", descriptions = "Destroys a currently existing session. Please abort requests if http authorization is required!")
@Security(module = "femoweb-user")
public class DestroyRestNode implements RestGet {

    @Override
    public int get(HttpRequest request, HttpResponse response) {
        Session.destroy();
        return 200;
    }
}