package io.femo.pot.rest;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.http.rest.RestGet;
import at.femoweb.xjs.modules.http.rest.RestNode;
import at.femoweb.xjs.modules.http.rest.Security;
import at.femoweb.xjs.util.ResultCallback;
import at.femoweb.xjs.util.SQL;
import at.femoweb.xjs.util.ValueProvider;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.lang.Integer;
import java.lang.Override;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@RestNode(path = "pot/projects", methods = "get", descriptions = "Returns a list of projects for the current user")
@Security(module = "femoweb-user")
public class ProjectsRestNode implements RestGet {

    @Override
    public int get(HttpRequest request, HttpResponse response) {
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            final UnifiedUser unifiedUser = (UnifiedUser) Session.get("_xjs_user");
            final JsonArray projects = new JsonArray();
            SQL.query(connection, "select idp_projects, p_name, p_short from (select distinct idp_projects, p_name, p_short from (select * from e_entries where e_u_user = ?) as a inner join p_projects on idp_projects = a.e_p_project inner join (select max(e_date) date, e_p_project from e_entries group by e_p_project) as b on a.e_p_project = b.e_p_project where a.e_date = b.date order by b.date desc) as a UNION (SELECT idp_projects, p_name, p_short FROM p_projects inner join w_works on w_p_project = idp_projects where w_u_user = ? order by p_short)")
                    .values(new ValueProvider() {
                        @Override
                        public boolean next() {
                            return false;
                        }

                        @Override
                        public void putData(PreparedStatement preparedStatement) throws SQLException {
                            preparedStatement.setInt(1, Integer.parseInt(unifiedUser.getProperty("user.id")));
                            preparedStatement.setInt(2, Integer.parseInt(unifiedUser.getProperty("user.id")));
                        }
                    })
                    .execute(new ResultCallback() {
                        @Override
                        public void result(ResultSet resultSet) throws SQLException {
                            while (resultSet.next()) {
                                JsonObject project = new JsonObject();
                                project.addProperty("id", resultSet.getInt(1));
                                project.addProperty("name", resultSet.getString(2));
                                project.addProperty("short", resultSet.getString(3));
                                projects.add(project);
                            }
                        }
                    });
            response.setEntity(projects.toString());
            return 200;
        } catch (SQLException e) {
            Server.log.warn("Error while loading projects", e);
            return 500;
        }

    }
}