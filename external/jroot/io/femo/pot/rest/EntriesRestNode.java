package io.femo.pot.rest;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.security.ACL;
import at.femoweb.xjs.annotations.security.Permit;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.http.rest.RestNode;
import at.femoweb.xjs.modules.http.rest.RestPost;
import at.femoweb.xjs.modules.http.rest.Security;
import at.femoweb.xjs.util.SQL;
import at.femoweb.xjs.util.ValueProvider;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.sql.*;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@RestNode(path = "pot/entries", descriptions = "Creates a new entry for the current user with the supplied information", methods = "post")
@Security(module = "femoweb-user")
public class EntriesRestNode implements RestPost {

    @Override
    @ACL
    @Permit(Permit.Check.AUTHENTICATED_USER)
    public int post(HttpRequest request, HttpResponse response) {
        try {
            JsonElement jsonElement = readJsonData(request);
            final JsonObject data = jsonElement.getAsJsonObject();
            Connection connection = (Connection) Session.get("connection");
            SQL.query(connection, "INSERT INTO e_entries (e_date, e_hours, e_descr," +
                    "e_t_type, e_p_project, e_u_user, e_inschool, e_ta_task) VALUES (?, ?, ?, ?, ?, ?, ?, ?)").values(new ValueProvider() {
                @Override
                public boolean next() {
                    return false;
                }

                @Override
                public void putData(PreparedStatement preparedStatement) throws SQLException{
                    preparedStatement.setDate(1, new Date(data.get("date").getAsLong()));
                    preparedStatement.setDouble(2, data.get("hours").getAsDouble());
                    preparedStatement.setString(3, data.get("description").getAsString());
                    preparedStatement.setInt(4, data.get("typeId").getAsInt());
                    preparedStatement.setInt(5, data.get("projectId").getAsInt());
                    preparedStatement.setInt(6, Integer.parseInt(Session.getUser().getProperty("user.id")));
                    preparedStatement.setBoolean(7, data.get("inSchool").getAsBoolean());
                }
            }).execute();
            return 200;
        } catch (IOException | SQLException e) {
            return 500;
        }
    }
}