package io.femo.pot.rest;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.modules.http.rest.RestGet;
import at.femoweb.xjs.modules.http.rest.RestNode;
import at.femoweb.xjs.modules.http.rest.Security;

import java.lang.Override;

@RestNode(path = "pot/validate", descriptions = "Checks if a supplied user is valid or not", methods = "get")
@Security(module = "femoweb-user")
public class ValidateRestNode implements RestGet {

    @Override
    public int get(HttpRequest request, HttpResponse response) {
        response.setEntity("1");
        return 200;
    }

}