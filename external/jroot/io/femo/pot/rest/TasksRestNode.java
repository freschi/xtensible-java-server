package io.femo.pot.rest;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.security.ACL;
import at.femoweb.xjs.annotations.security.Permit;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.http.rest.RestGet;
import at.femoweb.xjs.modules.http.rest.RestNode;
import at.femoweb.xjs.modules.http.rest.RestPost;
import at.femoweb.xjs.modules.http.rest.Security;
import at.femoweb.xjs.util.ResultCallback;
import at.femoweb.xjs.util.SQL;
import at.femoweb.xjs.util.ValueProvider;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.lang.Integer;
import java.lang.Override;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@RestNode(path = "pot/tasks", methods = "post", descriptions = "Returns a list of all tasks stored for the project. Call with JSON Object {project: <project_id>}")
@Security(module = "femoweb-user")
public class TasksRestNode implements RestPost {

    @Override
    @ACL
    @Permit(Permit.Check.AUTHENTICATED_USER)
    public int post(HttpRequest request, HttpResponse response) {
        try {
            JsonElement jsonElement = readJsonData(request);
            JsonObject data = jsonElement.getAsJsonObject();
            Connection connection = (Connection) Session.get("connection");
            String project = data.get("project").getAsString();
            final JsonArray tasks = new JsonArray();
            SQL.query(connection, "SELECT ta_name, ta_id, idta_tasks FROM ta_tasks WHERE ta_p_project = ?").values(new ValueProvider() {
                @Override
                public boolean next() {
                    return false;
                }

                @Override
                public void putData(PreparedStatement preparedStatement) throws SQLException {
                    preparedStatement.setInt(1, Integer.parseInt(project));
                }
            }).execute(new ResultCallback() {
                @Override
                public void result(ResultSet r) throws SQLException {
                    while (r.next()) {
                        JsonObject task = new JsonObject();
                        task.addProperty("name", r.getString(1));
                        task.addProperty("id", r.getString(3));
                        task.addProperty("taskId", r.getString(2));
                        tasks.add(task);
                    }
                }
            });
            /*Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT ta_name, ta_id, idta_tasks FROM ta_tasks WHERE ta_p_project = " + project);
            while (resultSet.next()) {
                JsonObject task = new JsonObject();
                task.addProperty("name", resultSet.getString(1));
                task.addProperty("id", resultSet.getString(3));
                task.addProperty("taskId", resultSet.getString(2));
                tasks.add(task);
            }
            statement.close();*/
            response.setEntity(tasks.toString());
            return 200;
        } catch (SQLException | IOException e) {
            return 500;
        }
    }
}