package io.femo.pot.rest;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.http.rest.RestGet;
import at.femoweb.xjs.modules.http.rest.RestNode;
import at.femoweb.xjs.services.DataConnectionService;
import at.femoweb.xjs.util.ResultCallback;
import at.femoweb.xjs.util.SQL;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@RestNode(path = "pot/types", methods = "get", descriptions = "Returns all types available for type creation")
public class TypesRestNode implements RestGet {

    @Override
    public int get(HttpRequest request, HttpResponse response) {
        DataConnectionService dataConnectionService = ((BootableLogicalUnit) Session.getLogicalUnit()).getService(DataConnectionService.class);
        try {
            Connection connection = dataConnectionService.connectDataSource(null);
            final JsonArray types = new JsonArray();
            SQL.query(connection, "SELECT idt_types, t_name FROM t_types").execute(new ResultCallback() {
                @Override
                public void result(ResultSet r) throws SQLException {
                    while (r.next()) {
                        JsonObject type = new JsonObject();
                        type.addProperty("id", r.getInt(1));
                        type.addProperty("name", r.getString(2));
                        types.add(type);
                    }
                }
            });
            response.setEntity(types.toString());
            connection.close();
            return 200;
        } catch (SQLException e) {
            return 500;
        }
    }
}