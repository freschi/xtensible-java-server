package io.femo.pot.code;

import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.TagContent;

import java.lang.Override;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("pot-settings-store")
public class PotSettingsPrepare implements TagContent {

    @Override
    public Tag generateTags() {
        Connection connection = (Connection) Session.get("connection");
        try {
            String id = String.valueOf(Session.get("curr-project"));
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT p_name, p_short, p_descr FROM p_projects WHERE idp_projects = " + id);
            if(resultSet.first()) {
                Session.set("pot-settings-name", resultSet.getString("p_name"));
                Session.set("pot-settings-short", resultSet.getString("p_short"));
                Session.set("pot-settings-descr", resultSet.getString("p_descr"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Tag empty = new Tag();
        empty.add("");
        return empty;
    }
}