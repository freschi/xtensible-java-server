package io.femo.pot.pages.project.changelog;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.XJSFile;
import at.femoweb.xjs.modules.Session;

import at.femoweb.xjs.modules.xjs.Component;
import io.femo.xjs.tags.*;

import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@XJSFile(path = "project/changelog/version_details.xjs", timestamp = 0L)
@Package("femoio-pot")
public class VersionDetails extends Tag {

    public VersionDetails(String name) {
        super(name);
    }

    public VersionDetails() {
        super();
    }

    @Override
    public String render(int level, boolean alone) {
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            Tag panel = new Panel();
            ResultSet resultSet = statement.executeQuery("SELECT v_no, v_name FROM v_versions WHERE idv_version = " + Session.get("pot-curr-version-id"));
            if(!resultSet.first()) {
                throw new RuntimeException("Illegal access for version");
            }
            Tag head = new Tag("xjs:head", panel);
            Tag heading = new Tag("h4", head);
            heading.add(resultSet.getString(1));
            if(resultSet.getString(2) != null && !resultSet.getString(2).equals("")) {
                heading.add(" - ");
                heading.add(resultSet.getString(2));
            }
            Tag body = new Tag("xjs:body", panel);
            Tag label = new Tag("strong", body);
            label.add("Changes");
            Tag list = new Tag("ul", body);
            resultSet = statement.executeQuery("select c_descr from c_changes where c_v_version = " + Session.get("pot-curr-version-id") + " order by idc_changes asc;");
            if(resultSet.first()) {
                do {
                    Tag item = new Tag("li", list);
                    item.add(resultSet.getString(1));
                } while (resultSet.next());
            }
            return panel.render();
        } catch (SQLException e) {
            Server.log.warn("Could not create version view", e);
        }
        return "";
    }
}