package io.femo.pot.pages.project.changelog;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.annotations.XJSFile;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import com.google.gson.JsonElement;

import java.lang.Override;
import java.lang.String;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@XJSFile(path = "project/changelog/changelog_view.xjs", timestamp = 0L)
@Package("femoio-pot")
public class ChangelogView extends Tag {

    public ChangelogView(String name) {
        super(name);
    }

    public ChangelogView() {
        super();
    }

    @Override
    public String render() {
        Connection connection = (Connection) Session.get("connection");
        String result = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select v_no, c_descr, v_name, idc_changes, idv_version from c_changes inner join v_versions on c_v_version = idv_version where v_p_project = " + Session.get("curr-project") + " order by v_no desc, idc_changes desc");
            if(resultSet.first()) {
                Tag log = new Tag("div");
                Tag list = null;
                String version = null;
                do {
                    if(version == null || !version.equals(resultSet.getString(1))) {
                        version = resultSet.getString(1);
                        Tag heading = new Tag("h5", log);
                        Tag headingA = new Tag("a", heading);
                        headingA.add(version);
                        headingA.attribute("style", "color: #444");
                        headingA.attribute("onclick", "invoke('pot_changelog_view_version', {version: " + resultSet.getString(5) + "});");
                        if(resultSet.getString(3) != null && !resultSet.getString(3).equals("")) {
                            headingA.add(" - ");
                            headingA.add(resultSet.getString(3));
                        }
                        list = new Tag("ul");
                        log.add(list);
                    }
                    Tag item = new Tag("li", list);
                    Tag link = new Tag("a", item);
                    link.add(resultSet.getString(2));
                    link.attribute("style", "color: #444");
                    link.attribute("onclick", "invoke('pot_changelog_view_item', {change: " + resultSet.getString(4) + "});");
                } while (resultSet.next());
                result = log.render();
            } else {
                result = "";
            }
            statement.close();
        } catch (SQLException e) {
            Server.log.warn("Could not generate changelog list", e);
            result =  "";
        }
        return result;
    }

    @Override
    public String render(int level, boolean alone) {
        return render();
    }

    @XAction(name = "pot_changelog_view_item")
    public void showItem(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        Connection connection = (Connection) Session.get("connection");
        String id = jsonElement.getAsJsonObject().get("change").getAsString();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select idc_changes, c_descr, v_name, v_no, u_display, u_email from c_changes inner join v_versions on c_v_version = idv_version inner join u_user on c_u_user = idu_user where idc_changes = " + id);
            if(resultSet.first()) {
                Session.set("pot-curr-change-version", resultSet.getString(4));
                Session.set("pot-curr-change-versionname", resultSet.getString(3));
                Session.set("pot-curr-change-descr", resultSet.getString(2));
                Session.set("pot-curr-change-user", resultSet.getString(5));
                Session.set("pot-curr-change-id", resultSet.getString(1));
                updates.setPartial("pot_changelog_content", "project/changelog/view.xjs");
            } else {
                updates.addMessage(Updates.Severity.DANGER, "Access violation!");
                updates.changeLocation("/");
                Session.destroy();
            }
        } catch (SQLException e) {
            Server.log.warn("Could not prepare view for change", e);
        }
    }

    @XAction(name = "pot_changelog_view_version")
    public void showVersion(@XUpdate Updates updates, @XOptional JsonElement jsonElement) {
        Session.set("pot-curr-version-id", jsonElement.getAsJsonObject().get("version").getAsString());
        updates.setPartial("pot_changelog_content", "project/changelog/version_view.xjs");
    }

    @XAction(name = "pot_changelog_back")
    public void back(@XUpdate Updates updates) {
        Session.remove("pot-curr-change-version");
        Session.remove("pot-curr-change-versionname");
        Session.remove("pot-curr-change-descr");
        Session.remove("pot-curr-change-user");
        updates.setPartial("pot_changelog_content", "project/changelog/table.xjs");
    }

    @XAction(name = "pot_changelog_add_change")
    public void addChange(@XUpdate Updates updates) {
        updates.setPartial("pot_changelog_content", "project/changelog/edit.xjs");
    }

    @XAction(name = "pot_changelog_back_view")
    public void backView(@XUpdate Updates updates) {
        updates.setPartial("pot_changelog_content", "project/changelog/table.xjs");
    }

    @XAction(name = "pot_changelog_edit_commit")
    public void editCommit(@XUpdate Updates updates, @XParam(name = "pot_changelog_descr") String descr) {
        Connection connection = (Connection) Session.get("connection");
        if(Session.is("pot-curr-change-descr")) {
            try {
                Statement statement = connection.createStatement();
                statement.execute("UPDATE c_changes SET c_descr = '" + descr + "' WHERE idc_changes = " + Session.get("pot-curr-change-id"));
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not store changelog change", e);
                updates.addMessage(Updates.Severity.WARNING, "Could not store change");
                return;
            }
            Session.remove("pot-curr-change-descr");
            Session.remove("pot-curr-change-id");
            back(updates);
        } else {
            try {
                Statement statement = connection.createStatement();
                statement.execute(
                        "INSERT INTO c_changes (c_descr, c_v_version, c_u_user) " +
                                "VALUES" +
                                " ('" + descr + "', (SELECT idv_version FROM v_versions WHERE v_p_project = " + Session.get("curr-project") + " ORDER BY v_no DESC LIMIT 1), " + Session.get("user.id") + ")");
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not store changelog change", e);
                updates.addMessage(Updates.Severity.WARNING, "Could not store change");
                return;
            }
        }
        updates.setPartial("pot_changelog_content", "project/changelog/table.xjs");
    }

    @XAction(name = "pot_changelog_add_version")
    public void addVersionView(@XUpdate Updates updates) {
        updates.setPartial("pot_changelog_content", "project/changelog/version_edit.xjs");
    }

    @XAction(name = "pot_changelog_edit_version")
    public void editVersionView(@XUpdate Updates updates) {
        Connection connection = (Connection) Session.get("connection");
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT v_no, v_name FROM v_versions WHERE idv_version = " + Session.get("pot-curr-version-id"));
            if(resultSet.first()) {
                Session.set("pot-curr-version-no", resultSet.getString(1));
                Session.set("pot-curr-version-name", resultSet.getString(2));
            }
            statement.close();
        } catch (SQLException e) {
            Server.log.warn("Could not fetch version information", e);
        }
        updates.setPartial("pot_changelog_content", "project/changelog/version_edit.xjs");
    }

    @XAction(name = "pot_changelog_version_commit")
    public void commitVersion(@XUpdate Updates updates, @XParam(name = "pot_changelog_version_no") String no, @XParam(name = "pot_changelog_version_name") String name) {
        if(no == null || no.equals("")) {
            updates.highlightError("pot_changelog_version_no");
            updates.addMessage(Updates.Severity.WARNING, "A version number needs to be supplied");
            return;
        }
        Connection connection = (Connection) Session.get("connection");
        if(Session.is("pot-curr-version-no")) {
            try {
                Statement statement = connection.createStatement();
                statement.execute("UPDATE v_versions SET v_no = '" + no + "', v_name = '" + name + "' WHERE idv_version = " + Session.get("pot-curr-version-id"));
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not update version", e);
            }
            Session.remove("pot-curr-version-id");
            Session.remove("pot-curr-version-no");
            Session.remove("pot-curr-version-name");
        } else {
            try {
                Statement statement = connection.createStatement();
                statement.execute("INSERT INTO v_versions (v_no, v_name, v_p_project) VALUES ('" + no + "', '" + name + "', " + Session.get("curr-project") + ")");
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not add new version", e);
            }
        }
        back(updates);
    }

    @XAction(name = "pot_changelog_change_delete")
    public void deleteShow(@XUpdate Updates updates) {
        updates.setPartial("pot_changelog_content", "project/changelog/view_delete.xjs");
    }

    @XAction(name = "pot_changelog_change_delete_commit")
    public void deleteChange(@XUpdate Updates updates) {
        Connection connection = (Connection) Session.get("connection");
        if(Session.is("pot-curr-change-id")) {
            try {
                Statement statement = connection.createStatement();
                statement.execute("DELETE FROM c_changes WHERE idc_changes = " + String.valueOf(Session.get("pot-curr-change-id")));
                statement.close();
            } catch (SQLException e) {
                Server.log.warn("Could not delete change", e);
            }
        }
        backView(updates);
    }
}