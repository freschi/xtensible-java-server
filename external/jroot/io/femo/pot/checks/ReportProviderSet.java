package io.femo.pot.checks;

import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.ConditionCheck;

import java.lang.Override;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Resource("report-provider-set")
public class ReportProviderSet implements ConditionCheck {

    @Override
    public boolean check(String[] args) {
        Connection connection = (Connection) Session.get("connection");
        try {
            String id = String.valueOf(Session.get("curr-project"));
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT p_r_reportprovider FROM p_projects WHERE idp_projects = " + id);
            if(resultSet.first()) {
                return resultSet.getObject("p_r_reportprovider") != null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}