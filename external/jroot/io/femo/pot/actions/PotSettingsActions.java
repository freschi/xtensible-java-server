package io.femo.pot.actions;

import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.modules.xjs.ConditionCheck;
import io.femo.security.RequiresAuthentication;
import io.femo.security.Secured;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class PotSettingsActions {

    @XAction(name = "pot_settings_name_onclick")
    @Secured
    @RequiresAuthentication
    public void setProjectName(@XUpdate Updates updates, @XParam(name = "pot_project_name") String name) {
        ConditionCheck conditionCheck = (ConditionCheck) Session.get("pot-creator-check");
        if(!conditionCheck.check(null)) {
            updates.addMessage(Updates.Severity.DANGER, "Illegal Action. Terminating Session");
            Session.destroy();
            return;
        }
        Connection connection = (Connection) Session.get("connection");
        try {
            String id = String.valueOf(Session.get("curr-project"));
            Statement statement = connection.createStatement();
            statement.execute("UPDATE p_projects SET p_name = '" + name + "' WHERE idp_projects = " + id);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @XAction(name = "pot_settings_short_onclick")
    @Secured
    @RequiresAuthentication
    public void setProjectShort(@XUpdate Updates updates, @XParam(name = "pot_project_short") String shortName) {
        ConditionCheck conditionCheck = (ConditionCheck) Session.get("pot-creator-check");
        if(!conditionCheck.check(null)) {
            updates.addMessage(Updates.Severity.DANGER, "Illegal Action. Terminating Session");
            Session.destroy();
            return;
        }
        Connection connection = (Connection) Session.get("connection");
        try {
            String id = String.valueOf(Session.get("curr-project"));
            Statement statement = connection.createStatement();
            statement.execute("UPDATE p_projects SET p_short = '" + shortName + "' WHERE idp_projects = " + id);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @XAction(name = "pot_settings_descr_onclick")
    @Secured
    @RequiresAuthentication
    public void setProjectDescr(@XUpdate Updates updates, @XParam(name = "pot_project_descr") String descr) {
        ConditionCheck conditionCheck = (ConditionCheck) Session.get("pot-creator-check");
        if(!conditionCheck.check(null)) {
            updates.addMessage(Updates.Severity.DANGER, "Illegal Action. Terminating Session");
            Session.destroy();
            return;
        }
        Connection connection = (Connection) Session.get("connection");
        try {
            String id = String.valueOf(Session.get("curr-project"));
            Statement statement = connection.createStatement();
            statement.execute("UPDATE p_projects SET p_descr = '" + descr + "' WHERE idp_projects = " + id);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}