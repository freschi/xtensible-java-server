 package io.femo.pot.reports;

import at.femoweb.config.entries.Group;
import at.femoweb.html.Tag;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.modules.Push;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;
import at.femoweb.pot.PotConnectionService;
import at.femoweb.xjs.structure.SessionedThread;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.ParseException;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.lang.Override;
import java.lang.String;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;


 @ContentProvider(regex = "\\/pot\\/report\\/work_units.*")
@Named("pot-work-units-report-provider")
@Package("femoweb_pot")
public class WorkUnitReportProvider extends BootableModule implements Provider {

    private BootableLogicalUnit logicalUnit;

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        String query = request.getRequestPath();
        String provider = null, project = null;
        if(query.contains("?")) {
            String[] params = query.substring(query.indexOf("?") + 1).split("&");
            for(String param : params) {
                String key, value;
                key = param.substring(0, param.indexOf("="));
                value = param.substring(param.indexOf("=") + 1, param.length());
                if(key.equals("provider")) {
                    provider = value;
                } else if (key.equals("project")) {
                    project = value;
                }
            }
            PotConnectionService connectionService = (PotConnectionService) logicalUnit.getService("pot-connection-service");
            try {
                Connection connection = connectionService.connectDataSource(null);
                Statement statement = connection.createStatement();
                Document document = new Document(PageSize.A4, 36, 36, 36 * 2, (float)(36f * 1.5));
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                PdfWriter writer = PdfWriter.getInstance(document, byteArrayOutputStream);
                writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
                WorkUnitsReportCreator.createDocument(writer, document, provider, project, statement);
                response.setEntity(byteArrayOutputStream.toByteArray());
                response.header("Content-Type", "application/pdf");
            } catch (SQLException e) {
                Server.log.warn("Could not connect to database", e);
            } catch (DocumentException e) {
                Server.log.warn("Could not create document", e);
            }
        }
        response.setStatus(200);
    }

     @Override
     public boolean handles(HttpRequest request) {
        return true;
    }

     @XAction(name = "pot_work_units_report_dl")
     public void downloadWUReport(@XUpdate Updates updates) {
         updates.openPopup("/pot/report/work_units.pdf?provider=at.femoweb.pot.SPGProvider&project=" + String.valueOf(Session.get("curr-project")));
     }

     @XAction(name = "pot_work_units_report_creators")
     public void sendToCreators(@XUpdate Updates updates) {
         updates.push(true);
         new SessionedThread() {

             @Override
             public void execute() {
                 Push push = Push.get();
                 sendWUReport(Session.get("curr-project").toString(), 3);
                 push.addMessage(Updates.Severity.SUCCESS, "Messages sent successfuly");
                 push.push(false);
             }

         }.start();
         updates.addMessage(Updates.Severity.INFO, "Sending messages");
     }

     @XAction(name = "pot_work_units_report_custodians")
     public void sendToCustodians(@XUpdate Updates updates) {
         updates.push(true);
         new SessionedThread() {

             @Override
             public void execute() {
                 Push push = Push.get();
                 sendWUReport(Session.get("curr-project").toString(), 1);
                 push.addMessage(Updates.Severity.SUCCESS, "Messages sent successfuly");
                 push.push(false);
             }

         }.start();
         updates.addMessage(Updates.Severity.INFO, "Sending messages");
     }

     private void sendWUReport(String project, int level) {
         Connection connection = (Connection) Session.get("connection");
         try {
             Statement statement = connection.createStatement();
             ArrayList<String> addresses = new ArrayList<String>();
             ResultSet resultSet = statement.executeQuery("select u_email from u_user join w_works on w_u_user = idu_user where w_p_project = " + project + " and w_info_perm = " + level);
             if(resultSet.first()) {
                 SimpleDateFormat sqlDate = new SimpleDateFormat("yyyy-MM-dd");
                 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                 do {
                     addresses.add(resultSet.getString(1));
                 } while (resultSet.next());
                 Server.log.info(addresses);
                 Document document = new Document(PageSize.A4, 36, 36, 36 * 2, (float)(36f * 1.5));
                 ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                 PdfWriter writer = PdfWriter.getInstance(document, byteArrayOutputStream);
                 writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
                 WorkUnitsReportCreator.createDocument(writer, document, "at.femoweb.pot.SPGProvider", project, statement);
                 Address[] adds = new Address[addresses.size()];
                 MimeMessage mimeMessage = getMessage();
                 for (int i = 0; i < adds.length; i++) {
                     adds[i] = new InternetAddress(addresses.get(i));
                 }
                 mimeMessage.setRecipients(Message.RecipientType.TO, adds);
                 mimeMessage.setSender(new InternetAddress("noreply@femo.io"));
                 ResultSet nameSet = statement.executeQuery("select p_name from p_projects where idp_projects = " + project);
                 String projectName;
                 if(nameSet.first()) {
                     projectName = nameSet.getString(1);
                 } else {
                     Push.get().addMessage(Updates.Severity.DANGER, "Could not fetch project name");
                     return;
                 }
                 mimeMessage.setSubject("[INFO] Work Unit Report for " + projectName);
                 Multipart multipart = new MimeMultipart();
                 BodyPart bodyPart = new MimeBodyPart();
                 bodyPart.setText(createMessage(projectName, ((UnifiedUser) Session.get("_xjs_user")).getUsername(), simpleDateFormat).render());
                 bodyPart.addHeader("Content-Type", "text/html");
                 multipart.addBodyPart(bodyPart);
                 InternetHeaders headers = new InternetHeaders();
                 headers.addHeader("Content-Type", "application/pdf");
                 ByteArrayDataSource dataSource = new ByteArrayDataSource(byteArrayOutputStream.toByteArray(), "application/pdf");
                 dataSource.setName("report_" + projectName.toLowerCase().replace(" ", "_") + ".pdf");
                 MimeBodyPart attachment = new MimeBodyPart();
                 attachment.setDataHandler(new DataHandler(dataSource));
                 attachment.setFileName(dataSource.getName());
                 multipart.addBodyPart(attachment);
                 mimeMessage.setContent(multipart);
                 Server.log.info("Forwarding messages to server");
                 Transport.send(mimeMessage);
                 Server.log.info("Sending complete");
                 Push.get().addMessage(Updates.Severity.SUCCESS, "Messages sent!");
                 Push.get().push(false);
             } else {
                 return;
             }
             statement.close();
         } catch (SQLException | MessagingException e) {
             Server.log.warn("Could not send E-Mail", e);
         } catch (DocumentException e) {
             Server.log.warn("Could not create document", e);
         }
     }

     @XAction(name = "pot_work_units_report_qr")
     public void showQRWUReport(@XUpdate Updates updates) {
         HttpRequest request = (HttpRequest) Session.get("_xjs_http_request");
         String url = "http://" + request.header("Host") + "/pot/report/work_units.pdf?provider=at.femoweb.pot.SPGProvider&project=" + Session.get("curr-project").toString();
         try {
             String qr_url = "https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=" + URLEncoder.encode(url, "UTF-8");
             updates.openPopup(qr_url);
         } catch (Exception e) {
             Server.log.warn("Error while encodign QR request url", e);
         }
     }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        WorkUnitReportProvider workUnitReportProvider = new WorkUnitReportProvider();
        workUnitReportProvider.logicalUnit = logicalUnit;
        return workUnitReportProvider;
    }

    @Override
    public void logInfo() {
        Server.log.info("Work Unit report provider for " + logicalUnit.getName());
    }

    @Override
    public void setup() {

    }

     private Tag createMessage(String projectName, String user, DateFormat format) throws ParseException {
         Tag msg = new Tag("body");
         Tag heading = new Tag("h1");
         heading.add(projectName);
         msg.add(heading);
         Tag message = new Tag("span");
         message.add("This is an automatically sent email, requested by " + user);
         msg.add(message);
         Tag detailHeading = new Tag("h3");
         detailHeading.add("Details");
         msg.add(detailHeading);
         Tag details = new Tag("div");
         Tag table = new Tag("table");
         {
             Tag userRow = new Tag("tr");
             Tag userStronk = new Tag("th");
             userStronk.add("Requested by");
             userRow.add(userStronk);
             Tag userName = new Tag("td");
             userName.add(user);
             userRow.add(userName);
             table.add(userRow);
         }
         {
             Tag dateRow = new Tag("tr");
             Tag dateStronk = new Tag("th");
             dateStronk.add("Request date");
             dateRow.add(dateStronk);
             Tag date = new Tag("td");
             date.add(format.format(new Date()));
             dateRow.add(date);
             table.add(dateRow);
         }
         {
             Tag projectRow = new Tag("tr");
             Tag projectStronk = new Tag("th");
             projectStronk.add("Project");
             projectRow.add(projectStronk);
             Tag project = new Tag("td");
             project.add(projectName);
             projectRow.add(project);
             table.add(projectRow);
         }
         details.add(table);
         msg.add(details);
         msg.add(new Tag("hr"));
         message = new Tag("small");
         message.attribute("style", "color: light-gray");
         message.add("Please do not respond to this email address. If you received this email unwillingly please contact the");
         Tag webmaster = new Tag("a");
         webmaster.attribute("href", "mailto:office@femo.io");
         webmaster.add("webmaster");
         message.add(webmaster);
         msg.add(message);
         return msg;
     }

     private MimeMessage getMessage() {
         Properties props = new Properties();
         props.put("mail.smtp.host", "alfa3026.alfahosting-server.de");
         props.put("mail.smtp.socketFactory.port", "465");
         props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
         props.put("mail.smtp.auth", "true");
         props.put("mail.smtp.port", "465");

         javax.mail.Session session = javax.mail.Session.getInstance(props,
                 new Authenticator() {
                     @Override
                     protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                         return new javax.mail.PasswordAuthentication("web1448p5", "MpZsOZSr9RO5");
                     }
                 });
         try {
             MimeMessage message = new MimeMessage(session);
             message.setSentDate(new Date());
             message.setFrom(new InternetAddress("noreply@femoweb.at"));
             return message;
         } catch (javax.mail.MessagingException e) {
             e.printStackTrace();
         }
         return null;
     }
}