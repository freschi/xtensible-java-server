package io.femo.pot.reports;

import at.femoweb.http.HttpRequest;
import at.femoweb.pot.PotReportProvider;
import at.femoweb.pot.PotReportRequest;
import at.femoweb.pot.PotReportProvider.*;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Package;
import at.femoweb.xjs.dependencies.Dependencies;
import at.femoweb.xjs.dependencies.Dependency;
import at.femoweb.xjs.modules.xjs.ListProvider;
import com.itextpdf.text.*;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.lang.ClassNotFoundException;
import java.lang.IllegalAccessException;
import java.lang.InstantiationException;
import java.lang.NoSuchMethodException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

@Package("femoio-pot")
@Dependencies({
        @Dependency("femoweb-pot")
})
public class WorkUnitsReportCreator {

    private static float bold = 1.5f;
    private static float normal = 0.5f;
    private static float none = 0.f;

    public static void createDocument(PdfWriter pdfWriter, Document document, String providerClass, String project, Statement statement) throws SQLException, DocumentException {
        PotReportProvider provider = null;
        FontStorage fontStorage = new FontStorage();
        Translation translation = new Translation();
        PotReportRequest reportRequest = new PotReportRequest(document, pdfWriter, null, null, project, providerClass);
        try {
            Class<?> clazz = Class.forName(providerClass);
            provider = (PotReportProvider) clazz.newInstance();
        } catch (ClassNotFoundException e) {
            Server.log.warn("Could not find provider class", e);
        } catch (IllegalAccessException e) {
            Server.log.warn("Could not access constructor for provider", e);
        } catch (InstantiationException e) {
            Server.log.warn("Could not instantiate provider", e);
        }
        if(provider == null)
            return;
        provider.setReportType(PotReportProvider.REPORT_TYPE_WORK_UNITS);
        provider.setTranslation(translation);
        provider.setStatement(statement);
        provider.setRequest(reportRequest);
        provider.setupFonts(fontStorage);
        provider.setupTranslation(translation);
        provider.setupPageEventHandler(pdfWriter);
        document.addCreationDate();
        document.addAuthor("XJS Server");
        SimpleDateFormat dateFormat = new SimpleDateFormat(translation.getStrDateFormat());
        String name;
        ResultSet resultSet = statement.executeQuery("SELECT p_name FROM p_projects WHERE idp_projects = " + project);
        if(resultSet.first()) {
            name = resultSet.getString(1);
        } else {
            Server.log.warn("Project not found");
            return;
        }
        document.addTitle("Work Unit Report - " + name);
        document.open();
        Paragraph title = new Paragraph(translation.getStrWorkUnitReport(), fontStorage.getTitleFont());
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);
        Paragraph subtitle = new Paragraph(name, fontStorage.getSubtitleFont());
        subtitle.setAlignment(Element.ALIGN_CENTER);
        document.add(subtitle);
        Paragraph subsubtitle = new Paragraph(translation.getStrCreatedOn() + " " + new Date().toString(), fontStorage.getSubsubtitleFont());
        subsubtitle.setAlignment(Element.ALIGN_CENTER);
        document.add(subsubtitle);
        resultSet = statement.executeQuery("select ta_tasks.ta_id, ta_tasks.ta_name, parent.ta_id, parent.ta_name, ta_tasks.ta_start, ta_tasks.ta_end, u_display, ta_tasks.ta_hours, ta_tasks.idta_tasks, ta_tasks.ta_descr, ta_tasks.ta_problems " +
                "from ta_tasks left join ta_tasks as parent on ta_tasks.ta_ta_parent = parent.idta_tasks " +
                "left join u_user on ta_tasks.ta_u_responsible = idu_user " +
                "where ta_tasks.ta_p_project = " + project);
        Statement altStatement = statement.getConnection().createStatement();
        if(resultSet.first()) {
            do {
                PdfPTable table = new PdfPTable(8);
                table.setWidthPercentage(90);
                table.setWidths(new float[]{30, 5, 5, 10, 30, 15, 10, 20});
                PdfPCell nameCell = new PdfPCell(new Paragraph(resultSet.getString(1) + " - " + resultSet.getString(2), fontStorage.getSectionFont()));
                nameCell.setColspan(8);
                nameCell.setMinimumHeight(15f);
                provider.styleCell(nameCell, bold, bold, none, bold);
                table.addCell(nameCell);
                PdfPCell labelResponsible = new PdfPCell(new Paragraph(translation.getStrResponsible(), fontStorage.getTableHeaderFont()));
                provider.styleCell(labelResponsible, normal, normal, none, bold);
                table.addCell(labelResponsible);
                PdfPCell responsibleCell = new PdfPCell(new Paragraph(resultSet.getString(7), fontStorage.getTableCellFont()));
                responsibleCell.setColspan(4);
                provider.styleCell(responsibleCell, normal, normal, none, none);
                table.addCell(responsibleCell);
                PdfPCell labelPlan = new PdfPCell(new Paragraph(translation.getStrPlanHours(), fontStorage.getTableHeaderFont()));
                labelPlan.setColspan(2);
                provider.styleCell(labelPlan, normal, normal, none, none);
                table.addCell(labelPlan);
                PdfPCell planCell = new PdfPCell(new Paragraph(provider.formatHour(resultSet.getDouble(8)), fontStorage.getTableCellFont()));
                planCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                provider.styleCell(planCell, normal, bold, none, none);
                table.addCell(planCell);
                PdfPCell labelWorked = new PdfPCell(new Paragraph(translation.getStrWorkedHours(), fontStorage.getTableHeaderFont()));
                labelWorked.setColspan(3);
                provider.styleCell(labelWorked, normal, normal, none, bold);
                table.addCell(labelWorked);
                ResultSet altResult = altStatement.executeQuery("select sum(e_hours) from e_entries where e_ta_task = " + resultSet.getString(9));
                PdfPCell workedCell;
                double dif;
                if(altResult.first()) {
                    dif = resultSet.getDouble(8) - altResult.getDouble(1);
                    workedCell = new PdfPCell(new Paragraph(provider.formatHour(altResult.getDouble(1)), fontStorage.getTableCellFont()));
                } else {
                    workedCell = new PdfPCell(new Paragraph("---", fontStorage.getTableCellFont()));
                    Server.log.warn(String.valueOf(resultSet.getDouble(8)));
                    dif = resultSet.getDouble(8);
                }
                workedCell.setColspan(2);
                provider.styleCell(workedCell, normal, normal, none, none);
                table.addCell(workedCell);
                PdfPCell labelDue = new PdfPCell(new Paragraph(translation.getStrDueDate(), fontStorage.getTableHeaderFont()));
                provider.styleCell(labelDue, normal, normal, none, none);
                table.addCell(labelDue);
                PdfPCell dueCell = new PdfPCell(new Paragraph(dateFormat.format(resultSet.getDate(6)), fontStorage.getTableCellFont()));
                provider.styleCell(dueCell, normal, bold, none, none);
                dueCell.setColspan(2);
                table.addCell(dueCell);
                table.completeRow();
                PdfPCell labelDiff = new PdfPCell(new Paragraph(translation.getStrDifference(), fontStorage.getTableHeaderFont()));
                labelDiff.setColspan(2);
                provider.styleCell(labelDiff, normal, normal, none, bold);
                table.addCell(labelDiff);
                PdfPCell diffCell = new PdfPCell(new Paragraph(String.format("%+02.2f h", dif), fontStorage.getTableCellFont()));
                diffCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                provider.styleCell(diffCell, normal, bold, none, none);
                if(dif < 0) {
                    diffCell.setBackgroundColor(new BaseColor(1f, 0.8f, 0.8f));
                } else {
                    diffCell.setBackgroundColor(new BaseColor(0.8f, 1f, 0.8f));
                }
                diffCell.setColspan(6);
                table.addCell(diffCell);
                table.completeRow();
                altResult = altStatement.executeQuery("select concat(ta_id, ' ', ta_name) from de_dependencies inner join ta_tasks on de_ta_dependency = idta_tasks where de_ta_task = " + resultSet.getString(9));
                if(altResult.first()) {
                    PdfPCell labelDependencies = new PdfPCell(new Paragraph(translation.getStrDependencies(), fontStorage.getTableHeaderFont()));
                    labelDependencies.setColspan(2);
                    provider.styleCell(labelDependencies, normal, normal, none, bold);
                    table.addCell(labelDependencies);
                    List list = new List();
                    do {
                        ListItem listItem = new ListItem(altResult.getString(1), fontStorage.getTableCellFont());
                        listItem.setSpacingBefore(0);
                        listItem.setExtraParagraphSpace(0f);
                        listItem.setListSymbol(new Chunk("\u2022 "));
                        list.add(listItem);
                    } while (altResult.next());
                    PdfPCell dependenciesCell = new PdfPCell();
                    dependenciesCell.addElement(list);
                    provider.styleCell(dependenciesCell, normal, bold, none, none);
                    dependenciesCell.setColspan(6);
                    table.addCell(dependenciesCell);
                }
                altResult = altStatement.executeQuery("select concat(ta_id, ' ', ta_name) from de_dependencies inner join ta_tasks on de_ta_task = idta_tasks where de_ta_dependency = " + resultSet.getString(9));
                if(altResult.first()) {
                    PdfPCell labelRequired = new PdfPCell(new Paragraph(translation.getStrRequiredFor(), fontStorage.getTableHeaderFont()));
                    labelRequired.setColspan(2);
                    provider.styleCell(labelRequired, normal, normal, none, bold);
                    table.addCell(labelRequired);
                    List list = new List();
                    do {
                        ListItem listItem = new ListItem(altResult.getString(1), fontStorage.getTableCellFont());
                        listItem.setSpacingBefore(0);
                        listItem.setExtraParagraphSpace(0f);
                        listItem.setListSymbol(new Chunk("\u2022 "));
                        list.add(listItem);
                    } while (altResult.next());
                    PdfPCell dependenciesCell = new PdfPCell();
                    dependenciesCell.addElement(list);
                    provider.styleCell(dependenciesCell, normal, bold, none, none);
                    dependenciesCell.setColspan(6);
                    table.addCell(dependenciesCell);
                }
                if(resultSet.getString(10) != null) {
                    PdfPCell labelDescription = new PdfPCell(new Paragraph(translation.getStrDescription(), fontStorage.getTableHeaderFont()));
                    labelDescription.setColspan(2);
                    provider.styleCell(labelDescription, normal, normal, none, bold);
                    table.addCell(labelDescription);
                    PdfPCell descriptionCell = new PdfPCell(new Paragraph(resultSet.getString(10), fontStorage.getTableCellFont()));
                    descriptionCell.setColspan(6);
                    provider.styleCell(descriptionCell, normal, bold, none, none);
                    table.addCell(descriptionCell);
                }
                if(resultSet.getString(11) != null) {
                    PdfPCell labelDescription = new PdfPCell(new Paragraph(translation.getStrDependencyProblems(), fontStorage.getTableHeaderFont()));
                    provider.styleCell(labelDescription, normal, normal, none, bold);
                    labelDescription.setColspan(3);
                    table.addCell(labelDescription);
                    PdfPCell descriptionCell = new PdfPCell(new Paragraph(resultSet.getString(11), fontStorage.getTableCellFont()));
                    descriptionCell.setColspan(5);
                    provider.styleCell(descriptionCell, normal, bold, none, none);
                    table.addCell(descriptionCell);
                }
                PdfPCell[] cells = table.getRow(table.getRows().size() - 1).getCells();
                for(PdfPCell cell : cells) {
                    if(cell != null)
                        cell.setBorderWidthBottom(bold);
                }
                for(PdfPRow pdfPRow : table.getRows()) {
                    for(PdfPCell cell : pdfPRow.getCells()) {
                        if(cell != null) {
                            cell.setFixedHeight((float)(cell.getFixedHeight() * 1.2));
                        }
                    }
                }
                table.setKeepTogether(true);
                table.setSpacingBefore(20f);
                document.add(table);
            } while (resultSet.next());
        }
        document.close();
    }

}