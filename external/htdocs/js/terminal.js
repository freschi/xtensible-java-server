jQuery.fn.terminal = function(val) {
  if(!val.uri) {
    return this.each(function() {
      $(this).addClass('error');
      $(this).append('URI not set');
    });
  } else {
    return this.each(function () {
      if($(this).attr('id')) {
	attachWebSocket($(this), val.uri, val.exit);
      } else {
	$(this).addClass('error');
	$(this).append('Could not find id');
      }
    });
  }
};

jQuery.fn.cssInt = function (prop) {
  return parseInt(this.css(prop), 10) || 0;
}

function attachWebSocket(elem, uri, exit) {
  var id = elem.attr('id');
  $('<pre>', {
    id: id + '_output',
    class: 'term-output'
  }).appendTo(elem);
  $('<span>', {
    id: id + '_path',
    class: 'term-path'
  }).appendTo(elem);
  $('<input>', {
    id: id + '_input',
    class: 'term-input',
    type: 'text'
  }).appendTo(elem);
  _termPrint(id, 'WebTerm (c) 2015, Felix Resch');
  _termPrint(id, 'Connecting to ' + uri + '...');
  var websocket = createWebSocket(id, uri, exit);
  $('#' + id + '_input').keydown(function (evt) {
    if(evt.which == 13) {
      _termPrint(id, $('#' + id + '_path').html() + $(this).val());
      var msg = {};
      msg.type = 'cmd';
      msg.cmd = $(this).val();
      websocket.send(JSON.stringify(msg));
      $(this).val('');
      evt.preventDefault();
    } else if(evt.which == 9) {
      evt.preventDefault();
    }
  });
  $('#' + id).click(function () {
    $('#' + id + '_input').focus();
  });
  $(window).unload(function() {
    websocket.close();
  });
}

function createWebSocket(id, uri, exit) {
  var websocket = new WebSocket(uri);
  websocket.onopen = function () {
    _termPrint(id, 'Connected');
    _termPath(id, 'xjs@xjs-proxerread');
    $('#' + id + '_input').focus();
  };
  websocket.onclose = function() {
    _termPrint(id, 'Disconnected');
    _termPath(id, '');
    exit();
  };
  websocket.onmessage = function (evt) {
    var json = JSON.parse(evt.data);
    if(json.state) {
        if(json.state == 'okay') {
            if(json.message) {
                _termPrint(id, json.message);
            } else if (json.action_performed) {
                console.log('Actions performed: ' + json.action_performed);
            }
        } else if (json.state == 'malformed') {
            _termPrint(id, 'Server returned malformed json error');
        } else if (json.state == 'error') {
            _termPrint(id, 'Error while evaluating data on server');
        }
    } else if (json.type) {
        if(json.type == 'init') {
            if(json.session) {
                _termPrint(id, 'Session enabled on connection');
            } else {
                _termPrint(id, 'No session on connection');
            }
            if(json.version) {
                _termPrint(id, 'Protocol version ' + json.version);
            }
            if(json.beKind) {
                _termPrint(id, 'Server requests kind mode. Sending only well-formed data');
            }
            if(json.path) {
                _termPath(id, json.path);
            }
        } else if (json.type == 'path') {
            _termPath(id, json.path);
        } else if (json.type == 'message') {
            _termPrint(id, json.message);
        }
    }
  };
  websocket.onerror = function (evt) {
    _termPrint(id, '<span style="color:#f00">' + evt.data + '</span>');
  }
  return websocket;
}

function _termPrint(id, line) {
  if($('#' + id + '_output').html() == '') {
    $('#' + id + '_output').append(line);
  } else {
    $('#' + id + '_output').append('\n' + line);
    $('#' + id).scrollTop($('#' + id + '_output').cssInt('height') + $('#' + id + '_input').cssInt('height'));
  }
}

function _termPath(id, path) {
  $('#' + id + '_path').html(path + '> ');
  $('#' + id + '_input').css('width', ($('#' + id).cssInt('width') - $('#' + id + '_path').cssInt('width') - 30) + 'px');
}
