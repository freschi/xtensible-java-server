package at.femoweb.xjs;

import at.femoweb.html.Document;
import at.femoweb.html.HTMLReader;
import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.Object;
import java.lang.Override;
import java.util.Properties;

@at.femoweb.xjs.annotations.Service(type = ServiceType.CONTENT)
@Named("xjs-parser-service")
public class XJSParserService extends Module implements Service {
    
    private LogicalUnit logicalUnit;
    
    public XJSParserService () {

    }
    
    public XJSParserService (LogicalUnit logicalUnit, Properties properties) {
        this.logicalUnit = logicalUnit;
        Server.log.info("This is ma logical unit: " + logicalUnit);
    }
    
    private Document openDocument(String path) {
        path = logicalUnit.getName() + "/htdocs/" + path;
        byte[] buffer = new byte[2048];
        String content = "";
        try {
            FileInputStream inputStream = new FileInputStream(path);
            int count;
            while ((count = inputStream.read(buffer)) > 0) {
                content += new String(buffer, 0, count);
            }
        } catch (IOException e) {
            Server.log.warn("Failed to load content for " + path);
        }
        return Document.readDocument(content);
    } 

    public boolean isPartial(String path) {
        Document document = openDocument(path);
        return document.doctype().equalsIgnoreCase("xjs:partial");
    }

    public boolean isPage(String path) {
        Document document = openDocument(path);
        return document.doctype().equalsIgnoreCase("xjs:page");
    }

    public String getDoctype(String path) {
        Document document = openDocument(path);
        return document.doctype();
    }
    
    public boolean exists(String path) {
        path = logicalUnit.getName() + "/htdocs/" + path;
        return new File(path).exists();
    }
    
    @Override
    public Object put(Object... objects) {
        if(logicalUnit == null) {
            logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        }
        String path = (String) objects[0];
        if(exists(path)) {
            if(isPartial(path)) {
                Replacer replacer = (Replacer) Session.get("_xjs_replacer");
                Document document = openDocument(path);
                Tag root = document.content();
                Tag partial = new Tag("div");
                partial.add(root.children());
                replacer.replace(partial);
                return partial.render();
            } else if (isPage(path)) {
                return "Page found!";
            } else {
                return "<strong>Unknown Doctype " + getDoctype(path) + "</strong>";
            }
        } else {
            return "<strong>Not found!</strong>";
        }
    }
}