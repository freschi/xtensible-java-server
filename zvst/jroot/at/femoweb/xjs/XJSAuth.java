package at.femoweb.xjs;

import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.services.LoginService;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;

import java.lang.Object;
import java.lang.Override;
import java.lang.String;

@Service(type = ServiceType.OTHER)
@Named("at.femoweb-user")
public class XJSAuth extends Module implements LoginService {

    @Override
    public boolean checkCredentials(String username, byte[] password) {
        return username.equals("felix") && new String(password).equals("4309");
    }

    @Override
    public UnifiedUser getUserData(String username, byte[] password) {
        XJSUser user = new XJSUser();
        user.store("user.name", "Admin");
        user.setUsername("Admin");
        return user;
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }
}