package at.femoweb.xjs;

import at.femoweb.xjs.UnifiedUser;

import java.lang.Override;
import java.lang.String;
import java.util.HashMap;

public class XJSUser extends UnifiedUser {

    public HashMap<String, String> values;

    @Override
    protected void store(String name, String value) {
        values.put(name, value);
    }

    @Override
    protected void remove(String name) {
        values.remove(name);
    }

    @Override
    protected String read(String value) {
        return values.get(value);
    }

    @Override
    protected String[] getKeys() {
        return values.keySet().toArray(new String[]{});
    }

    @Override
    protected void connect() {
        values = new HashMap<>();
    }

    @Override
    protected void lock() {

    }

    @Override
    protected void unlock() {

    }
}