package at.femoweb.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xjs.Replacer;

import java.lang.Override;
import java.util.ArrayList;

public class DefaultReplaceHandler implements OnLoginHandler {

    private ArrayList<Tag> children;
    private String id;

    public DefaultReplaceHandler (ArrayList<Tag> children, String id) {
        this.children = children;
        this.id = id;
    }

    @Override
    public void replace(Updates updates) {
        Tag tag = new Tag("div");
        tag.add(children);
        Replacer replacer = (Replacer) Session.get("_xjs_replacer");
        replacer.replace(tag);
        String html = tag.render();
        updates.html(id, html);
    }
}