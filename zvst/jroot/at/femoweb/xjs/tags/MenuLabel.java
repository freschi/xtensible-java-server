package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:menuLabel")
public class MenuLabel implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag label = new Tag("li");
        label.attribute("style", "font-weight:bold");
        return label;
    }
}