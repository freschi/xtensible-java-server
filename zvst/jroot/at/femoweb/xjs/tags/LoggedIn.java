package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.OnLoginHandler;
import at.femoweb.xjs.DefaultReplaceHandler;

import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@TagReplacerInfo(tag = "xjs:loggedin", type = TagReplacerInfo.ReplacementType.IGNORE_INTERNAL)
public class LoggedIn implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Tag replace(Tag tag) {
        Tag t = new Tag("div");
        t.type(TagType.FULL);
        String id = null;
        if(tag.attribute("id") == null) {
            id = "_xjs_placeholder" + Long.toHexString(System.nanoTime());
        } else {
            id = tag.attribute("id");
        }
        t.attribute("id", id);
        if(!Session.is("_xjs_user")) {
            if(!Session.is("_xjs_onlogin_list")) {
                Session.set("_xjs_onlogin_list", new ArrayList<OnLoginHandler>());
            }
            List list = (List) Session.get("_xjs_onlogin_list");
            list.add(new DefaultReplaceHandler(tag.children(), id));
        } else {
            Replacer replacer = (Replacer) Session.get("_xjs_replacer");
            t.add(tag.children());
            replacer.replace(t);
        }
        return t;
    }
}