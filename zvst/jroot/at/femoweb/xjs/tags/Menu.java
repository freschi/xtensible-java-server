package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(type = TagReplacerInfo.ReplacementType.PROCESS_INTERNAL, tag = "xjs:menu")
public class Menu implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag menu = new Tag("ul");
        menu.attribute("style", "list-style-type: none; padding-left: 10px");
        Tag input = new Tag("input");
        input.attribute("type", "hidden");
        input.attribute("id", "_xjs_menu_content");
        if(tag.attribute("content") != null) {
            input.attribute("value", tag.attribute("content"));
        }
        menu.add(input);
        input = new Tag("input");
        input.attribute("type", "hidden");
        input.attribute("id", "_xjs_menu_title");
        if(tag.attribute("title") != null) {
            input.attribute("value", tag.attribute("title"));
        }
        menu.add(input);
        return menu;
    }
}