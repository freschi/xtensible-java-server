package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:separator", type = TagReplacerInfo.ReplacementType.DROP_INTERNAL)
public class Separator implements TagReplacer {
    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag r = new Tag();
        r.name("hr");
        if(tag.attribute("style") != null) {
            r.attribute("style", tag.attribute("style"));
        }
        return r;
    }
}