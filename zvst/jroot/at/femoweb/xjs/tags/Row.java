package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:row", type = TagReplacerInfo.ReplacementType.PROCESS_INTERNAL)
public class Row implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag r = new Tag();
        r.name("div");
        r.type(TagType.FULL);
        if(tag.attribute("class") != null) {
            r.attribute("class", "row " + tag.attribute("class"));
        } else {
            r.attribute("class", "row");
        }
        if(tag.attribute("style") != null) {
            r.attribute("style", tag.attribute("style"));
        }
        return r;
    }
}