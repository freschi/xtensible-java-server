package at.femoweb;

import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XUpdate;

public class Actions {

    @XAction(name = "destroy_session")
    public void destroySession (@XUpdate Updates updates) {
        updates.changeLocation("/");
        Session.destroy();
    }
}