SELECT u_display,
  u_email,
  Coalesce(DATA.ahours, 0) ahours,
  Coalesce(DATA.bhours, 0) bhours,
  w_info_perm
FROM   w_works
  INNER JOIN u_user
    ON w_u_user = idu_user
  LEFT JOIN (SELECT A.e_p_project project,
                    A.e_u_user    user,
                    A.hours       ahours,
                    B.hours       bhours
             FROM   (SELECT e_p_project,
                       e_u_user,
                       Sum(e_hours)       hours,
                       Count(ide_entries) count
                     FROM   e_entries
                     WHERE  e_inschool = 0
                     GROUP  BY e_u_user,
                       e_p_project) AS A
               JOIN (SELECT e_p_project,
                       e_u_user,
                       Sum(e_hours)       hours,
                       Count(ide_entries) count
                     FROM   e_entries
                     WHERE  e_inschool = 1
                     GROUP  BY e_u_user,
                       e_p_project) AS B
                 ON A.e_u_user = B.e_u_user
                    AND A.e_p_project = B.e_p_project) AS DATA
    ON w_p_project = project
       AND w_u_user = user
WHERE  w_p_project = 25
       AND w_info_perm >= 2
ORDER  BY w_info_perm DESC,
  u_lastname,
  u_firstname;


