import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.Module;

import java.lang.Override;

@ContentProvider(regex = "\\/hello_world\\/")
@Named("hello_world")
public class HelloWorldProvider extends Module implements Provider {
    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        response.setStatus(200);
        response.setEntity("<!doctype html>\n" +
                "<title>Site Maintenance</title>\n" +
                "<style>\n" +
                "body { text-align: center; padding: 150px; }\n" +
                "h1 { font-size: 50px; }\n" +
                "body { font: 20px Helvetica, sans-serif; color: #333; }\n" +
                "article { display: block; text-align: left; width: 650px; margin: 0 auto; }\n" +
                "a { color: #dc8100; text-decoration: none; }\n" +
                "a:hover { color: #333; text-decoration: none; }\n" +
                "</style>\n" +
                " \n" +
                "<article>\n" +
                "<h1>We&rsquo;ll be back soon!</h1>\n" +
                "<div>\n" +
                "<p>Sorry for the inconvenience but we&rsquo;re performing some maintenance at the moment. If you need to you can always <a href=\"mailto:office@at.femoweb.at\">contact us</a>, otherwise we&rsquo;ll be back online shortly!</p>\n" +
                "<p>&mdash; The Team</p>\n" +
                "</div>\n" +
                "</article>");
        response.header("Content-Type", "text/html");
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }
}