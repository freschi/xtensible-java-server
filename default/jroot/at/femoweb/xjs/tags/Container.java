package at.femoweb.xjs.tags;

import at.femoweb.html.Tag;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;

import java.lang.Override;
import java.lang.String;

@TagReplacerInfo(tag = "xjs:container")
public class Container implements TagReplacer {

    @Override
    public boolean replaces(String tagName) {
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag r = new Tag("div");
        if(tag.attribute("class") != null) {
            r.attribute("class", "container " + tag.attribute("class"));
        } else {
            r.attribute("class", "container");
        }
        return r;
    }
}