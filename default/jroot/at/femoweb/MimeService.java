package at.femoweb;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.Module;

import java.lang.Object;
import java.lang.Override;
import java.lang.String;

@at.femoweb.xjs.annotations.Service
@Named("mime-service")
public class MimeService extends Module implements Service {

    @Override
    public Object put(Object... objects) {
        if(objects.length == 1 && objects[0] instanceof String) {
            String file = (String) objects[0];
            if(file.endsWith(".css")) {
                return "text/css";
            } else if (file.endsWith(".html") || file.endsWith(".htm")) {
                return "text/html";
            } else if (file.endsWith(".js")) {
                return "text/javascript";
            }
        }
        return "text";
    }

}