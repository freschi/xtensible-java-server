import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Push;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.xaction.Updates;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.structure.SessionedThread;

import java.lang.Override;

public class Test {

    @XAction(name="jroot_test")
    public void test() {
        System.out.println("Hello World");
    }

    @XAction(name="hello_world")
    public void test_action (@XParam(name = "test") String test, @XUpdate Updates updates) {
        if(Session.get("hello_world.thread") == null) {
            updates.value("value", "0");
            updates.push(true);
            updates.html("task", "Verarbeite Eingabe \"" + test + "\" ...");
            Session.set("hello_world.thread", "set");
            SessionedThread sessionedThread = new SessionedThread() {
                @Override
                public void execute() {
                    for(int i = 0; i < 20; i++) {
                        Push.get().value("value", (i + 1) * 5 + "");
                        Push.get().html("hello", (i + 1) * 5 + " %");
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Session.remove("hello_world.thread");
                    Push.get().html("hello", "Verarbeitung Abgeschlossen");
                    Push.get().html("task", "Daten gespeichert");
                    Push.get().value("value", "-1");
                    Push.get().push(false);
                }
            };
            sessionedThread.start();
        }
    }

    @XAction(name = "reload_server")
    public void reloadServer(@XUpdate Updates updates) {
        updates.addMessage(Updates.Severity.INFO, "Reloading Server. Please wait!");
        updates.push(true);
        updates.changeLocation("/");
        SessionedThread sessionedThread = new SessionedThread() {
            @Override
            public void execute() {
                Server.reload();
                Push.get().addMessage(Updates.Severity.SUCCESS, "Reload successfull!");
                Push.get().push(false);
            }
        };
        sessionedThread.start();
    }
}