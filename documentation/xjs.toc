\select@language {english}
\contentsline {part}{I\hspace {1em}XJS}{4}
\contentsline {chapter}{\numberline {1}Introduction}{5}
\contentsline {section}{\numberline {1.1}Goal}{5}
\contentsline {section}{\numberline {1.2}Artifacts}{5}
\contentsline {chapter}{\numberline {2}Startup}{6}
\contentsline {chapter}{\numberline {3}Bootstrap}{7}
\contentsline {section}{\numberline {3.1}Scripting}{7}
\contentsline {section}{\numberline {3.2}Compiling}{7}
\contentsline {section}{\numberline {3.3}Dependencies}{7}
\contentsline {chapter}{\numberline {4}Services}{8}
\contentsline {chapter}{\numberline {5}Content Providers}{9}
\contentsline {chapter}{\numberline {6}Config}{10}
\contentsline {part}{II\hspace {1em}XJS Files}{11}
\contentsline {chapter}{\numberline {7}Introduction}{12}
\contentsline {chapter}{\numberline {8}XJS:HTML dialect}{13}
\contentsline {chapter}{\numberline {9}Partials}{14}
\contentsline {chapter}{\numberline {10}XAction}{15}
\contentsline {section}{\numberline {10.1}Define Actions in Code}{15}
\contentsline {section}{\numberline {10.2}Reference Actions in XJS}{15}
\contentsline {section}{\numberline {10.3}Upload Files}{15}
\contentsline {part}{III\hspace {1em}Data}{16}
\contentsline {chapter}{\numberline {11}Introduction}{17}
\contentsline {chapter}{\numberline {12}Define Meta-Types}{18}
\contentsline {chapter}{\numberline {13}Query Data}{19}
\contentsline {chapter}{\numberline {14}REST API}{20}
\contentsline {section}{\numberline {14.1}Attach to XJS Server}{20}
