
var push;
var ws;
var actions;
var scripts;
var scripts_pos;

function detect_jquery() {
    if(window.jQuery) {
        test_js();
    } else {
        console.log("hurr durr me iz buzy");
    }
}

function test_js () {
    check_server();
}

function invoke(name, optional) {
    optional = typeof optional !== 'undefined' ? optional : null;
    if($('#_js_messages').length == 0) {
        var element = '<div style="position: fixed; top: 5px; left: 5px; width: 300px; z-index: 10" id="_js_messages"></div>';
        $('body').append(element);
    }
    $.ajax({
        url : '/xjs/action/' + name + '.action',
        cache : false,
        method : 'get'
    }).done(function (data) {
        if(data.state == 'none') {
            console.log('Action not found ' + data.action.name);
            handle_updates([{type: 'message', data : 'Action ' + data.action.name + ' could not be found', severity: 'warning'}])
        } else if (data.state == 'direct-updates') {
             handle_updates(data.updates);
        } else {
            var args;
            if(data.action.requireFormEncoded) {
                console.log('Form Encoded requested. Performing operation in form encoded mode!');
                args = new FormData();
                var actionStore = new Object();
                if(data.action.id) {
                    actions[data.action.id] = actionStore;
                }
                if(data.action.param) {
                    var delay = false;
                    for(var key in data.action.param) {
                        var name = data.action.param[key];
                        if($('#' + name).attr('type') && $('#' + name).attr('type') == 'file') {
                            var reader = new FileReader();
                            if(!actionStore.fileReaders) {
                                actionStore.fileReaders = [];
                            }
                            actionStore.fileReaders.push(reader);
                            reader.onloadend = function (evt) {
                                var done = true;
                                args.append(name, evt.target.result);
                                for(var index in actionStore.fileReaders) {
                                    if(actionStore.fileReaders[index].readyState != 2) {
                                        done = false;
                                    }
                                }
                                if(done) {
                                    $.ajax({
                                        url: '/xjs/action/' + data.action.name + '.action',
                                        method: 'post',
                                        cache: false,
                                        data: args,
                                        processData: false,
                                        contentType: false,
                                        mimeType: 'multipart/form-data',
                                        dataType: 'json'
                                    }).done(function (data) {
                                        console.log(JSON.stringify(data));
                                        console.log('Action State ' + data.action.state);
                                        if(data.updates) {
                                            handle_updates(data.updates);
                                        }
                                    }).fail(function () {
                                        console.log('Failed to invoke Action');
                                        handle_updates([{type: 'message', data: 'Internal Server error.<br />Please check Server logs!', severity: 'danger'}])
                                    });
                                }
                            }
                            var file = document.getElementById(name).files[0];
                            if(file) {
                                reader.readAsText(file);
                                delay = true;
                            }
                        } else if($('#' + name).attr('type') && $('#' + name).attr('type') == 'checkbox') {
                            args.append(name, $('#' + name).is('checked'));
                        } else {
                            var val = $('#' + name).val();
                            if(val == undefined && document.getElementById(name)) {
                                val = document.getElementById(name).placeholder;
                            }
                            args.append(name, val);
                        }
                        if(!delay) {
                            $.ajax({
                                url: '/xjs/action/' + data.action.name + '.action',
                                method: 'post',
                                cache: false,
                                data: args,
                                processData: false,
                                contentType: false,
                                mimeType: 'multipart/form-data',
                                dataType: 'json'
                            }).done(function (data) {
                                console.log(JSON.stringify(data));
                                console.log('Action State ' + data.action.state);
                                if(data.updates) {
                                    handle_updates(data.updates);
                                }
                            }).fail(function () {
                                console.log('Failed to invoke Action');
                                handle_updates([{type: 'message', data: 'Internal Server error.<br />Please check Server logs!', severity: 'danger'}])
                            });
                        }
                    }
                }
            } else {
                if(data.action.param) {
                    args = new Object();
                    for(var key in data.action.param) {
                        var name = data.action.param[key];
                        if($('#' + name).attr('type') && $('#' + name).attr('type') == 'checkbox') {
                            args[name] = $('#' + name)[0].checked;
                        } else {
                            var val = $('#' + name).val();
                            if(val == undefined && document.getElementById(name)) {
                                val = document.getElementById(name).placeholder;
                            }
                            args[name] = val;
                        }
                    }
                } else {
                    args = {};
                }
                if(optional != null) {
                    args._optional = optional;
                }
                args = JSON.stringify(args);
                $.ajax({
                    url: '/xjs/action/' + data.action.name + '.action',
                    method: 'post',
                    cache: false,
                    data: args,
                    contentType: "application/json; charset=UTF-8",
                    processData: false
                }).done(function (data) {
                    console.log('Action State ' + data.action.state);
                    if(data.updates) {
                        handle_updates(data.updates);
                    }
                }).fail(function () {
                    console.log('Failed to invoke Action');
                    handle_updates([{type: 'message', data: 'Internal Server error.<br />Please check Server logs!', severity: 'danger'}])
                });
            }
        }
    }).fail(function () {
        console.log('Failed to invoke Action');
    });
}

function start_push () {
    push = window.setInterval(load_push, 500);
}

function stop_push () {
    if(push)
        window.clearInterval(push);
}

function check_server () {
    $.ajax({
        url: '/xjs/check/server.cap',
        cache: false,
        method: 'get'
    }).done(function (data) {
        if(data.services) {
            if(data.services.push && data.services.push == 'active') {
                start_push();
            }
            if(data.services.ws) {
                ws = true;
            }
            if(data.scripts) {
                scripts = [];
                for(var key in data.scripts) {
                    console.log(data.scripts[key]);
                    scripts.push(data.scripts[key]);
                }
                scripts_pos = 0;
                loadScript();
            }
        }
    }).fail(function () {
        alert('Could not contact server. Try to reload the page');
    });
}

function loadScript() {
    var script = scripts[scripts_pos];
    $.getScript(script).fail(function(jqxhr, settings, exception) {
        if(jqxhr.status >= 500) {
            loadScript();
        }
    }).done(function(data) {
      console.log('Successfully loaded ' + script);
      if(scripts_pos + 1 < scripts.length) {
        scripts_pos++;
        loadScript();
      }
    });
}

function load_push () {
    $.ajax({
        url: '/xjs/push/session.push',
        cache: false,
        method: 'get'
    }).done(function (data) {
        if(data.state != 'none') {
            handle_updates(data.updates);
        }
    }).fail(function () {
        stop_push();
    });
}

function handle_updates(updates) {
    //console.log("Updates: " + JSON.stringify(updates));
    for(var key in updates) {
        var update = updates[key];
        if(update.type == 'message') {
            if(update.severity) {
                $('#_js_messages').append('<div class="alert alert-' + update.severity + '" style="opacity: 0.8; display: none; width: 90%; padding-left: auto; padding-right: auto; z-index: 10">' + update.data + '</div>');
            } else {
                $('#_js_messages').append('<div class="alert alert-warning" style="opacity: 0.8; display: none; width: 90%; padding-left: auto; padding-right: auto; z-index: 10">' + update.data + '</div>');
            }
            if(update.time) {
                $('#_js_messages>div.alert').last().slideDown('fast').delay(update.time).slideUp('fast', function() {$(this).remove()});
            } else {
                $('#_js_messages>div.alert').last().slideDown('fast').delay(3000).slideUp('fast', function() {$(this).remove()});
            }
        } else if (update.type == 'value') {
            if($('#' + update.id).hasClass('progress')) {
                var bar = $('#' + update.id + ' .progress-bar');
                var text = $('#' + update.id + ' .progress-bar span');
                if(update.value > -1) {
                    bar.css('width', update.value + '%');
                    if(!update.striped) {
                        bar.removeClass('progress-bar-striped');
                    }
                    bar.removeClass('active');
                    bar.attr('aria-valuenow', update.value);
                    text.html(update.value + ' %');
                } else {
                    bar.css('width', '100%');
                    bar.addClass('progress-bar-striped');
                    bar.addClass('active');
                    bar.attr('aria-valuenow', '100%');
                    text.html('Pending ...');
                }
            } else {
                $('#' + update.id).val(update.value);
            }
        } else if (update.type == 'html') {
            $('#' + update.id).html(update.html);
        } else if (update.type == 'push') {
            if(update.push == 'on') {
                stop_push();
                start_push();
            } else if (update.push == 'off') {
                stop_push();
            }
        } else if (update.type == 'location') {
            window.location = update.location;
        } else if (update.type == 'add-class') {
            $('#' + update.id).addClass(update.class);
        } else if (update.type == 'remove-class') {
            $('#' + update.id).removeClass(update.class);
        } else if (update.type == 'focus') {
            $('#' + update.id).focus().select();
        } else if (update.type == 'title') {
            document.title = update.title;
        } else if (update.type == 'popup-window') {
            window.open(update.url, "_blank");
        }
    }
    createComponents();
}

function _xjs_update_combobox(id, value) {
    $('#' + id).val(value);
    $('#' + id + '_btn').html($('#' + id + '_' + value).html() + ' <span class="caret"></span>');
}

function createComponents () {
    $('._xjs_datepicker').each(function() {
        console.log("Creating Date Picker");
        $(this).datetimepicker({pickTime: false});    
    });
    $('._xjs_sqldatepicker').each(function () {
        console.log("Creating SQL Date Picker");
        $(this).datetimepicker({
            pickTime: false
        })
    });
    $(':file').each(function() {
        var name = 'btn-default', icon = 'glyphicon-folder-open', btnSize = 'nr', text = 'Choose file';
        if($(this).attr('data-buttonText')) {
            text = $(this).attr('data-buttonText');
        }
        if($(this).attr('data-size')) {
            btnSize = $(this).attr('data-size');
        }
        if($(this).attr('data-buttonName')) {
            name = $(this).attr('data-buttonName');
        }
        if($(this).attr('data-iconName')) {
            icon = $(this).attr('data-iconName');
        }
        $(this).filestyle({buttonText: text, size: btnSize, iconName: icon, buttonName: name});
    });
}

detect_jquery();