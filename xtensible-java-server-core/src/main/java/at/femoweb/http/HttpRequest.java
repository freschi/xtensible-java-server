package at.femoweb.http;

import at.femoweb.xjs.Server;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by felix on 9/23/14.
 */
public class HttpRequest {

    private boolean entityRead = false;
    private byte[] entityBuf;

    private String method;
    private String requestLine;
    private String requestPath;
    private String httpVersion;

    private HashMap<String, String> header;
    private HashMap<String, String> cookies;

    private HashMap<String, String> params;

    private InputStream entity;

    private HttpRequest() {
        header = new HashMap<String, String>();
        cookies = new HashMap<String, String>();
    }

    public static HttpRequest readFromStream(InputStream inputStream) throws IOException {
        HttpRequest request = new HttpRequest();
        request.method = null;
        int val;
        String line = "";
        while ((val = inputStream.read()) != -1) {
            if(val == '\r') {
                if(inputStream.available() > 0 && inputStream.read() == '\n') {
                    if(request.method == null) {
                        int offset;
                        request.method = line.substring(0, line.indexOf(" ")).toUpperCase();
                        request.requestPath = line.substring(line.indexOf(" ") + 1, offset = line.indexOf(" ", line.indexOf(" ") + 1));
                        request.httpVersion = line.substring(offset + 1, line.length());
                        request.requestLine = line;
                    } else if (line.length() == 0) {
                        request.entity = inputStream;
                        break;
                    } else {
                        String header = line.substring(0, line.indexOf(":")).trim();
                        if(header.equalsIgnoreCase("cookie")) {
                            String cookie = line.substring(line.indexOf(":") + 1);
                            String[] cookies = cookie.split(";");
                            for(String c : cookies) {
                                request.cookies.put(c.substring(0, c.indexOf("=")).trim(), c.substring(c.indexOf("=") + 1).trim());
                            }
                        } else {
                            request.header.put(header, line.substring(line.indexOf(":") + 1).trim());
                        }
                    }
                    line = "";
                }
            } else {
                line += (char) val;
            }
        }
        return request;
    }

    public String getRequestLine() {
        return requestLine;
    }

    public void setRequestLine(String requestLine) {
        this.requestLine = requestLine;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public void setHttpVersion(String httpVersion) {
        this.httpVersion = httpVersion;
    }

    public InputStream getEntity() {
        return entity;
    }

    public void setEntity(InputStream entity) {
        this.entity = entity;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public HashMap<String, String> getHeader () {
        return header;
    }

    public void setHeader (HashMap<String, String> header) {
        this.header = header;
    }

    public String header(String name) {
        return header.get(name);
    }

    public void header(String name, String value) {
        header.put(name, value);
    }

    public String cookie(String name) {
        return cookies.get(name);
    }

    public void cookie(String name, String value) {
        cookies.put(name, value);
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }

    public byte[] entity() {
        if(entity != null && !entityRead) {
            int max;
            if (header("Content-Length") != null) {
                max = Integer.parseInt(header("Content-Length"));
            } else {
                try {
                    max = entity.available();
                } catch (IOException e) {
                    return null;
                }
            }
            byte[] buffer = new byte[max];
            for (int i = 0; i < max; i++) {
                int read;
                try {
                    read = entity.read();
                    buffer[i] = (byte) read;
                } catch (IOException e) {
                    continue;
                }
                if (read == -1) {
                    byte[] buf = new byte[i];
                    for (int j = 0; j < i; j++) {
                        buf[j] = buffer[j];
                    }
                    buffer = buf;
                    break;
                }
            }
            entityBuf = buffer;
            entityRead = true;
            return buffer;
        } else if (entityRead) {
            return entityBuf;
        } else {
            return null;
        }
    }

    public String entityString () {
        try {
            return new String(entity(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Server.log.fatal("Server can't use utf-8", e);
            return new String(entity());
        }
    }
}
