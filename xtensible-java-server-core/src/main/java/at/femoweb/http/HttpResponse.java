package at.femoweb.http;

import at.femoweb.xjs.Server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by felix on 9/23/14.
 */
public class HttpResponse {

    private int status;
    private String statusLine;
    private byte[] entity;

    private HashMap<String, String> header;
    private HashMap<String, String> cookies;

    private HttpResponse () {
        entity = null;
        header = new HashMap<String, String>();
        cookies = new HashMap<String, String>();
    }

    public static HttpResponse getDefaultInstance(HttpRequest request) {
        return new HttpResponse();
    }

    public String getResponseLine() {
        return "HTTP/1.1 " + status + " " + statusLine;
    }

    private void setDefaultStatusLine() {
        if(status == 200) {
            statusLine = "OK";
        } else if (status == 404) {
            statusLine = "Not Found";
        } else if (status == 500) {
            statusLine = "Internal Server Error";
        }
    }

    public void writeToStream(OutputStream out) {
        setDefaultStatusLine();
        if(header.containsKey("Set-Cookie")) {
            String cookie = header.get("Set-Cookie");
            String[] cookies = cookie.split(";");
            for(String c : cookies) {
                this.cookies.put(c.substring(0, c.indexOf("=")), c.substring(c.indexOf("=") + 1));
            }
        }
        if(cookies.size() > 0) {
            String cookie = "";
            for (String name : cookies.keySet()) {
                cookie += name + "=" + cookies.get(name) + ";";
            }
            cookie = cookie.substring(0, cookie.length() - 1);
            header("Set-Cookie", cookie);
        }
        try {
            out.write((getResponseLine() + "\r\n").getBytes());
            if (!header.containsKey("Content-Length") && !header.containsKey("content-Length") && !header.containsKey("Content-length") && !header.containsKey("content-length")) {
                if (entity != null) {
                    header("Content-Length", entity.length + "");
                }
            }
            for (String key : header.keySet()) {
                out.write((key + ": " + header.get(key) + "\r\n").getBytes());
            }
            out.write("\r\n".getBytes());
            if (entity != null) {
                out.write(entity);
                out.write("\r\n".getBytes());
                out.write("\r\n".getBytes());
            }
        } catch (SocketException e) {
            Server.log.info("Socket errror: " + e.getMessage());
        } catch (IOException e) {
            Server.log.warn("Error while writing response to Stream", e);
        }
    }

    public void header(String name, String value) {
        header.put(name, value);
    }

    public String header(String name) {
        return header.get(name);
    }

    public String cookie(String name) {
        return cookies.get(name);
    }

    public void cookie(String name, String value) {
        cookies.put(name, value);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusLine() {
        return statusLine;
    }

    public void setStatusLine(String statusLine) {
        this.statusLine = statusLine;
    }

    public byte[] getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity.getBytes();
    }

    public void setEntity(byte[] entity) {
        this.entity = entity;
    }

    public HashMap<String, String> getHeader() {
        return header;
    }

    public void setHeader(HashMap<String, String> header) {
        this.header = header;
    }


}
