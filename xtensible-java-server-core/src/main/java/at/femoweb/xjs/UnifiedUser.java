package at.femoweb.xjs;

import java.util.HashMap;

/**
 * Created by felix on 10/14/14.
 */
public abstract class UnifiedUser {

    private String login;
    private String username;

    private HashMap<String, String> properties;

    public UnifiedUser () {
        properties = new HashMap<String, String>();
        connect();
        lock();
        String[] keys = getKeys();
        for(String key : keys) {
            properties.put(key, read(key));
        }
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    protected abstract void store(String name, String value);
    protected abstract void remove(String name);
    protected abstract String read(String value);
    protected abstract String[] getKeys();
    protected abstract void connect();
    protected abstract void lock();
    protected abstract void unlock();

    public void setProperty(String name, String value) {
        properties.put(name, value);
        store(name, value);
    }

    public void removeProperty(String name) {
        properties.remove(name);
        remove(name);
    }
}
