package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Session;

import java.util.HashMap;

/**
 * Created by felix on 10/28/14.
 */
public class Replacer {

    //private ArrayList<TagReplacer> replacer;
    private HashMap<String, TagReplacer> replacerHashMap;

    public Replacer() {
        this(LinkReplacer.class, ButtonReplacer.class, ProgressReplacer.class);
    }

    private Replacer(Class<? extends TagReplacer>... replacer) {
        //this.replacer = new ArrayList<TagReplacer>();
        replacerHashMap = new HashMap<>();
        if (replacer != null) {
            for (Class<? extends TagReplacer> r : replacer) {
                addReplacer(r);
            }
        }
    }

    public void addReplacer(Class<? extends TagReplacer> replacer) {
        TagReplacerInfo info = replacer.getAnnotation(TagReplacerInfo.class);
        try {
            replacerHashMap.put(info.tag(), replacer.newInstance());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private TagReplacerInfo getInfo(String name) {
        return getInfo(replacerHashMap.get(name));//.getClass().getAnnotation(TagReplacerInfo.class);
    }

    private TagReplacerInfo getInfo(TagReplacer replacer) {
        if(replacer == null)
            return null;
        return replacer.getClass().getAnnotation(TagReplacerInfo.class);
    }

    public void replace(Tag tag) {
        Session.set("_xjs_replacer", this);
        if(tag.name() != null && tag.name().startsWith("xjs:")) {
            TagReplacer replacer = replacerHashMap.get(tag.name());
            TagReplacerInfo info = getInfo(replacer);
            if(info == null) {
                Server.log.warn("No replacer for name " + tag.name() + " found");
                return;
            }
            if(info.type() == TagReplacerInfo.ReplacementType.DROP_INTERNAL) {
                tag.children().clear();
            }
            if (info.type() == TagReplacerInfo.ReplacementType.PROCESS_INTERNAL) {
                for(Tag t : tag.children()) {
                    if(t.parent() != tag) {
                        t.parent(tag);
                    }
                    replace(t);
                }
            }
            tag.replace(replacer.replace(tag), info.type() != TagReplacerInfo.ReplacementType.IGNORE_INTERNAL);
        } else {
            for (Tag t : tag.children()) {
                if (t.parent() != tag) {
                    t.parent(tag);
                }
                replace(t);
            }
        }
    }
}
