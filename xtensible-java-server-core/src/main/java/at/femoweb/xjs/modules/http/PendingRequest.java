package at.femoweb.xjs.modules.http;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by felix on 3/25/15.
 */
public class PendingRequest {

    private Socket socket;
    private HttpRequest request;
    private HttpResponse response;

    public PendingRequest(Socket socket) {
        this.socket = socket;
    }

    public void readRequest() throws IOException {
        if(Thread.currentThread() instanceof HttpHandlerThread) {
            throw new RuntimeException("PendingRequest.readRequest() should not be executed in the main HTTP Handler Thread");
        }
        request = HttpRequest.readFromStream(socket.getInputStream());
        response = HttpResponse.getDefaultInstance(request);
    }

    public Socket getSocket() {
        return socket;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public HttpResponse getResponse() {
        return response;
    }
}
