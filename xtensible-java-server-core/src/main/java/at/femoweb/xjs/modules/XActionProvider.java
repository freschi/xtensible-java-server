package at.femoweb.xjs.modules;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Depends;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.modules.xaction.*;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sun.misc.IOUtils;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by felix on 10/29/14.
 */
@Named("x-action")
@ContentProvider(regex = "\\/xjs\\/action\\/.*\\.action\\??.*")
@Depends("jres-service")
public class XActionProvider extends BootableModule implements Provider, Reloadable, Initable {

    private static Pattern action = Pattern.compile("\\/xjs\\/action\\/(.*)\\.action\\??.*");
    private static final String BOUNDARY = "boundary=";

    private static Logger log = LogManager.getLogger(XActionProvider.class);

    private HashMap<String, Method> xactions;

    private boolean logical = false;

    private String jroot;

    private LogicalUnit logicalUnit;

    public XActionProvider () {
        this.logical = false;
    }

    public XActionProvider (LogicalUnit logicalUnit, Properties properties) {
        this.logical = true;
        this.logicalUnit = logicalUnit;
        /*if(properties.containsKey("xaction-jroot"))
            this.jroot = properties.getProperty("xaction-jroot");*/
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        Matcher matcher = action.matcher(request.getRequestPath());
        if(response.getStatus() == 599) {
            JsonObject json = new JsonObject();
            json.addProperty("state", "direct-updates");
            Updates updates = new Updates();
            updates.changeLocation("/");
            json.add("updates", updates.getUpdates());
            response.setEntity(json.toString());
            response.header("Content-Type", "application/json");
            response.setStatus(200);
            return;
        }
        if(!matcher.matches()) {
            response.setStatus(422);
            response.setStatusLine("Unprocessable Entity");
        } else {
            if (request.getMethod().equalsIgnoreCase("get")) {
                response.setStatus(200);
                String action = matcher.group(1);
                Method xaction = xactions.get(action);
                if(xaction != null) {
                    JsonObject info = new JsonObject();
                    info.addProperty("state", "ok");
                    JsonObject jsaction = new JsonObject();
                    jsaction.addProperty("name", action);
                    JsonArray params = new JsonArray();
                    for (Parameter parameter : xaction.getParameters()) {
                        if (parameter.isAnnotationPresent(XParam.class)) {
                            XParam xParam = parameter.getAnnotation(XParam.class);
                            if (!xParam.name().equals("")) {
                                params.add(new JsonPrimitive(xParam.name()));
                            } else {
                                params.add(new JsonPrimitive(parameter.getName()));
                            }
                        }
                    }
                    if(params.size() > 0) {
                        jsaction.add("param", params);
                    }
                    if(xaction.getAnnotation(XAction.class).requireFormEncoded()) {
                        jsaction.addProperty("requireFormEncoded", true);
                    }
                    info.add("action", jsaction);
                    response.setEntity(info.toString());
                } else {
                    response.setEntity("{\"state\":\"none\", \"action\":{\"name\":\"" + action + "\"}}");
                }
                //response.setEntity("{\"state\":\"ok\", \"action\":{\"name\":\"" + action + "\", \"param\":[\"test\"]}}");
                response.header("Content-Type", "application/json; charset=UTF-8");
            } else if (request.getMethod().equalsIgnoreCase("post")) {
                if(request.header("Content-Type").equalsIgnoreCase("application/json; charset=UTF-8")) {
                    response.setStatus(200);
                    String action = matcher.group(1);
                    Method xaction = xactions.get(action);
                    JsonObject data = (JsonObject) new JsonParser().parse(request.entityString());
                    Object[] params = new Object[xaction.getParameterCount()];
                    Updates updates = new Updates();
                    for (int i = 0; i < params.length; i++) {
                        try {
                            Parameter parameter = xaction.getParameters()[i];
                            if (parameter.isAnnotationPresent(XParam.class)) {
                                String name = parameter.getAnnotation(XParam.class).name();
                                if (name != null && data.has(name)) {
                                    if (parameter.getType() == int.class) {
                                        params[i] = data.get(name).getAsInt();
                                    } else if (parameter.getType() == double.class) {
                                        params[i] = data.get(name).getAsDouble();
                                    } else if (parameter.getType() == short.class) {
                                        params[i] = data.get(name).getAsShort();
                                    } else if (parameter.getType() == byte.class) {
                                        params[i] = data.get(name).getAsByte();
                                    } else if (parameter.getType() == boolean.class) {
                                        params[i] = data.get(name).getAsBoolean();
                                    } else if (parameter.getType() == char.class) {
                                        params[i] = data.get(name).getAsCharacter();
                                    } else if (parameter.getType() == String.class) {
                                        params[i] = data.get(name).getAsString();
                                    } else if (parameter.getType() == float.class) {
                                        params[i] = data.get(name).getAsFloat();
                                    } else if (parameter.getType() == long.class) {
                                        params[i] = data.get(name).getAsLong();
                                    } else if (parameter.getType() == BigInteger.class) {
                                        params[i] = data.get(name).getAsBigInteger();
                                    } else if (parameter.getType() == BigDecimal.class) {
                                        params[i] = data.get(name).getAsBigDecimal();
                                    } else {
                                        params[i] = null;
                                    }
                                } else {
                                    params[i] = null;
                                }
                            } else if (parameter.isAnnotationPresent(XUpdate.class) && parameter.getType().isAssignableFrom(Updates.class)) {
                                params[i] = updates;
                            } else if (parameter.isAnnotationPresent(XOptional.class) && parameter.getType().isAssignableFrom(JsonElement.class)) {
                                if (data.has("_optional")) {
                                    params[i] = data.get("_optional");
                                }
                            } else {
                                params[i] = null;
                            }
                        } catch (SessionNotFoundException e) {
                            response.setStatus(303);
                            response.setStatusLine("See Other");
                            response.header("Location", "/");
                        } catch (Throwable t) {
                            Server.notifyUncaughtException(Thread.currentThread(), t, false);
                        }
                    }
                    try {
                        xaction.invoke(xaction.getDeclaringClass().newInstance(), params);
                    } catch (IllegalAccessException e) {
                        Server.log.warn("Can't access Action method", e);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), e, false);
                    } catch (InvocationTargetException e) {
                        Server.log.warn("Error while executing action", e);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), e, false);
                    } catch (InstantiationException e) {
                        Server.log.warn("Can't instatiate method handler", e);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), e, false);
                    } catch (Throwable t) {
                        Server.log.warn("Unknown error while executing Action", t);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), t, false);
                    }
                    JsonObject json = new JsonObject();
                    JsonObject jaction = new JsonObject();
                    json.add("action", jaction);
                    jaction.addProperty("name", action);
                    jaction.addProperty("state", "ok");
                    if (!updates.empty()) {
                        json.add("updates", updates.getUpdates());
                    }
                    Server.log.debug("Response to " + request.getRequestLine() + ": " + json.toString());
                    response.setEntity(json.toString());
                    response.header("Content-Type", "application/json");
                } else if (request.header("Content-Type").toLowerCase().startsWith("multipart/form-data")) {
                    Server.log.debug(request.header("Content-Type"));
                    if(!request.header("Content-Type").contains(BOUNDARY)) {
                        response.setStatus(400);
                        response.setStatusLine("Bad Request");
                        return;
                    }
                    String contentType = request.header("Content-Type");
                    String boundary = contentType.substring(contentType.indexOf(BOUNDARY) + BOUNDARY.length());
                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                    byte[] buf = new byte[2048];
                    int read;
                    int total = 0;
                    int length = Integer.parseInt(request.header("Content-Length"));
                    try {
                        while (total < length && (read = request.getEntity().read(buf)) > 0) {
                            buffer.write(buf, 0, read);
                            total += read;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buffer.toByteArray())));
                    String line;
                    HashMap<String, String> vars = new HashMap<>();
                    String current = null;
                    String currentName = null;
                    boolean recentBoundary = false;
                    try {
                        while((line = reader.readLine()) != null) {
                            Server.log.debug(line);
                            if(line.equals("--" + boundary)) {
                                recentBoundary = true;
                            } else if (line.equals("--" + boundary + "--")) {
                                if (current != null && currentName != null) {
                                    if(current.endsWith("\n")) {
                                        current = current.substring(0, current.length() - 1);
                                    }
                                    vars.put(currentName, current);
                                    current = null;
                                    currentName = null;
                                }
                            } else if (recentBoundary && line.startsWith("Content-Disposition:")) {
                                if(line.contains("name=")) {
                                    currentName = line.substring(line.indexOf("name=") + 6, line.length() - 1);
                                }
                                reader.readLine(); //Remove wrong newline at the end of the data
                                recentBoundary = false;
                            } else {
                                if(current == null) {
                                    current = "";
                                }
                                current += line + "\n";
                                recentBoundary = false;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //TODO clean code up sometime









                    String action = matcher.group(1);
                    Method xaction = xactions.get(action);
                    Object[] params = new Object[xaction.getParameterCount()];
                    Updates updates = new Updates();
                    for (int i = 0; i < params.length; i++) {
                        try {
                            Parameter parameter = xaction.getParameters()[i];
                            if (parameter.isAnnotationPresent(XParam.class)) {
                                String name = parameter.getAnnotation(XParam.class).name();
                                if (name != null && vars.containsKey(name)) {
                                    if (parameter.getType() == int.class) {
                                        params[i] = Integer.parseInt(vars.get(name));
                                    } else if (parameter.getType() == double.class) {
                                        params[i] = Double.parseDouble(vars.get(name));
                                    } else if (parameter.getType() == short.class) {
                                        params[i] = Short.parseShort(vars.get(name));
                                    } else if (parameter.getType() == byte.class) {
                                        params[i] = Byte.parseByte(vars.get(name));
                                    } else if (parameter.getType() == boolean.class) {
                                        params[i] = Boolean.parseBoolean(vars.get(name));
                                    } else if (parameter.getType() == char.class) {
                                        params[i] = vars.get(name).charAt(0);
                                    } else if (parameter.getType() == String.class) {
                                        params[i] = vars.get(name);
                                    } else if (parameter.getType() == float.class) {
                                        params[i] = Float.parseFloat(vars.get(name));
                                    } else if (parameter.getType() == long.class) {
                                        params[i] = Long.parseLong(vars.get(name));
                                    } else {
                                        params[i] = null;
                                    }
                                } else {
                                    params[i] = null;
                                }
                            } else if (parameter.isAnnotationPresent(XUpdate.class) && parameter.getType().isAssignableFrom(Updates.class)) {
                                params[i] = updates;
                            } else if (parameter.isAnnotationPresent(XOptional.class) && parameter.getType().isAssignableFrom(JsonElement.class)) {
                                if (vars.containsKey("_optional")) {
                                    params[i] = new JsonParser().parse(vars.get("_optional"));
                                }
                            } else {
                                params[i] = null;
                            }
                        } catch (SessionNotFoundException e) {
                            response.setStatus(303);
                            response.setStatusLine("See Other");
                            response.header("Location", "/");
                        } catch (Throwable t) {
                            Server.notifyUncaughtException(Thread.currentThread(), t, false);
                        }
                    }
                    try {
                        xaction.invoke(xaction.getDeclaringClass().newInstance(), params);
                    } catch (IllegalAccessException e) {
                        Server.log.warn("Can't access Action method", e);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), e, false);
                    } catch (InvocationTargetException e) {
                        Server.log.warn("Error while executing action", e);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), e, false);
                    } catch (InstantiationException e) {
                        Server.log.warn("Can't instatiate method handler", e);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), e, false);
                    } catch (Throwable t) {
                        Server.log.warn("Unknown error while executing Action", t);
                        response.setStatus(500);
                        Server.notifyUncaughtException(Thread.currentThread(), t, false);
                    }
                    JsonObject json = new JsonObject();
                    JsonObject jaction = new JsonObject();
                    json.add("action", jaction);
                    jaction.addProperty("name", action);
                    jaction.addProperty("state", "ok");
                    if (!updates.empty()) {
                        json.add("updates", updates.getUpdates());
                    }
                    Server.log.debug("Response to " + request.getRequestLine() + ": " + json.toString());
                    response.setEntity(json.toString());
                    response.header("Content-Type", "application/json");












                    response.setStatus(200);
                } else {
                    Server.log.warn("Unknown Content-Type: " + request.header("Content-Type"));
                    response.setStatus(415);
                    response.setStatusLine("Unsupported Media Type");
                }
            } else {
                response.setStatus(405);
                response.setStatusLine("Method not allowed");
                response.header("Allow", "GET, POST");
            }
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }

    @Override
    public void init(Properties properties) {
        reload();
        if(logical) {
            Service jres = logicalUnit.getService("jres-service");
            jres.put(new AnnotationHandlerBundle(XAction.class, new AnnotationHandler<XAction>() {
                @Override
                public void foundType(Class<XAction> annotation, Class<?> type) {

                }

                @Override
                public void foundMethod(Class<XAction> annotation, Method method) {
                    Server.log.info("Found Method " + method.getName() + "@" + method.getDeclaringClass().getName());
                    XAction xAction = method.getAnnotation(XAction.class);
                    xactions.put(xAction.name(), method);
                }

                @Override
                public void foundField(Class<XAction> annotation, Field field) {

                }
            }));
        }
    }

    @Override
    public void reload() {
        if(logical) {
            //File dir = new File(logicalUnit.getName() + "/" + jroot);
            if (xactions == null) {
                xactions = new HashMap<String, Method>();
            } else {
                xactions.clear();
            }
            /*if (!dir.exists()) {
                dir.mkdir();
            } else {
                ArrayList<File> files = new ArrayList<File>();
                walkTree(dir, files, ".java");
                if(files.isEmpty())
                    return;
                JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
                StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
                Iterable<? extends JavaFileObject> units;
                units = fileManager.getJavaFileObjectsFromFiles(files);
                List<String> optionList = new ArrayList<String>();
                //File xjs_jar = new File("/home/felix/IdeaProjects/XJS/out/artifacts/XJS_jar/XJS.jar");
                Server.log.info("Class Path: " + System.getProperty("java.class.path"));
                optionList.addAll(Arrays.asList("-classpath", System.getProperty("java.class.path")/* + ":" + xjs_jar.getAbsolutePath()));
                JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, diagnostic -> Server.log.error(diagnostic.getMessage(Locale.getDefault()) + " @" + diagnostic.getSource().getName() + ":" + diagnostic.getLineNumber()), optionList, null, units);
                task.call();
                try {
                    fileManager.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                files.clear();
                walkTree(dir, files, ".class");
                try {
                    URLClassLoader classLoader = new URLClassLoader(new URL[]{dir.toURI().toURL()});
                    for (File file : files) {
                        String name = file.getAbsolutePath().substring(dir.getAbsolutePath().length() + 1);
                        name = name.substring(0, name.lastIndexOf(".")).replace("/", ".");
                        log.info("Loading compiled class " + name);
                        try {
                            Class<?> clazz = classLoader.loadClass(name);
                            log.info("Successfully loaded " + clazz.getCanonicalName());
                            for (Method method : clazz.getDeclaredMethods()) {
                                method.setAccessible(true);
                                if (method.isAnnotationPresent(XAction.class)) {
                                    log.info("Found Action " + method.getAnnotation(XAction.class).name() + " in " + clazz.getCanonicalName() + "." + method.getName());
                                    XAction xAction = method.getAnnotation(XAction.class);
                                    xactions.put(xAction.name(), method);
                                }
                            }
                        } catch (Exception e) {
                            log.warn("Couldn't load class " + name);
                        }
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }*/
        }
    }

    private void walkTree(File file, ArrayList<File> compile, String filter) {
        for(File f : file.listFiles()) {
            if(f.isDirectory()) {
                walkTree(f, compile, filter);
            } else {
                if(f.getName().toLowerCase().endsWith(filter))
                    compile.add(f);
            }
        }
    }


    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        XActionProvider provider = new XActionProvider();
        provider.logical = true;
        provider.logicalUnit = logicalUnit;
        return provider;
    }

    @Override
    public void logInfo() {
        Server.log.info("XAction Provider for unit " + logicalUnit.getName());
    }

    @Override
    public void setup() {

    }
}
