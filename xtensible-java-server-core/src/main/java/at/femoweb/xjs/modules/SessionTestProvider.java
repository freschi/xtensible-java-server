package at.femoweb.xjs.modules;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

/**
 * Created by felix on 10/17/14.
 */
@ContentProvider(regex = ".*\\.ses")
@Named("session-content")
public class SessionTestProvider extends Module implements Provider {

    public SessionTestProvider () {

    }

    public SessionTestProvider(LogicalUnit logicalUnit) {

    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        if(Session.get("hello-world") != null) {
            response.setEntity(Session.get("hello-world").toString());
        } else {
            response.setEntity("Setting");
            Session.set("hello-world", "This haz workd");
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }
}
