package at.femoweb.xjs.modules.http;

import at.femoweb.xjs.Server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by felix on 3/18/15.
 */
public class HttpHandlerThread extends Thread {

    private HttpHandler handler;
    private ServerSocket serverSocket;

    public HttpHandlerThread(HttpHandler httpHandler) {
        this.handler = httpHandler;
    }

    @Override
    public void run() {
        try {
            if(handler.getIp() == null) {
                serverSocket = new ServerSocket(handler.getPort(), 100);
            } else {
                serverSocket = new ServerSocket(handler.getPort(), 100, InetAddress.getByName(handler.getIp()));
            }
            while (true) {
                Socket socket = serverSocket.accept();
                PendingRequest pendingRequest = new PendingRequest(socket);
                handler.enqueue(pendingRequest);
            }
        } catch (IOException e) {
            Server.log.warn("Error while listening for client connections to " + handler.getName(), e);
        }
    }

    @Override
    public void interrupt() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            Server.log.fatal("Could not shutdown server socket for " + handler.getName());
        }
        super.interrupt();
    }
}
