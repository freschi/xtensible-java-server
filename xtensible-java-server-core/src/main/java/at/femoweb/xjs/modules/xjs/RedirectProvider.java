package at.femoweb.xjs.modules.xjs;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.ContentProviderType;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.util.Properties;

/**
 * Created by felix on 2/24/15.
 */
@ContentProvider(regex = ".*", type = ContentProviderType.FALLBACK)
@Named("redirect-provider")
public class RedirectProvider extends BootableModule implements Provider, Initable {

    private LogicalUnit logicalUnit;
    private String location;


    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        RedirectProvider redirectProvider = new RedirectProvider();
        redirectProvider.logicalUnit = logicalUnit;
        redirectProvider.location = settings.getString("location");
        return redirectProvider;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    public RedirectProvider () {

    }

    public RedirectProvider (LogicalUnit unit, Properties properties) {
        if(properties.containsKey("location")) {
            this.location = properties.getProperty("location");
        }
        logicalUnit = unit;
    }

    @Override
    public void init(Properties properties) {

    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        response.setStatus(301);
        response.setStatusLine("Moved Permanently");
        response.header("Location", location);
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }
}
