package at.femoweb.xjs.modules;

/**
 * Created by felix on 3/18/15.
 */
public class HandoverFailedException extends Exception {

    public enum Reason {
        WRONG_REQUEST, ILLEGAL_STATE
    }

    private Reason reason;

    public HandoverFailedException(Reason reason) {
        super("Handover failed because of " + reason);
        this.reason = reason;
    }
}
