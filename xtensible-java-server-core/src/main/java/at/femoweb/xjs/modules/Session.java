package at.femoweb.xjs.modules;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.services.SessionService;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;
import at.femoweb.xjs.structure.SessionedThread;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 10/17/14.
 */
@Service(type = ServiceType.SESSION)
@Named("session")
public final class Session extends Module implements SessionService, Initable, Reloadable {

    private static final int SESSION_TOKEN_LENGTH = 50;
    private static HashMap<String, Session> sessions;
    private static SecureRandom secureRandom;
    private static HashMap<Thread, Session> currentSession;
    private HashMap<String, Object> values;
    private String key;
    private LogicalUnit unit;
    private static ArrayList<String> discardedSessions;

    public Session () {

    }

    public Session (LogicalUnit logicalUnit, Properties properties) {
        unit = logicalUnit;
    }

    private Session(HttpRequest request, HttpResponse response) {
        key = new BigInteger(SESSION_TOKEN_LENGTH, secureRandom).toString(32);
        values = new HashMap<>();
        values.put("session.key", key);
        values.put("server.name", Server.physicalUnit.getName());
        response.cookie("XJS-Session-Token", key + "; Path=/");
        Server.log.debug("Creating Session " + key);
    }

    public static void register(SessionedThread thread) {
        currentSession.put(thread, currentSession.get(Thread.currentThread()));
    }

    public static void remove(String name) {
        getCurrent().values.remove(name);
    }

    private static Session getCurrent() {
        if(!currentSession.containsKey(Thread.currentThread()))
            throw new SessionNotFoundException();
        return currentSession.get(Thread.currentThread());
    }

    public static void destroy() {
        Session session = getCurrent();
        discardedSessions.add(session.getKey());
        sessions.remove(session.getKey());
        currentSession.values().removeAll(Collections.singleton(session));
    }

    public static Object get(String name) {
        Session session = getCurrent();
        if(session.values.containsKey(name)) {
            return session.values.get(name);
        } else {
            if(session.values.get("_xjs_logical_unit") != null) {
                LogicalUnit logicalUnit = (LogicalUnit) session.values.get("_xjs_logical_unit");
                if(logicalUnit.hasService("resources")) {
                    return logicalUnit.getService("resources").put(name);
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    public static boolean is(String name) {
        return getCurrent().values.containsKey(name);
    }

    public static UnifiedUser getUser() {
        return (UnifiedUser) get("_xjs_user");
    }

    public static boolean isAuthenticated() {
        return is("_xjs_user");
    }

    public static void set(String name, Object value) {
        getCurrent().values.put(name, value);
    }

    @Override
    public void init(Properties properties) {
        sessions =  new HashMap<String, Session>();
        discardedSessions = new ArrayList<>();
        Server.log.debug("Initializing Random Session Token Generator");
        secureRandom = new SecureRandom();
        currentSession = new HashMap<Thread, Session>();

    }

    @Override
    public void setupSession(HttpRequest request, HttpResponse response) {
        if(sessions.containsKey(request.cookie("XJS-Session-Token"))) {
            currentSession.put(Thread.currentThread(), sessions.get(request.cookie("XJS-Session-Token")));
            getCurrent().put("_xjs_request", request);
            getCurrent().put("_xjs_response", response);
            getCurrent().put("_xjs_logical_unit", unit);
            response.cookie("XJS-Session-Token", request.cookie("XJS-Session-Token"));
        } else if (!discardedSessions.contains(request.cookie("XJS-Session-Token"))) {
            Session session = new Session(request, response);
            currentSession.put(Thread.currentThread(), session);
            sessions.put(session.getKey(), session);
            getCurrent().put("_xjs_request", request);
            getCurrent().put("_xjs_response", response);
            getCurrent().put("_xjs_logical_unit", unit);
        } else {
            throw new SessionNotFoundException();
        }
    }

    @Override
    public void requestNewSession(HttpRequest request, HttpResponse response) {
        discardedSessions.remove(request.cookie("XJS-Session-Token"));
    }

    @Override
    public void destroySession(HttpResponse response) {
        discardedSessions.add(getKey());
        sessions.remove(getKey());
        if(response != null)
            response.cookie("XJS-Session-Token", "");
    }

    private String getKey() {
        return key;
    }

    private void setKey(String key) {
        this.key = key;
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }

    @Override
    public void reload() {
        Server.log.info("Destroying all sessions");
        ArrayList<Session> sessions = new ArrayList<>();
        sessions.addAll(Session.sessions.values());
        for(Session session : sessions) {
            try {
                session.destroySession(null);
            } catch (SessionNotFoundException e) {
                Server.log.info("Could not destroy session. Not found!");
            }
        }
        Session.sessions.clear();
        currentSession.clear();
    }

    public static LogicalUnit getLogicalUnit() {
        return (LogicalUnit) get("_xjs_logical_unit");
    }

    public static boolean exists() {
        return currentSession.containsKey(Thread.currentThread());
    }
}
