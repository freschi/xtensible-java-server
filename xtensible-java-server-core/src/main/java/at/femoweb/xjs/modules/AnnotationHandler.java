package at.femoweb.xjs.modules;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by felix on 11/13/14.
 */
public interface AnnotationHandler<T extends Annotation> {

    public abstract void foundType (Class<T> annotation, Class<?> type);
    public abstract void foundMethod (Class<T> annotation, Method method);
    public abstract void foundField (Class<T> annotation, Field field);
}
