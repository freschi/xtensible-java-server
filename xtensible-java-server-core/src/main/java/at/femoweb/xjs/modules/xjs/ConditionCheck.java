package at.femoweb.xjs.modules.xjs;

/**
 * Created by felix on 1/8/15.
 */
public interface ConditionCheck {

    public abstract boolean check(String[] args);
}
