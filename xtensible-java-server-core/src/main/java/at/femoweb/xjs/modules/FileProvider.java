package at.femoweb.xjs.modules;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.io.*;
import java.util.Properties;

/**
 * Created by felix on 10/13/14.
 */
@Named("file-provider")
@ContentProvider(regex = ".*")
public class FileProvider extends BootableModule implements Provider, Initable {

    private boolean logical;

    private String path;
    private LogicalUnit unit;
    private boolean createStructure;
    private boolean directoryIndexing;
    private File htdocs;

    public FileProvider () {
        logical = false;
    }

    public FileProvider(LogicalUnit logicalUnit, Properties properties) {
        this.unit = logicalUnit;
        this.path = properties.getProperty("htdocs");
        directoryIndexing = false;
        if(properties.containsKey("directory-indexing")) {
            directoryIndexing = properties.getProperty("directory-indexing").equalsIgnoreCase("true");
        }
        logical = true;
        createStructure = Boolean.parseBoolean(properties.getProperty("create-structure"));
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        String path = request.getRequestPath();
        if(request.getRequestPath().contains("?")) {
            path = request.getRequestPath().substring(0, path.indexOf("?"));
        }
        File file = new File(htdocs, path);
        if(!file.exists()) {
            response.setStatus(404);
            response.setStatusLine("Not found");
            response.setEntity("File Not Found".getBytes());
            response.header("Content-Length", "14");
        } else if (file.isFile() || new File(file, "index.html").exists()) {
            if(new File(file, "index.html").exists()) {
                file = new File(file, "index.html");
            }
            response.setStatus(200);
            response.setStatusLine("OK");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[2048];
                int read;
                while((read = fis.read(buffer)) != -1) {
                    bos.write(buffer, 0, read);
                }
            } catch (IOException e) {
                Server.log.warn("Unable to serve file: " + request.getRequestPath(), e);
                e.printStackTrace(new PrintStream(bos));
                response.setStatus(500);
                response.setStatusLine("Internal Server Error");
            }
            response.header("Content-Length", bos.size() + "");
            if(unit.hasService("mime-service")) {
                Object mime = unit.getService("mime-service").put(file.getName());
                if(mime instanceof String) {
                    response.header("Content-Type", (String) mime);
                }
            }
            if(response.header("Content-Type") == null) {
                response.header("Content-Type", "text");
            }
            response.setEntity(bos.toByteArray());
        } else if (file.isDirectory() && directoryIndexing) {
            response.setStatus(200);
            response.setStatusLine("OK");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(bos);
            ps.println("<!DOCTYPE html>");
            ps.println("<html>");
            ps.println("<head><title>" + request.getRequestPath() + "</title></head>");
            ps.println("<body>");
            ps.println("<h1>Index of: " + request.getRequestPath() + "</h1>");
            ps.println("<hr style=\"height:0.5px\" />");
            File[] files = file.listFiles();
            for(File f : files) {
                ps.println("- <a href=\"" + request.getRequestPath() + (request.getRequestPath().endsWith("/") ? "" : "/") + f.getName() + "\">" + f.getName() + "</a><br />");
            }
            ps.println("<hr style=\"height:0.5px\" />");
            ps.println("</body></html>");
            response.setEntity(bos.toByteArray());
            response.header("Content-Length", bos.size() + "");
        } else {
            response.setStatus(403);
            response.setStatusLine("Forbidden");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                bos.write("Directory indexing is forbidden on this server!".getBytes());
            } catch (IOException e) {
                Server.log.warn("Could not write error message", e);
            }
            response.header("Content-Length", bos.size() + "");
            response.setEntity(bos.toByteArray());
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        String path = request.getRequestPath();
        if(request.getRequestPath().contains("?")) {
            path = request.getRequestPath().substring(0, path.indexOf("?"));
        }
        File file = new File(htdocs, path);
        return file.exists();
    }

    @Override
    public void init(Properties properties) {
        if(logical) {
            htdocs = new File(this.unit.getName() + "/" + path);
            if (createStructure) {
                htdocs.mkdirs();
            }
        }
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        FileProvider fileProvider = new FileProvider();
        fileProvider.htdocs = new File(logicalUnit.getRootDir(), settings.getString("htdocs"));
        fileProvider.unit = logicalUnit;
        fileProvider.createStructure = settings.getBoolean("createStructure");
        fileProvider.directoryIndexing = settings.getBoolean("directoryIndexing");
        return fileProvider;
    }

    @Override
    public void logInfo() {
        Server.log.info("FileProvider for " + unit.getName() + "@" + htdocs);
    }

    @Override
    public void setup() {

    }
}
