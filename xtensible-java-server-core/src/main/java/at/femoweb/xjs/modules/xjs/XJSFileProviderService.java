package at.femoweb.xjs.modules.xjs;

import at.femoweb.config.entries.Group;
import at.femoweb.html.Document;
import at.femoweb.html.Tag;
import at.femoweb.html.TextTag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.XJSFile;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.modules.JavaRessourceService;
import at.femoweb.xjs.modules.xjs.strategies.BoldNamingStrategy;
import at.femoweb.xjs.modules.xjs.strategies.NamingStrategy;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;
import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.LongMemberValue;
import javassist.bytecode.annotation.StringMemberValue;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 5/10/15.
 */
@at.femoweb.xjs.annotations.Service(type = ServiceType.CONTENT)
@Named("xjs-file-provider-service")
public class XJSFileProviderService extends BootableModule implements Initable, Service {

    private BootableLogicalUnit logicalUnit;
    private HashMap<String, Class<? extends DynamicTag>> tagDefinitions;
    private NamingStrategy namingStrategy = new BoldNamingStrategy();
    private HashMap<String, Class<?>> pathMappings;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        XJSFileProviderService service = new XJSFileProviderService();
        service.logicalUnit = logicalUnit;
        service.tagDefinitions = new HashMap<>();
        service.pathMappings = new HashMap<>();
        return service;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public void init(Properties properties) {
        if(logicalUnit != null) {
            if (logicalUnit.hasService("jres-service")) {
                JavaRessourceService ressourceService = (JavaRessourceService) logicalUnit.getService("jres-service");
                Class<?>[] classes = ressourceService.getAllClasses(XJSTag.class);
                for (Class<?> clazz : classes) {
                    XJSTag xjsTag = clazz.getAnnotation(XJSTag.class);
                    if (DynamicTag.class.isAssignableFrom(clazz)) {
                        tagDefinitions.put(xjsTag.value(), (Class<? extends DynamicTag>) clazz);
                    }
                }
                classes = ressourceService.getAllClasses(XJSFile.class);
                for(Class<?> clazz : classes) {
                    XJSFile xjsFile = clazz.getAnnotation(XJSFile.class);
                    String dPath = logicalUnit.getRootDir().getAbsolutePath() + "/htdocs/" + xjsFile.path();
                    File dFile = new File(dPath);
                    if(dFile.lastModified() <= xjsFile.timestamp()) {
                        pathMappings.put(xjsFile.path(), clazz);
                    }
                }
            }
        }
    }

    @Override
    public Object put(Object... objects) {
        for(Object object : objects) {
            if (object instanceof String) {
                try {
                    Class clazz;
                    if(pathMappings.containsKey(object)) {
                        clazz = pathMappings.get(object);
                    } else {
                        String name = createClassName((String) object);
                        try {
                            clazz = Class.forName(name);
                        } catch (ClassNotFoundException e) {
                            clazz = createXJSComponent(name, (String) object);
                        }
                        pathMappings.put((String) object, clazz);
                    }
                    Constructor constructor = clazz.getConstructor(String.class);
                    Object tagObject = constructor.newInstance("");
                    return tagObject;
                } catch (NotFoundException | CannotCompileException | IOException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private String createClassName(String path) {
        try {
            String dPath = logicalUnit.getRootDir().getAbsolutePath() + "/htdocs/" + path;
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(dPath));
            byte[] buffer = new byte[(int) new File(dPath).length()];
            dataInputStream.readFully(buffer);
            String doc = new String(buffer);
            Document document = Document.readDocument(doc);
            return namingStrategy.findName(path, document);
        } catch (IOException e) {
            return null;
        }
    }

    private Class createXJSComponent(String name, String path) throws NotFoundException, CannotCompileException, IOException {
        Server.log.info("Creating virtual type " + name + " for " + path);
        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.makeClass(name);
        ctClass.setSuperclass(classPool.get(Tag.class.getName()));
        ClassFile classFile = ctClass.getClassFile();
        ConstPool constPool = classFile.getConstPool();
        String dPath = logicalUnit.getRootDir().getAbsolutePath() + "/htdocs/" + path;
        CtMethod ctMethod = CtNewMethod.make("public String render(int level, boolean alone) {String val = \"Hello World\"; return val;}", ctClass);
        Server.log.info(dPath);
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(dPath));
        byte[] buffer = new byte[(int) new File(dPath).length()];
        dataInputStream.readFully(buffer);
        String doc = new String(buffer);
        Document document = Document.readDocument(doc);

        AnnotationsAttribute annotationsAttribute = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag);
        Annotation annotation = new Annotation("at.femoweb.xjs.annotations.XJSFile", constPool);
        StringMemberValue stringMemberValue = new StringMemberValue(constPool);
        stringMemberValue.setValue(path);
        LongMemberValue longMemberValue = new LongMemberValue(new File(dPath).lastModified(), constPool);
        annotation.addMemberValue("path", stringMemberValue);
        annotation.addMemberValue("timestamp", longMemberValue);
        annotationsAttribute.addAnnotation(annotation);
        if(document.content().attribute("package") != null) {
            annotation = new Annotation("at.femoweb.xjs.annotations.Package", constPool);
            stringMemberValue = new StringMemberValue(constPool);
            stringMemberValue.setValue(document.content().attribute("package"));
            annotation.addMemberValue("value", stringMemberValue);
            annotationsAttribute.addAnnotation(annotation);
        }
        classFile.addAttribute(annotationsAttribute);

        if(document.doctype().equals("xjs:html") || document.doctype().equals("xjs:page")) {
            ctMethod.setBody("{" + createPage(document) + "}");
        } else if (document.doctype().equals("xjs:partial")) {
            ctMethod.setBody("{" + createPartial(document) + "}");
        }
        ctMethod.insertBefore("at.femoweb.xjs.Server.log.debug(\"Calling virtual page type for " + path + " [\" + $0.getClass().getName() + \"]\");");
        ctClass.addMethod(ctMethod);
        ctClass.writeFile(logicalUnit.getRootDir().getAbsolutePath() + "/jbin/");
        return ctClass.toClass();
    }

    private String createPage(Document document) {
        String pageCode = "String result = \"<!DOCTYPE html>\\n\";";
        Tag page = document.content().get(0);
        pageCode += "ClassLoader loader = null;";
        pageCode += "if(at.femoweb.xjs.modules.Session.is(\"_xjs_logical_unit\")) {";
        pageCode += "at.femoweb.xjs.structure.LogicalUnit unit = (at.femoweb.xjs.structure.LogicalUnit) at.femoweb.xjs.modules.Session.get(\"_xjs_logical_unit\");";
        pageCode += "if(unit.hasService(\"jres-service\")) {";
        pageCode += "at.femoweb.xjs.modules.JavaRessourceService jres = (at.femoweb.xjs.modules.JavaRessourceService) unit.getService(\"jres-service\");";
        pageCode += "loader = jres.getClassLoader();";
        pageCode += "}}";
        pageCode += "at.femoweb.html.Tag html = new at.femoweb.html.Tag(\"html\");\n";
        pageCode += "at.femoweb.html.Tag head = new at.femoweb.html.Tag(\"head\", html);\n";
        pageCode += "at.femoweb.html.Tag body = new at.femoweb.html.Tag(\"body\", html);\n";
        pageCode += "if(at.femoweb.xjs.modules.Session.is(\"_xjs_logical_unit\")) {";
        pageCode += "at.femoweb.xjs.structure.LogicalUnit unit = (at.femoweb.xjs.structure.LogicalUnit) at.femoweb.xjs.modules.Session.get(\"_xjs_logical_unit\");\n";
        pageCode += "if(unit.hasService(\"xjs-style-service\")) {";
        pageCode += "java.util.Collection collection = (java.util.Collection) unit.getService(\"xjs-style-service\").put(new Object[]{\"" + page.attribute("style") + "\"});\n";
        pageCode += "java.util.Iterator iterator = collection.iterator();\n";
        pageCode += "while(iterator.hasNext()) {";
        pageCode += "head.add((at.femoweb.html.Tag) iterator.next());\n";
        pageCode += "}}}";
        if(page.attribute("title") != null) {
            pageCode += "at.femoweb.html.Tag title = new at.femoweb.html.Tag(\"title\", head);\n";
            pageCode += "title.add(\"" + page.attribute("title") + "\");\n";
        }
        pageCode += "if(at.femoweb.xjs.modules.Session.is(\"_xjs_logical_unit\")) {";
        pageCode += "at.femoweb.xjs.structure.LogicalUnit unit = (at.femoweb.xjs.structure.LogicalUnit) at.femoweb.xjs.modules.Session.get(\"_xjs_logical_unit\");\n";
        pageCode += "if(unit.hasProvider(\"x-scripts\")) {";
        pageCode += "at.femoweb.html.Tag script = new at.femoweb.html.Tag(\"script\", head);\n";
        pageCode += "script.attribute(\"src\", \"/js/jquery-2.1.3.min.js\");\n";
        pageCode += "script.attribute(\"type\", \"text/javascript\");\n";
        pageCode += "script.type(at.femoweb.html.TagType.FULL);\n";
        pageCode += "script = new at.femoweb.html.Tag(\"script\", head);\n";
        pageCode += "script.attribute(\"src\", \"/xjs/xaction.js\");\n";
        pageCode += "script.attribute(\"type\", \"text/javascript\");\n";
        pageCode += "script.type(at.femoweb.html.TagType.FULL);\n";
        pageCode += "}}";
        pageCode += "at.femoweb.html.Tag container = new at.femoweb.html.Tag(\"div\", body);";
        pageCode += "container.attribute(\"class\", \"container\");";

        //Test code
        pageCode += createTags(page, "container");

        pageCode += "result += html.render();";
        return pageCode + "return result;";
    }

    private String createPartial(Document document) {
        String pageCode = "String result = \"\";";
        Tag page = document.content();
        pageCode += "ClassLoader loader = null;";
        pageCode += "if(at.femoweb.xjs.modules.Session.is(\"_xjs_logical_unit\")) {";
        pageCode += "at.femoweb.xjs.structure.LogicalUnit unit = (at.femoweb.xjs.structure.LogicalUnit) at.femoweb.xjs.modules.Session.get(\"_xjs_logical_unit\");";
        pageCode += "if(unit.hasService(\"jres-service\")) {";
        pageCode += "at.femoweb.xjs.modules.JavaRessourceService jres = (at.femoweb.xjs.modules.JavaRessourceService) unit.getService(\"jres-service\");";
        pageCode += "loader = jres.getClassLoader();";
        pageCode += "}}";
        pageCode += "at.femoweb.html.Tag container = new at.femoweb.html.Tag(\"div\");";

        pageCode += createTags(page, "container");

        pageCode += "result += container.render();";
        return pageCode + "return result;";
    }

    private String createTags(Tag parent, String tag) {
        return createTags(parent, new int[]{0}, tag);
    }

    private String createTags(Tag parent, int[] counter, String tag) {
        String pageCode = "";
        Field field = null;
        try {
            field = Tag.class.getDeclaredField("attributes");
            field.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        for(Tag child : parent.children()) {
            if(child instanceof TextTag) {
                pageCode += tag + ".add(\"" + child.text() + "\");";
            } else {
                String name = "tag_" + counter[0];
                counter[0]++;
                if(tagDefinitions.containsKey(child.name())) {
                    pageCode += "at.femoweb.html.Tag " + name + " = null;";
                    pageCode += "try {";
                    pageCode += "if(loader != null) {at.femoweb.xjs.Server.log.debug(\"Creating dynamic tag from class\");";
                    pageCode += name + " = (at.femoweb.html.Tag) at.femoweb.xjs.modules.xjs.DynamicTag.createTag(loader.loadClass(\"" + tagDefinitions.get(child.name()).getName() + "\"));";
                    pageCode += tag + ".add(" + name + ");";
                    counter[0]++;
                    pageCode += "} else { at.femoweb.xjs.Server.log.warn(\"ClassLoader not found\");}} catch (ClassNotFoundException e) { at.femoweb.xjs.Server.log.warn(\"Could not load tag class\", e);}";
                } else {
                    pageCode += "at.femoweb.html.Tag " + name + " = new at.femoweb.html.Tag(\"" + child.name() + "\");";
                    pageCode += tag + ".add(" + name + ");";
                }
                try {
                    assert field != null;
                    HashMap<String, String> attributes = (HashMap<String, String>) field.get(child);
                    for(String key : attributes.keySet()) {
                        pageCode += name + ".attribute(\"" + key + "\", \"" + attributes.get(key) + "\");";
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if(child.children().size() > 0) {
                    pageCode += createTags(child, counter, name);
                }
                /*if(tagDefinitions.containsKey(child.name())) {
                    pageCode += "try {";
                    pageCode += "if(loader != null) {at.femoweb.xjs.Server.log.info(\"Creating dynamic tag from class\");";
                    pageCode += "at.femoweb.html.Tag tag_" + counter[0] + " = (at.femoweb.html.Tag) at.femoweb.xjs.modules.xjs.DynamicTag.createTag(loader.loadClass(\"" + tagDefinitions.get(child.name()).getName() + "\"), " + name + ");";
                    pageCode += tag + ".add(tag_" + counter[0] + ");";
                    pageCode += "for(int i = 0; i < " + name + ".children().size(); i++) {";
                    pageCode += "at.femoweb.html.Tag tag = " + name + ".get(i);";
                    pageCode += "tag_" + counter[0] + ".add(tag);";
                    pageCode += "}";
                    counter[0]++;
                    pageCode += "} else { at.femoweb.xjs.Server.log.warn(\"ClassLoader not found\");}} catch (ClassNotFoundException e) { at.femoweb.xjs.Server.log.warn(\"Could not load tag class\", e);}";
                } else {
                    pageCode += tag + ".add(" + name + ");";
                }*/
            }
        }
        return pageCode;
    }
}
