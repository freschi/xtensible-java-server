package at.femoweb.xjs.modules.security;

import at.femoweb.config.entries.Group;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.security.ACL;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.services.JavaCompilerService;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;

import java.lang.reflect.Method;
import java.util.Properties;

/**
 * Created by felix on 9/17/15.
 */
@Named("security-service")
@at.femoweb.xjs.annotations.Service
public class SecurityService extends BootableModule implements Service, Initable {

    private BootableLogicalUnit bootableLogicalUnit;

    @Override
    public Object put(Object... objects) {
        return null;
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        SecurityService securityService = new SecurityService();
        securityService.bootableLogicalUnit = logicalUnit;
        return securityService;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public void init(Properties properties) {
        if(bootableLogicalUnit != null) {
            JavaCompilerService compilerService = bootableLogicalUnit.getService(JavaCompilerService.class);
            Method[] acls = compilerService.getAllMethods(ACL.class);
            for (Method method : acls) {
                Server.log.info("Creating ACL Proxy for Class " + method.getDeclaringClass().getName());
            }
        }
    }
}
