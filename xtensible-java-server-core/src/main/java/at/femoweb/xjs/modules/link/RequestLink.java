package at.femoweb.xjs.modules.link;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 3/24/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@JResTarget(ElementType.TYPE)
@Target(ElementType.TYPE)
public @interface RequestLink {

    public enum Type {
        WEB_SOCKET
    }

    public String type();
    public String path();
    public Type connectionType() default Type.WEB_SOCKET;
    public boolean authenticate() default false;
    public String authModule() default "";
}
