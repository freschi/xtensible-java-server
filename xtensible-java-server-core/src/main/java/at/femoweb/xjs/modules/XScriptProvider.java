package at.femoweb.xjs.modules;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.Module;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by felix on 10/29/14.
 */
@Named("x-scripts")
@ContentProvider(regex = "\\/xjs\\/.*\\.js")
public class XScriptProvider extends Module implements Provider, Initable {

    @Override
    public void init(Properties properties) {
        File scripts = new File("data/xjs/");
        if(!scripts.exists()) {
            scripts.mkdirs();
        }
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        if(request.getRequestPath().equalsIgnoreCase("/xjs/xaction.js")) {
            response.setStatus(200);
            response.header("Content-Type", "text/javascript");
            try {
                response.setEntity(Files.readAllBytes(Paths.get("data/xjs/xaction.js")));
            } catch (IOException e) {
                response.setStatus(500);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(byteArrayOutputStream));
                response.setEntity(byteArrayOutputStream.toByteArray());
            }
        } else {
            response.setStatus(404);
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        return request.getRequestPath().equalsIgnoreCase("/xjs/xaction.js");
    }
}
