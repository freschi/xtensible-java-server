package at.femoweb.xjs.modules;

import at.femoweb.config.entries.Group;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Resource;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 1/6/15.
 */
@Service
@Named("resources")
public class Resources extends BootableModule implements at.femoweb.xjs.services.Service, Initable, Reloadable{

    private LogicalUnit logicalUnit;
    private HashMap<String, Object> resources;

    public Resources () {

    }

    public Resources (LogicalUnit logicalUnit, Properties properties) {
        this.logicalUnit = logicalUnit;
        this.resources = new HashMap<>();
    }

    @Override
    public void init(Properties properties) {
        if(logicalUnit != null) {
            if(logicalUnit.hasService("jres-service")) {
                logicalUnit.getService("jres-service").put(new AnnotationHandlerBundle<Resource>(Resource.class, new AnnotationHandler() {
                    @Override
                    public void foundType(Class annotation, Class type) {
                        Resource resource = (Resource) type.getAnnotation(annotation);
                        try {
                            Server.log.info("Found ressource " + resource.value() + "@" + type.getName());
                            resources.put(resource.value(), type.newInstance());
                        } catch (InstantiationException | IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void foundMethod(Class annotation, Method method) {

                    }

                    @Override
                    public void foundField(Class annotation, Field field) {

                    }
                }));
            }
        }
    }

    @Override
    public void reload() {
        if(logicalUnit != null) {
            if(logicalUnit.hasService("jres-service")) {
                logicalUnit.getService("jres-service").put(new AnnotationHandlerBundle<Resource>(Resource.class, new AnnotationHandler() {
                    @Override
                    public void foundType(Class annotation, Class type) {
                        Resource resource = (Resource) type.getAnnotation(annotation);
                        try {
                            resources.put(resource.value(), type.newInstance());
                        } catch (InstantiationException | IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void foundMethod(Class annotation, Method method) {

                    }

                    @Override
                    public void foundField(Class annotation, Field field) {

                    }
                }));
            }
            resources.clear();
        }
    }

    @Override
    public Object put(Object... objects) {
        if(resources.containsKey(objects[0])) {
            return resources.get(objects[0]);
        }
        return null;
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        Resources resources = new Resources();
        resources.logicalUnit = logicalUnit;
        resources.resources = new HashMap<>();
        return resources;
    }

    @Override
    public void logInfo() {
        Server.log.info("Resource service for " + logicalUnit.getName());
    }

    @Override
    public void setup() {

    }
}
