package at.femoweb.xjs.modules;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.lifecycle.Startable;
import at.femoweb.xjs.lifecycle.Stopable;
import at.femoweb.xjs.net.PendingRequest;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.ConnectionService;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.IntStream;

/**
 * Created by felix on 10/13/14.
 */
@Named("http")
@Service
public class HttpService extends Module implements Initable, Startable, Stopable, ConnectionService, Reloadable {

    private LogicalUnit unit;
    private HashMap<String, HttpService> modules;
    private Thread handler;
    private ArrayList<PendingRequest> pendingRequests;
    private ArrayList<Thread> pool;
    private int httpPort;
    private String httpIp;
    private ServerSocket serverSocket;

    private static final Logger httpLogger = LogManager.getLogger("Excessive-HTTP");


    public HttpService () {
        modules = new HashMap<String, HttpService>();
    }

    public HttpService(LogicalUnit logicalUnit, Properties properties) {
        this.unit = logicalUnit;
        pendingRequests = new ArrayList<PendingRequest>();
        for (String name : logicalUnit.getAliases()) {
            ((HttpService) Server.physicalUnit.getModule("http")).addModule(name, this);
        }
    }

    public void addModule(String name, Module module) {
        if(modules != null) {
            Server.log.debug("Added HTTP handler for " + name);
            modules.put(name, (HttpService) module);
        } else {
            Server.log.warn("It is not possible to add a http handler to to a logical unit");
        }
    }

    @Override
    public int getThreadCount() {
        return pool == null ? modules.values().stream().flatMapToInt(http -> IntStream.of(http.getThreadCount())).sum() : pool.size();
    }

    @Override
    public int getConnectionCount() {
        return 1;
    }

    @Override
    public void init(Properties properties) {
        if(modules != null) {
            Server.log.debug("HTTP Config");
            if(properties == null) {
                Server.log.warn("No http config set");
                return;
            }
            for (Object key : properties.keySet()) {
                Server.log.debug(key + " = " + properties.get(key));
            }
            Server.log.debug("Preparing HTTP Handler Thread");
            if(properties.containsKey("http-ip")) {
                handler = new HttpHandlerThread(httpPort = Integer.parseInt(properties.getProperty("http-port")), httpIp = properties.getProperty("http-ip"));
            } else {
                handler = new HttpHandlerThread(httpPort = Integer.parseInt(properties.getProperty("http-port")));
            }
        } else {
            Server.log.debug("Preparing HTTP Unit Handler Thread");
            handler = new PendingRequestHandlerThread(unit);
            pool = new ArrayList<Thread>();
            pool.add(new ThreadPoolWorkerThread(unit, 0));
        }
    }

    @Override
    public void start() {
        if(handler == null)
            return;
        if(modules != null)
            Server.log.debug("HTTP Handlers: " + modules.toString());
        if(handler.getState() != Thread.State.NEW) {
            handler = new PendingRequestHandlerThread(unit);
        }
        handler.start();
        Server.log.info("Starting Handler Thread");
        if(pool != null) {
            Server.log.info("Starting Handling Thread Pool");
            for(Thread t : pool) {
                t.start();
            }
        }
    }

    @Override
    public void shutdown(Properties properties) {
        if(pool != null) {
            Server.log.info("Stopping Handling Thread Pool");
            for(Thread t : pool) {
                t.interrupt();
            }
        }
        Server.log.info("Shutting down HTTP Service");
        if(serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        handler.interrupt();
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }

    @Override
    public void reload() {
        Server.log.info("Reloading HTTP Service");
        if(modules != null) {
            Server.log.debug("HTTP Config");
            Server.log.debug("Preparing HTTP Handler Thread");
            handler = new HttpHandlerThread(httpPort);
        } else {
            Server.log.debug("Preparing HTTP Unit Handler Thread");
            handler = new PendingRequestHandlerThread(unit);
            pool = new ArrayList<Thread>();
            pool.add(new ThreadPoolWorkerThread(unit, 0));
        }
    }

    public synchronized PendingRequest next() {
        synchronized (pendingRequests) {
            for (PendingRequest pendingRequest : pendingRequests) {
                if (pendingRequest.getState() == PendingRequest.State.PENDING) {
                    return pendingRequest;
                }
            }
        }
        return null;
    }

    public void handle(HttpRequest request, HttpResponse response) {
        unit.handle(request, response);
        if (response.header("Content-Type") == null) {
            response.header("Content-Type", "text/html; charset=UTF-8");
        } else {
            String contentType = response.header("Content-Type");
            String[] types = contentType.split(";");
            boolean set = false;
            for (String type : types) {
                if (type.trim().startsWith("charset")) {
                    set = true;
                    break;
                }
            }
            if (!set) {
                contentType += "; charset=UTF-8";
            }
            response.header("Content-Type", contentType);
        }
        response.header("Server", "XJS/0.1a");
        response.header("X-Powered-By", this.getClass().getName());
        if (response.header("Pragma") == null) {
            response.header("Pragma", "no-cache");
        }
        if (response.header("Cache-Control") == null) {
            response.header("Cache-Control", "no-cache, no-store");
        }
        if (response.header("Expires") == null) {
            response.header("Expires", "-1");
        }
        response.header("Date", getServerTime());
    }

    protected String getServerTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }

    public void addPending(PendingRequest pendingRequest) {
        synchronized (pendingRequests) {        //ONE FUCKING S, ONE FUCKING S
            pendingRequests.add(pendingRequest);
        }
    }

    public class HttpHandlerThread extends Thread {

        private int port;
        private String ip;

        public HttpHandlerThread (int port) {
            this.port = port;
            setName("HTTP-" + port);
        }

        public HttpHandlerThread (int port, String ip) {
            this.port = port;
            this.ip = ip;
            setName("HTTP-" + port);
        }

        public void run () {
            try {
                if(ip != null) {
                    serverSocket = new ServerSocket(port, 100, InetAddress.getByName(ip));
                } else {
                    serverSocket = new ServerSocket(port, 100);
                }
                Server.log.info("HTTP Service active on port " + port);
                while (true) {
                    Socket socket = serverSocket.accept();
                    Server.log.debug("Incoming connection from " + socket.getInetAddress().getCanonicalHostName());
                    //BufferedReader din = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    try {
                        HttpRequest request = HttpRequest.readFromStream(socket.getInputStream());
                        httpLogger.info(request.getRequestLine());
                        if (request.header("Host") == null && request.header("host") == null) {
                            Server.log.warn("Host not set in incoming Request from " + socket.getInetAddress().getCanonicalHostName());
                            HttpResponse response = HttpResponse.getDefaultInstance(null);
                            response.setStatus(400);
                            response.setStatusLine("Bad Request");
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            PrintStream ps = new PrintStream(byteArrayOutputStream);
                            ps.print("Host not set. No resolving possible");
                            response.setEntity(byteArrayOutputStream.toByteArray());
                            response.header("Server", "XJS/0.1");
                            if (Server.physicalUnit.getModule("error-pages") != null) {
                                ((Provider) Server.physicalUnit.getModule("error-pages")).handle(request, response);
                            }
                            response.writeToStream(socket.getOutputStream());
                            socket.close();
                            httpLogger.warn("No Header <Host> found");
                            continue;
                        } else {
                            String host = request.header("Host") != null ? request.header("Host") : request.header("host");
                            if (host.contains(":"))
                                host = host.substring(0, host.indexOf(":"));
                            if (modules.containsKey(host)) {
                                Server.log.debug("Incoming Request from " + socket.getInetAddress().getCanonicalHostName() + ": " + request.getRequestLine());
                                PendingRequest pendingRequest = new PendingRequest(request, HttpResponse.getDefaultInstance(request), socket);
                                pendingRequest.setState(PendingRequest.State.UNKNOWN);
                                HttpService httpService = modules.get(host);
                                httpService.addPending(pendingRequest);
                            } else {
                                Server.log.warn("Unknown Host " + host + " in incoming Request from " + socket.getInetAddress().getCanonicalHostName());
                                HttpResponse response = HttpResponse.getDefaultInstance(null);
                                response.setStatus(400);
                                response.setStatusLine("Bad Request");
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                PrintStream ps = new PrintStream(byteArrayOutputStream);
                                ps.print("Unknown Host: " + host);
                                response.setEntity(byteArrayOutputStream.toByteArray());
                                response.header("Server", "XJS/0.1");
                                if (Server.physicalUnit.getModule("error-pages") != null) {
                                    ((Provider) Server.physicalUnit.getModule("error-pages")).handle(request, response);
                                }
                                httpLogger.warn("No host for " + request.header("Host") + "@" + request.getRequestLine());
                                response.writeToStream(socket.getOutputStream());
                                socket.close();
                                continue;
                            }
                        }
                    } catch (Throwable t) {
                        Server.log.warn("Exception while parsing Request", t);
                        httpLogger.warn("Bad Request");
                        HttpResponse response = HttpResponse.getDefaultInstance(null);
                        response.setStatus(400);
                        response.setStatusLine("Bad Request");
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        t.printStackTrace(new PrintStream(byteArrayOutputStream));
                        response.setEntity(byteArrayOutputStream.toByteArray());
                        response.header("Server", "XJS/0.1");
                        response.writeToStream(socket.getOutputStream());
                        socket.close();
                        continue;
                    }
                }
            } catch (SocketException e) {
                Server.log.warn("Error while listening on HTTP Port " + port);
            } catch (IOException e) {
                Server.log.error("Could not open HTTP Port " + port);
            } catch (Throwable t) {
                Server.log.error("Unknown error in HTTP Service", t);
            }
        }
    }

    public class PendingRequestHandlerThread extends Thread {

        public PendingRequestHandlerThread(LogicalUnit logicalUnit) {
            this.setName("HTTP-" + logicalUnit.getName());
        }

        public void run() {
            while(true) {
                try {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        return;
                    }
                    if (pendingRequests.size() > 0)
                        Server.log.debug("Iterating over Pending Requests: " + pendingRequests.size());
                    else
                        continue;
                    synchronized (pendingRequests) {
                        Iterator<PendingRequest> iterator = pendingRequests.iterator();
                        while (iterator.hasNext()) {
                            PendingRequest pendingRequest = iterator.next();
                            if (pendingRequest == null) {
                                iterator.remove();
                                continue;
                            }
                            try {
                                if (pendingRequest.getState() == PendingRequest.State.FINISHED) {
                                    pendingRequest.getResponse().writeToStream(pendingRequest.getSocket().getOutputStream());
                                    httpLogger.info(pendingRequest.getRequest().getRequestLine() + " -> " + pendingRequest.getResponse().getResponseLine() + " [" + pendingRequest.getResponse().header("Content-Length") + " byte(s)]");
                                    pendingRequest.getSocket().close();
                                    Server.log.debug("Sent " + pendingRequest.getResponse().getResponseLine());
                                    iterator.remove();
                                } else if (pendingRequest.getState() == PendingRequest.State.UNKNOWN) {
                                    pendingRequest.setState(PendingRequest.State.PENDING);
                                }
                            } catch (SocketException t) {
                                iterator.remove();
                            } catch (ConcurrentModificationException e) {
                                Server.log.info("Handling Queue overrun");
                                httpLogger.warn("Error @" + pendingRequest.getRequest().getRequestLine(), e);
                            } catch (Throwable t) {
                                Server.log.warn("Exception while handling Request", t);
                                httpLogger.warn("Error @" + pendingRequest.getRequest().getRequestLine(), t);
                                HttpResponse response = pendingRequest.getResponse();
                                response.setStatus(500);
                                response.setStatusLine("Internal Server Error");
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                t.printStackTrace(new PrintStream(byteArrayOutputStream));
                                response.setEntity(byteArrayOutputStream.toByteArray());
                                response.header("Server", "XJS/0.1");
                                try {
                                    response.writeToStream(pendingRequest.getSocket().getOutputStream());
                                    pendingRequest.getSocket().close();
                                } catch (Throwable t1) {
                                    pendingRequest.setState(PendingRequest.State.FINISHED);
                                    Server.log.warn("Error while replying to response: " + t1.getMessage());
                                }
                            }
                        }
                    }
                } catch (ConcurrentModificationException e){
                    Server.log.warn("Too many requests on pending Requests. Relaunching loop");
                }
            }
        }
    }

    public class ThreadPoolWorkerThread extends Thread {

        public ThreadPoolWorkerThread (LogicalUnit logicalUnit, int thread) {
            this.setName("HTTP-" + logicalUnit.getName() + "-" + thread);
        }

        public void run () {
            while (true) {
                PendingRequest pendingRequest = next();
                if(pendingRequest != null) {
                    try {
                        handle(pendingRequest.getRequest(), pendingRequest.getResponse());
                    } catch (Throwable t) {
                        pendingRequest.getResponse().setStatus(500);
                        pendingRequest.getResponse().setStatusLine("Internal Server Error");
                        Server.notifyUncaughtException(Thread.currentThread(), t, false);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        t.printStackTrace(new PrintStream(byteArrayOutputStream));
                        pendingRequest.getResponse().setEntity(byteArrayOutputStream.toByteArray());
                        pendingRequest.getResponse().header("Server", "XJS/0.1");
                        httpLogger.warn("Error @" + pendingRequest.getRequest().getRequestLine(), t);
                        try {
                            Server.log.debug("Sending out response to " + pendingRequest.getSocket().getInetAddress().getCanonicalHostName());
                            pendingRequest.getResponse().writeToStream(pendingRequest.getSocket().getOutputStream());
                            Server.log.debug("Closing connection to " + pendingRequest.getSocket().getInetAddress().getCanonicalHostName());
                            pendingRequest.getSocket().close();
                        } catch (Throwable t1) {
                            Server.log.warn("Error while replying to response", t1);
                            httpLogger.warn("Error @" + pendingRequest.getRequest().getRequestLine(), t1);
                        }
                    }
                    pendingRequest.setState(PendingRequest.State.FINISHED);
                } else {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        }
    }
}
