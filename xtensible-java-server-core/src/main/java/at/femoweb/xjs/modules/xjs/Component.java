package at.femoweb.xjs.modules.xjs;

import java.util.Properties;

/**
 * Created by felix on 5/15/15.
 */
public interface Component {

    void onClick(String action);
    void onClick(String action, Properties extra);
    void onKeyDown(String action);
    void onKeyDown(String action, Properties extra);

    static String convertProperties(Properties properties) {
        String result = "{";
        for (Object key : properties.keySet()) {
            result += String.valueOf(key) + ":";
            result += "'" + String.valueOf(properties.get(key)) + "'" + ", ";
        }
        result = result.substring(0, result.length() - 2);
        return result + "}";
    }
}
