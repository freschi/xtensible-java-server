package at.femoweb.xjs.modules.http;

import at.femoweb.config.Article;
import at.femoweb.config.Config;
import at.femoweb.config.Paragraph;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.lifecycle.Startable;
import at.femoweb.xjs.lifecycle.Stopable;
import at.femoweb.xjs.services.ConnectionService;
import at.femoweb.xjs.services.StreamHandoverHandler;
import at.femoweb.xjs.services.StreamHandoverService;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 3/18/15.
 */
@Service
@Named("http")
public class BootCompatibleHttpService extends BootableModule implements at.femoweb.xjs.services.Service, Initable, Startable, Stopable, Reloadable, StreamHandoverService, ConnectionService {

    private HashMap<String, HttpHandler> handlers;
    private HttpHandler handler;
    private LogicalUnit logicalUnit;
    private String ip;

    public void setup() {
        handlers = new HashMap<>();
        Article handlers = Config.openCatalog("global").openArticle("handlers");
        Paragraph http = handlers.openParagraph("http");
        List h = http.openEntry().getList("handlers");
        for (int i = 0; i < h.count(); i++) {
            Group config = h.getGroup(i);
            HttpHandler handler;
            if(config.has("ip")) {
                handler = new HttpHandler(config.getString("name"), config.getInt("port"), config.getBoolean("ssl"), config.getInt("threads"), config.getString("ip"));
            } else {
                handler = new HttpHandler(config.getString("name"), config.getInt("port"), config.getBoolean("ssl"), config.getInt("threads"));
            }
            this.handlers.put(handler.getName(), handler);
        }
    }

    @Override
    public void init(Properties properties) {
        if(handlers != null) {
            for (HttpHandler httpHandler : handlers.values()) {
                httpHandler.init(properties);
            }
        }
    }

    @Override
    public void reload() {
        if(handlers != null) {
            for (HttpHandler httpHandler : handlers.values()) {
                httpHandler.reload();
            }
        }
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }

    @Override
    public void start() {
        if(handlers != null) {
            for (HttpHandler httpHandler : handlers.values()) {
                httpHandler.start();
            }
        }
    }

    @Override
    public void shutdown(Properties properties) {
        if(handlers != null) {
            for (HttpHandler httpHandler : handlers.values()) {
                httpHandler.shutdown(properties);
            }
        }
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        BootCompatibleHttpService httpService = new BootCompatibleHttpService();
        httpService.logicalUnit = logicalUnit;
        if(settings.has("handler")) {
            if(handlers.containsKey(settings.getString("handler"))) {
                HttpHandler handler = handlers.get(settings.getString("handler"));
                httpService.handler = handler;
                handler.registerUnit(logicalUnit);
            }
        }
        return httpService;
    }

    @Override
    public void logInfo() {
        Server.log.info("Handler " + handler.getName() + " on port " + handler.getPort() + " for unit " + logicalUnit.getName());
    }

    @Override
    public boolean registerHandler(StreamHandoverHandler handler) {
        this.handler.registerHandoverHandler(handler, logicalUnit);
        return true;
    }

    @Override
    public int getThreadCount() {
        return 0;
    }

    @Override
    public int getConnectionCount() {
        return 0;
    }
}
