package at.femoweb.xjs.modules.xjs.strategies;

import at.femoweb.html.Document;

/**
 * Created by felix on 5/17/15.
 */
public interface NamingStrategy {

    String findName(String path, Document document);
}
