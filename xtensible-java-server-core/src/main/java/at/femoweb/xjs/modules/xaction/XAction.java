package at.femoweb.xjs.modules.xaction;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 10/31/14.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@JResTarget(ElementType.METHOD)
public @interface XAction {
    public String name();
    public boolean requireFormEncoded() default false;
}
