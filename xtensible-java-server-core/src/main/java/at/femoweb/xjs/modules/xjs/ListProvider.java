package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;

import java.util.List;

/**
 * Created by felix on 1/6/15.
 */
public interface ListProvider {

    public abstract List<ListItem> createList();

    public static final class ListItem {
        private Tag tag;
        private String value;

        public Tag getTag() {
            return tag;
        }

        public void setTag(Tag tag) {
            this.tag = tag;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
