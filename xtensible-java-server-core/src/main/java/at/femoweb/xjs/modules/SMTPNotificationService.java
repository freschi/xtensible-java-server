package at.femoweb.xjs.modules;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.services.NotificationService;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Created by felix on 10/16/14.
 */
@Service
@Named("mail-notifier")
public class SMTPNotificationService extends Module implements NotificationService, Initable {

    private boolean notifyAll = false;

    @Override
    public void notifyServerStarted() {
        if(!notifyAll)
            return;
        MimeMessage message = getMessage();
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(bos);
            ps.println("<h1>Status Report - " + Server.physicalUnit.getName() + "</h1>");
            ps.println("<span style=\"color:gray\">Startup Successful</span><br />");
            ps.println("<hr style=\"height: 0.5px\" />");
            ps.println("<h2>Loaded Modules</h2>");
            ps.println("<ul>");
            for(Module module : Server.physicalUnit.getModules().values()) {
                ps.print("<li>");
                ps.print("<strong>" + module.getName() + "</strong> - ");
                ps.print(module.getClass().getCanonicalName());
                ps.println("</li>");
            }
            ps.println("</ul>");
            ps.println("<hr style=\"height:0.5px\" />");
            ps.println("<h2>Logical Units</h2>");
            for (Object object : Server.physicalUnit.getLogicalUnits().stream().distinct().collect(Collectors.toList())) {
                LogicalUnit logicalUnit = (LogicalUnit) object;
                ps.println("<h3>" + logicalUnit.getName() + "</h3>");
                ps.println("<h4>Aliases</h4>");
                ps.println("<ul>");
                for(String alias : logicalUnit.getAliases()) {
                    ps.println("<li><a href=\"http://" + alias + ":8080\">" + alias + "</a></li>");
                }
                ps.println("</ul>");
                ps.println("<h4>Services</h4>");
                ps.println("<ul>");
                for(Module service : logicalUnit.getServices()) {
                    ps.print("<li>");
                    ps.print(service.getName());
                    ps.println("</li>");
                }
                ps.println("</ul>");
                ps.println("<h4>Content Providers</h4>");
                ps.println("<ul>");
                for(Module service : logicalUnit.getContentProviders()) {
                    ps.print("<li>");
                    ps.print(service.getName());
                    ps.println("</li>");
                }
                ps.println("</ul>");
                ps.println("<hr style=\"height:0.5px\" />");
            }
            ps.println("<h2>System Properties</h2>");
            ps.println("<ul>");
            for(Object key : System.getProperties().keySet()) {
                ps.println("<li><strong>" + key + "</strong> - " + System.getProperty((String) key) + "</li>");
            }
            ps.println("</ul>");
            message.setText(bos.toString(), "utf-8", "html");
            message.setSubject("[INFO] Automated Server Notification - " + Server.physicalUnit.getName());
            Transport.send(message);
            Server.log.debug("Sent E-Mail to Notifier");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void notifyUncaughtException(Thread thread, Throwable throwable) {
        MimeMessage message = getMessage();
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(bos);
            ps.println("<h1>Status Report - " + Server.physicalUnit.getName() + "</h1>");
            ps.println("<span style=\"color:gray\">Uncaught Exception in Thread " + thread.getName() + "</span><br />");
            ps.println("<hr style=\"height: 0.5px\" />");
            throwable.printStackTrace(ps);
            ps.println("<hr style=\"height: 0.5px\" />");
            message.setText(bos.toString(), "utf-8", "html");
            message.setSubject("[WARN] Automated Server Notification - " + Server.physicalUnit.getName());
            Transport.send(message);
            Server.log.debug("Sent E-Mail to Notifier");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void notifyServerStopped() {
        if(!notifyAll)
            return;
        MimeMessage message = getMessage();
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(bos);
            ps.println("<h1>Status Report - " + Server.physicalUnit.getName() + "</h1>");
            ps.println("<span style=\"color:gray\">Server Shutdown</span><br />");
            ps.println("<hr style=\"height: 0.5px\" />");
            message.setText(bos.toString(), "utf-8", "html");
            message.setSubject("[INFO] Automated Server Notification - " + Server.physicalUnit.getName());
            Transport.send(message);
            Server.log.debug("Sent E-Mail to Notifier");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private MimeMessage getMessage() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "alfa3026.alfahosting-server.de");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        javax.mail.Session session = javax.mail.Session.getDefaultInstance(props,
                new Authenticator() {
                    @Override
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication("web1448p5", "MpZsOZSr9RO5");
                    }
                });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setSentDate(new Date());
            message.setFrom(new InternetAddress("noreply@femoweb.at"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("felix.resch@femoweb.at"));
            return message;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void init(Properties properties) {
        if(properties == null)
            return;
        Server.log.debug("Notification Configuration");
        for (Object key : properties.keySet()) {
            Server.log.debug(key + " = " + properties.get(key));
        }
        if(properties.containsKey("notify-all") && properties.getProperty("notify-all").equalsIgnoreCase("true")) {
            notifyAll = true;
            Server.log.info("Notifying admin about everything");
        }
        Server.addNotificationService(this);
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }
}
