package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;

/**
 * Created by felix on 10/28/14.
 */
@TagReplacerInfo(tag = "xjs:link")
public class LinkReplacer implements TagReplacer {
    @Override
    public boolean replaces(String tagName) {
        if(tagName != null)
            return tagName.equalsIgnoreCase("xjs:link");
        return false;
    }

    @Override
    public Tag replace(Tag tag) {
        Tag n = new Tag("a");
        if(tag.attribute("target") != null) {
            n.attribute("href", tag.attribute("target"));
        } else {
            n.attribute("href", "#");
        }
        if(tag.attribute("open") != null) {
            String open = tag.attribute("open");
            if(open.equalsIgnoreCase("new"))
                n.attribute("target", "_blank");
            else if (open.equalsIgnoreCase("this"))
                n.attribute("target", "_self");
            else if (open.equalsIgnoreCase("super"))
                n.attribute("target", "_parent");
            else if (open.equalsIgnoreCase("window"))
                n.attribute("target", "top");
            else
                n.attribute("target", open);
        } else {
            n.attribute("target", "_self");
        }
        if(tag.attribute("style") != null) {
            n.attribute("style", tag.attribute("style"));
        }
        return n;
    }
}
