package at.femoweb.xjs.modules.ws;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.link.*;
import at.femoweb.xjs.services.SessionService;
import at.femoweb.xjs.structure.LogicalUnit;
import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by felix on 3/22/15.
 */
public class WebSocketHandlingThread extends Thread {

    private static final Logger log = LogManager.getLogger("WebSocket");

    private LogicalUnit logicalUnit;
    private Socket socket;
    private HttpRequest request;
    private static int number = 0;
    private boolean closing;
    private boolean pinging;
    private ArrayList<Class<?>> links;
    private HashMap<String, Object> linkMap;
    private UnifiedUser unifiedUser;

    public WebSocketHandlingThread(LogicalUnit logicalUnit, Socket socket, HttpRequest request, ArrayList<Class<?>> links, UnifiedUser unifiedUser) {
        this.unifiedUser = unifiedUser;
        this.logicalUnit = logicalUnit;
        this.socket = socket;
        this.request = request;
        setName("ws-" + logicalUnit.getName() + "-" + number);
        this.linkMap = new HashMap<>();
        this.links = links;
        for(Class<?> c : links) {
            if(c.getAnnotation(RequestLink.class).path().equals(request.getRequestPath())) {
                try {
                    linkMap.put(c.getAnnotation(RequestLink.class).type(), c.newInstance());
                } catch (InstantiationException e) {
                    Server.log.warn("Could not instantiate class", e);
                } catch (IllegalAccessException e) {
                    Server.log.warn("Could not instantiate class", e);
                }
            }
        }
    }

    public void run() {
        log.info("WebSocket listener started");
        if(logicalUnit.hasService("session")) {
            SessionService sessionService = (SessionService) logicalUnit.getService("session");
            sessionService.setupSession(request, HttpResponse.getDefaultInstance(request));
            if(unifiedUser != null) {
                Session.set("_xjs_user", unifiedUser);
            }
        }
        try {
            InputStream inputStream = socket.getInputStream();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("session", false);
            jsonObject.addProperty("version", 0.1);
            jsonObject.addProperty("beKind", true);
            jsonObject.addProperty("path", Server.physicalUnit.getName());
            jsonObject.addProperty("type", "init");
            sendMessage(1, jsonObject.toString());
            int flag;
            int retries = 3;
            while (128 == ((flag = inputStream.read()) & 128)) {
                int opcode = (flag & 15);
                int length = inputStream.read() - 128;
                if(length == 126) {
                    ByteBuffer buffer = ByteBuffer.allocate(2);
                    for(int i = 0; i < 2; i++) {
                        buffer.put(i, (byte) inputStream.read());
                    }
                    length = buffer.getShort();
                } else if (length == 127) {
                    ByteBuffer buffer = ByteBuffer.allocate(4);
                    for(int i = 0; i < 2; i++) {
                        buffer.put(i, (byte) inputStream.read());
                    }
                    length = buffer.getInt();
                }
                log.debug("Expecting opcode " + opcode + " package with " + length + " bytes");
                byte[] key = new byte[4];
                int read = inputStream.read(key);
                if(read < 4) {
                    Server.log.warn("Illegal length read");
                    inputStream.close();
                    socket.close();
                    return;
                }
                byte[] buffer = new byte[length];
                read = inputStream.read(buffer, 0, length);
                if(read < length) {
                    Server.log.warn("Read " + read + " expected " + length + " bytes");
                    inputStream.close();
                    socket.close();
                    return;
                }
                byte[] message = new byte[length];
                for(int i = 0; i < buffer.length; i++) {
                    message[i] = (byte) (buffer[i] ^ key[i % 4]);
                }
                if(opcode == 8) {
                    log.info("Client is requesting a connection closure");
                    if(!closing) {
                        sendMessage(8, new String(message));
                    }
                    //TODO notify closed
                    break;
                } else if (opcode == 9) {
                    log.debug("Received ping from client");
                    sendMessage(10, new String(message));
                } else if (opcode == 10) {
                    if(pinging) {
                        //TODO notify ping succesfull
                        log.debug("Ping succesfull");
                    }
                }
                if(opcode == 1) { //Text
                    log.debug("Received[" + message.length + "]: " + new String(message, "UTF-8"));
                    try {
                        JsonElement element = new JsonParser().parse(new String(message));
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("state", "okay");
                        jsonObject.addProperty("action_performed", handleMessage(element.getAsJsonObject()));
                        jsonObject.add("modules", new JsonArray());
                        sendMessage(1, jsonObject.toString());
                    } catch (JsonParseException e) {
                        jsonObject = new JsonObject();
                        jsonObject.addProperty("state", "malformed");
                        jsonObject.addProperty("action_performed", "none");
                        jsonObject.add("modules", new JsonArray());
                        jsonObject.addProperty("retries", retries--);
                        sendMessage(1, jsonObject.toString());
                    }
                    ping();
                }
                if(retries <= 0) {
                    close();
                }
            }
            socket.close();
        } catch (IOException e) {
            Server.log.warn("Error while reading from WebSocket", e);
        }

    }

    public String handleMessage(JsonObject object) {
        Object handler = linkMap.get(object.get("type").getAsString());
        for(Method method : handler.getClass().getDeclaredMethods()) {
            if(method.isAnnotationPresent(LinkHandler.class)) {
                Object[] values = new Object[method.getParameterCount()];
                boolean execute = true;
                for(int i = 0; i < method.getParameterCount(); i++) {
                    Parameter parameter = method.getParameters()[i];
                    if(parameter.isAnnotationPresent(LinkParam.class)) {
                        LinkParam param = parameter.getAnnotation(LinkParam.class);
                        if (object.has(param.name())) {
                            values[i] = object.get(param.name());
                        } else {
                            if (param.required()) {
                                execute = false;
                                break;
                            }
                        }
                    } else if (parameter.isAnnotationPresent(LinkConnection.class)) {
                        values[i] = new WebSocketLink(this);
                    } else if (parameter.isAnnotationPresent(LinkUnit.class)) {
                        values[i] = logicalUnit;
                    } else {
                        values[i] = null;
                    }
                }
                if(!execute)
                    continue;
                try {
                    return (String) method.invoke(handler, values);
                } catch (IllegalAccessException e) {
                    Server.log.warn("Could not execute link handler", e);
                } catch (InvocationTargetException e) {
                    Server.log.warn("Could not execute link handler", e);
                }
            }
        }
        return null;
    }

    public void sendMessage(int opcode, String message) {
        log.debug("Sending \"" + message + "\" to " + socket.getRemoteSocketAddress());
        ByteArrayOutputStream encoded = new ByteArrayOutputStream();
        if(opcode >= 16) {
            throw new RuntimeException("Opcode is only 4 bit, no values over 15 allowed");
        }
        encoded.write(128 | opcode);
        byte[] msg = message.getBytes();
        if(msg.length <= 125) {
            encoded.write(msg.length);
        } else if(msg.length <= Short.MAX_VALUE) {
            encoded.write(126);
            ByteBuffer byteBuffer = ByteBuffer.allocate(2);
            byteBuffer.putShort((short) msg.length);
            encoded.write(byteBuffer.array(), 0, 2);
        } else {
            encoded.write(127);
            ByteBuffer byteBuffer = ByteBuffer.allocate(4);
            byteBuffer.putInt(msg.length);
            encoded.write(byteBuffer.array(), 0, 4);
        }
        encoded.write(msg, 0, msg.length);
        try {
            encoded.writeTo(socket.getOutputStream());
        } catch (IOException e) {
            Server.log.warn("Could not write to client", e);
        }
    }

    public void ping() {
        pinging = true;
        sendMessage(9, "");
    }

    public void close() {
        closing = true;
        sendMessage(8, "User requested");
    }

    @Override
    public void interrupt() {
        try {
            socket.close();
        } catch (IOException e) {
            Server.log.info("Could not shutdown WebSocket");
        }
        super.interrupt();
    }
}
