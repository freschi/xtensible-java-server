package at.femoweb.xjs.modules.xjs;

/**
 * Created by felix on 5/15/15.
 */
public interface TableDataSource {

    boolean nextRow() throws Throwable;
    boolean first() throws Throwable;
    void init() throws Throwable;

    <T> T getColumn(Class<T> type, String column) throws Throwable;
}
