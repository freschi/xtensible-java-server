package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;

/**
 * Created by felix on 10/28/14.
 */
public interface TagReplacer {

    public abstract boolean replaces(String tagName);
    public abstract Tag replace(Tag tag);
}
