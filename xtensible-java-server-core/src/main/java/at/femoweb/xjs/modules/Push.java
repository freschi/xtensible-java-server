package at.femoweb.xjs.modules;

import at.femoweb.xjs.modules.xaction.Updates;

import java.util.HashMap;

/**
 * Created by felix on 11/3/14.
 */
public class Push extends Updates {

    private static HashMap<String, Push> push;

    private String key;

    private Push(String key) {
        this.key = key;
    }

    public static Push get() {
        String key = (String) Session.get("session.key");
        if(push == null) {
            push = new HashMap<String, Push>();
        }
        if(push.containsKey(key)) {
            return push.get(key);
        } else {
            Push p = new Push(key);
            push.put(key, p);
            return p;
        }
    }

    public void clear () {
        super.clear();
    }
}
