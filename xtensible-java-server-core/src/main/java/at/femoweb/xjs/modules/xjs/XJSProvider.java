package at.femoweb.xjs.modules.xjs;

import at.femoweb.config.entries.Group;
import at.femoweb.html.Tag;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;

import java.io.File;

/**
 * Created by felix on 5/11/15.
 */
@Named("xjs-provider")
@ContentProvider(regex = ".*")
public class XJSProvider extends BootableModule implements Provider {

    private BootableLogicalUnit logicalUnit;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        XJSProvider provider = new XJSProvider();
        provider.logicalUnit = logicalUnit;
        return provider;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        XJSFileProviderService service = (XJSFileProviderService) logicalUnit.getService("xjs-file-provider-service");
        Object object = service.put(createDocumentPath(request));
        if(object instanceof Tag) {
            response.setEntity(((Tag) object).render());
            response.setStatus(200);
        } else {
            response.setEntity("Did not work properly");
            response.setStatus(500);
        }
    }

    private String createDocumentPath(HttpRequest request) {
        File file = new File(logicalUnit.getRootDir(), "htdocs/" + request.getRequestPath());
        if(file.isDirectory()) {
            return request.getRequestPath().endsWith("/") ? request.getRequestPath() + "index.xjs" : request.getRequestPath() + "/index.xjs";
        } else {
            return request.getRequestPath();
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        File file = new File(logicalUnit.getRootDir(), "htdocs/" + createDocumentPath(request));
        return file.exists() && file.getName().endsWith(".xjs");
    }
}
