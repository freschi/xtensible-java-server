package at.femoweb.xjs.modules.link;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 3/24/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@JResTarget(ElementType.METHOD)
@Target(ElementType.METHOD)
public @interface LinkHandler {

}
