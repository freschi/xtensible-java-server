package at.femoweb.xjs.modules.http.rest;

import at.femoweb.http.HttpRequest;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;

/**
 * Created by felix on 9/7/15.
 */
public interface RestOperation {

    default JsonElement readJsonData(HttpRequest request) throws IOException {
        int length = Integer.parseInt(request.header("Content-Length"));
        byte[] buffer = new byte[length];
        request.getEntity().read(buffer, 0, length);
        String json = new String(buffer);
        return new JsonParser().parse(json);
    }
}
