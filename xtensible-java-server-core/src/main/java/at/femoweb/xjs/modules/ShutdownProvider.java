package at.femoweb.xjs.modules;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.Module;

/**
 * Created by felix on 11/13/14.
 */
@ContentProvider(regex = "\\/xjs\\/control\\/.*")
@Named("shutdown-provider")
public class ShutdownProvider extends Module implements Provider {


    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        String lockString = request.header("XJS-Lock-String");
        if(lockString.equals(Server.getLockString())) {
            response.setStatus(200);
        } else {
            response.setStatus(401);
        }
        response.setEntity(Server.getStartupTime() + "");
        response.header("Content-Length", ("" + Server.getStartupTime()).length() + "");
        Server.log.info("Server was requested to shut down. Preparing for shutdown.");
        new Thread() {

            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Server.stop();
            }

        }.start();
    }

    @Override
    public boolean handles(HttpRequest request) {
        return request.getRequestPath().startsWith("/xjs/control/") && request.header("XJS-Lock-String") != null && request.header("User-Agent") != null && request.header("User-Agent").startsWith("XJS/");

    }
}
