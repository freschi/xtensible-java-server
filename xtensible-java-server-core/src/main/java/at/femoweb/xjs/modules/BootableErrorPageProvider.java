package at.femoweb.xjs.modules;

import at.femoweb.config.Config;
import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.ContentProviderType;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.io.*;

/**
 * Created by felix on 3/19/15.
 */
@ContentProvider(regex = ".*", type = ContentProviderType.FALLBACK)
@Named("error-pages")
public class BootableErrorPageProvider extends BootableModule implements Provider {

    private boolean useGlobal;
    private boolean overrideGlobal;
    private boolean global;
    private File errorRoot;
    private File defaultFile;
    private LogicalUnit logicalUnit;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        BootableErrorPageProvider errorPageProvider = new BootableErrorPageProvider();
        errorPageProvider.useGlobal = settings.getBoolean("useGlobal");
        errorPageProvider.global = false;
        errorPageProvider.overrideGlobal = settings.getBoolean("overrideGlobal");
        errorPageProvider.errorRoot = new File(logicalUnit.getRootDir(), settings.getString("errorPagesDir"));
        errorPageProvider.defaultFile = new File(errorPageProvider.errorRoot, settings.getString("defaultFile"));
        errorPageProvider.logicalUnit = logicalUnit;
        return errorPageProvider;
    }

    @Override
    public void logInfo() {
        Server.log.info("Error Page provider for unit " + logicalUnit.getName());
    }

    @Override
    public void setup() {
        global = true;
        Group settings = Config.openCatalog("global").openArticle("handlers").openParagraph("errors").openEntry();
        errorRoot = new File(settings.getString("errorPagesDir"));
        defaultFile = new File(errorRoot, settings.getString("defaultFile"));
        overrideGlobal = false;

    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        if(response.getStatus() == 0 && response.getStatusLine() == null) {
            response.setStatus(500);
            response.setStatusLine("Internal Server Error");
            response.setEntity("Unknown internal error happened!");
        }
        File page = new File(errorRoot, response.getStatus() + ".xjs");
        if(!page.exists()) {
            if(!global) {
                if (overrideGlobal) {
                    page = defaultFile;
                    if (!page.exists()) {
                        callGlobal(request, response);
                        return;
                    }
                } else if (useGlobal) {
                    callGlobal(request, response);
                    return;
                } else {
                    return;
                }
            } else {
                page = defaultFile;
                if (!page.exists()) {
                    callGlobal(request, response);
                    return;
                }
            }
        }
        try {
            BufferedReader din = new BufferedReader(new InputStreamReader(new FileInputStream(page)));
            String content = "", line;
            while((line = din.readLine()) != null) {
                content += line + "\n";
            }
            din.close();
            content = content.replace("{Response.Status}", response.getStatus() + "").replace("{Response.StatusLine}", response.getStatusLine())
                    .replace("{Request.Path}", request.getRequestPath()).replace("{Response.Entity}", response.getEntity() != null ? new String(response.getEntity()) : "")
                    .replace("{Server.Name}", Server.physicalUnit.getName());
            response.setEntity(content);
            response.header("Content-Type", "text/html; charset=utf-8");
            response.setStatusLine("Internal Server Error");
        } catch (Exception e) {
            if(!global) {
                callGlobal(request, response);
                return;
            }
            response.setStatus(500);
            response.setStatusLine("Internal Server Error");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(byteArrayOutputStream));
            response.setEntity(byteArrayOutputStream.toByteArray());
        }
    }

    public void callGlobal(HttpRequest request, HttpResponse response) {
        ((Provider)Server.physicalUnit.getModule("error-pages")).handle(request, response);
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }
}
