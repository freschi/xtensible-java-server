package at.femoweb.xjs.modules;

/**
 * Created by felix on 2/12/15.
 */
public class SessionNotFoundException extends RuntimeException {

    public SessionNotFoundException() {
        super("Session could not be found!");
    }
}
