package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;

/**
 * Created by felix on 1/7/15.
 */
public interface TagContent {

    public abstract Tag generateTags();
}
