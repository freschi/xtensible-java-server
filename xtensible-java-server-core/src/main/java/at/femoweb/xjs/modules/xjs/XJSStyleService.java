package at.femoweb.xjs.modules.xjs;

import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.html.Tag;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by felix on 5/11/15.
 */
@at.femoweb.xjs.annotations.Service
@Named("xjs-style-service")
public class XJSStyleService extends BootableModule implements Service {

    private BootableLogicalUnit logicalUnit;
    private Group styles;
    private String pathPrefix;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        XJSStyleService styleService = new XJSStyleService();
        styleService.logicalUnit = logicalUnit;
        styleService.styles = logicalUnit.getCatalog().openArticle("unit").openParagraph("styles").openEntry();
        if(settings != null && settings.has("prefix")) {
            styleService.pathPrefix = settings.getString("prefix");
        } else {
            styleService.pathPrefix = "/css/";
        }
        return styleService;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public Object put(Object... objects) {
        if(objects.length >= 1 && objects[0] instanceof String) {
            if(styles.has((String) objects[0])) {
                List sheets = styles.getGroup((String) objects[0]).getList("stylesheets");
                Collection<Tag> tags = new ArrayList<>();
                for (int i = 0; i < sheets.count(); i++) {
                    String path = pathPrefix + sheets.getString(i);
                    Tag link = new Tag("link");
                    link.attribute("rel", "stylesheet");
                    link.attribute("href", path);
                    link.attribute("type", "text/css");
                    tags.add(link);
                }
                return tags;
            }
        }
        return null;
    }
}
