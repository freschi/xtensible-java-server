package at.femoweb.xjs.modules;

import at.femoweb.config.entries.Group;
import at.femoweb.html.Document;
import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.dependencies.Dependencies;
import at.femoweb.xjs.dependencies.Dependency;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.modules.xjs.Replacer;
import at.femoweb.xjs.modules.xjs.TagReplacer;
import at.femoweb.xjs.modules.xjs.TagReplacerInfo;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Created by felix on 10/21/14.
 */
@ContentProvider(regex = ".*[\\.xjs|$\\/]")
@Named("xjs-provider")
@Dependencies({@Dependency("x-action"), @Dependency("x-scripts")})
public class XJSFileProvider extends BootableModule implements Provider, Initable {

    private LogicalUnit logicalUnit;
    private Pattern vars;
    private File htdocs;
    private Replacer replacer;

    public XJSFileProvider () {

    }

    public XJSFileProvider(LogicalUnit logicalUnit, Properties properties) {
        this.logicalUnit = logicalUnit;
        this.replacer = new Replacer();
        if(logicalUnit == null)
            return;
        vars = Pattern.compile("\\{[A-Za-z0-9\\.-_][^\\\\\\{\\}]*\\}");
        if(properties.containsKey("xjs-home")) {
            htdocs = new File(logicalUnit.getName() + "/" + properties.getProperty("xjs-home"));
        } else if (properties.containsKey("htdocs")) {
            htdocs = new File(logicalUnit.getName() + "/" + properties.getProperty("htdocs"));
        } else {
            htdocs = null;
        }
    }

    @Override
    public void init(Properties properties) {
        if(logicalUnit != null) {
            Service ressourceService = logicalUnit.getService("jres-service");
            if (ressourceService != null) {
                ressourceService.put(new AnnotationHandlerBundle(TagReplacerInfo.class, new AnnotationHandler<TagReplacerInfo>() {
                    @Override
                    public void foundType(Class<TagReplacerInfo> annotation, Class<?> type) {
                        try {
                            replacer.addReplacer((Class<? extends TagReplacer>) type);
                        } catch (ClassCastException e) {
                            Server.log.warn("Error while adding Replacer", e);
                        }
                    }

                    @Override
                    public void foundMethod(Class<TagReplacerInfo> annotation, Method method) {

                    }

                    @Override
                    public void foundField(Class<TagReplacerInfo> annotation, Field field) {

                    }
                }));
            }
        }
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        if(true || request.header("User-Agent") != null && request.header("User-Agent").contains("Firefox")) {
            File requested = new File(htdocs, request.getRequestPath());
            if (response.getStatus() == 599) {
                response.setStatusLine("Found");
                response.setStatus(302);
                response.header("Location", request.getRequestPath());
                return;
            }
            if (requested.isDirectory()) {
                requested = new File(requested, "index.xjs");
            }
            if (!requested.exists()) {
                response.setStatus(404);
                return;
            }
            response.setStatus(200);
            byte[] buffer = new byte[2048];
            String content = "";
            try {
                FileInputStream inputStream = new FileInputStream(requested);
                int count;
                while ((count = inputStream.read(buffer)) > 0) {
                    content += new String(buffer, 0, count);
                }
            } catch (IOException e) {
                Server.log.warn("Failed to load content for " + request.getRequestLine());
                response.setStatus(500);
                response.setStatusLine("Internal Server Error");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(byteArrayOutputStream));
                response.setEntity(byteArrayOutputStream.toByteArray());
            }

            /*Matcher matcher = vars.matcher(content);
            while (matcher.find()) {
                String key = matcher.group();
                key = key.substring(1, key.length() - 1);
                if (Session.get(key) != null) {
                    content = matcher.replaceFirst(Session.get(key).toString());
                }
                matcher = vars.matcher(content);
            }*/

            Document document = Document.readDocument(content);
            if (document.doctype().equalsIgnoreCase("html")) {
            } else if (document.doctype().equalsIgnoreCase("xjs:html")) {
                Document output = new Document("html");
                Tag title = new Tag("title", output.head());
                title.add(document.content().get(0).attribute("title"));
                Tag style = new Tag("link", output.head());
                style.attribute("rel", "stylesheet");
                style.attribute("type", "text/css");
                if (document.content().get(0).attribute("style").equalsIgnoreCase("bootstrap")) {
                    style.attribute("href", "/css/bootstrap.min.css");
                }
                style = new Tag("link", output.head());
                style.attribute("rel", "stylesheet");
                style.attribute("type", "text/css");
                if (document.content().get(0).attribute("style").equalsIgnoreCase("bootstrap")) {
                    style.attribute("href", "/css/bootstrap-datetimepicker.min.css");
                }
                style = new Tag("link", output.head());
                style.attribute("rel", "stylesheet");
                style.attribute("type", "text/css");
                if (document.content().get(0).attribute("style").equalsIgnoreCase("bootstrap")) {
                    style.attribute("href", "/css/font-awesome.min.css");
                }
                style = new Tag("link", output.head());
                style.attribute("rel", "stylesheet");
                style.attribute("type", "text/css");
                if (document.content().get(0).attribute("style").equalsIgnoreCase("bootstrap")) {
                    style.attribute("href", "/css/awesome-bootstrap-checkbox.css");
                }
                style = new Tag("link", output.head());
                style.attribute("rel", "stylesheet");
                style.attribute("type", "text/css");
                if (document.content().get(0).attribute("style").equalsIgnoreCase("bootstrap")) {
                    style.attribute("href", "/css/femoweb.css");
                }
                Tag script = new Tag("script", output.head());
                script.type(TagType.FULL);
                script.attribute("src", "/js/jquery-2.1.3.min.js");
                if (logicalUnit.hasProvider("x-scripts")) {
                    Tag xscript = new Tag("script", output.head());
                    xscript.attribute("type", "text/javascript");
                    xscript.attribute("src", "/xjs/xaction.js");
                    xscript.type(TagType.FULL);
                }
                output.body().attribute("style", "margin-top: 5px");
                Tag container = new Tag("div", output.body());
                container.attribute("class", "container");
                container.add(document.content().get(0).children());
                replacer.replace(output.content().get(1));
                content = output.render();
            } else {
                response.setStatus(401);
                response.setStatusLine("Forbidden");
                response.setEntity("This kind of file can not be accessed by a browser");
                return;
            }
            response.header("X-Powered-By", "XJS-Pages/0.1");
            response.setEntity(content);
        } else {
            response.setStatus(406);
            response.setStatusLine("Not Acceptable");
            response.setEntity("Your browser with User-Agent <" + request.header("User-Agent") + "> is not compatible with this service.\nPlease consider downloading a compatible browser (eg Firefox)");
            response.header("Content-Type", "text/plain");
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        if(htdocs == null)
            return false;
        File xjs = new File(htdocs, request.getRequestPath());
        if(xjs.isDirectory()) {
            xjs = new File(xjs, "index.xjs");
        }
        if(xjs.exists() || xjs.isFile()) {
            byte[] info = new byte[19];
            try {
                FileInputStream fileInputStream = new FileInputStream(xjs);
                if(fileInputStream.read(info) == info.length) {
                    String string = new String(info);
                    return string.equals("<!DOCTYPE xjs:html>") || string.equals("<!DOCTYPE xjs:page>");
                }
            } catch (IOException e) {
                Server.log.warn("Could not determine file type for " + xjs.getAbsolutePath(), e);
            }
        }
        return false;
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        XJSFileProvider xjsFileProvider = new XJSFileProvider();
        if(settings.has("htdocs")) {
            xjsFileProvider.htdocs = new File(logicalUnit.getRootDir(), settings.getString("htdocs"));
        }
        xjsFileProvider.logicalUnit = logicalUnit;
        xjsFileProvider.replacer = new Replacer();
        xjsFileProvider.vars = Pattern.compile("\\{[A-Za-z0-9\\.-_][^\\\\\\{\\}]*\\}");
        return xjsFileProvider;
    }

    @Override
    public void logInfo() {
        Server.log.info("XJS File Provider for unit " + logicalUnit.getName() + " enabled");
    }

    @Override
    public void setup() {

    }
}
