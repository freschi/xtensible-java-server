package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;

/**
 * Created by felix on 10/29/14.
 */
@TagReplacerInfo(tag = "xjs:button")
public class ButtonReplacer extends LinkReplacer {

    @Override
    public Tag replace(Tag tag) {
        Tag button;
        if(tag.attribute("onclick") != null) {
            button = new Tag("a");
            button.attribute("onclick", "invoke('" + tag.attribute("onclick") + "');");
        } else if (tag.attribute("target") != null) {
            button = new Tag("a");
            button.attribute("href", tag.attribute("target"));
        } else {
            button = new Tag("a");
        }
        if(tag.attribute("type") != null) {
            String s = tag.attribute("type");
            if (s.equalsIgnoreCase("primary")) {
                button.attribute("class", "btn btn-primary");
            } else if (s.equalsIgnoreCase("info")) {
                button.attribute("class", "btn btn-info");
            } else if (s.equalsIgnoreCase("warning")) {
                button.attribute("class", "btn btn-warning");
            } else if (s.equalsIgnoreCase("danger")) {
                button.attribute("class", "btn btn-danger");
            } else if (s.equalsIgnoreCase("success")) {
                button.attribute("class", "btn btn-success");
            } else if (s.equalsIgnoreCase("link")) {
                button.attribute("class","btn btn-link");
            } else {
                button.attribute("class", "btn btn-default");
            }
        } else {
            button.attribute("class", "btn btn-default");
        }
        if(tag.attribute("size") != null) {
            String s = tag.attribute("size");
            if(s.equalsIgnoreCase("large")) {
                button.attribute("class" , button.attribute("class") + " btn-lg");
            } else if(s.equalsIgnoreCase("small")) {
                button.attribute("class" , button.attribute("class") + " btn-sm");
            } else if(s.equalsIgnoreCase("mini")) {
                button.attribute("class" , button.attribute("class") + " btn-xs");
            }
        }
        if(tag.attribute("open") != null) {
            String open = tag.attribute("open");
            if(open.equalsIgnoreCase("new"))
                button.attribute("target", "_blank");
            else if (open.equalsIgnoreCase("this"))
                button.attribute("target", "_self");
            else if (open.equalsIgnoreCase("super"))
                button.attribute("target", "_parent");
            else if (open.equalsIgnoreCase("window"))
                button.attribute("target", "top");
            else
                button.attribute("target", open);
        } else {
            button.attribute("target", "_self");
        }
        if(tag.attribute("block") != null) {
            button.attribute("class", button.attribute("class") + " btn-block");
        }
        if(tag.attribute("style") != null) {
            button.attribute("style", tag.attribute("style"));
        }
        return button;
    }

    @Override
    public boolean replaces(String tagName) {
        return tagName != null && tagName.equalsIgnoreCase("xjs:button");
    }
}
