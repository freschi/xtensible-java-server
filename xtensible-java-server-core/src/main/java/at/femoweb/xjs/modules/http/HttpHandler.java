package at.femoweb.xjs.modules.http;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.lifecycle.Startable;
import at.femoweb.xjs.lifecycle.Stopable;
import at.femoweb.xjs.modules.HandoverFailedException;
import at.femoweb.xjs.services.StreamHandoverHandler;
import at.femoweb.xjs.structure.LogicalUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;

/**
 * Created by felix on 3/18/15.
 */
public class HttpHandler implements Initable, Startable, Stopable, Reloadable {

    public static Logger log = LogManager.getLogger("HTTP");

    private String name;
    private int port;
    private boolean ssl;
    private int threads;
    private String ip;
    private ArrayList<LogicalUnit> units;
    private Thread handlingThread;
    private PendingRequestHandlerThread[] pool;
    private boolean running;
    private ConcurrentLinkedQueue<PendingRequest> requests;
    private HashMap<LogicalUnit, ArrayList<StreamHandoverHandler>> handoverMap;
    private HashMap<String, LogicalUnit> mappings;

    public HttpHandler(String name, int port, boolean ssl, int threads) {
        this.name = name;
        this.port = port;
        this.ssl = ssl;
        this.threads = threads;
        units = new ArrayList<>();
        requests = new ConcurrentLinkedQueue<>();
        mappings = new HashMap<>();
        handoverMap = new HashMap<>();
    }

    public HttpHandler(String name, int port, boolean ssl, int threads, String ip) {
        this(name, port, ssl, threads);
        this.ip = ip;
    }

    public void registerUnit(LogicalUnit unit) {
        units.add(unit);
        for(String alias : unit.getAliases()) {
            mappings.put(alias, unit);
        }
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }

    public boolean isSsl() {
        return ssl;
    }

    public int getThreads() {
        return threads;
    }

    public synchronized void enqueue(PendingRequest pendingRequest) {
        requests.offer(pendingRequest);
        int no;
        if((no = noOfWorkingThreads()) < requests.size() && no < threads) {
            for (int i = 0; i < threads; i++) {
                synchronized (pool[i]) {
                    if (pool[i].isIdle()) {
                        log.debug("Waking " + pool[i].getName());
                        pool[i].notify();
                        break;
                    }
                }
            }
        }
    }

    private int noOfWorkingThreads() {
        int num = 0;
        for(PendingRequestHandlerThread t : pool) {
            if (!t.isIdle()) num++;
        }
        return num;
    }

    @Override
    public void init(Properties properties) {
        handlingThread = new HttpHandlerThread(this);
        handlingThread.setName("handler-" + getName());
        pool = new PendingRequestHandlerThread[threads];
        for (int i = 0; i < threads; i++) {
            pool[i] = new PendingRequestHandlerThread(this);
            pool[i].setName("handler-" + getName() + ".pool-" + i);
        }
    }

    @Override
    public void reload() {

    }

    @Override
    public void start() {
        running = true;
        handlingThread.start();
        for (int i = 0; i < threads; i++) {
            pool[i].start();
        }
    }

    @Override
    public void shutdown(Properties properties) {
        running = false;
        handlingThread.interrupt();
        try {
            handlingThread.join();
        } catch (InterruptedException e) {
            Server.log.fatal("Could not shutdown HttpHandlerThread for " + getName());
        }
    }

    public boolean isRunning() {
        return running;
    }

    public synchronized PendingRequest getNext() {
        synchronized (requests) {
            if(requests.isEmpty()) {
                return null;
            } else {
                log.debug("Requests after operation: " + (requests.size() - 1));
                return requests.poll();
            }
        }
    }

    public void handle(PendingRequest pendingRequest) {
        HttpRequest request = pendingRequest.getRequest();
        HttpResponse response = pendingRequest.getResponse();
        if(request.header("Host") == null) {
            response.setStatus(400);
            response.setStatusLine("Bad Request");
            response.setEntity("Host not set");
        } else {
            String host = request.header("Host");
            if (host.contains(":")) {
                host = host.substring(0, host.indexOf(":"));
            }
            if (mappings.containsKey(host)) {
                LogicalUnit unit = mappings.get(host);
                if(handoverMap.containsKey(unit)) {
                    ArrayList<StreamHandoverHandler> handlers = handoverMap.get(unit);
                    for(StreamHandoverHandler handler : handlers) {
                        for(String path : handler.getPaths()) {
                            Pattern pattern = Pattern.compile(path);
                            if(pattern.matcher(request.getRequestPath()).matches()) {
                                Server.log.info("Found matching handover candidate for " + request.getRequestPath() + "@" + handler.getClass().toString());
                                try {
                                    handler.handle(request, pendingRequest.getSocket());
                                    return;
                                } catch (HandoverFailedException e) {
                                    Server.log.warn("Handover handler defined path for handover but failed to take over", e);
                                } catch (Throwable throwable) {
                                    Server.log.warn("Handover failed for path " + path, throwable);
                                }
                            }
                        }
                    }
                }
                try {
                    unit.handle(request, response);
                } catch (Throwable throwable) {
                    Server.log.warn("Internal Server Error", throwable);
                    response.setStatus(500);
                    response.setStatusLine("Internal Server Error");
                }
                if (response.getStatus() == 200) {
                    response.setStatusLine("OK");
                }
            } else {
                response.setStatus(406);
                response.setStatusLine("Not Acceptable");
                response.setEntity("Unknown Host");
            }
        }
        if(response.header("Content-Type") == null) {
            response.header("Content-Type", "text/html;charset=utf-8");
        }
        response.header("Server", "XJS/0.1");
        response.header("X-Powered-By", "XJS Engine");
        response.header("Connection", "close");
        try {
            log.debug("Writing " + request.getRequestLine() + " ->  " + response.getResponseLine());
            response.writeToStream(pendingRequest.getSocket().getOutputStream());
            pendingRequest.getSocket().close();
        } catch (IOException e) {
            Server.log.warn("Could not serve " + request.getRequestLine());
        }
    }

    public void registerHandoverHandler(StreamHandoverHandler handler, LogicalUnit logicalUnit) {
        ArrayList<StreamHandoverHandler> handlers;
        if(handoverMap.containsKey(logicalUnit)) {
            handlers = handoverMap.get(logicalUnit);
        } else {
            handlers = new ArrayList<>();
            handoverMap.put(logicalUnit, handlers);
        }
        handlers.add(handler);
    }

    public String getIp() {
        return ip;
    }
}
