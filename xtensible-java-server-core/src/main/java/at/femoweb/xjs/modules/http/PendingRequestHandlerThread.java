package at.femoweb.xjs.modules.http;

import at.femoweb.xjs.Server;

import java.io.IOException;

/**
 * Created by felix on 3/18/15.
 */
public class PendingRequestHandlerThread extends Thread {

    private HttpHandler handler;
    private boolean idle;

    public PendingRequestHandlerThread(HttpHandler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        while(handler.isRunning()) {
            PendingRequest request = handler.getNext();
            if(request != null) {
                try {
                    request.readRequest();
                } catch (IOException e) {
                    Server.log.warn("Could not read request from stream", e);
                }
                handler.handle(request);
            } else {
                idle = true;
                try {
                    synchronized (this) {
                        wait(10000);
                    }
                } catch (InterruptedException e) {
                }
                idle = false;
            }
        }
    }

    public boolean isIdle() {
        return idle;
    }
}
