package at.femoweb.xjs.modules.link;

/**
 * Created by felix on 3/24/15.
 */
public interface LinkPingListener {

    public void receivedPong();
}
