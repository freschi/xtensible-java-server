package at.femoweb.xjs.modules.xaction;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Push;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.structure.LogicalUnit;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by felix on 11/2/14.
 */
public class Updates {

    private JsonArray updates;

    public Updates () {
        updates = new JsonArray();
    }

    public void addMessage(String message) {
        addMessage(null, message, -1);
    }

    public void addMessage(Severity severity, String message) {
        addMessage(severity, message, -1);
    }

    public void addMessage(String message, int time) {
        addMessage(null, message, time);
    }

    public void addMessage(Severity severity, String message, int time) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "message");
        json.addProperty("data", message);
        if(severity != null) {
            switch (severity) {
                case DANGER:
                    json.addProperty("severity", "danger");
                    break;
                case INFO:
                    json.addProperty("severity", "info");
                    break;
                case SUCCESS:
                    json.addProperty("severity", "success");
                    break;
                case WARNING:
                    json.addProperty("severity", "warning");
                    break;
            }
        }
        if(time > 0) {
            json.addProperty("time", time);
        }
        addUpdate(json);
    }

    public void changeLocation (String location) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "location");
        json.addProperty("location", location);
        addUpdate(json);
    }

    public void value(String id, String value) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "value");
        json.addProperty("id", id);
        json.addProperty("value", value);
        addUpdate(json);
    }

    public void html(String id, String html) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "html");
        json.addProperty("id", id);
        json.addProperty("html", html);
        addUpdate(json);
    }

    public void push(boolean active) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "push");
        json.addProperty("push", active ? "on" : "off");
        addUpdate(json);
    }

    public void addClass(String id, String clazz) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "add-class");
        json.addProperty("class", clazz);
        json.addProperty("id", id);
        addUpdate(json);
    }

    public void removeClass(String id, String clazz) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "remove-class");
        json.addProperty("class", clazz);
        json.addProperty("id", id);
        addUpdate(json);
    }

    public void focus(String id) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "focus");
        json.addProperty("id", id);
        addUpdate(json);
    }

    public void setPartial(String id, String partial) {
        LogicalUnit logicalUnit = (LogicalUnit) Session.get("_xjs_logical_unit");
        assert logicalUnit != null;
        if(logicalUnit.hasService("xjs-file-provider-service")) {
            Object result = logicalUnit.getService("xjs-file-provider-service").put(partial);
            if(result != null && result instanceof Tag) {
                html(id, ((Tag) result).render());
            }
        } else if(logicalUnit.hasService("xjs-parser-service")) {
            html(id, logicalUnit.getService("xjs-parser-service").put(partial).toString());
        } else {
            html(id, "<strong>Parser Service not found!</strong>");
        }
    }

    public void title(String title) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "title");
        json.addProperty("title", title);
        addUpdate(json);
    }

    public void highlightError(String id) {
        addClass(id + "_group", "has-error");
    }

    public void removeError(String id) {
        removeClass(id + "_group", "has-error");
    }
    
    public void openPopup(String url) {
        JsonObject json = new JsonObject();
        json.addProperty("type", "popup-window");
        json.addProperty("url", url);
        addUpdate(json);
    }

    protected void addUpdate(JsonObject jsonObject) {
        updates.add(jsonObject);
    }

    public boolean empty () {
        return updates.size() == 0;
    }

    public JsonArray getUpdates () {
        if(!(this instanceof Push)) {
            try {
                if (Push.get() != null) {
                    JsonArray push = Push.get().getUpdates();
                    for (JsonElement json : push) {
                        updates.add(json);
                    }
                    Push.get().clear();
                }
            } catch (Exception e) {
                Server.log.debug("Nothing to push", e);
            }
        }
        return updates;
    }

    protected void clear() {
        updates = new JsonArray();
    }

    public enum Severity {
        SUCCESS, INFO, WARNING, DANGER
    }
}
