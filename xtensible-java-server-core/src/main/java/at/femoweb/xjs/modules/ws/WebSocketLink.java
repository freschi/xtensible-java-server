package at.femoweb.xjs.modules.ws;

import at.femoweb.xjs.modules.link.Link;
import at.femoweb.xjs.modules.link.LinkPingListener;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Created by felix on 3/24/15.
 */
public class WebSocketLink implements Link {

    private WebSocketHandlingThread thread;

    public WebSocketLink(WebSocketHandlingThread thread) {
        this.thread = thread;
    }


    @Override
    public void write(String message) {
        JsonObject object = new JsonObject();
        object.addProperty("type", "message");
        object.addProperty("message", message);
        thread.sendMessage(1, object.toString());
    }

    @Override
    public void changePath(String path) {
        JsonObject object = new JsonObject();
        object.addProperty("type", "path");
        object.addProperty("path", path);
        thread.sendMessage(1, object.toString());
    }

    @Override
    public void close() {
        thread.close();
    }

    @Override
    public void ping(LinkPingListener listener) {
        thread.ping();
    }

}
