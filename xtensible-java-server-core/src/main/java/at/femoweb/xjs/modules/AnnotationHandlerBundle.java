package at.femoweb.xjs.modules;

import java.lang.annotation.Annotation;

/**
 * Created by felix on 11/13/14.
 */
public class AnnotationHandlerBundle<T extends Annotation> {

    private Class<T> annotation;
    private AnnotationHandler handler;

    public AnnotationHandlerBundle (Class<T> annotation, AnnotationHandler handler) {
        this.annotation = annotation;
        this.handler = handler;
    }

    public Class<T> getAnnotation() {
        return annotation;
    }

    public void setAnnotation(Class<T> annotation) {
        this.annotation = annotation;
    }

    public AnnotationHandler getHandler() {
        return handler;
    }

    public void setHandler(AnnotationHandler handler) {
        this.handler = handler;
    }
}
