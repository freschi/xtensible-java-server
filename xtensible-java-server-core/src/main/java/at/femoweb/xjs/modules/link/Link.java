package at.femoweb.xjs.modules.link;

/**
 * Created by felix on 3/24/15.
 */
public interface Link {

    public abstract void write(String message);
    public abstract void changePath(String path);
    public abstract void close();
    public abstract void ping(LinkPingListener listener);
}
