package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;

/**
 * Created by felix on 11/6/14.
 */
@TagReplacerInfo(tag = "xjs:progress", type = TagReplacerInfo.ReplacementType.DROP_INTERNAL)
public class ProgressReplacer implements TagReplacer {

    private static int counter = 0;

    @Override
    public boolean replaces(String tagName) {
        return tagName != null && tagName.equalsIgnoreCase("xjs:progress");
    }

    @Override
    public Tag replace(Tag tag) {
        Tag progress = new Tag("div");
        Tag bar = new Tag("div", progress);
        progress.attribute("class", "progress");
        bar.attribute("class", "progress-bar");
        bar.attribute("role", "progressbar");
        int min = 0, max = 100, val = -1;
        if(tag.attribute("min") != null) {
            min = Integer.parseInt(tag.attribute("min"));
        }
        if(tag.attribute("max") != null) {
            max = Integer.parseInt(tag.attribute("max"));
        }
        if(tag.attribute("value") != null) {
            val = Integer.parseInt(tag.attribute("value"));
        }
        bar.attribute("aria-valuemin", min + "");
        bar.attribute("aria-valuemax", max + "");
        if(val == -1) {
            bar.attribute("aria-valuenow", "100");
            bar.attribute("style", "width: 100%");
            bar.attribute("class" , bar.attribute("class") + " progress-bar-striped active");
            Tag span = new Tag("span");
            span.attribute("class", "sr-only");
            span.add("Pending ...");
            bar.add(span);
        } else {
            bar.attribute("aria-valuenow", val + "");
            bar.attribute("style", "width: " + val + "%");
            Tag span = new Tag("span");
            span.attribute("class", "sr-only");
            span.add(val + " %");
            bar.add(span);
        }
        if(tag.attribute("id") != null) {
            progress.attribute("id", tag.attribute("id"));
        } else {
            progress.attribute("id", "xjs_progress_" + (counter++));
        }
        return progress;
    }
}
