package at.femoweb.xjs.modules;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.ContentProviderType;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.io.*;
import java.util.Properties;

/**
 * Created by felix on 10/21/14.
 */
@ContentProvider(type = ContentProviderType.FALLBACK, regex = ".*")
@Named("error-pages")
public class ErrorPageProvider extends Module implements Provider, Initable {

    private boolean global;
    private LogicalUnit logicalUnit;
    private File root;

    public ErrorPageProvider () {
        global = true;
    }

    public ErrorPageProvider(LogicalUnit logicalUnit, Properties properties) {
        global = false;
        this.logicalUnit = logicalUnit;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        if(root != null) {
            File page = new File(root, response.getStatus() + ".xjs");
            if(!page.exists()) {
                page = new File(root, response.getStatusLine() + ".xjs");
            }
            if(!page.exists()) {
                page = new File(root, "error.xjs");
            }
            try {
                BufferedReader din = new BufferedReader(new InputStreamReader(new FileInputStream(page)));
                String content = "", line;
                while((line = din.readLine()) != null) {
                    content += line + "\n";
                }
                din.close();
                content = content.replace("{Response.Status}", response.getStatus() + "").replace("{Response.StatusLine}", response.getStatusLine())
                        .replace("{Request.Path}", request.getRequestPath()).replace("{Response.Entity}", new String(response.getEntity()))
                        .replace("{Server.Name}", Server.physicalUnit.getName());
                response.setEntity(content);
            } catch (Exception e) {
                if(!global) {
                    callGlobal(request, response);
                    return;
                }
                response.setStatus(500);
                response.setStatusLine("Internal Server Error");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                e.printStackTrace(new PrintStream(byteArrayOutputStream));
                response.setEntity(byteArrayOutputStream.toByteArray());
            }
        } else {
            if(!global) {
                callGlobal(request, response);
                return;
            }
            response.setStatus(500);
            response.setStatusLine("Internal Server Error");
            response.setEntity("Error while attempting to create error page!");
        }
    }

    private void callGlobal(HttpRequest request, HttpResponse response) {
        ((Provider)Server.physicalUnit.getModule(this.getName())).handle(request, response);
    }

    @Override
    public boolean handles(HttpRequest request) {
        return true;
    }

    @Override
    public void init(Properties properties) {
        if(properties != null && properties.containsKey("error-pages-dir")) {
            if(global)
                root = new File(properties.getProperty("error-pages-dir"));
            else
                root = new File(logicalUnit.getName() + "/" + properties.getProperty("error-pages-dir"));
            Server.log.debug("Looking for error pages in " + root.getAbsolutePath());
            if(!root.exists()) {
                root.mkdirs();
            }
        }
    }

}
