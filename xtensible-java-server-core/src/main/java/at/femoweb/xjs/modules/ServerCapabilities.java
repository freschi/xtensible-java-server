package at.femoweb.xjs.modules;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by felix on 11/3/14.
 */
@Named("server-check")
@ContentProvider(regex = "\\/xjs\\/check\\/.*\\.cap\\??.*")
public class ServerCapabilities extends BootableModule implements Provider {

    private LogicalUnit logicalUnit;
    private File directory;
    private String handlingPrefix;
    private Group scripts;

    public ServerCapabilities () {}

    public ServerCapabilities(LogicalUnit logicalUnit) {
        this.logicalUnit = logicalUnit;
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        response.setStatus(200);
        JsonObject json = new JsonObject();
        JsonObject services = new JsonObject();
        json.add("services", services);
        services.addProperty("push", logicalUnit.hasService("push"));
        services.addProperty("ws", logicalUnit.hasService("ws-service"));
        JsonArray scripts = new JsonArray();
        File[] files = directory.listFiles();
        ArrayList<String> strings = new ArrayList<>();
        for(int i = 0; i < this.scripts.getList("ignore").count(); i++) {
            strings.add(this.scripts.getList("ignore").getString(i));
        }
        for(int i = 0; i < this.scripts.getList("load").count(); i++) {
            strings.add(this.scripts.getList("load").getString(i));
            scripts.add(new JsonPrimitive(handlingPrefix + this.scripts.getList("load").getString(i)));
        }
        for(File file : files) {
            if(strings.contains(file.getName()))
                continue;
            if(!file.getName().contains("min.js") && file.getName().endsWith(".js")) {
                File extra = new File(directory, file.getName().replace(".js", "min.js"));
                if(extra.exists()) {
                    scripts.add(new JsonPrimitive(handlingPrefix + extra.getName()));
                } else {
                    scripts.add(new JsonPrimitive(handlingPrefix + file.getName()));
                }
            } else {
                scripts.add(new JsonPrimitive(handlingPrefix + file.getName()));
            }
        }
        json.add("scripts", scripts);
        response.setEntity(json.toString());
        response.header("Content-Type", "application/json");
    }

    @Override
    public boolean handles(HttpRequest request) {
        return request.getRequestPath().toLowerCase().startsWith("/xjs/check/server.cap");
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        ServerCapabilities capabilities = new ServerCapabilities();
        capabilities.logicalUnit = logicalUnit;
        capabilities.directory = new File(logicalUnit.getRootDir(), settings.getString("jsDir"));
        capabilities.handlingPrefix = settings.getString("handlingPrefix");
        capabilities.scripts = settings.getGroup("scripts");
        return capabilities;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }
}
