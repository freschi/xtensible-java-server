package at.femoweb.xjs.modules.http.rest;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;

/**
 * Created by felix on 9/7/15.
 */
public interface RestPost extends RestOperation {

    int post(HttpRequest request, HttpResponse response);
}
