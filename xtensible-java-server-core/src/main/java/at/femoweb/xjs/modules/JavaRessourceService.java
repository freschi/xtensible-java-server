package at.femoweb.xjs.modules;

import at.femoweb.config.Catalog;
import at.femoweb.config.Config;
import at.femoweb.config.entries.*;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.JResPreprocessor;
import at.femoweb.xjs.annotations.JResTarget;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.StatefullElement;
import at.femoweb.xjs.services.JavaCompilerService;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;
import io.femo.security.services.JResManagementInterface;
import io.femo.security.services.JResPreprocessorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.tools.*;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.List;

/**
 * Created by felix on 11/13/14.
 */
@Named("jres-service")
@Service(type = ServiceType.OTHER)
public class JavaRessourceService extends BootableModule implements StatefullElement, JavaCompilerService {

    private static Logger log = LogManager.getLogger(JavaRessourceService.class);

    private LogicalUnit logicalUnit;
    private HashMap<Class<? extends Annotation>, AnnotationHandler> methodHandler;
    private HashMap<Class<? extends Annotation>, AnnotationHandler> typeHandler;
    private HashMap<Class<? extends Annotation>, AnnotationHandler> fieldHandler;
    private ArrayList<Class<?>> loadedClasses;
    private LinkedHashSet<String> packages;
    private File jroot;
    private File jbin;
    private static boolean legacy = false;
    private ClassLoader classLoader;
    private List<JResPreprocessorService> preprocessorServices;

    public JavaRessourceService () {
        Catalog global = Config.openCatalog("global");
        Group entry = global.openArticle("xjs").openParagraph("jres").openEntry();
        log.debug(entry.export());
        if(entry.has("src")) {
            String jroot = entry.getString("src");
            this.jroot = new File(jroot);
            String jbin = entry.getString("bin");
            this.jbin = new File(jbin);
        } else {
            log.warn("Starting in legacy mode. There might be differences in operation!");
            legacy = true;
        }
        methodHandler = new HashMap<>();
        typeHandler = new HashMap<>();
        fieldHandler = new HashMap<>();
        packages = new LinkedHashSet<>();
        preprocessorServices = new ArrayList<>();
    }

    public File getJroot() {
        return jroot;
    }

    public File getJbin() {
        return jbin;
    }

    public JavaRessourceService (LogicalUnit logicalUnit, Properties properties) {
        this.logicalUnit = logicalUnit;
        methodHandler = new HashMap<>();
        typeHandler = new HashMap<>();
        fieldHandler = new HashMap<>();
        packages = new LinkedHashSet<>();
        if(properties.containsKey("jres-root"))
            jbin = jroot = new File(logicalUnit.getName() + "/" + properties.getProperty("jres-root"));
    }

    private void walkTree(File file, ArrayList<File> compile, String filter) {
        try {
            for (File f : file.listFiles()) {
                if (f.isDirectory()) {
                    walkTree(f, compile, filter);
                } else {
                    if (f.getName().toLowerCase().endsWith(filter))
                        compile.add(f);
                }
            }
        } catch (NullPointerException e) {
            Server.log.warn("File " + file + " had problems to be added to " + compile + " with filter " + filter);
        }
    }

    @Override
    public void preInit(Properties properties) {
        if(jroot == null && properties.containsKey("jres-root"))
            jroot = new File(logicalUnit.getName() + "/" + properties.getProperty("jres-root"));
        preReload();
    }

    @Override
    public void postInit(Properties properties) {
        postReload();
    }

    @Override
    public void preStart() {

    }

    @Override
    public void postStart() {

    }

    @Override
    public void preReload() {
        if(logicalUnit != null) {
            compileAllClasses();
        }
    }

    @Override
    public void postReload() {
        if(logicalUnit != null) {
            for (Class<?> type : loadedClasses) {
                for (Class<? extends Annotation> annotation : typeHandler.keySet()) {
                    if (type.isAnnotationPresent(annotation)) {
                        typeHandler.get(annotation).foundType(annotation, type);
                    }
                }
                if (methodHandler.size() > 0) {
                    for (Method method : type.getMethods()) {
                        for (Class<? extends Annotation> annotation : methodHandler.keySet()) {
                            if (method.isAnnotationPresent(annotation)) {
                                methodHandler.get(annotation).foundMethod(annotation, method);
                            }
                        }
                    }
                }
                if (fieldHandler.size() > 0) {
                    for (Field field : type.getFields()) {
                        for (Class<? extends Annotation> annotation : fieldHandler.keySet()) {
                            if (field.isAnnotationPresent(annotation)) {
                                fieldHandler.get(annotation).foundField(annotation, field);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void preShutdown(Properties properties) {

    }

    @Override
    public void postShutdown(Properties properties) {

    }

    @Override
    public void init(Properties properties) {

    }

    @Override
    public void reload() {

    }

    @Override
    public void start() {

    }

    @Override
    public void shutdown(Properties properties) {

    }


    @Override
    public Object put(Object... objects) {
        for(Object o : objects) {
            if(o instanceof AnnotationHandlerBundle) {
                addLoadListener((AnnotationHandlerBundle<? extends Annotation>) o);
            }
        }
        return null;
    }

    @Override
    public void compileAllClasses() {
        if (loadedClasses == null) {
            loadedClasses = new ArrayList<>();
        } else {
            loadedClasses.clear();
        }
        if (!jroot.exists()) {
            jroot.mkdirs();
        }
        ArrayList<File> files = new ArrayList<File>();
        walkTree(jroot, files, ".java");
        if (files.isEmpty())
            return;
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        JavaFileManager manager = null;
        if(!legacy) {
            manager = new ForwardingJavaFileManager(fileManager) {
                @Override
                public JavaFileObject getJavaFileForOutput(Location location, String s, JavaFileObject.Kind kind, FileObject fileObject) throws IOException {
                    JavaFileObject stdExport = super.getJavaFileForOutput(location, s, kind, fileObject);
                    JavaFileObject export = new ForwardingJavaFileObject(stdExport) {
                        @Override
                        public String getName() {
                            String export = super.getName();
                            if(export.startsWith(jroot.toString())) {
                                String f = export.substring(jroot.toString().length());
                                File n = new File(jbin, f);
                                n.getParentFile().mkdirs();
                                log.debug("Changing location from " + export + " to " + n.toString());
                                return n.toString();
                            }
                            return export;
                        }

                        @Override
                        public InputStream openInputStream() throws IOException {
                            return new FileInputStream(new File(getName()));
                        }

                        @Override
                        public OutputStream openOutputStream() throws IOException {
                            log.debug("Output stream created");
                            return new FileOutputStream(new File(getName()));
                        }

                        @Override
                        public long getLastModified() {
                            return new File(getName()).lastModified();
                        }

                        @Override
                        public URI toUri() {
                            return new File(getName()).toURI();
                        }

                        @Override
                        public boolean delete() {
                            return new File(getName()).delete();
                        }

                        @Override
                        public Writer openWriter() throws IOException {
                            return new OutputStreamWriter(openOutputStream());
                        }
                    };
                    return export;
                }
            };
        }
        Iterable<? extends JavaFileObject> units;
        units = fileManager.getJavaFileObjectsFromFiles(files);
        List<String> optionList = new ArrayList<String>();
        log.debug("Class Path: " + System.getProperty("java.class.path"));
        optionList.addAll(Arrays.asList("-classpath", System.getProperty("java.class.path"), "-source", "1.8", "-target", "1.8"));
        JavaCompiler.CompilationTask task = compiler.getTask(null, legacy ? fileManager : manager, diagnostic -> {
            if(diagnostic == null || diagnostic.getSource() == null) {
                log.fatal("Diagnostics is not set or source not set");
                log.fatal(String.valueOf(diagnostic));
                if(diagnostic != null)
                    log.fatal(String.valueOf(diagnostic.getSource()));
                return;
            }
            log.error(diagnostic.getMessage(Locale.getDefault()) + " @" + diagnostic.getSource().getName() + ":" + diagnostic.getLineNumber());
        }, optionList, null, units);
        try {
            task.call();
        } catch (Throwable t) {
            t.printStackTrace();
            return;
        }
        try {
            fileManager.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        files.clear();
        log.debug("Loading files from " + jbin.getAbsolutePath());
        walkTree(jbin, files, ".class");
        try {
            classLoader = new URLClassLoader(new URL[]{jbin.toURI().toURL()});
            for (File file : files) {
                String name = file.getAbsolutePath().substring(jbin.getAbsolutePath().length() + 1);
                name = name.substring(0, name.lastIndexOf(".")).replace("/", ".");
                log.debug("Loading compiled class " + name + " from " + file.toURI().toString());
                try {
                    Class<?> clazz = classLoader.loadClass(name);
                    packages.add(clazz.getPackage() != null ? clazz.getPackage().getName() : "");
                    log.debug("Successfully loaded " + clazz.getName());
                    loadedClasses.add(clazz);
                } catch (Exception e) {
                    log.warn("Couldn't load class " + name, e);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Server.log.info("Running " + preprocessorServices.size() + " preprocessors on " + loadedClasses.size() + " for unit " + (logicalUnit != null ? logicalUnit.getName() : "global"));
        this.preprocessorServices.forEach(service -> {
            List<Class<?>> classes = loadedClasses;
            loadedClasses = new ArrayList<>();
            Server.log.info("Running preprocessor " + service.getClass().getName());
            classes.forEach(c -> loadedClasses.add(service.process(c, classLoader, new JResManagementInterface() {
            })));
        });
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    @Override
    public Class<?>[] getAllClasses(Class<? extends Annotation> annotation) {
        ArrayList<Class<?>> classes = new ArrayList<>();
        for(Class<?> clazz : loadedClasses) {
            if(clazz.isAnnotationPresent(annotation)) {
                classes.add(clazz);
            }
        }
        return classes.toArray(new Class[]{});
    }

    @Override
    public Method[] getAllMethods(Class<? extends Annotation> annotation) {
        ArrayList<Method> methods = new ArrayList<>();
        for(Class<?> clazz : loadedClasses) {
            for(Method method : clazz.getMethods()) {
                if(method.isAnnotationPresent(annotation)) {
                    methods.add(method);
                }
            }
        }
        return methods.toArray(new Method[]{});
    }

    @Override
    public Field[] getAllFields(Class<? extends Annotation> annotation) {
        ArrayList<Field> fields = new ArrayList<>();
        for(Class<?> clazz : loadedClasses) {
            for(Field field : clazz.getFields()) {
                if(field.isAnnotationPresent(annotation)) {
                    fields.add(field);
                }
            }
        }
        return fields.toArray(new Field[]{});
    }

    @Override
    public void addLoadListener(AnnotationHandlerBundle<? extends Annotation> bundle) {
        if(bundle.getAnnotation().isAnnotationPresent(JResTarget.class)) {
            JResTarget target = (JResTarget) bundle.getAnnotation().getAnnotation(JResTarget.class);
            for(ElementType type : target.value()) {
                if(type == ElementType.TYPE) {
                    typeHandler.put(bundle.getAnnotation(), bundle.getHandler());
                } else if(type == ElementType.METHOD) {
                    methodHandler.put(bundle.getAnnotation(), bundle.getHandler());
                } else if(type == ElementType.FIELD) {
                    fieldHandler.put(bundle.getAnnotation(), bundle.getHandler());
                }
            }
        }
    }

    public Set<String> getPackages() {
        return (Set<String>) packages.clone();
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        JavaRessourceService javaRessourceService = new JavaRessourceService();
        File root = logicalUnit.getRootDir();
        if(settings.has("inheritGlobal") && settings.getBoolean("inheritGlobal")) {
            Catalog global = Config.openCatalog("global");
            Group entry = global.openArticle("xjs").openParagraph("jres").openEntry();
            javaRessourceService.jroot = new File(root, entry.getString("src"));
            javaRessourceService.jbin = new File(root, entry.getString("bin"));
        } else {
            javaRessourceService.jroot = new File(root, settings.getString("src"));
            javaRessourceService.jbin = new File(root, settings.getString("bin"));
        }
        if(!javaRessourceService.jroot.exists()) {
            javaRessourceService.jroot.mkdir();
        }
        if(!javaRessourceService.jbin.exists()) {
            javaRessourceService.jbin.mkdir();
        }
        Server.log.info("Loading Preprocessors for unit " + logicalUnit.getName());
        if(settings.has("preprocessor")) {
            at.femoweb.config.entries.List preprocessor = settings.getList("preprocessor");
            for (int i = 0; i < preprocessor.count(); i++) {
                String name = preprocessor.getString(i);
                try {
                    Class<?> c = Class.forName(name);
                    JResPreprocessorService preprocessorService = (JResPreprocessorService) c.newInstance();
                    if(preprocessorService instanceof BootableModule) {
                        preprocessorService = (JResPreprocessorService) ((BootableModule) preprocessorService).getBootableLogicalInstance(logicalUnit, null);
                    }
                    preprocessorService.configure(javaRessourceService);
                    javaRessourceService.preprocessorServices.add(preprocessorService);
                } catch (ClassNotFoundException | InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        javaRessourceService.logicalUnit = logicalUnit;
        legacy = false;
        return javaRessourceService;
    }

    public class ExportFileObject<T extends JavaFileObject> extends ForwardingJavaFileObject<T> {

        private File export;
        private String name;

        public ExportFileObject(T t, String name) {
            super(t);
            export = new File(this.name = jbin.getName() + "/" + name.replace(".", "/") + ".class");
            export.getParentFile().mkdirs();
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            log.debug("File opened " + export.getAbsolutePath());
            return new FileOutputStream(export);
        }

        @Override
        public URI toUri() {
            return export.toURI();
        }
    }

    @Override
    public void logInfo() {
        log.info("JRES Service for " + (logicalUnit != null ? logicalUnit.getName() : "") + " jroot = " + jroot.getAbsolutePath() + ", jbin = " + jbin.getAbsolutePath());
    }

    @Override
    public void setup() {

    }
}
