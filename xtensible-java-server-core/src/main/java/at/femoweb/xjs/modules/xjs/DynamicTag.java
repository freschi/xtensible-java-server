package at.femoweb.xjs.modules.xjs;

import at.femoweb.html.Tag;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;

import java.lang.reflect.Field;

/**
 * Created by felix on 5/10/15.
 */
public abstract class DynamicTag extends Tag {

    public abstract Tag renderInternal();

    @Override
    public String render(int level, boolean alone) {
        Tag tmp = renderInternal();
        String value;
        try {
            value = tmp.render(level, alone);
        } catch (Throwable t) {
            Server.log.warn("Could not render tag " + String.valueOf(this), t);
            return "";
        }
        return value;
    }

    @Override
    public final void attribute(String name, String value) {
        try {
            Field field = this.getClass().getDeclaredField(name);
            field.setAccessible(true);
            field.set(this, value);
            field.setAccessible(false);
            return;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            //Server.log.warn("Could not dynamically set fields");
        }
        for(Field field : this.getClass().getDeclaredFields()) {
            if(field.isAnnotationPresent(Named.class)) {
                Named named = field.getAnnotation(Named.class);
                if(named.value().equals(name)) {
                    field.setAccessible(true);
                    try {
                        field.set(this, value);
                    } catch (IllegalAccessException e) {
                        //Server.log.warn("Could not dynamically set fields");
                    }
                    field.setAccessible(false);
                    return;
                }
            }
        }
        super.attribute(name, value);
    }

    @Override
    protected Tag copy(Tag parent) {
        return this;
    }

    public static DynamicTag createTag(Class<? extends DynamicTag> clazz) throws IllegalAccessException, InstantiationException {
        DynamicTag dynamicTag = clazz.newInstance();
        return dynamicTag;
    }
}
