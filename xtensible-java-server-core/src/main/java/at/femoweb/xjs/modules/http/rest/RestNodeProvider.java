package at.femoweb.xjs.modules.http.rest;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.JavaCompilerService;
import at.femoweb.xjs.services.LoginService;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Base64;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Created by felix on 9/7/15.
 */
@ContentProvider(regex = "\\/rest\\/nodes\\/.*")
@Named("rest-node-provider")
public class RestNodeProvider extends BootableModule implements Provider, Initable {

    private BootableLogicalUnit logicalUnit;

    private TreeMap<String, Object> nodes;

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        if(request.getMethod().equals("GET") && request.getRequestPath().equals("/rest/nodes/")) {
            Server.log.info("Building REST Nodes Description");
            JsonObject info = new JsonObject();
            info.addProperty("version", 1);
            info.addProperty("format", "objects");
            JsonObject paths = new JsonObject();
            info.add("paths", paths);
            nodes.forEach((path, handler) -> {
                JsonObject node = new JsonObject();
                RestNode restNode = handler.getClass().getAnnotation(RestNode.class);
                String[] methods = restNode.methods();
                for (int i = 0; i < methods.length; i++) {
                    node.addProperty(methods[i].toLowerCase(), restNode.descriptions()[i]);
                }
                paths.add(path, node);
            });
            response.header("Content-Type", "application/json");
            response.setEntity(info.toString());
            response.setStatus(200);
            response.setStatusLine("OK");
        } else {
            String nodePath = request.getRequestPath().substring(12);
            if(nodes.containsKey(nodePath)) {
                Object node = nodes.get(nodePath);
                if(node.getClass().isAnnotationPresent(Security.class)) {
                    Security security = node.getClass().getAnnotation(Security.class);
                    if(!Session.isAuthenticated()) {
                        if(request.header("Authorization") != null) {
                            String authentication = request.header("Authorization");
                            if(authentication.contains("Basic")) {
                                LoginService loginService = (LoginService) logicalUnit.getService(security.module());
                                String upass = authentication.substring(authentication.indexOf(" ") + 1);
                                byte[] info = Base64.getDecoder().decode(upass);
                                String credentials = new String(info);
                                String uname = credentials.split(":")[0];
                                String pass = credentials.split(":")[1];
                                if(!loginService.checkCredentials(uname, pass.getBytes())) {
                                    response.setStatus(403);
                                    response.setStatusLine("Unauthorized");
                                    return;
                                }
                                Session.set("_xjs_user", loginService.getUserData(uname, pass.getBytes()));
                            }
                        } else {
                            response.header("WWW-Authenticate", "Basic realm=\"" + nodePath + "\"");
                            response.setStatus(401);
                            response.setStatusLine("Unauthorized");
                            return;
                        }
                    }
                }
                try {
                    Method method = node.getClass().getMethod(request.getMethod().toLowerCase(), HttpRequest.class, HttpResponse.class);
                    int status = (int) method.invoke(node, request, response);
                    response.setStatus(status);
                } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                    response.setStatus(500);
                    response.setStatusLine("Internal Server Error");
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    e.printStackTrace(new PrintStream(byteArrayOutputStream));
                    response.setEntity(byteArrayOutputStream.toByteArray());
                    Server.log.warn("REST call failed ", e);
                }
            } else {
                response.setStatus(404);
                response.setStatusLine("Not Found");
            }
        }
    }

    @Override
    public boolean handles(HttpRequest request) {
        return request.getRequestPath().equals("/rest/nodes/") || nodes.containsKey(request.getRequestPath().substring(12));
    }

    @Override
    public void init(Properties properties) {
        if(logicalUnit != null) {
            JavaCompilerService compilerService = logicalUnit.getService(JavaCompilerService.class);
            Class[] classes = compilerService.getAllClasses(RestNode.class);
            for(Class<?> clazz : classes) {
                RestNode restNode = clazz.getAnnotation(RestNode.class);
                Server.log.info("Found node " + clazz.getName() + "@" + restNode.path());
                Object node = null;
                try {
                    node = clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                nodes.put(restNode.path(), node);
            }
        }
    }

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        RestNodeProvider nodeProvider = new RestNodeProvider();
        nodeProvider.logicalUnit = logicalUnit;
        nodeProvider.nodes = new TreeMap<>();
        return nodeProvider;
    }

    @Override
    public void logInfo() {
        Server.log.info("Rest Node Provider for " + logicalUnit.getName() + " running");
    }

    @Override
    public void setup() {

    }
}
