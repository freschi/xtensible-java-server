package at.femoweb.xjs.modules.xjs;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 11/30/14.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@JResTarget(ElementType.TYPE)
public @interface TagReplacerInfo {

    public String tag();

    public ReplacementType type() default ReplacementType.PROCESS_INTERNAL;
    public enum ReplacementType {
        DROP_INTERNAL, PROCESS_INTERNAL, IGNORE_INTERNAL
    }
}
