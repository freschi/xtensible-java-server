package at.femoweb.xjs.modules.xjs;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 5/10/15.
 */
@JResTarget(ElementType.TYPE)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface XJSTag {

    String value();
}
