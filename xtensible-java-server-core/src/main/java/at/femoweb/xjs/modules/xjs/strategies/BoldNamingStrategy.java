package at.femoweb.xjs.modules.xjs.strategies;

import at.femoweb.html.Document;
import at.femoweb.html.Tag;

/**
 * Created by felix on 5/17/15.
 */
public class BoldNamingStrategy implements NamingStrategy {
    @Override
    public String findName(String path, Document document) {
        Tag root = document.content();
        if(root.attribute("class") != null) {
            return root.attribute("class");
        } else {
            if(path.startsWith("/")) {
                path = path.substring(1);
            }
            if(root.attribute("basePackage") != null) {
                String base = root.attribute("basePackage");
                if(!base.endsWith(".")) {
                    base = base + ".";
                }
                path = base + path;
            }
            if(path.endsWith(".xjs")) {
                path = path.substring(0, path.length() - 4);
            }
            path = path.replace("/", ".");
            String[] parts = path.split("\\.");
            for (int i = 0; i < parts.length; i++) {
                String part = parts[i];
                part = prepareUnderlinedString(part);
                if (i == parts.length - 1) {
                    if (part.charAt(0) >= 'a' || part.charAt(0) <= 'z') {
                        part = ((char) (part.charAt(0) ^ 32)) + part.substring(1);
                    }
                    parts[i] = part;
                } else {
                    parts[i] = part.toLowerCase();
                }
            }
            String name;
            if(parts.length != 0) {
                name = String.join(".", parts);
            } else {
                if (path.charAt(0) >= 'a' || path.charAt(0) <= 'z') {
                    path = ((char) (path.charAt(0) ^ 32)) + path.substring(1);
                }
                name = path;
            }
            return name;
        }
    }

    private String prepareUnderlinedString(String value) {
        while (value.indexOf("_") >= 0) {
            int pos = value.indexOf("_");
            if(pos == 0) {
                value = value.substring(1);
            } else if (pos == value.length() - 1) {
                value = value.substring(0, value.length() - 1);
            } else {
                String part = value.substring(pos + 1, value.length());
                if (part.charAt(0) >= 'a' || part.charAt(0) <= 'z') {
                    part = ((char) (part.charAt(0) ^ 32)) + part.substring(1);
                }
                value = value.substring(0, pos) + part;
            }
        }
        return value;
    }
}
