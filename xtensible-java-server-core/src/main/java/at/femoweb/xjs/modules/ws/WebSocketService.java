package at.femoweb.xjs.modules.ws;

import at.femoweb.config.entries.Group;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.UnifiedUser;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Startable;
import at.femoweb.xjs.lifecycle.Stopable;
import at.femoweb.xjs.modules.JavaRessourceService;
import at.femoweb.xjs.modules.Resources;
import at.femoweb.xjs.modules.link.RequestLink;
import at.femoweb.xjs.services.*;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

/**
 * Created by felix on 3/18/15.
 */
@Service
@Named("ws-service")
public class WebSocketService extends BootableModule implements at.femoweb.xjs.services.Service, StreamHandoverHandler, Initable, Startable, Stopable {

    private Group settings;
    private BootableLogicalUnit logicalUnit;
    private int port = -1;
    private Thread handlingThread;
    private MessageDigest sha1;
    private ArrayList<WebSocketHandlingThread> threads;
    private ArrayList<Class<?>> links;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        WebSocketService service = new WebSocketService();
        service.settings = settings;
        service.logicalUnit = logicalUnit;
        try {
            service.sha1 = MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e) {
            Server.log.warn("Server does not support SHA1, WebSockets not available");
        }
        service.threads = new ArrayList<>();
        service.links = new ArrayList<>();
        return service;
    }

    @Override
    public void logInfo() {
        Server.log.info("WebSocket Service for unit " + logicalUnit.getName());
    }

    @Override
    public void setup() {

    }

    @Override
    public Object put(Object... objects) {
        return null;
    }

    @Override
    public List<String> getPaths() {
        ArrayList<String> regexs = new ArrayList<>();
        for(Class<?> c : links) {
            regexs.add(c.getAnnotation(RequestLink.class).path());
        }
        return regexs;
    }

    @Override
    public void handle(HttpRequest request, Socket socket) {
        HttpResponse response = HttpResponse.getDefaultInstance(request);
        UnifiedUser unifiedUser = null;
        RequestLink link = links.stream().filter(l -> l.getAnnotation(RequestLink.class).path().equals(request.getRequestPath())).findFirst().get().getAnnotation(RequestLink.class);
        /*if(link.authenticate() && request.header("Authorization") == null) {
            response.header("WWW-Authenticate", "Basic realm=\"WebSocket " + link.path() + "\"");
            response.setStatus(401);
            response.setStatusLine("Unauthorized");
            try {
                response.writeToStream(socket.getOutputStream());
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if(link.authenticate()) {
                String authentication = request.header("Authorization");
                if(authentication.contains("Basic")) {
                    LoginService loginService = (LoginService) logicalUnit.getService(link.authModule());
                    String upass = authentication.substring(authentication.indexOf(" ") + 1);
                    byte[] info = Base64.getDecoder().decode(upass);
                    String credentials = new String(info);
                    String uname = credentials.split(":")[0];
                    String pass = credentials.split(":")[1];
                    if(!loginService.checkCredentials(uname, pass.getBytes())) {
                        response.setStatus(403);
                        response.setStatusLine("Unauthorized");
                        try {
                            response.writeToStream(socket.getOutputStream());
                            socket.close();
                        } catch (IOException e) {
                            Server.log.warn("Could not send response to client", e);
                        }
                        return;
                    }
                    unifiedUser = loginService.getUserData(uname, pass.getBytes());
                } else {
                    response.setStatus(403);
                    response.setStatusLine("Unauthorized");
                    try {
                        response.writeToStream(socket.getOutputStream());
                        socket.close();
                    } catch (IOException e) {
                        Server.log.warn("Could not send response to client", e);
                    }
                    return;
                }
            }*/
            if (sha1 != null) {
                response.header("Connection", "Upgrade");
                response.header("Upgrade", "websocket");
                response.header("Protocol", "terminal");
                try {
                    byte[] bytes = sha1.digest((request.header("Sec-WebSocket-Key") + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes("UTF-8"));
                    response.header("Sec-WebSocket-Accept", Base64.getEncoder().encodeToString(bytes));
                } catch (UnsupportedEncodingException e) {
                    Server.log.fatal("Server does not support UTF-8", e);
                    Server.shutdown();
                }
                WebSocketHandlingThread thread = new WebSocketHandlingThread(logicalUnit, socket, request, links, unifiedUser);
                threads.add(thread);
                thread.start();
                response.setStatus(101);
                response.setStatusLine("Switching Protocols");
            } else {
                response.setStatus(500);
                response.setStatusLine("Internal Server Error");
            }
            try {
                response.writeToStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        //}
    }

    @Override
    public void init(Properties properties) {
        if(logicalUnit != null) {
            if (logicalUnit.hasService("http")) {
                ConnectionService httpService = (ConnectionService) logicalUnit.getService("http");
                if (httpService instanceof StreamHandoverService) {
                    Server.log.info("Handover possible, registering handover handler");
                    ((StreamHandoverService) httpService).registerHandler(this);
                } else {
                    Server.log.warn("HTTP Service can not hand streams over. WebSockets would need to launch an own instance.");
                    if (settings.has("launchOwn") && settings.has("port")) {
                        if (settings.getBoolean("launchOwn")) {
                            port = settings.getInt("port");
                            Server.log.info("Launching HTTP Service on port " + port);
                        }
                    }
                }
            } else {
                Server.log.warn("No HTTP service found");
                if (settings.has("port")) {
                    port = settings.getInt("port");
                    Server.log.info("Launching HTTP Service on port " + port);
                }
            }
            if (port > 0) {
                handlingThread = new Thread(new HttpReceiverThread());
            }
            if(logicalUnit.hasService("jres-service")) {
                JavaRessourceService javaRessourceService = (JavaRessourceService) logicalUnit.getService("jres-service");
                Class<?>[] classes = javaRessourceService.getAllClasses(RequestLink.class);
                for(Class<?> c : classes) {
                    RequestLink link = c.getAnnotation(RequestLink.class);
                    if(link.connectionType() == RequestLink.Type.WEB_SOCKET) {
                        Server.log.info("Found request link for " +  c.getName() + "@" + link.path());
                        links.add(c);
                    }
                }
            }
        }
    }

    @Override
    public void start() {
        if(port > 0)
            handlingThread.start();
    }

    @Override
    public void shutdown(Properties properties) {
        if(port > 0)
            handlingThread.interrupt();
        if(threads != null) {
            for (Thread thread : threads) {
                thread.interrupt();
            }
        }
    }

    public class HttpReceiverThread implements Runnable {

        @Override
        public void run() {
            try {
                ServerSocket serverSocket = new ServerSocket(port, 100);
                while(true) {
                    Socket socket = serverSocket.accept();
                    HttpRequest request = HttpRequest.readFromStream(socket.getInputStream());
                    if(request.header("Upgrade") != null) {
                        if(request.header("Upgrade").equals("websocket")) {
                            handle(request, socket);
                            continue;
                        }
                    }
                    HttpResponse response = HttpResponse.getDefaultInstance(request);
                    response.setStatus(400);
                    response.setStatusLine("Bad Request");
                    response.writeToStream(socket.getOutputStream());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
