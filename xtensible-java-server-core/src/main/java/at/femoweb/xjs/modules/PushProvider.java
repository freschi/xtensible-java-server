package at.femoweb.xjs.modules;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.structure.Module;
import com.google.gson.JsonObject;

/**
 * Created by felix on 11/3/14.
 */
@Named("push")
@ContentProvider(regex = "\\/xjs\\/push\\/.*\\.push\\??.*")
public class PushProvider extends Module implements Provider {

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        JsonObject json = new JsonObject();
        if(Push.get().empty()) {
            json.addProperty("state", "none");
        } else {
            json.addProperty("state", "found");
            json.add("updates", Push.get().getUpdates());
            Push.get().clear();
        }
        response.setStatus(200);
        response.header("Content-Type", "application/json");
        response.setEntity(json.toString());
    }

    @Override
    public boolean handles(HttpRequest request) {
        return request.getRequestPath().toLowerCase().startsWith("/xjs/push/session.push");
    }
}
