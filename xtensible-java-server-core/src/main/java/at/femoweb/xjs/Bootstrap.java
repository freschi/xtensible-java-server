package at.femoweb.xjs;

import at.femoweb.config.Catalog;
import at.femoweb.config.Config;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.internal.BootablePhysicalUnit;
import at.femoweb.xjs.services.JavaCompilerService;
import at.femoweb.xjs.structure.BootstrapScriptInterface;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.PhysicalUnit;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

/**
 * Created by felix on 1/20/15.
 */
public class Bootstrap {

    private static final Logger log = LogManager.getLogger("Bootstrapper");

    public static void main(String[] args) {
        log.debug("Placing virtual machine shutdown hook");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                Properties properties = new Properties();

                properties.setProperty("shutdown.reason", "interrupt");
                properties.setProperty("shutdown.type", "gracefull");
                Server.log.info("Entering Runlevel 5 - Shutdown");
                if (Server.physicalUnit != null) {
                    Server.physicalUnit.shutdown(properties);
                }
                Server.notifyServerStopped();
                log.info("Entered Runlevel 6 - Halt");
                log.info("Unlocking Directory");
                Server.unlockDir(Server.getLockString());
            }
        });
        log.debug("Placing uncaught exception handler");
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                throwable.printStackTrace();
                Server.notifyUncaughtException(thread, throwable);
            }
        });
        Catalog global = Config.openCatalog("global", true);
        Group startup = global.openArticle("server").openParagraph("startup").openEntry();
        log.info("Startup method is " + startup.getString("method").toUpperCase());
        if(Server.isLocked()) {
            log.warn("Other instance running. Please shut other instance down or remove lockfile");
            System.exit(1);
        }
        log.info("Entering Runlevel 0 - Boot");
        log.info("Locking directory");
        Server.lockDir();
        if(startup.getBoolean("first")) {
            log.info("Starting FIRST_BOOT.js");
            ScriptEngineManager engineManager = new ScriptEngineManager();
            ScriptEngine engine = engineManager.getEngineByName("nashorn");
            engine.put("$", new BootstrapScriptInterface(global));
            try {
                URL res = Bootstrap.class.getResource("FIRST_BOOT.js");
                Server.log.info("Running " + res);
                engine.eval(new InputStreamReader(Bootstrap.class.getResourceAsStream("FIRST_BOOT.js")));
            } catch (ScriptException e) {
                e.printStackTrace();
            } catch (Throwable t) {
                Server.log.fatal("Could not execute FIRST_BOOT.js", t);
                return;
            }
            startup.setBoolean("first", false);
        }
        if(startup.getString("method").equalsIgnoreCase("boot")) {
            log.info("Preparing Loader Service");
            Group loader = startup.getGroup("loader");
            JavaCompilerService service;
            try {
                Class<?> loaderClass = Class.forName(loader.getString("class"));
                log.debug("Loaded " + loaderClass.toGenericString());
                Module jres = (Module) loaderClass.newInstance();
                log.info("JRES Service "  + jres.getName() + " created");
                service = (JavaCompilerService) jres;
                log.debug("Compiling startup sources");
                service.compileAllClasses();
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                log.fatal("Could not instantiate loader", e);
                System.exit(-1);
                return;
            }
            for(String pack : service.getPackages()) {
                log.debug("Loaded package " + pack);
            }
            log.info("Starting server " + startup.getString("name"));
            PhysicalUnit physicalUnit = Server.physicalUnit = new BootablePhysicalUnit(startup.getString("name"), service);
            Group machine = global.openArticle("server").openParagraph("machine").openEntry();
            List units = machine.getList("logicalUnits");
            for (int i = 0; i < units.count(); i++) {
                Group logical = units.getGroup(i);
                String msg = "Found unit " + logical.getString("name");
                msg += " (";
                List aliases = logical.getList("aliases");
                for (int j = 0; j < aliases.count(); j++) {
                    msg += aliases.getString(j);
                    if(j != aliases.count() - 1) {
                        msg += ", ";
                    }
                }
                msg += ")";
                log.info(msg);
                BootableLogicalUnit logicalUnit = new BootableLogicalUnit(logical);
                logicalUnit.bootInit();
                physicalUnit.addLogical(logicalUnit);
            }
            log.info("Running init scripts");
            log.info("Done");
            log.info("Entering Runlevel 1 - Init");
            physicalUnit.init(null);
            log.info("Entering Runlevel 2 - Start");
            physicalUnit.start();
            log.info("Entering Runlevel 3 - Running");
            Server.notifyServerStarted();
        }
    }
}
