package at.femoweb.xjs.provider;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;

/**
 * Created by felix on 10/15/14.
 */
public interface Provider {

    public abstract void handle(HttpRequest request, HttpResponse response);
    public abstract boolean handles(HttpRequest request);
}
