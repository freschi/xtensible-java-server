package at.femoweb.xjs;

import at.femoweb.xjs.services.NotificationService;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.PhysicalUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by felix on 10/10/14.
 */
public abstract class Server {

    public static Logger log = LogManager.getLogger(Server.class);
    public static PhysicalUnit physicalUnit;
    private static ArrayList<NotificationService> notifiers = new ArrayList<NotificationService>();
    private static long startupTime;

    public static void addNotificationService(Module notificationService) {
        if(notificationService instanceof NotificationService)
            notifiers.add((NotificationService) notificationService);
    }

    public static void removeNotificationService(Module notificationService) {
        if(notificationService instanceof NotificationService) {
            if(notifiers.contains(notificationService))
                notifiers.remove(notificationService);
        }
    }

    public static void notifyServerStarted() {
        for(NotificationService notificationService : notifiers) {
            notificationService.notifyServerStarted();
        }
    }

    public static void notifyUncaughtException(Thread thread, Throwable t) {
        notifyUncaughtException(thread, t, true);
    }

    public static void notifyUncaughtException(Thread thread, Throwable t, boolean restart) {
        t.printStackTrace();
        for(NotificationService notificationService : notifiers) {
            notificationService.notifyUncaughtException(thread, t);
        }
        if(restart) {
            Properties properties = new Properties();
            properties.setProperty("exit.code", "500");
            properties.setProperty("exit.reason", "exception.uncaught");
            properties.setProperty("exit.info", "start.following");
            physicalUnit.shutdown(properties);
            notifyServerStopped();
            physicalUnit.start();
            notifyServerStarted();
        }
    }

    public static void notifyServerStopped() {
        for(NotificationService notificationService : notifiers) {
            notificationService.notifyServerStopped();
        }
    }

    public static void shutdown () {
        Properties properties = new Properties();
        properties.setProperty("exit.code", "200");
        properties.setProperty("exit.reason", "exit.gracefull");
        properties.setProperty("exit.info", "start.new_instance");
        physicalUnit.shutdown(properties);
        notifyServerStopped();
        unlockDir(getLockString());
    }

    public static void reload () {
        Properties properties = new Properties();
        properties.setProperty("exit.code", "100");
        properties.setProperty("exit.reason", "exit.gracefull");
        properties.setProperty("exit.info", "start.reload");
        physicalUnit.shutdown(properties);
        physicalUnit.reload();
        physicalUnit.start();
    }

    public static void lockDir () {
        File lockfile = new File(".lock");
        try {
            PrintStream printStream = new PrintStream(lockfile);
            printStream.println(System.currentTimeMillis() + "");
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static boolean isLocked () {
        File lockfile = new File(".lock");
        return lockfile.exists();
    }

    public static void stop () {
        shutdown();
        Server.log.info("Server has shut down. Deytroying Virtual Machine");
        System.exit(0);
    }

    public static String getLockString() {
        File lockfile = new File(".lock");
        if(lockfile.exists()) {
            try (BufferedReader din = new BufferedReader(new InputStreamReader(new FileInputStream(lockfile)))) {
                return din.readLine();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void unlockDir (String lockString) {
        try {
            if (getLockString().equals(lockString)) {
                File lockfile = new File(".lock");
                lockfile.delete();
            }
        } catch (NullPointerException e) {

        }
    }

    public static long getStartupTime() {
        return startupTime;
    }

    public static void setStartupTime(long startupTime) {
        Server.startupTime = startupTime;
    }
}
