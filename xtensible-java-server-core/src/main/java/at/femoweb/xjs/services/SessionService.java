package at.femoweb.xjs.services;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;

/**
 * Created by felix on 10/17/14.
 */
public interface SessionService extends Service {

    public abstract void setupSession(HttpRequest request, HttpResponse response);
    public abstract void requestNewSession(HttpRequest request, HttpResponse response);
    public abstract void destroySession(HttpResponse response);

}
