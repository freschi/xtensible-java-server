package at.femoweb.xjs.services;

/**
 * Created by felix on 10/14/14.
 */
public interface ConnectionService extends Service{

    public abstract int getThreadCount();
    public abstract int getConnectionCount();
}
