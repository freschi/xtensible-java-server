package at.femoweb.xjs.services;

import at.femoweb.xjs.modules.AnnotationHandlerBundle;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * Created by felix on 1/21/15.
 */
public interface JavaCompilerService extends Service {

    public abstract void compileAllClasses();
    public abstract Class<?>[] getAllClasses(Class<? extends Annotation> annotation);
    public abstract Method[] getAllMethods(Class<? extends Annotation> annotation);
    public abstract Field[] getAllFields(Class<? extends Annotation> annotation);
    public abstract void addLoadListener(AnnotationHandlerBundle<? extends Annotation> bundle);
    public abstract Set<String> getPackages();
}
