package at.femoweb.xjs.services;

import at.femoweb.data.core.ConnectionFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by felix on 2/16/15.
 */
public interface DataConnectionService extends Service {

    public Connection connectDataSource(Properties properties) throws SQLException;
    public ConnectionFactory connectDataCore(Properties properties) throws SQLException;
}
