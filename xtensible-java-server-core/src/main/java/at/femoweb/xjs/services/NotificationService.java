package at.femoweb.xjs.services;

/**
 * Created by felix on 10/16/14.
 */
public interface NotificationService extends Service {

    public abstract void notifyServerStarted();
    public abstract void notifyUncaughtException(Thread thread, Throwable throwable);
    public abstract void notifyServerStopped();
}
