package at.femoweb.xjs.services;

import at.femoweb.http.HttpRequest;
import at.femoweb.xjs.modules.HandoverFailedException;
import at.femoweb.xjs.structure.LogicalUnit;

import java.net.Socket;
import java.util.List;

/**
 * Created by felix on 3/18/15.
 */
public interface StreamHandoverHandler {

    public abstract List<String> getPaths();
    public abstract void handle(HttpRequest request, Socket socket) throws HandoverFailedException;
}
