package at.femoweb.xjs.services;

/**
 * Created by felix on 2/16/15.
 */
public interface RegistrationService extends Service {

    public boolean register(String username, byte[] password, String firstname, String lastname, String display, String email);
}
