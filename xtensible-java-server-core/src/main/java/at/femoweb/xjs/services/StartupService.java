package at.femoweb.xjs.services;

/**
 * Created by felix on 2/8/15.
 */
public interface StartupService extends Service {
    
    public abstract boolean onStartup();
}
