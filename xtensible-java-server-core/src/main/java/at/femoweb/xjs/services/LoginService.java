package at.femoweb.xjs.services;

import at.femoweb.xjs.UnifiedUser;

/**
 * Created by felix on 10/14/14.
 */
public interface LoginService extends Service{

    public abstract boolean checkCredentials(String username, byte[] password);
    public abstract UnifiedUser getUserData(String username, byte[] password);
}
