package at.femoweb.xjs.services;

/**
 * Created by felix on 3/18/15.
 */
public interface StreamHandoverService {

    public abstract boolean registerHandler(StreamHandoverHandler handler);
}
