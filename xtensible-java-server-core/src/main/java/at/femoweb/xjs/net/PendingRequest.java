package at.femoweb.xjs.net;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;

import java.net.Socket;

/**
 * Created by felix on 10/15/14.
 */
public class PendingRequest {

    private HttpRequest request;
    private HttpResponse response;
    private Socket socket;
    private State state;

    public PendingRequest(HttpRequest request, HttpResponse response, Socket socket) {
        this.request = request;
        this.response = response;
        this.socket = socket;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public void setRequest(HttpRequest request) {
        this.request = request;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public void setResponse(HttpResponse response) {
        this.response = response;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        Server.log.debug("Pending Request " + getRequest().getRequestLine() + " " + state);
        this.state = state;
    }
    public enum State {
        UNKNOWN, PENDING, FINISHED
    }
}
