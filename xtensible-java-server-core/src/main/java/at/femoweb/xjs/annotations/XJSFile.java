package at.femoweb.xjs.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 5/17/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@JResTarget(ElementType.TYPE)
@Target(ElementType.TYPE)
public @interface XJSFile {

    String path();
    long timestamp();
}
