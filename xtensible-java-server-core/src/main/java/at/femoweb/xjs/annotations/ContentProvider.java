package at.femoweb.xjs.annotations;

import at.femoweb.xjs.structure.ContentProviderType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 10/13/14.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@JResTarget(ElementType.TYPE)
public @interface ContentProvider {
    public String regex() default "";
    public ContentProviderType type() default ContentProviderType.CONTENT;
}
