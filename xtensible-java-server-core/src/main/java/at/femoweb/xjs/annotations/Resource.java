package at.femoweb.xjs.annotations;

import java.lang.annotation.*;

/**
 * Created by felix on 1/6/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
@JResTarget(ElementType.TYPE)
public @interface Resource {
    public String value();
}
