package at.femoweb.xjs;

import at.femoweb.xjs.internal.AbstractPhysicalUnit;
import at.femoweb.xjs.internal.PhysicalUnitImpl;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.io.*;
import java.util.Properties;

/**
 * Created by felix on 10/10/14.
 */
public class Startup {

    public static void main(String[] args) {
        if(Server.isLocked()) {
            Server.log.warn("Server directory is locked. There is probably another instance running");
            Server.log.info("Attempting to shut down other instance");
            try {
                Connection.Response response = Jsoup.connect("http://127.0.0.1:8080/xjs/control/shutdown").userAgent("XJS/0.1").header("XJS-Lock-String", Server.getLockString()).ignoreContentType(true).method(Connection.Method.GET).execute();
                if(response.statusCode() == 200) {
                    Server.log.info("Other instance shut down gracefully.");
                    long timeout = Long.parseLong(response.body().substring(0, Integer.parseInt(response.header("Content-Length"))));
                    Server.log.info("Waiting for suggested timeout " + timeout + " ms");
                    try {
                        Thread.sleep(timeout);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Server.log.info("Continuing with XJS Bootstraping Sequence");
                } else {
                    Server.log.fatal("Failed to shutdown other instance! Ports may be blocked! Exiting!");
                }
            } catch (IOException e) {
                Server.log.warn("Could not shut down other instance", e);
            }
        }
        long start = System.currentTimeMillis();
        Server.log.debug("Initiating Startup Sequence");
        Server.log.info("Installing Uncaught Exception Hook");
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                throwable.printStackTrace();
                Server.notifyUncaughtException(thread, throwable);
            }
        });
        Server.log.info("Installing Virtual Machine Shutdown Hook");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                Properties properties = new Properties();
                properties.setProperty("shutdown.reason", "interrupt");
                properties.setProperty("shutdown.type", "gracefull");
                if(Server.physicalUnit != null) {
                    Server.physicalUnit.shutdown(properties);
                }
                Server.log.info("Informing Notifiers about Shutdown");
                Server.notifyServerStopped();
                Server.log.info("Server has shut down!");
                Server.log.info("Unlocking Directory");
                Server.unlockDir(Server.getLockString());
            }
        });
        Server.log.info("Locking Directory");
        Server.lockDir();
        Server.log.debug("Checking for Configuration File in ressources");
        InputStream config;
        if(ClassLoader.getSystemClassLoader().getResource("server.xml") != null) {
            Server.log.debug("Found ressource file server.xml");
            config = ClassLoader.getSystemClassLoader().getResourceAsStream("server.xml");
            Server.log.debug("Openened configuration from ressources");
        } else {
            Server.log.debug("Checking for Configuration in classpath");
            File xml = new File("server.xml");
            if(xml.exists()) {
                Server.log.debug("Found configuration file");
                try {
                    config = new FileInputStream(xml);
                } catch (FileNotFoundException e) {
                    Server.log.fatal("Could not open configuration file", e);
                    System.exit(1);
                    return;
                }
                Server.log.debug("Configuration file opened from classpath");
            } else {
                Server.log.fatal("No configuration file found! Exiting Server!");
                System.exit(2);
                return;
            }
        }
        Server.log.info("Valid configuration found!");
        Server.log.debug("Loading configuration file");
        Document document;
        try {
            document = Jsoup.parse(config, "utf-8", "", Parser.xmlParser());
            Server.log.info("Physical Machine Name: " + document.select("physical").first().attr("name"));
            Server.log.debug("Logical Units: " + document.select("physical").first().select("logical").size());
            int unitNo = 0;
            for(Element element : document.select("physical").first().select("logical")) {
                Server.log.debug("Aliases for Logical Unit " + unitNo);
                for(Element alias : element.select("aliases").first().select("alias")) {
                    Server.log.debug(alias.text());
                }
                Server.log.debug("Services for Logical Unit " + unitNo);
                for(Element service : element.select("services").first().select("module")) {
                    Server.log.debug(service.attr("name"));
                }
                Server.log.debug("Content Provider for Logical Unit " + unitNo);
                for(Element service : element.select("content-providers").first().select("module")) {
                    Server.log.debug(service.attr("name"));
                }
                unitNo++;
            }
            Server.log.debug("Creating Physical Unit");
            AbstractPhysicalUnit abstractPhysicalUnit = new PhysicalUnitImpl();
            abstractPhysicalUnit.setName(document.select("physical").first().attr("name"));
            Server.physicalUnit = abstractPhysicalUnit;
            String file;
            Server.log.debug("Loading Modules File: " + (file = document.select("physical").first().select("modules").first().attr("file")));
            Document modules = Jsoup.parse(new FileInputStream(file), "utf-8", "", Parser.xmlParser());
            Server.log.debug("Loading Modules");
            abstractPhysicalUnit.loadModules(modules);
            Server.log.info("Loaded Modules");
            Server.log.debug("Running through Logical Units");
            for(Element logical : document.select("physical").first().select("logical")) {
                abstractPhysicalUnit.addLogical(logical);
            }
            Server.log.debug("Reading configuration for physical unit");
            Properties init = new Properties();
            for(Element property : document.select("physical").first().select("config").first().select("property")) {
                init.setProperty(property.attr("name"), property.text());
                Server.log.debug("Found property " + property.attr("name") + "=" + property.text());
            }
            Server.log.debug(abstractPhysicalUnit.toString());
            Server.log.info("Initiating Server Modules");
            abstractPhysicalUnit.init(init);
            Server.log.info("Starting Server Modules");
            abstractPhysicalUnit.start();
            Server.log.debug("Informing Notifiers about Server Start");
            Server.notifyServerStarted();
        } catch (IOException e) {
            Server.log.fatal("Error while parsing XML Configuration", e);
            System.exit(3);
            return;
        }
        Server.log.debug("Closing configuration file");
        try {
            config.close();
        } catch (IOException e) {
            Server.log.warn("Error while closing configuration file", e);
        }
        if(document != null)
            Server.log.error("Server on Machine " + document.select("physical").first().attr("name") + " has successfully started!");
        long end = System.currentTimeMillis();
        Server.setStartupTime(end - start);
        Server.log.info("Startup took " + Server.getStartupTime() + " ms");
    }
}
