package at.femoweb.xjs.dependencies;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by felix on 1/21/15.
 */
@Target(ElementType.TYPE)
@JResTarget(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dependency {
    public String value();
}
