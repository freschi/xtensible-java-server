package at.femoweb.xjs.dependencies;

import at.femoweb.xjs.annotations.JResTarget;

import java.lang.annotation.*;

/**
 * Created by felix on 1/21/15.
 */
@Target(ElementType.TYPE)
@JResTarget(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Dependencies {
    public Dependency[] value();
}
