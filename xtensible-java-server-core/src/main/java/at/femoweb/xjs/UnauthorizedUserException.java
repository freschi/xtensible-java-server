package at.femoweb.xjs;

/**
 * Created by felix on 1/7/15.
 */
public class UnauthorizedUserException extends RuntimeException {

    public enum Reason {
        AUTHENTICATION_FAILURE
    }

    public UnauthorizedUserException (Reason reason) {
        super(generateMessage(reason));
    }

    private static String generateMessage(Reason reason) {
        switch (reason) {
            case AUTHENTICATION_FAILURE:
                return "Authentication failed in a previous step. This server may be compromised!";
            default:
                return "Generic Authentication failure. This may mean your java is retarded or this server has been updated badly";
        }
    }
}
