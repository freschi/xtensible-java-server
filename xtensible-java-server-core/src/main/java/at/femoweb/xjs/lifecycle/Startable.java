package at.femoweb.xjs.lifecycle;

/**
 * Created by felix on 10/14/14.
 */
public interface Startable {

    public abstract void start();
}
