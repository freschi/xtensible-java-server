package at.femoweb.xjs.lifecycle;

/**
 * Created by felix on 10/31/14.
 */
public interface Reloadable {

    public abstract void reload();
}
