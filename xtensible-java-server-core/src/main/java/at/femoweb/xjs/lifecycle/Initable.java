package at.femoweb.xjs.lifecycle;

import java.util.Properties;

/**
 * Created by felix on 10/14/14.
 */
public interface Initable {

    public abstract void init(Properties properties);
}
