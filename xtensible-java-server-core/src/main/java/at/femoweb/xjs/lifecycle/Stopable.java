package at.femoweb.xjs.lifecycle;

import java.util.Properties;

/**
 * Created by felix on 10/14/14.
 */
public interface Stopable {

    public abstract void shutdown(Properties properties);
}
