package at.femoweb.xjs.lifecycle;

import java.util.Properties;

/**
 * Created by felix on 11/13/14.
 */
public interface StatefullElement extends Initable, Startable, Reloadable, Stopable {

    public abstract void preInit(Properties properties);
    public abstract void postInit(Properties properties);
    public abstract void preStart();
    public abstract void postStart();
    public abstract void preReload();
    public abstract void postReload();
    public abstract void preShutdown(Properties properties);
    public abstract void postShutdown(Properties properties);

}
