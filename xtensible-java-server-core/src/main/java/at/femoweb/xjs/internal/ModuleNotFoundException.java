package at.femoweb.xjs.internal;

/**
 * Created by felix on 5/26/15.
 */
public class ModuleNotFoundException extends RuntimeException {

    public ModuleNotFoundException(String name) {
        super("Module not found: " + name);
    }

}
