package at.femoweb.xjs.internal;

import at.femoweb.config.Config;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.lifecycle.Initable;
import at.femoweb.xjs.lifecycle.Reloadable;
import at.femoweb.xjs.lifecycle.Startable;
import at.femoweb.xjs.lifecycle.Stopable;
import at.femoweb.xjs.services.JavaCompilerService;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.PhysicalUnit;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 1/21/15.
 */
public class BootablePhysicalUnit implements PhysicalUnit {

    private String name;
    private JavaCompilerService service;
    private HashMap<String, Module> modules;
    private HashMap<String, LogicalUnit> logicalUnits;

    public BootablePhysicalUnit (String name, JavaCompilerService service) {
        this.service = service;
        this.name = name;
        this.logicalUnits = new HashMap<>();
        loadModules();
    }

    private void loadModules() {
        this.modules = new HashMap<>();
        Group modules = Config.openCatalog("global").openArticle("server").openParagraph("modules").openEntry();
        List shipped = modules.getList("shipped");
        for (int i = 0; i < shipped.count(); i++) {
            Group module = shipped.getGroup(i);
            String name = module.getString("name");
            String clazz = module.getString("class");
            Server.log.debug("Found module " + name + "@" + clazz);
            try {
                Class<?> modClass = Class.forName(clazz);
                Module mod = (Module) modClass.newInstance();
                if(mod.isNamed() && mod.getName().equals(name)) {
                    if(mod.isService()) {
                        Server.log.info("Found module " + mod.getName() + " which is a Service");
                    } else if (mod.isContentProvider()) {
                        Server.log.info("Found module " + mod.getName() + " which is a Content Provider");
                    } else {
                        Server.log.warn("Module " + mod.getName() + " is neither service nor content provider");
                        continue;
                    }
                    if(mod instanceof BootableModule) {
                        ((BootableModule) mod).setup();
                    }
                    this.modules.put(name, mod);
                } else {
                    Server.log.warn("Module " + clazz + " was illegally named");
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
                Server.log.warn("Could not load module " + name, e);
                Server.log.warn("Blacklisting module " + name);
            }
        }
        Server.log.info("Loading compiled classes");
        Class<?>[] classes = service.getAllClasses(Named.class);
        for(Class clazz : classes) {
            try {
                Module mod = (Module) clazz.newInstance();
                if(mod.isService()) {
                    Server.log.debug("Found module " + mod.getName() + " which is a Service");
                } else if (mod.isContentProvider()) {
                    Server.log.debug("Found module " + mod.getName() + " which is a Content Provider");
                } else {
                    Server.log.warn("Module " + mod.getName() + " is neither service nor content provider");
                    continue;
                }
                if(mod instanceof BootableModule) {
                    ((BootableModule) mod).setup();
                }
                this.modules.put(mod.getName(), mod);
            } catch (InstantiationException | IllegalAccessException e) {
                Server.log.warn("Could not load module " + clazz.getName(), e);
            }
        }
        Server.log.info("All machine modules loaded.");
    }

    @Override
    public void addLogical(Element unit) {
        Server.log.warn("Using legacy way of adding logical unit has been discouraged by default");
    }

    @Override
    public void addLogical(LogicalUnit unit) {
        Server.log.debug("Adding logical unit " + unit.getName());
        logicalUnits.put(unit.getName(), unit);
    }

    @Override
    public Module getModule(String name) {
        if(!modules.containsKey(name))
            throw new ModuleNotFoundException(name);
        return modules.get(name);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public HashMap<String, Module> getModules() {
        return null;
    }

    @Override
    public Collection<LogicalUnit> getLogicalUnits() {
        return logicalUnits.values();
    }

    @Override
    public void init(Properties properties) {
        for(Module module : modules.values()) {
            if(module instanceof Initable) {
                Server.log.debug("Initailizing global module " + module.getName());
                ((Initable) module).init(properties);
            }
        }
        for(LogicalUnit logicalUnit : logicalUnits.values()) {
            Server.log.debug("Initializing logical unit " + logicalUnit.getName());
            logicalUnit.init(properties);
        }
    }

    @Override
    public void start() {
        for(Module module : modules.values()) {
            if(module instanceof Startable) {
                Server.log.debug("Starting global module " + module.getName());
                ((Startable) module).start();
            }
        }
        for(LogicalUnit logicalUnit : logicalUnits.values()) {
            Server.log.debug("Starting logical unit " + logicalUnit.getName());
            logicalUnit.start();
        }
    }

    @Override
    public void reload() {
        for(Module module : modules.values()) {
            if(module instanceof Reloadable) {
                Server.log.debug("Reloading global module " + module.getName());
                ((Reloadable) module).reload();
            }
        }
        for(LogicalUnit logicalUnit : logicalUnits.values()) {
            Server.log.debug("Reloading logical unit " + logicalUnit.getName());
            logicalUnit.reload();
        }
    }

    @Override
    public void shutdown(Properties properties) {
        for(Module module : modules.values()) {
            if(module instanceof Stopable) {
                Server.log.debug("Stopping global module " + module.getName());
                ((Stopable) module).shutdown(properties);
            }
        }
        for(LogicalUnit logicalUnit : logicalUnits.values()) {
            Server.log.debug("Stopping logical unit " + logicalUnit.getName());
            logicalUnit.shutdown(properties);
        }
        try {
            Config.openCatalog("global").export();
        } catch (IOException e) {
            Server.log.warn("Could not store configuration", e);
        }
    }
}
