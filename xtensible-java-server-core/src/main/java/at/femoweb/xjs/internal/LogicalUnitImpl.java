package at.femoweb.xjs.internal;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.lifecycle.*;
import at.femoweb.xjs.modules.AnnotationHandler;
import at.femoweb.xjs.modules.AnnotationHandlerBundle;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.SessionNotFoundException;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.SessionService;
import at.femoweb.xjs.services.StartupService;
import at.femoweb.xjs.structure.ContentProviderType;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Created by felix on 10/15/14.
 */
public class LogicalUnitImpl extends AbstractLogicalUnit {

    @Override
    public void init(Properties properties) {
        synchronized (getServices()) {
            for (Module service : getServices()) {
                if (service instanceof StatefullElement) {
                    ((StatefullElement) service).preInit(properties);
                }
            }
        }
        synchronized (getContentProviders()) {
            for (Module provider : getContentProviders()) {
                if (provider instanceof StatefullElement) {
                    ((StatefullElement) provider).preInit(properties);
                }
            }
        }
        synchronized (getServices()) {
            for (Module service : getServices()) {
                Server.log.debug("Initializing Service " + service.getName() + " for Logical Unit " + this.getName());
                if (service instanceof Initable) {
                    ((Initable) service).init(properties);
                }
            }
        }
        synchronized (getContentProviders()) {
            for (Module provider : getContentProviders()) {
                Server.log.debug("Initializing Content Provider " + provider.getName() + " for Logical Unit " + this.getName());
                if (provider instanceof Initable) {
                    ((Initable) provider).init(properties);
                }
            }
        }
        synchronized (getServices()) {
            if (hasService("jres-service")) {
                getService("jres-service").put(new AnnotationHandlerBundle(ContentProvider.class, new AnnotationHandler<ContentProvider>() {
                    @Override
                    public void foundType(Class<ContentProvider> annotation, Class<?> type) {
                        if (type.isAnnotationPresent(Named.class)) {
                            Server.log.info("Found named Content Provider " + type.getAnnotation(Named.class).value());
                            try {
                                Module provider = (Module) type.newInstance();
                                if (provider instanceof Initable) {
                                    ((Initable) provider).init(properties);
                                }
                                synchronized (getContentProviders()) {
                                    Iterator<Module> iterator = getContentProviders().iterator();
                                    while (iterator.hasNext()) {
                                        Module module = iterator.next();
                                        if (module.getName() != null && module.getName().equals(provider.getName())) {
                                            iterator.remove();
                                        }
                                    }
                                    getContentProviders().add(provider);
                                }
                            } catch (InstantiationException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (ClassCastException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void foundMethod(Class<ContentProvider> annotation, Method method) {

                    }

                    @Override
                    public void foundField(Class<ContentProvider> annotation, Field field) {

                    }
                }), new AnnotationHandlerBundle(Service.class, new AnnotationHandler<Service>() {
                    @Override
                    public void foundType(Class<Service> annotation, Class<?> type) {
                        Server.log.info("Found named Service " + type.getAnnotation(Named.class).value());
                        if (type.isAnnotationPresent(Named.class)) {
                            try {
                                Module provider = (Module) type.newInstance();
                                if (provider instanceof Initable) {
                                    ((Initable) provider).init(properties);
                                }
                                synchronized (getServices()) {
                                    Iterator<Module> iterator = getServices().iterator();
                                    while (iterator.hasNext()) {
                                        Module module = iterator.next();
                                        if (module.getName() != null && module.getName().equals(provider.getName())) {
                                            iterator.remove();
                                        }
                                    }
                                    getServices().add(provider);
                                }
                            } catch (InstantiationException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (ClassCastException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void foundMethod(Class<Service> annotation, Method method) {

                    }

                    @Override
                    public void foundField(Class<Service> annotation, Field field) {

                    }
                }));
            }
        }
        if(hasService("jres-service")) {
            ((StatefullElement)getService("jres-service")).postInit(properties);
        }
        try {
            synchronized (getServices()) {
                for (Module service : getServices()) {
                    if (service instanceof StatefullElement && !service.getName().equalsIgnoreCase("jres-service")) {
                        ((StatefullElement) service).postInit(properties);
                    }
                }
            }
            for (Module provider : getContentProviders()) {
                if (provider instanceof StatefullElement) {
                    ((StatefullElement) provider).postInit(properties);
                }
            }
        } catch (ConcurrentModificationException e) {
            Server.log.warn("Concurrent module error", e);
        }
    }

    @Override
    public void start() {
        for(Module service : getServices()) {
            if(service instanceof StartupService) {
                ((StartupService) service).onStartup();
            }
        }
        for(Module service : getServices()) {
            if(service instanceof StatefullElement) {
                ((StatefullElement)service).preStart();
            }
        }
        for(Module provider : getContentProviders()) {
            if(provider instanceof  StatefullElement) {
                ((StatefullElement)provider).preStart();
            }
        }
        for(Module service : getServices()) {
            Server.log.info("Starting Service " + service.getName() + " for Logical Unit " + this.getName());
            if(service instanceof Startable) {
                ((Startable)service).start();
            }
        }
        for(Module provider : getContentProviders()) {
            Server.log.info("Starting Content Provider " + provider.getName() + " for Logical Unit " + this.getName());
            if(provider instanceof Startable)
                ((Startable)provider).start();
        }
        for(Module service : getServices()) {
            if(service instanceof StatefullElement) {
                ((StatefullElement)service).postStart();
            }
        }
        for(Module provider : getContentProviders()) {
            if(provider instanceof  StatefullElement) {
                ((StatefullElement)provider).postStart();
            }
        }
    }

    @Override
    public void reload() {
        for(Module service : getServices()) {
            if(service instanceof StatefullElement) {
                ((StatefullElement)service).preReload();
            }
        }
        for(Module provider : getContentProviders()) {
            if(provider instanceof  StatefullElement) {
                ((StatefullElement)provider).preReload();
            }
        }
        for(Module service : getServices()) {
            Server.log.debug("Reloading Service " + service.getName() + " for Logical Unit " + this.getName());
            if(service instanceof Reloadable) {
                ((Reloadable)service).reload();
            }
        }
        for(Module provider : getContentProviders()) {
            Server.log.debug("Reloading Content Provider " + provider.getName() + " for Logical Unit " + this.getName());
            if(provider instanceof Reloadable) {
                ((Reloadable)provider).reload();
            }
        }
        try {
            if(hasService("jres-service")) {
                ((StatefullElement)getService("jres-service")).postReload();
            }
            for (Module service : getServices()) {
                if (service instanceof StatefullElement && !service.getName().equalsIgnoreCase("jres-service")) {
                    ((StatefullElement) service).postReload();
                }
            }
            for (Module provider : getContentProviders()) {
                if (provider instanceof StatefullElement) {
                    ((StatefullElement) provider).postReload();
                }
            }
        } catch (ConcurrentModificationException e) {
            Server.log.warn("Concurrent error while reload", e);
        }
    }

    @Override
    public void shutdown(Properties properties) {
        for(Module service : getServices()) {
            if(service instanceof StatefullElement) {
                ((StatefullElement)service).preShutdown(properties);
            }
        }
        for(Module provider : getContentProviders()) {
            if(provider instanceof  StatefullElement) {
                ((StatefullElement)provider).preShutdown(properties);
            }
        }
        for(Module service : getServices()) {
            Server.log.info("Stopping Service " + service.getName() + " for Logical Unit " + this.getName());
            if(service instanceof Stopable) {
                ((Stopable)service).shutdown(properties);
            }
        }
        for(Module provider : getContentProviders()) {
            Server.log.debug("Stopping Content Provider " + provider.getName() + " for Logical Unit " + this.getName());
            if(provider instanceof Stopable)
                ((Stopable)provider).shutdown(properties);
        }
        for(Module service : getServices()) {
            if(service instanceof StatefullElement) {
                ((StatefullElement)service).postShutdown(properties);
            }
        }
        for(Module provider : getContentProviders()) {
            if(provider instanceof  StatefullElement) {
                ((StatefullElement)provider).postShutdown(properties);
            }
        }
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        synchronized (getServices()) {
            for (Module module : getServices()) {
                if (module.getClass().getAnnotation(Service.class).type() == ServiceType.SESSION && module instanceof SessionService) {
                    try {
                        ((SessionService) module).setupSession(request, response);
                    } catch (SessionNotFoundException e) {
                        ((SessionService) module).requestNewSession(request, response);
                        response.setStatus(599);
                        response.setStatusLine("Session Not Found");
                    }
                }
            }
            try {
                if (hasService("session")) {
                    Session.set("_xjs_logical_unit", this);
                    Session.set("_xjs_http_request", request);
                }
            } catch (SessionNotFoundException e) {

            }
            synchronized (getContentProviders()) {
                for (Module module : getContentProviders()) {
                    if (module.getClass().getAnnotation(ContentProvider.class).type() == ContentProviderType.CONTENT) {
                        Pattern regex = Pattern.compile(module.getClass().getAnnotation(ContentProvider.class).regex());
                        if (regex.matcher(request.getRequestPath()).matches()) {
                            Provider provider = (Provider) module;
                            if (provider.handles(request)) {
                                provider.handle(request, response);
                                if (response.getStatus() == 0) {
                                    response.setStatus(200);
                                }
                                return;
                            }
                        }
                    }
                }
            }
            response.setStatus(422);
            response.setStatusLine("Unprocessable Entity");
            response.setEntity("No Provider could be found for your request".getBytes());
            synchronized (getContentProviders()) {
                for (Module module : getContentProviders()) {
                    if (module.getClass().getAnnotation(ContentProvider.class).type() == ContentProviderType.FALLBACK) {
                        Pattern regex = Pattern.compile(module.getClass().getAnnotation(ContentProvider.class).regex());
                        if (regex.matcher(request.getRequestPath()).matches()) {
                            Provider provider = (Provider) module;
                            if (provider.handles(request)) {
                                if (response.getStatus() != 200) {
                                    provider.handle(request, response);
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}
