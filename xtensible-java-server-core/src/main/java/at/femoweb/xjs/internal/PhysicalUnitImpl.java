package at.femoweb.xjs.internal;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.lifecycle.*;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Created by felix on 10/14/14.
 */
public class PhysicalUnitImpl extends  AbstractPhysicalUnit {

    @Override
    public void init(Properties properties) {
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).preInit(properties);
            }
        }
        for(Module module : getModules().values()) {
            Server.log.debug("Initializing Module " + module.getName());
            if(module instanceof Initable) {
                ((Initable)module).init(properties);
            }
        }
        for(Object unit : getLogicalUnits().stream().distinct().collect(Collectors.toList())) {
            LogicalUnit logicalUnit = (LogicalUnit) unit;
            Server.log.debug("Initializing Unit " + logicalUnit.getName());
            logicalUnit.init(properties);
        }
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).postInit(properties);
            }
        }
    }

    @Override
    public void start() {
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).preStart();
            }
        }
        for(Module module : getModules().values()) {
            Server.log.info("Starting Module " + module.getName());
            if(module instanceof Startable) {
                ((Startable)module).start();
            }
        }
        for(Object unit : getLogicalUnits().stream().distinct().collect(Collectors.toList())) {
            LogicalUnit logicalUnit = (LogicalUnit) unit;
            Server.log.info("Starting Unit " + logicalUnit.getName());
            logicalUnit.start();
        }
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).postStart();
            }
        }
    }

    @Override
    public void reload() {
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).preReload();
            }
        }
        for(Module module : getModules().values()) {
            Server.log.debug("Reloading Module " + module.getName());
            if(module instanceof Reloadable) {
                ((Reloadable)module).reload();
            }
        }
        for(Object unit : getLogicalUnits().stream().distinct().collect(Collectors.toList())) {
            LogicalUnit logicalUnit = (LogicalUnit) unit;
            Server.log.debug("Reloading Unit " + logicalUnit.getName());
            logicalUnit.reload();
        }
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).postReload();
            }
        }
    }

    @Override
    public void shutdown(Properties properties) {
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).preShutdown(properties);
            }
        }
        for(Object unit : getLogicalUnits().stream().distinct().collect(Collectors.toList())) {
            LogicalUnit logicalUnit = (LogicalUnit) unit;
            Server.log.info("Stoping Unit " + logicalUnit.getName());
            logicalUnit.shutdown(properties);
        }
        for(Module module : getModules().values()) {
            Server.log.info("Stop Module " + module.getName());
            if(module instanceof Stopable) {
                ((Stopable)module).shutdown(properties);
            }
        }
        for(Module module : getModules().values()) {
            if(module instanceof StatefullElement) {
                ((StatefullElement)module).postShutdown(properties);
            }
        }
    }

    @Override
    public void addLogical(LogicalUnit unit) {

    }
}
