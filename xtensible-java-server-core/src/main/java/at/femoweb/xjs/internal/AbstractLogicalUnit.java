package at.femoweb.xjs.internal;


import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.structure.LogicalUnit;
import at.femoweb.xjs.structure.Module;

import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by felix on 10/13/14.
 */
public abstract class AbstractLogicalUnit implements LogicalUnit {

    private String[] alias;

    private String name;

    private ArrayList<Module> services;
    private ArrayList<Module> contentProviders;

    public AbstractLogicalUnit() {
        alias = new String[0];
        services = new ArrayList<Module>();
        contentProviders = new ArrayList<Module>();
    }

    public ArrayList<Module> getServices() {
        return services;
    }

    public ArrayList<Module> getContentProviders() {
        /*Server.log.info("Accessing Content Providers");
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        for(StackTraceElement element : stack) {
            System.err.println(element.getClassName() + "." + element.getMethodName() + "(" + element.getClassName() + ".java:" + element.getLineNumber());
        }*/
        synchronized (contentProviders) {
            return contentProviders;
        }
    }

    @Override
    public void addAlias(String alias) {
        String[] tmp = new String[this.alias.length + 1];
        for(int i = 0; i < this.alias.length; i++) {
            tmp[i] = this.alias[i];
        }
        tmp[this.alias.length] = alias;
        this.alias = tmp;
    }

    @Override
    public String[] getAliases() {
        return alias;
    }

    @Override
    public synchronized void addService(Module module, Properties properties) {
        if(module.isService())
            services.add(module.createLogicalInstance(this, properties));
    }

    @Override
    public void addContentProvider(Module module, Properties properties) {
        if(module.isContentProvider())
            contentProviders.add(module.createLogicalInstance(this, properties));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AbstractLogicalUnit{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        return this.getName().equals(((LogicalUnit)o).getName());
    }


    @Override
    public boolean hasProvider(String name) {
        for(Module module : contentProviders) {
            if(module.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasService(String name) {
        for(Module module : services) {
            if(module.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Service getService(String name) {
        for(Module module : services) {
            if(module.getName().equalsIgnoreCase(name) && module instanceof Service) {
                return (Service) module;
            }
        }
        return null;
    }
}
