package at.femoweb.xjs.internal;

import at.femoweb.config.Catalog;
import at.femoweb.config.Config;
import at.femoweb.config.entries.Group;
import at.femoweb.config.entries.List;
import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.lifecycle.*;
import at.femoweb.xjs.modules.JavaRessourceService;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.SessionNotFoundException;
import at.femoweb.xjs.modules.http.HttpHandler;
import at.femoweb.xjs.provider.Provider;
import at.femoweb.xjs.services.Service;
import at.femoweb.xjs.services.SessionService;
import at.femoweb.xjs.services.StartupService;
import at.femoweb.xjs.structure.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by felix on 1/22/15.
 */
public class BootableLogicalUnit implements LogicalUnit {

    private ArrayList<String> aliases;
    private Catalog catalog;
    private String name;

    private ArrayList<Service> services;
    private ArrayList<Provider> contentProviders;
    private TreeSet<PrioritizedHandler> handlers;
    private File rootDir;

    private HashMap<Provider, Pattern> mappings;

    public File getRootDir() {
        return rootDir;
    }

    public BootableLogicalUnit(Group config) {
        this.name = config.getString("name");
        Server.log.debug("Building logical unit catalog for " + name);
        this.catalog = Config.openCatalog(name, true);
        Server.log.debug("Writing catalog for logical unit " + name);
        try {
            this.catalog.export();
        } catch (IOException e) {
            Server.log.warn("Could not write catalog for logical unit " + name, e);
        }
        List list = config.getList("aliases");
        this.aliases = new ArrayList<>();
        for (int i = 0; i < list.count(); i++) {
            aliases.add(list.getString(i));
        }
        services = new ArrayList<>();
        contentProviders = new ArrayList<>();
        handlers = new TreeSet<>();
    }

    @Override
    public void addAlias(String alias) {
        aliases.add(alias);
    }

    @Override
    public String[] getAliases() {
        return aliases.toArray(new String[]{});
    }

    @Override
    public void handle(HttpRequest request, HttpResponse response) {
        for(Service service : services) {
            at.femoweb.xjs.annotations.Service s = service.getClass().getAnnotation(at.femoweb.xjs.annotations.Service.class);
            if(s.type() == ServiceType.SESSION) {
                HttpHandler.log.debug("Session " + request.getRequestLine() + " with " + service.getClass().getName());
                SessionService sessionService = (SessionService) service;
                try {
                    sessionService.setupSession(request, response);
                } catch (SessionNotFoundException e) {
                    sessionService.requestNewSession(request, response);
                    sessionService.setupSession(request, response);
                }
                Session.set("_xjs_logical_unit", this);
                Session.set("_xjs_http_request", request);
                Session.set("_xjs_http_response", response);
            }
        }
        boolean handled = false;
        for(PrioritizedHandler handler : handlers) {
            Provider provider = handler.getProvider();
            ContentProvider contentProvider = provider.getClass().getAnnotation(ContentProvider.class);
            if(contentProvider.type() == ContentProviderType.CONTENT) {
                Pattern pattern = mappings.get(provider);
                if(pattern.matcher(request.getRequestPath()).matches() && provider.handles(request)) {
                    HttpHandler.log.debug("Handling " + request.getRequestLine() + " with " + provider.getClass().getName());
                    handled = true;
                    try {
                        provider.handle(request, response);
                    } catch (Throwable t) {
                        response.setStatus(500);
                        response.setStatusLine("Internal Server Error");
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        t.printStackTrace(new PrintStream(outputStream));
                        response.setEntity(outputStream.toByteArray());
                    }
                    break;
                }
            }
        }
        if(!handled) {
            for(PrioritizedHandler handler : handlers) {
                Provider provider = handler.getProvider();
                ContentProvider contentProvider = provider.getClass().getAnnotation(ContentProvider.class);
                if (handler.getPriority() > 0 && contentProvider.type() == ContentProviderType.FALLBACK) {
                    Pattern pattern = mappings.get(provider);
                    if (pattern.matcher(request.getRequestPath()).matches() && provider.handles(request)) {
                        HttpHandler.log.debug("Handling " + request.getRequestLine() + " with " + provider.getClass().getName());
                        handled = true;
                        try {
                            provider.handle(request, response);
                        } catch (Throwable t) {
                            response.setStatus(500);
                            response.setStatusLine("Internal Server Error");
                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            t.printStackTrace(new PrintStream(outputStream));
                            response.setEntity(outputStream.toByteArray());
                        }
                        break;
                    }
                }
            }
        }
        if(!handled) {
            response.setStatus(404);
            response.setStatusLine("Not Found");
            response.setEntity("The requested ressource could not be served");
        }
        if(response.getStatus() > 399) {
            Optional<PrioritizedHandler> oHandler = handlers.stream().filter(h -> h.getPriority() < 0).findFirst();
            if(oHandler.isPresent()) {
                oHandler.get().getProvider().handle(request, response);
            }
        }
    }

    @Override
    public void addService(Module module, Properties properties) {
        services.add((Service) module);
    }

    @Override
    public void addContentProvider(Module module, Properties properties) {
        contentProviders.add((Provider) module);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {

    }

    protected void createRegexMap() {
        mappings = new HashMap<>();
        for(Provider provider : contentProviders) {
            ContentProvider contentProvider = provider.getClass().getAnnotation(ContentProvider.class);
            Pattern pattern = Pattern.compile(contentProvider.regex());
            mappings.put(provider, pattern);
        }
    }

    @Override
    public Collection<Module> getServices() {
        ArrayList<Module> modules = new ArrayList<>();
        for(Service service : services) {
            modules.add((Module) service);
        }
        return modules;
    }

    @Override
    public Collection<Module> getContentProviders() {
        ArrayList<Module> modules = new ArrayList<>();
        for(Provider provider : contentProviders) {
            modules.add((Module) provider);
        }
        return modules;
    }

    @Override
    public boolean hasProvider(String name) {
        return contentProviders.stream().anyMatch(p -> ((Module)p).getName().equals(name));
    }

    @Override
    public boolean hasService(String name) {
        return services.stream().anyMatch(p -> ((Module) p).getName().equals(name));
    }

    @Override
    public Service getService(String name) {
        try {
            return services.stream().filter(s -> ((Module) s).getName().equals(name)).findFirst().get();
        } catch (NoSuchElementException e) {
            Server.log.warn("Could not find service " + name, e);
            throw new ModuleNotFoundException(name);
        }
    }

    public void bootInit() {
        Group unit = catalog.openArticle("unit").openParagraph("unit").openEntry();
        if(unit.has("dir")) {
            rootDir = new File(unit.getString("dir"));
        } else {
            Server.log.warn("No directory set. Using deprecated fallback default logical unit name");
            rootDir = new File(name);
        }
        Server.log.debug("Logical Unit directory: " + rootDir.getAbsolutePath());
        if (!rootDir.exists()) {
            Server.log.debug("Logical Unit directory does not exists. Creating it!");
            rootDir.mkdirs();
        }
        if(unit.has("services")) {
            List services = unit.getList("services");
            for (int i = 0; i < services.count(); i++) {
                Group service = services.getGroup(i);
                String moduleName = service.getString("module");
                Server.log.debug("Adding module " + moduleName + " to logical unit " + name);
                Module module = Server.physicalUnit.getModule(moduleName);
                if(module.isService()) {
                    if (module instanceof BootableModule) {
                        Server.log.debug("Module is bootable");
                        BootableModule bootableModule = (BootableModule) ((BootableModule) module).getBootableLogicalInstance(this, service.has("settings") ? service.getGroup("settings") : null);
                        bootableModule.logInfo();
                        this.services.add((Service) bootableModule);
                    } else if (module instanceof Module) {
                        Server.log.info("Using legacy settings for module " + moduleName);
                        addService(module, null);
                    } else {
                        Server.log.error("Module " + moduleName + " is not a module!");
                    }
                } else {
                    Server.log.error("Module is no service!");
                }
            }
        }
        if(unit.has("contentProviders")) {
            List providers = unit.getList("contentProviders");
            for(int i = 0; i < providers.count(); i++) {
                Group provider = providers.getGroup(i);
                String moduleName = provider.getString("module");
                Server.log.debug("Adding module " + moduleName + " to logical unit " + name);
                Module module = Server.physicalUnit.getModule(moduleName);
                Module mod = null;
                if(module.isContentProvider()) {
                    if(module instanceof BootableModule) {
                        Server.log.debug("Module is bootable");
                        BootableModule bootableModule = (BootableModule) ((BootableModule) module).getBootableLogicalInstance(this, provider.has("settings") ? provider.getGroup("settings") : null);
                        mod = bootableModule;
                        bootableModule.logInfo();
                        this.contentProviders.add((Provider) bootableModule);
                    } else if (module instanceof  Module) {
                        Server.log.info("Using legacy settings for module " + moduleName);
                        mod = module;
                        addContentProvider(module, null);
                    }
                    if(mod != null && provider.has("priority")) {
                        int priority = 0;
                        try {
                            priority = provider.getInt("priority");
                        } catch (ClassCastException e) {
                            String p = provider.getString("priority");
                            if(p.equals("fallback")) {
                                priority = 0;
                            } else if (p.equals("highest")) {
                                priority = Integer.MAX_VALUE;
                            } else if (p.equals("high")) {
                                priority = Integer.MAX_VALUE / 2;
                            } else if (p.equals("error")) {
                                priority = -1000;
                            }
                        }
                        PrioritizedHandler handler = new PrioritizedHandler(priority, (Provider) mod);
                        handlers.add(handler);
                    }
                } else {
                    Server.log.error("Module is no content provider!");
                }
            }
            Server.log.info("Prioritized handlers");
            for(PrioritizedHandler handler : handlers) {
                Server.log.info(String.format("%11d - %s", handler.getPriority(), handler.getProvider().getClass().getName()));
            }
        }
    }

    @Override
    public void init(Properties properties) {
        for(Service module : services) {
            if(module instanceof StatefullElement) {
                Server.log.debug("Pre-Initializing service " + ((Module)module).getName() + " for unit " + getName());
                ((StatefullElement) module).preInit(properties);
            }
        }
        for(Provider module : contentProviders) {
            if(module instanceof StatefullElement) {
                Server.log.debug("Pre-Initializing service " + ((Module)module).getName() + " for unit " + getName());
                ((StatefullElement) module).preInit(properties);
            }
        }
        if(hasService("jres-service")) {
            JavaRessourceService ressourceService = (JavaRessourceService) getService("jres-service");
            Class<?>[] classes = ressourceService.getAllClasses(at.femoweb.xjs.annotations.Service.class);
            for(Class aClass : classes) {
                if(Module.class.isAssignableFrom(aClass)) {
                    if(aClass.isAnnotationPresent(Named.class) && aClass.isAnnotationPresent(at.femoweb.xjs.annotations.Service.class)) {
                        try {
                            Service service = (Service) aClass.newInstance();
                            if(service instanceof BootableModule) {
                                service = (Service) ((BootableModule)service).getBootableLogicalInstance(this, null);
                            }
                            services.add(service);
                            Server.log.debug("Adding service " + service.getClass().getName() + " to unit " + getName());
                        } catch (InstantiationException e) {
                            Server.log.warn("Could not instantiate " + aClass.getName(), e);
                        } catch (IllegalAccessException e) {
                            Server.log.warn("Could not access " + aClass.getName(), e);
                        }
                    }
                }
            }
            classes = ressourceService.getAllClasses(ContentProvider.class);
            int priority = 0;
            for(Class aClass : classes) {
                if(Module.class.isAssignableFrom(aClass)) {
                    if(aClass.isAnnotationPresent(Named.class) && aClass.isAnnotationPresent(ContentProvider.class)) {
                        try {
                            Provider provider = (Provider) aClass.newInstance();
                            if(provider instanceof BootableModule) {
                                provider = (Provider) ((BootableModule)provider).getBootableLogicalInstance(this, null);
                            }
                            contentProviders.add(provider);
                            handlers.add(new PrioritizedHandler(Integer.MAX_VALUE / 2 + priority, provider));
                            priority++;
                            Server.log.debug("Adding content provider " + provider.getClass().getName() + " to unit " + getName());
                        } catch (InstantiationException e) {
                            Server.log.warn("Could not instantiate " + aClass.getName(), e);
                        } catch (IllegalAccessException e) {
                            Server.log.warn("Could not access " + aClass.getName(), e);
                        }
                    }
                }
            }
        }
        for(Service module : services) {
            if(module instanceof Initable) {
                Server.log.debug("Initializing service " + ((Module)module).getName() + " for unit " + getName());
                ((Initable) module).init(properties);
            }
        }
        for(Provider module : contentProviders) {
            if(module instanceof Initable) {
                Server.log.debug("Initializing content provider " + ((Module)module).getName() + " for unit " + getName());
                ((Initable) module).init(properties);
            }
        }
        for(Service module : services) {
            if(module instanceof StatefullElement) {
                Server.log.debug("Post-Initializing service " + ((Module)module).getName() + " for unit " + getName());
                ((StatefullElement) module).postInit(properties);
            }
        }
        for(Provider module : contentProviders) {
            if(module instanceof StatefullElement) {
                Server.log.debug("Post-Initializing service " + ((Module)module).getName() + " for unit " + getName());
                ((StatefullElement) module).postInit(properties);
            }
        }
        createRegexMap();
    }

    @Override
    public void start() {
        for(Service module : services) {
            if(module instanceof StartupService) {
                ((StartupService) module).onStartup();
            }
        }
        for(Service module : services) {
            if(module instanceof Startable) {
                Server.log.debug("Starting service " + ((Module)module).getName() + " for unit " + getName());
                ((Startable) module).start();
            }
        }
        for(Provider module : contentProviders) {
            if(module instanceof Startable) {
                Server.log.debug("Starting content provider " + ((Module)module).getName() + " for unit " + getName());
                ((Startable) module).start();
            }
        }
    }

    @Override
    public void reload() {
        this.catalog = Config.openCatalog(name, true);
        for(Service module : services) {
            if(module instanceof Reloadable) {
                Server.log.debug("Reloading service " + ((Module)module).getName() + " for unit " + getName());
                ((Reloadable) module).reload();
            }
        }
        for(Provider module : contentProviders) {
            if(module instanceof Reloadable) {
                Server.log.debug("Reloading content provider " + ((Module)module).getName() + " for unit " + getName());
                ((Reloadable) module).reload();
            }
        }
        createRegexMap();
    }

    @Override
    public void shutdown(Properties properties) {
        for(Service module : services) {
            if(module instanceof Stopable) {
                Server.log.debug("Stopping service " + ((Module)module).getName() + " for unit " + getName());
                ((Stopable) module).shutdown(properties);
            }
        }
        for(Provider module : contentProviders) {
            if(module instanceof Stopable) {
                Server.log.debug("Stopping content provider " + ((Module)module).getName() + " for unit " + getName());
                ((Stopable) module).shutdown(properties);
            }
        }
        try {
            Server.log.info("Writing catalog for logical unit " + name);
            catalog.export();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public <T> T getService(Class<T> type) {
        Service service = services.stream().filter(s -> type.isAssignableFrom(s.getClass())).findFirst().get();
        return (T) service;
    }
}
