package at.femoweb.xjs.internal;

import at.femoweb.xjs.provider.Provider;

/**
 * Created by felix on 3/19/15.
 */
public class PrioritizedHandler implements Comparable<PrioritizedHandler> {

    private int priority;
    private Provider provider;

    public PrioritizedHandler(int priority, Provider provider) {
        this.priority = priority;
        this.provider = provider;
    }

    public int getPriority() {
        return priority;
    }

    public Provider getProvider() {
        return provider;
    }

    @Override
    public int compareTo(PrioritizedHandler prioritizedHandler) {
        return prioritizedHandler.priority - priority;
    }

}
