package at.femoweb.xjs.internal;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.structure.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 10/13/14.
 */
public abstract class AbstractPhysicalUnit implements PhysicalUnit {

    private String name;

    private HashMap<String, Module> modules;
    private HashMap<String, LogicalUnit> logicalUnits;

    public AbstractPhysicalUnit() {
        modules = new HashMap<String, Module>();
        logicalUnits = new HashMap<String, LogicalUnit>();
    }

    public HashMap<String, Module> getModules() {
        return modules;
    }

    public Collection<LogicalUnit> getLogicalUnits() {
        return logicalUnits.values();
    }

    public void loadModules(Document document) {
        for(Element mod : document.select("modules").first().select("module")) {
            try {
                Server.log.debug("Found Module " + mod.select("class").first().text() + " with name " + mod.attr("name"));
                Class<? extends Module> moduleClass = (Class<? extends Module>) Class.forName(mod.select("class").first().text());
                Server.log.debug("Module Class found!");
                Module module = moduleClass.newInstance();
                Server.log.debug("Module instantiated!");
                if(module.isNamed() && mod.attr("name").equalsIgnoreCase(module.getName())) {
                    Server.log.debug("Name fits configuration");
                    modules.put(mod.attr("name"), module);
                    for(Element inject : mod.select("inject")) {
                        RequiredValue requiredValue = new RequiredValue();
                        requiredValue.setConfigName(inject.attr("config"));
                        requiredValue.setFieldName(inject.attr("field"));
                        requiredValue.setOptional(!Boolean.parseBoolean(inject.attr("required")));
                        requiredValue.setType(Type.valueOf(inject.attr("type").toUpperCase()));
                        Server.log.debug("Found required value: " + requiredValue.getType() + " from " + requiredValue.getConfigName() + " to " + requiredValue.getFieldName() + (requiredValue.isOptional() ? " optional" : " required"));
                        module.addRequirement(requiredValue);
                    }
                }
            } catch (ClassNotFoundException e) {
                Server.log.warn("Could not instantiate Modules. Class not found!", e);
            } catch (InstantiationException e) {
                Server.log.warn("Could not instantiate Modules. Constructor not found!", e);
            } catch (IllegalAccessException e) {
                Server.log.warn("Could not instantiate Modules. Constructor not accessible!", e);
            } catch (Throwable t) {
                Server.log.warn("Could not instantiate Modules", t);
            }
        }
    }

    @Override
    public void addLogical(Element unit) {
        AbstractLogicalUnit logicalUnit = new LogicalUnitImpl();
        Server.log.info("Found logical Unit " + unit.attr("name"));
        logicalUnit.setName(unit.attr("name"));
        logicalUnits.put(logicalUnit.getName(), logicalUnit);
        for(Element e : unit.select("aliases").first().select("alias")) {
            logicalUnit.addAlias(e.text());
            logicalUnits.put(e.text(), logicalUnit);
            Server.log.debug("Alias " + e.text());
        }
        Server.log.debug("Searching for services");
        for(Element e : unit.select("services").first().select("module")) {
            Properties properties = new Properties();
            if(e.select("config").first() != null)
                for(Element property : e.select("config").first().select("property")) {
                    properties.setProperty(property.attr("name"), property.text());
                    Server.log.debug("Found property " + property.attr("name") + "=" + property.text());
                }
            logicalUnit.addService(modules.get(e.attr("name")), properties);
        }
        Server.log.debug("Searching for content providers");
        for(Element e : unit.select("content-providers").first().select("module")) {
            Properties properties = new Properties();
            if(e.select("config").first() != null) {
                for (Element property : e.select("config").first().select("property")) {
                    properties.setProperty(property.attr("name"), property.text());
                    Server.log.debug("Found property " + property.attr("name") + "=" + property.text());
                }
            }
            logicalUnit.addContentProvider(modules.get(e.attr("name")), properties);
        }
    }

    @Override
    public String toString() {
        return "AbstractPhysicalUnit{" +
                "modules=" + modules +
                ", logicalUnits=" + logicalUnits +
                '}';
    }

    @Override
    public Module getModule(String name) {
        return modules.get(name);
    }

    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
