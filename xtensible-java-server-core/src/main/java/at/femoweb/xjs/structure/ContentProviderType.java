package at.femoweb.xjs.structure;

/**
 * Created by felix on 10/21/14.
 */
public enum ContentProviderType {

    CONTENT, FALLBACK
}
