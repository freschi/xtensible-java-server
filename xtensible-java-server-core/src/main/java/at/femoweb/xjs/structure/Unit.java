package at.femoweb.xjs.structure;

import java.util.Properties;

/**
 * Created by felix on 10/13/14.
 */
public interface Unit {
    public abstract void init(Properties properties);
    public abstract void start();
    public abstract void reload();
    public abstract void shutdown(Properties properties);
}
