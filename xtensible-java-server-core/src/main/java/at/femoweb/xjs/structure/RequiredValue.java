package at.femoweb.xjs.structure;

/**
 * Created by felix on 10/13/14.
 */
public class RequiredValue {

    private String fieldName;
    private String configName;
    private boolean optional;
    private Type type;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "RequiredValue{" +
                "fieldName='" + fieldName + '\'' +
                ", configName='" + configName + '\'' +
                ", optional=" + optional +
                ", type=" + type +
                '}';
    }
}
