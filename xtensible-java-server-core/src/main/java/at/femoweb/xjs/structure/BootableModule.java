package at.femoweb.xjs.structure;

import at.femoweb.config.entries.Group;
import at.femoweb.xjs.internal.BootableLogicalUnit;

/**
 * Created by felix on 2/23/15.
 */
public abstract class BootableModule extends Module {

    public abstract Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings);
    public abstract void logInfo();
    public abstract void setup();
}
