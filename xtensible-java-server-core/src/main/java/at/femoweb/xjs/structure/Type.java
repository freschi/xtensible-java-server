package at.femoweb.xjs.structure;

/**
 * Created by felix on 10/13/14.
 */
public enum Type {

    BOOLEAN, STRING, NUMBER
}
