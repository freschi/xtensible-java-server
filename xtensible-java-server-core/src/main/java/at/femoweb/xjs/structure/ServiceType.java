package at.femoweb.xjs.structure;

/**
 * Created by felix on 10/17/14.
 */
public enum ServiceType {

    SESSION, PERSISTENCE, CONNECTION, CONTENT, OTHER
}
