package at.femoweb.xjs.structure;

import at.femoweb.config.Catalog;
import at.femoweb.config.Config;
import at.femoweb.xjs.Server;
import org.apache.logging.log4j.Logger;

/**
 * Created by felix on 2/27/15.
 */
public class BootstrapScriptInterface {

    public Catalog global;
    public Logger log = Server.log;

    public BootstrapScriptInterface(Catalog global) {
        this.global = global;
    }

    public Catalog catalog(String name) {
        return Config.openCatalog(name);
    }

    public Catalog catalog(String name, boolean buildXml) {
        return Config.openCatalog(name, buildXml);
    }

}
