package at.femoweb.xjs.structure;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.ContentProvider;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by felix on 10/13/14.
 */
public abstract class Module {

    private ArrayList<RequiredValue> requiredValues;

    public Module () {
        requiredValues = new ArrayList<RequiredValue>();
    }

    public boolean isNamed () {
        return this.getClass().isAnnotationPresent(Named.class);
    }

    public String getName() {
        return this.getClass().getAnnotation(Named.class).value();
    }

    public boolean isService() {
        return this.getClass().isAnnotationPresent(Service.class) && isNamed();
    }

    public boolean isContentProvider() {
        return this.getClass().isAnnotationPresent(ContentProvider.class) && isNamed();
    }

    public void addRequirement(RequiredValue requiredValue) {
        requiredValues.add(requiredValue);
    }

    public Module createLogicalInstance(LogicalUnit unit, Properties properties) {
        try {
            if(this.getClass().getDeclaredConstructor(LogicalUnit.class, Properties.class) != null) {
                Server.log.debug("Creating logical instance of " + getName() + " for " + unit.getName());
                Constructor<?> constructor = this.getClass().getDeclaredConstructor(LogicalUnit.class, Properties.class);
                constructor.setAccessible(true);
                return (Module) constructor.newInstance(unit, properties);
            }
        } catch (NoSuchMethodException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (InvocationTargetException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (InstantiationException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (IllegalAccessException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (NullPointerException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        }
        try {
            if(this.getClass().getDeclaredConstructor(LogicalUnit.class) != null) {
                Server.log.debug("Creating logical instance of " + getName() + " for " + unit.getName());
                Constructor<?> constructor = this.getClass().getDeclaredConstructor(LogicalUnit.class);
                constructor.setAccessible(true);
                return (Module) constructor.newInstance(unit);
            }
        } catch (NoSuchMethodException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (InvocationTargetException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (InstantiationException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (IllegalAccessException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        } catch (NullPointerException e) {
            Server.log.warn("Could not create logical instance of " + getName() + " for " + unit.getName());
        }
        return this;
    }

    @Override
    public String toString() {
        return "Module{" +
                "requiredValues=" + requiredValues +
                ", name=" + getName() +
                '}';
    }
}
