package at.femoweb.xjs.structure;

import at.femoweb.http.HttpRequest;
import at.femoweb.http.HttpResponse;
import at.femoweb.xjs.services.Service;

import java.util.Collection;
import java.util.Properties;

/**
 * Created by felix on 10/13/14.
 */
public interface LogicalUnit extends Unit {

    public abstract void addAlias(String alias);
    public abstract String[] getAliases();
    public abstract void handle(HttpRequest request, HttpResponse response);

    /**
     * Add a new service to this logical unit
     *
     * Should only be used on startup or on reload
     * @param module the module to add
     * @param properties <strong>Deprecated</strong> should only be used to install legacy modules
     */
    public abstract void addService(Module module,@Deprecated Properties properties);

    /**
     * Add a new content provider to this logical unit
     *
     * Should only be used on startup or on reload
     * @param module the module to add
     * @param properties <strong>Deprecated</strong> should only be used to install legacy modules
     */
    public abstract void addContentProvider(Module module,@Deprecated Properties properties);

    public abstract String getName();

    public abstract void setName(String name);

    public abstract Collection<Module> getServices();
    public abstract Collection<Module> getContentProviders();
    public abstract boolean hasProvider (String name);
    public abstract boolean hasService (String name);
    public abstract Service getService (String name);
}
