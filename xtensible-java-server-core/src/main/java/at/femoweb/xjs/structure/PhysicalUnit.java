package at.femoweb.xjs.structure;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashMap;

/**
 * Created by felix on 10/13/14.
 */
public interface PhysicalUnit extends Unit {

    public abstract void addLogical(Element unit);
    public abstract void addLogical(LogicalUnit unit);
    public abstract Module getModule(String name);

    public abstract String getName();
    public abstract void setName(String name);
    public abstract HashMap<String, Module> getModules();
    public abstract java.util.Collection<LogicalUnit> getLogicalUnits();

}
