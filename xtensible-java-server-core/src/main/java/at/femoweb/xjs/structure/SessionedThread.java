package at.femoweb.xjs.structure;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Session;
import at.femoweb.xjs.modules.SessionNotFoundException;

/**
 * Created by felix on 11/3/14.
 */
public abstract class SessionedThread extends Thread {

    private static int number;

    public SessionedThread () {
        Session.register(this);
        setName("SessionedThread-" + Session.get("session.key") + "-" + number);
    }

    @Override
    public void run() {
        try {
            execute();
        } catch (SessionNotFoundException e) {
            Server.log.warn("Session destroyed before push was complete!", e);
        }
    }

    public abstract void execute();
}
