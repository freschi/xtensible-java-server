package at.femoweb.xjs.util;

import java.sql.*;
import java.util.HashMap;

/**
 * Created by felix on 9/17/15.
 */
public final class SQL {

    private PreparedStatement preparedStatement;

    public static SQL query(Connection connection, String prepared) throws SQLException {
        SQL sql = new SQL();
        sql.preparedStatement = connection.prepareStatement(prepared);
        return sql;
    }

    public SQL values(ValueProvider provider) throws SQLException {
        do {
            provider.putData(preparedStatement);
            preparedStatement.addBatch();
        } while (provider.next());
        return this;
    }

    public SQL execute() throws SQLException {
        preparedStatement.executeBatch();
        preparedStatement.close();
        return this;
    }

    public SQL execute(ResultCallback resultCallback) throws SQLException {
        ResultSet resultSet = preparedStatement.executeQuery();
        resultCallback.result(resultSet);
        preparedStatement.close();
        return this;
    }
}
