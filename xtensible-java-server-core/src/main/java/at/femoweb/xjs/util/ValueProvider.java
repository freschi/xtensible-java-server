package at.femoweb.xjs.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by felix on 9/17/15.
 */
public interface ValueProvider {

    boolean next();
    void putData(PreparedStatement preparedStatement) throws SQLException;
}
