package at.femoweb.xjs.util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by felix on 9/17/15.
 */
public interface ResultCallback {

    void result(ResultSet resultSet) throws SQLException;
}
