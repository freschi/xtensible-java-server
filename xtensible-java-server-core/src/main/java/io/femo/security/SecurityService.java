package io.femo.security;

import at.femoweb.config.entries.Group;
import at.femoweb.xjs.Server;
import at.femoweb.xjs.annotations.JResPreprocessor;
import at.femoweb.xjs.annotations.Named;
import at.femoweb.xjs.annotations.Service;
import at.femoweb.xjs.internal.BootableLogicalUnit;
import at.femoweb.xjs.modules.JavaRessourceService;
import at.femoweb.xjs.modules.xaction.XAction;
import at.femoweb.xjs.modules.xaction.XOptional;
import at.femoweb.xjs.modules.xaction.XParam;
import at.femoweb.xjs.modules.xaction.XUpdate;
import at.femoweb.xjs.structure.BootableModule;
import at.femoweb.xjs.structure.Module;
import at.femoweb.xjs.structure.ServiceType;
import io.femo.security.services.JResManagementInterface;
import io.femo.security.services.JResPreprocessorService;
import javassist.*;
import javassist.bytecode.*;
import javassist.bytecode.annotation.*;
import javassist.bytecode.annotation.Annotation;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;

/**
 * Created by felix on 10/22/15.
 */
@Service(type = ServiceType.OTHER)
@Named("security-service")
@JResPreprocessor
public class SecurityService extends BootableModule implements JResPreprocessorService{

    private ClassPool classPool;
    private BootableLogicalUnit logicalUnit;
    private ClassPath classPath;
    private File jbin;

    @Override
    public Module getBootableLogicalInstance(BootableLogicalUnit logicalUnit, Group settings) {
        SecurityService securityService = new SecurityService();
        securityService.logicalUnit = logicalUnit;
        return securityService;
    }

    @Override
    public void logInfo() {

    }

    @Override
    public void setup() {

    }

    @Override
    public <T> Class process(Class<T> tClass, ClassLoader loader, JResManagementInterface managementInterface) {
        if(tClass.isAnnotationPresent(Secured.class)) {
            Server.log.info("Checking for Security Proxy for " + tClass.getName());
            /*try {
                Class<?> c = loader.loadClass(tClass.getName() + "SecurityProxy");
                return (Class<? extends T>) c;
            } catch (ClassNotFoundException e) {
                Server.log.info("Proxy not found");
            }*/
            try {
                CtClass base = classPool.get(tClass.getName());
                CtClass proxy = classPool.makeClass(base.getName() + "SecurityProxy");
                proxy.setSuperclass(base);
                CtMethod[] methods = base.getDeclaredMethods();
                for(CtMethod method : methods) {
                    if(method.getAnnotation(Secured.class) != null) {
                        Object[] annotations = method.getAnnotations();
                        MethodInfo methodInfo = new MethodInfo(proxy.getClassFile().getConstPool(), method.getName(), method.getSignature());
                        CtMethod ctMethod = CtMethod.make(methodInfo, proxy);
                        AttributeInfo parameterAttributeInfo = methodInfo.getAttribute(ParameterAnnotationsAttribute.visibleTag);
                        if(parameterAttributeInfo == null) {
                            parameterAttributeInfo = new ParameterAnnotationsAttribute(proxy.getClassFile().getConstPool(), ParameterAnnotationsAttribute.visibleTag);
                            methodInfo.addAttribute(parameterAttributeInfo);
                        }
                        ConstPool constPool = parameterAttributeInfo.getConstPool();
                        Annotation[][] paramArrays = new Annotation[method.getParameterTypes().length][];
                        for (int i = 0; i < method.getParameterTypes().length; i++) {
                            Object[] pAnnon = method.getParameterAnnotations()[i];
                            Annotation[] pano = new Annotation[pAnnon.length];
                            for (int j = 0; j < pAnnon.length; j++) {
                                //AnnotationImpl impl = (AnnotationImpl) pAnnon[j];
                                /*Proxy anotProxy = (Proxy) pAnnon[j];
                                Field field = anotProxy.getClass().getFields()[1];
                                field.setAccessible(true);
                                Method anotMethod = (Method) field.get(anotProxy);
                                anotMethod.setAccessible(true);*/
                                java.lang.annotation.Annotation an = (java.lang.annotation.Annotation) pAnnon[j];
                                Annotation annotation = new Annotation(pAnnon[j].toString().substring(1), constPool);
                                Class<? extends java.lang.annotation.Annotation> anClass = an.annotationType();
                                for(Method anMethod : anClass.getDeclaredMethods()) {
                                    String name = anMethod.getName();
                                    try {
                                        Object value = anMethod.invoke(an);
                                        if(anMethod.getReturnType() == String.class) {
                                            annotation.addMemberValue(name, new StringMemberValue((String) value, constPool));
                                        } else if (anMethod.getReturnType() == boolean.class) {
                                            annotation.addMemberValue(name, new BooleanMemberValue((Boolean) value, constPool));
                                        }
                                    } catch (IllegalAccessException | InvocationTargetException e) {
                                        e.printStackTrace();
                                    }
                                }
                                Object[][] anons = method.getParameterAnnotations();
                                Annotation[][] ctAnons = new Annotation[anons.length][];
                                for (int k = 0; k < anons.length; k++) {
                                    Server.log.info("Method " + method.getName() + " has " + anons[i].length + " annotations");
                                    ctAnons[k] = new Annotation[anons[k].length];
                                    for (int l = 0; l < anons[k].length; l++) {
                                        Server.log.info("Annotation for parameter " + k + " " + String.valueOf(anons[k][l]) + anons[k][l].getClass().getName());
                                        if(anons[k][l] instanceof XUpdate) {
                                            XUpdate update = (XUpdate) anons[k][l];
                                            Annotation a = new Annotation(update.getClass().getTypeName(), constPool);
                                            ctAnons[k][l] = a;
                                            Server.log.info("Updates found");
                                        } else if (anons[k][l] instanceof XParam) {
                                            XParam param = (XParam) anons[k][l];
                                            Annotation a = new Annotation(param.getClass().getTypeName(), constPool);
                                            a.addMemberValue("name", new StringMemberValue(param.name(), constPool));
                                            ctAnons[k][l] = a;
                                            Server.log.info("Parameter found");
                                        } else if (anons[k][l] instanceof XOptional) {
                                            XOptional optional = (XOptional) anons[k][l];
                                            Annotation a = new Annotation(optional.getClass().getTypeName(), constPool);
                                            ctAnons[k][l] = a;
                                            Server.log.info("Optional found");
                                        }
                                    }
                                }
                                ParameterAnnotationsAttribute parameterAttribute = ((ParameterAnnotationsAttribute) methodInfo.getAttribute(ParameterAnnotationsAttribute.visibleTag));
                                parameterAttribute.setAnnotations(ctAnons);
                                methodInfo.addAttribute(parameterAttribute);
                                /*an.getMemberNames().forEach(s -> {
                                    MemberValue memberValue = an.getMemberValue((String) s);
                                    annotation.addMemberValue((Integer) s, memberValue);
                                });*/
                                pano[j] = annotation;
                            }
                            paramArrays[i] = pano;
                        }
                        if(paramArrays.length > 0) {
                            ((ParameterAnnotationsAttribute) parameterAttributeInfo).setAnnotations(paramArrays);
                        }
                        ctMethod.setBody("io.femo.security.SecurityManager.logTrace();");
                        for(Object annotation : annotations) {
                            if(annotation instanceof RequiresAuthentication) {
                                ctMethod.insertAfter("io.femo.security.SecurityManager.requireAuthentication();");
                            } else if (annotation instanceof XAction) {
                                XAction xAction = (XAction) annotation;
                                ConstPool parameterConstPool = proxy.getClassFile().getConstPool();
                                Annotation a = new Annotation(parameterConstPool, classPool.get(XAction.class.getName()));
                                a.addMemberValue("name", new StringMemberValue(xAction.name(), parameterConstPool));
                                a.addMemberValue("requireFormEncoded", new BooleanMemberValue(xAction.requireFormEncoded(), parameterConstPool));
                                AnnotationsAttribute annotationsAttribute = new AnnotationsAttribute(parameterConstPool, AnnotationsAttribute.visibleTag);
                                annotationsAttribute.addAnnotation(a);
                                methodInfo.addAttribute(annotationsAttribute);
                            }
                        }
                        CtClass[] params = method.getParameterTypes();
                        String call = String.format("super.%s(", method.getName());
                        for (int i = 0; i < params.length; i++) {
                            call += "$" + (i + 1) + " ";
                            if(i < params.length - 1) {
                                call += ", ";
                            }
                        }
                        call += ");";
                        ctMethod.insertAfter(call);
                        ctMethod.setModifiers(Modifier.PUBLIC);
                        proxy.addMethod(ctMethod);
                    }
                }
                proxy.writeFile(jbin.getAbsolutePath());
                return loader.loadClass(proxy.getName());
            } catch (NotFoundException | ClassNotFoundException | CannotCompileException | IOException e) {
                e.printStackTrace();
            }
        }
        return tClass;
    }

    @Override
    public void configure(JavaRessourceService jresService) {
        classPool = ClassPool.getDefault();
        jbin = jresService.getJbin();
        try {
            classPath = classPool.appendClassPath(jresService.getJbin().getAbsolutePath());
            InputStream in = classPath.openClassfile("at.femoweb.Actions");
            in.close();
            URL url = classPool.find("at.femoweb.Actions");
            if(url != null) {
                Server.log.info("Class URL: " + url);
            } else {
                Server.log.warn("Class not found: at.femoweb.Actions");
            }
        } catch (NotFoundException | IOException | NullPointerException e) {
            //e.printStackTrace();
        }
    }

    @Override
    public Object put(Object... objects) {
        return null;
    }
}
