package io.femo.security.services;

import at.femoweb.xjs.modules.JavaRessourceService;
import at.femoweb.xjs.services.Service;

/**
 * Created by felix on 10/22/15.
 */
public interface JResPreprocessorService extends Service {

    <T> Class process(Class<T> tClass, ClassLoader loader, JResManagementInterface managementInterface);
    void configure(JavaRessourceService jresService);
}
