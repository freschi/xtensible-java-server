package io.femo.security;

import at.femoweb.xjs.Server;
import at.femoweb.xjs.modules.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by felix on 10/22/15.
 */
public class SecurityManager {

    private static final Logger log = LogManager.getLogger("SecurityManager");

    public static void logTrace() {
        StackTraceElement call = Thread.currentThread().getStackTrace()[2];
        log.info("Entering " + call.getClassName() + "." + call.getMethodName() + "()");
    }

    public static void requireAuthentication() {
        if(Session.exists()) {
            if(!Session.isAuthenticated()) {
                throw new SecurityException(getCall(), "Not Authenticated");
            }
        } else {
            throw new SecurityException(getCall(), "No Session present");
        }
    }

    private static String getCall() {
        StackTraceElement call = Thread.currentThread().getStackTrace()[3];
        return call.getClassName() + "." + call.getMethodName() + "()";
    }
}
