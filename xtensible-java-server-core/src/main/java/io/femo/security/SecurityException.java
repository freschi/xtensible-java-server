package io.femo.security;

/**
 * Created by felix on 10/30/15.
 */
public class SecurityException extends RuntimeException {

    public SecurityException(String at, String s) {
        super("Security Violation at " + at + " - " + s);
    }
}
