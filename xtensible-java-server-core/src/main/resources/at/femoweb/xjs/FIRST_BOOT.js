
$.log.info("Running first boot script!");

//If you modify this file, please remove this line
$.log.info("Official femoweb script (c) 2015 FeMoWeb");




//Creating logical unit config
var global = $.global;
var group = global.openArticle("server").openParagraph("machine").openEntry();
var list = group.getList("logicalUnits");

for(var i = 0; i < list.count(); i++) {
    var unit = list.getGroup(i.intValue());
    var name = unit.getString("name");
    $.log.info("Logical Unit " + name);
    var unitCatalog = $.catalog(name, true);
    var unitConfig = unitCatalog.openArticle("unit").openParagraph("unit").openEntry();
    $.log.info("Has contentProviders " + unitConfig.has("contentProviders"));
    if(!(unitConfig.has("contentProviders"))) {
        if(unitConfig.has("contentProviders")) {
            $.log.warn("If clauses apparently not working!");
        }
        var providers = unitConfig.getList("contentProviders");
        var fileProvider = providers.addGroup();
        fileProvider.setString("module", "file-provider");
        fileProvider.setString("priority", "fallback");
        var fileProperties = fileProvider.getGroup("properties");
        fileProperties.setString("htdocs", "htdocs");
        fileProperties.setBoolean("directoryIndexing", false);
        fileProperties.setBoolean("createStructure", true);
        var errorPages = providers.addGroup();
        errorPages.setString("module", "error-pages");
        errorPages.setString("priority", "error");
        var errorProperties = errorPages.getGroup("properties");
        errorProperties.setBoolean("overrideGlobal", true);
        errorProperties.setString("errorPagesDir", "errors");
        errorProperties.setBoolean("useGlobal", true);
    }
    if(!(unitConfig.has("services"))) {
        var services = unitConfig.getList("services");
        var http = services.addGroup();
        http.setString("module", "http");
        http.getGroup("properties").setString("handler", "http-alt");
    }
    if(!(unitConfig.has("dir"))) {
        unitConfig.setString("dir", name);
    }
}